#ifndef _STACK_H
#define _STACK_H

typedef enum stack_value_type_e {
	
	TYPE_EMPTY,			//0
	TYPE_POINTER,
	TYPE_CHAR,
	TYPE_SHORT,
	TYPE_INT,
	TYPE_LONG,
	TYPE_FLOAT,
	TYPE_DOUBLE,
	TYPE_CHAR_POINTER	//8
	
} STACK_VALUE_TYPE;

typedef union stack_value_u {
	
	void * ptr;
	char c;
	short h;
	int i;
	long l;
	float f;
	double d;
	char * cptr;
	
} STACK_VALUE;

typedef struct stack__t STACK;

STACK * stackCreate();
void stackPush(STACK * stack, STACK_VALUE_TYPE type, STACK_VALUE value);
STACK_VALUE stackPop(STACK * stack, STACK_VALUE_TYPE * type);
void stackPushString(STACK * stack, char * str, int copyStr);

#endif
