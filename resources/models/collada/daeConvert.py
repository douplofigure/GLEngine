import xml.etree.ElementTree as ET
import sys

def printTag(elem, lvl=0, f=sys.stdout):

    if (not bool(elem.attrib)) and elem.text is None:
        return
    
    if "id" in elem.attrib.keys() and elem.attrib["id"] is not None:
        print("%scomp %s {" % ("  " * lvl, elem.attrib["id"].replace("-", "_")), file=f)
    elif elem.tag == "input":
        print("%scomp input_%s {" % ("  " * lvl, elem.attrib["source"].replace("#", "").replace("-", "_")), file=f)
    elif elem.tag == "param":
        print("%scomp input_%s {" % ("  " * lvl, elem.attrib["name"].replace("#", "").replace("-", "_")), file=f)
    else:
        print("%scomp %s {" % ("  " * lvl, elem.tag), file=f)
    for key in elem.attrib:
        print('%sstring %s = "%s"' % ("  " * (lvl+1), key, elem.attrib[key].replace("#", "").replace("-", "_")), file=f)
    for c in elem:
        printTag(c, lvl+1, f)
    if elem.text is not None and len(elem.text.strip()) > 0:
        print('%sstring __text = "%s"' % ("  " * (lvl+1), elem.text), file=f)
    print('%s}' % ("  " * lvl), file=f)

f = open(sys.argv[1])
docData = f.read()

it = ET.iterparse(sys.argv[1])
for _, el in it:
    if '}' in el.tag:
        el.tag = el.tag.split('}', 1)[1]  # strip all namespaces
root = it.root

if len(sys.argv) > 2:
    out = open(sys.argv[2], "w")
else:
    out = sys.stdout
print("#ASCII", file=out)
printTag(root, f=out)
