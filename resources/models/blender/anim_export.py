import bpy

bone_order = []

def writeBone(file, bone, align=1):
    global bone_order
    bone_order.append(bone)
    file.write("%s{\n" % (("\t" * align)))
    align += 1
    file.write("%sstring name = \"%s\"\n" % (("\t" * align), bone.name))
    file.write("%sfloat64 [3] position = [%f, %f, %f]\n" % ("\t" * align, bone.bone.head.x, bone.bone.head.y, bone.bone.head.z))
    file.write("%sfloat64 [3] vector = [%f, %f, %f]\n" % ("\t" * align, bone.bone.tail_local.x - bone.bone.head_local.x, bone.bone.tail_local.y - bone.bone.head_local.y, bone.bone.tail_local.z - bone.bone.head_local.z))
    file.write("%sfloat64 [4] rotation = [%f, %f, %f, %f]\n" % ("\t" * align, bone.rotation_quaternion.w, bone.rotation_quaternion.x, bone.rotation_quaternion.y, bone.rotation_quaternion.z))
    if len(bone.children) >= 1:
        file.write("%scomp [%i] children = [\n" % (("\t" * align), len(bone.children)))
        for c in bone.children:
            writeBone(file, c, align+1)
            if c != bone.children[-1]:
                file.write(",\n")
            else:
                file.write("\n")
        file.write("%s]\n" % (("\t" * align)))
    #file.write("%sfloat64 [3] position = [%f, %f, %f]\n" % (("\t" * align), bone.location.x, bone.location.y, bone.location.z))
    #file.write("%sfloat64 [4] rotation = [%f, %f, %f, %f]\n" % (("\t" * align), bone.rotation_quaternion.w, bone.rotation_quaternion.x, bone.rotation_quaternion.y, bone.rotation_quaternion.z))
    align -= 1
    file.write("%s}" % ("\t" * align))

def writePoseBone(file, pbone, align=0):
    file.write("%s{\n" % ("\t" * align))
    align += 1
    file.write("%sstring name = \"%s\"\n" % ("\t" * align, pbone.name))
    file.write("%sfloat64 [4] rotation = [%f, %f, %f, %f]\n" % ("\t" * align, pbone.rotation_quaternion.w, pbone.rotation_quaternion.x, pbone.rotation_quaternion.y, pbone.rotation_quaternion.z))
    file.write("%sfloat64 [3] position = [%f, %f, %f]\n" % ("\t" * align, pbone.location.x, pbone.location.y, pbone.location.z))
    align -= 1
    file.write("%s}" % ("\t" * align))

def writeFrame(file, pose, frameNum, fps, align = 1):
    global bone_order
    sce.frame_set(frameNum) 
    file.write("%s{\n" % ("\t" * align))
    align += 1
    file.write("%sfloat64 time = %.16f\n" % (("\t" * align), float(frameNum) / float(fps)))
    file.write("%scomp [%i] transforms = [\n" % ("\t" * align, len(bone_order)))
    for bone in bone_order:
        writePoseBone(file,bone, align+1)
        if bone != pose.bones[-1]:
            file.write(",\n")
        else:
            file.write("\n")

    file.write("%s]\n" % (("\t" * align)))
    align -= 1
    file.write("%s}" % ("\t" * align))
    
def writeFrameData(file, groups, frameNum, align=0):
    file.write("%s{\n" % ("\t" * align))
    align += 1
    file.write("%sfloat64 time = %f\n" % ("\t" * align, float(frameNum - sce.frame_start) / float(sce.render.fps)))
    file.write("%scomp [%i] transforms = [\n" % ("\t" * align, len(groups)))
    align += 1
    
    for bone in groups:
        position = [0,0,0]
        rotation = [0,0,0,0]
        scale = [0,0,0]
        
        for c in bone.channels:
            val = c.evaluate(frameNum)
            if c.data_path.find("location") != -1:
                position[c.array_index] = val
            if c.data_path.find("rotation_quaternion") != -1:
                rotation[c.array_index] = val
            if c.data_path.find("scale") != -1:
                scale[c.array_index] = val
                
        file.write("%s{\n" % ("\t" * align))
        align += 1
        file.write("%sstring name = \"%s\"\n" % ("\t" * align, bone.name))
        file.write("%sfloat64 [3] position = [%f, %f, %f]\n" % ("\t" * align, position[0], position[1], position[2]))
        file.write("%sfloat64 [3] scale = [%f, %f, %f]\n" % ("\t" * align, scale[0], scale[1], scale[2]))
        file.write("%sfloat64 [4] rotation = [%f, %f, %f, %f]\n" % ("\t" * align, rotation[0], rotation[1], rotation[2], rotation[3]))
        align -= 1
        file.write("%s}" % ("\t" * align))
        
        if bone != groups[-1]:
            file.write(",\n")
        else:
            file.write("\n")
    align -= 1
    file.write("%s]\n" % ("\t" * align))
        
    align -= 1
    file.write("%s}" % ("\t" * align))
        
    
def writeAction(file, action, pose, align=1):
    file.write("%s{\n" % ("\t" *align))
    align += 1
    file.write("%sstring name = \"%s\"\n" % ("\t" * align, action.name))
    file.write("%scomp [%i] keyframes = [\n" % ("\t" * align, len(action.pose_markers)))
    for m in action.pose_markers:
        #writeFrame(file, pose, m.frame, sce.render.fps, align + 1)
        writeFrameData(file, action.groups, m.frame, align+1)
        if m != action.pose_markers[-1]:
            file.write(",\n")
        else:
            file.write("\n")
    file.write("%s]\n" % (("\t" * align)))
    align -= 1
    file.write("%s}" % ("\t" * align))

def writeMeshData(file, object):
    
    bpy.context.scene.objects.active = object
    bpy.ops.export_mesh.ply(filepath=bpy.path.abspath("//%s_%s.ply" % (ob.name, object.name)))    
    file.write("comp mesh {\n")
    align = 1
    
    file.write("%sstring filename = \"%s_%s\"\n" % ("\t"*align, ob.name, object.name))
    
    file.write("%scomp [%i] weights = [" % ("\t" * align, len(object.data.vertices)))
    for v in object.data.vertices:
        bn = 0
        ls = []
        file.write("{ ")
        for b in ob.pose.bones:
            val = 0.0
            
            try:
                val = object.vertex_groups[b.name].weight(v.index)
                ls.append((bn, val))
            except:
                pass
            bn += 1
        
        if len(ls) == 0:
            file.write("bool empty = true ")
            file.write(" float32 [3] position = [%f, %f, %f] }" % (v.co.x, v.co.y, v.co.z))
            if b != ob.pose.bones[-1] or v != object.data.vertices[-1]:
                file.write(",\n ")
            else:
                file.write("\n")
            continue    
        
        file.write("int32 [%i] bones = [" % len(ls))
        for e in ls:
            file.write("%i" % e[0])
            if e != ls[-1]:
                file.write(", ")
        file.write("]")
        
        file.write(" float32 [%i] values = [" % len(ls))
        for e in ls:
            file.write("%f" % e[1])
            if e != ls[-1]:
                file.write(", ")
        file.write("]")
        
        file.write(" float32 [3] position = [%f, %f, %f]" % (v.co.x, v.co.y, v.co.z))
        
        file.write("}")
        if b != ob.pose.bones[-1] or v != object.data.vertices[-1]:
            file.write(", ")
            
    file.write("]\n")
    
    file.write("%scomp [%i] bone_order = [" % ("\t" * align, len(ob.pose.bones)))
    for b in ob.pose.bones:
        file.write("{string name = \"%s\"}" % b.name)
        if b != ob.pose.bones[-1]:
            file.write(",\n")
            
    file.write("]\n")
    
    align -= 1
    file.write("}\n")
    

sce = bpy.context.scene
ob = bpy.context.object

fl = open("%s%s.anim" % (bpy.path.abspath("//"), ob.name), "w")

fl.write("#ASCII\n")

#Exporting Armature structure

rootBones = [b for b in ob.pose.bones if not b.parent]

fl.write("comp [%i] bones = [\n" % len(rootBones))
for b in rootBones:
    writeBone(fl, b)
    if b != rootBones[-1]:
        fl.write(",\n")
    
fl.write("\n]\n")
fl.write("comp [%i] animations = [\n" % (len(bpy.data.actions)))
for action in bpy.data.actions:
    writeAction(fl, action, ob.pose)
    if action != bpy.data.actions[-1]:
        fl.write(",\n")
fl.write("\n]\n")

for child in ob.children:
    if child.type == "MESH":
        writeMeshData(fl, child)

fl.close()
