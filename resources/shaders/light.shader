#ASCII

string vertexShader = "id.glsl"
string fragmentShader = "deffered_pbr.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string transformationMatrix = "transform"
	string projectionMatrix = "projection"
	string viewMatrix = "view"

	# Variables for ambient light
	string ambientDirection = "sunDirection"
	string ambientColor = "sunColor"

	# Variables for textures
	string diffuseMap = "tex"
	string normalMap = "normalMap"
	string specularMap = "specularMap"

	string time = "time"

}

comp [4] textures = [

    {
        string name = "gPosition"
        int32 slot = 0
    },
    {
        string name = "gNormal"
        int32 slot = 1
    },
    {
        string name = "gAlbedoSpec"
        int32 slot = 2
    },
    {
        string name = "tPosition"
        int32 slot = 3
    },
    {
        string name = "tNormal"
        int32 slot = 4
    },
    {
        string name = "tAlbedoSpec"
        int32 slot = 5
    },
    {
        string name = "skybox"
        int32 slot = 6
    },
    {
        string name = "shadowMap"
        int32 slot = 7
    }

]

comp [4] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	}
]

