#version 440 core
layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

uniform sampler2D diffuse;
uniform sampler2D specularMap;
uniform sampler2D normalMap;
uniform sampler2D bumpMap;

in vec2 uvPos;
in vec4 pos;
in vec3 faceNormal;
in vec3 lightDirection;
in vec3 cameraDirection;
in float visibility;
in float cameraDistance;
in vec4 shadowCoord;

in vec3 passTangent;

in mat3 tangentToWorld;


void main() {

    gPosition = vec4(pos.xyz + tangentToWorld * normalize(texture(normalMap, uvPos).xyz) * (texture(bumpMap, uvPos).r * 2.0 - 1.0), gl_FragCoord.z);
    float roughness = texture(specularMap, uvPos).g;
    gNormal = vec4(tangentToWorld * normalize(texture(normalMap, uvPos).xyz * 2.0 - 1.0), roughness);
    gAlbedoSpec.rgb = pow(texture(diffuse, uvPos).rgb, vec3(2.2));
    gAlbedoSpec.a = texture(specularMap, uvPos).r;

}

