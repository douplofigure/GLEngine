#ASCII

string vertexShader = "static_deffered.glsl"
string fragmentShader = "texture_bump.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string projectionMatrix = "projection"
	string viewMatrix = "view"

	# Variables for ambient light
	string ambientDirection = "sunDirection"

	string clipping = "useClipping"

	# Variables for textures

	string diffuseMap = "tex"
	string normalMap = "normalMap"
	string specularMap = "specularMap"

	string time = "time"

}

comp[4] textures = [
    {
        string name = "tex"
        int32 slot = 0
    },
    {
        string name = "normalMap"
        int32 slot = 1
    },
    {
        string name = "specularMap"
	    int32 slot = 2
    },
    {
        string name = "bumpMap"
        int32 slot = 3
    }
]

comp [8] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	},
	{
		string name = "transform_0"
		int32 id = 4
	},
	{
		string name = "transform_1"
		int32 id = 5
	},
	{
		string name = "transform_2"
		int32 id = 6
	},
	{
		string name = "transform_3"
		int32 id = 7
	}
]

