#version 330

uniform sampler2D tex;
uniform vec3 color;

in vec2 uvPos;

out vec4 outColor;

const float width = 0.5;
const float edge = 0.1;

void main(){

    //color = vec4(1,1,1,1);
    float distance = 1 - texture(tex, uvPos).a;
    float alpha = 1 - smoothstep(width, width + edge, distance);
    if (alpha <= 0.2) discard;
    outColor = vec4(color, alpha);

}
