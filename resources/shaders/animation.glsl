#version 140

const int maxBones = 128;

uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
uniform vec3 sunDirection;
uniform float useClipping;

uniform vec4 boneOffsets[maxBones];
uniform vec4 boneRotations[maxBones];

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;
in ivec4 bones;
in vec4 weights;

out vec3 lightDirection;
out vec2 uvPos;
out vec4 pos;
out vec3 faceNormal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 passTangent;

out mat3 tangentToWorld;

out float visibility;
out float cameraDistance;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.1;

const mat4 zMat = mat4(1,0,0,0,
                0,0,-1,0,
                0,1,0,0,
                0,0,0,1);

float distance(vec3 pos1, vec3 pos2) {

	return sqrt((pos2.x-pos1.x) * (pos2.x-pos1.x) + (pos2.y-pos1.y) * (pos2.y-pos1.y) + (pos2.z-pos1.z) * (pos2.z-pos1.z));

}

mat4 matrixFromQuaternion(vec4 q) {

    mat4 mat;

    float s = 1.0;// / (q.length() * q.length());

    /*mat[0] = vec4(1 - 2 * s * (q.z*q.z + q.w*q.w), 2 * s * (q.y*q.z + q.w*q.x), 2 * s * (q.y*q.w - q.z * q.x), 0);
    mat[1] = vec4(2 * s * (q.y*q.z - q.w*q.x), 1 - 2 * s * (q.y*q.y + q.w*q.w), 2 * s * (q.z*q.w + q.y * q.x), 0);
    mat[2] = vec4(2 * s * (q.y*q.w + q.z*q.x), 2 * s * (q.z*q.w - q.y * q.x), 1 - 2 * s * (q.y*q.y + q.z*q.z), 0);
    mat[3] = vec4(pos.x, pos.y, pos.z, 1);*/

    return transpose(mat4(

        1-2*s*(q.z * q.z + q.w * q.w), 2*s* (q.y * q.z - q.x * q.w), 2*s* (q.y * q.w + q.z * q.x), 0,
        2*s* (q.y * q.z + q.x * q.w), 1-2*s*(q.y * q.y + q.w * q.w), 2*s* (q.z * q.w - q.y * q.x), 0,
        2*s* (q.y * q.w - q.z * q.x), 2*s* (q.z * q.w + q.x * q.y), 1-2*s*(q.y * q.y + q.z * q.z), 0,
        0, 0, 0,1

    ))
    ;

}

vec4 getAnimPosition(vec3 position, mat4 animTransform, vec4 boneOffset) {

    //return (animTransform * vec4(position - boneOffset.xyz, 1)) + bonePosition;
    return vec4((animTransform * vec4(position, 1)).xyz - boneOffset.xyz, 1);

}

void main(void) {

    //mat4 toObjectSpace = inverse(transform);

    mat4 tMat = transform * zMat;

    vec4 tmpPos = vec4(0);
    vec4 tmpNorm = vec4(0);

    for (int i = 0; i < 4; ++i) {

        mat4 animTransform = matrixFromQuaternion(boneRotations[bones[i]]);

        tmpPos += getAnimPosition(position, animTransform, boneOffsets[bones[i]]) * weights[i];
        tmpNorm += (animTransform * vec4(normal, 0.0)) * weights[i];

    }

	transformPos = tMat * tmpPos;
	vec4 positionFromCamera = view * transformPos;

	vec3 t = (tMat * vec4(tangent, 0.0)).xyz;
	vec3 n = (tMat * tmpNorm).xyz;

	vec3 bitangent = -cross(t, n);

	mat3 toTangent = (

        mat3(
            t.x, t.y, t.z,
            bitangent.x, bitangent.y, bitangent.z,
            n.x, n.y, n.z
        )

	);

	tangentToWorld = toTangent;

    lightDirection = -normalize(sunDirection).xyz;
	faceNormal = n;

	cameraDirection = ((inverse(view) * vec4(0,0,0,0)) - transformPos).xyz;

	cameraDistance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(cameraDistance * density, gradient)),0,1);
	//shadowCoord = shadowTransform * pos;

	passTangent = t;

	pos = transformPos;
	gl_Position = projection * positionFromCamera;
	if (abs(useClipping) > 0.5)
        gl_ClipDistance[0] = pos.y;
    else
        gl_ClipDistance[0] = 1;
	uvPos = uv;

}


