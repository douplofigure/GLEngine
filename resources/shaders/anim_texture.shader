#ASCII

string vertexShader = "animation.glsl"
string fragmentShader = "texture_deffered.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string transformationMatrix = "transform"
	string projectionMatrix = "projection"
	string viewMatrix = "view"

	# Variables for ambient light
	string ambientDirection = "sunDirection"
	string ambientColor = "sunColor"

	# Variables for textures
	string diffuseMap = "tex"
	string normalMap = "normalMap"
	string specularMap = "specularMap"

	string time = "time"

}

comp[4] textures = [
    {
        string name = "diffuse"
        int32 slot = 0
    },
    {
        string name = "normalMap"
        int32 slot = 1
    },
    {
        string name = "specularMap"
	    int32 slot = 2
    }
]

comp [4] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	},
	{
        int32 id = 4
        string name = "bones"
	},
	{
        int32 id = 5
        string name = "weights"
	}
]


