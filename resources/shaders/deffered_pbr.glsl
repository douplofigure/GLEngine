#version 330 core

out vec4 FragColor;

#define PI 3.14159265358979323846

in vec2 uvPos;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

uniform sampler2D tPosition;
uniform sampler2D tNormal;
uniform sampler2D tAlbedoSpec;

uniform samplerCube skybox;

uniform sampler2DShadow shadowMap;

uniform vec3 viewPos;

uniform vec3 viewDir;
uniform float fov;
uniform float aspect;

uniform vec3 positions[128];
uniform vec3 colors[128];
uniform int types[128];
uniform float falloffs[128];

uniform int activeLights;

uniform float time;
uniform vec3 lightPos;
uniform mat4 toLightSpace;
uniform mat4 lightViewMatrix;

const float near = 0.0;
const float far = 200.0;

vec3 fresnelSchlick(float cosTheta, vec3 F0) {

    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);

}

float DistributionGGX(vec3 N, vec3 H, float roughness) {

    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness) {

    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {

    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}


vec3 calculateRadiance(vec3 lPos, vec3 lColor, int type, vec3 WorldPos, vec3 V, float falloff) {

    if (type == 0) return vec3(0);
    else if (type == 1) {
        float distance    = length(lPos - WorldPos);
        float attenuation = 1.0 / (distance * distance);
        return lColor * attenuation;
    } else if (type == 2) {

        return lColor;

    } else if (type == 3) {
        float distance    = length(lPos - WorldPos);
        float attenuation = 1.0 / (distance * distance);
        return lColor * attenuation * (0.5*sin(falloff*time)+1+0.3*cos(falloff*time*3));

    } else if (type == 4) {

        vec3 lightDir = normalize(lPos - WorldPos);
        float distance    = length(lPos - WorldPos);
        float attenuation = (cos(PI * clamp(acos(dot(lightDir, normalize(-lColor))) / falloff, 0, 1))+1)*0.5 / (distance);
        return vec3(2,2,2) * attenuation;

    }

}

vec3 computeLO(vec3 WorldPos, int i, vec3 V, vec3 N, float roughness, vec3 F0, vec3 albedo, float metallic) {

    // calculate per-light radiance
        vec3 L = normalize(positions[i] - WorldPos);
        if (types[i] == 2) L = -normalize(positions[i]);
        vec3 H = normalize(V + L);
        vec3 radiance = calculateRadiance(positions[i], colors[i], types[i], WorldPos, V, falloffs[i]);

        // cook-torrance brdf
        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        vec3 nominator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 specular     = nominator / max(denominator, 0.001);

        // add to outgoing radiance Lo
        float NdotL = max(dot(N, L), 0.0);

        return max((kD * albedo / PI + specular) * radiance * NdotL, vec3(0,0,0));

}

vec4 getGColor(float shadow) {

    vec3 Normal = texture(gNormal, uvPos).rgb;

    vec3 camPos     = viewPos;
    vec3 WorldPos   = texture(gPosition, uvPos).rgb;
    vec3 albedo     = texture(gAlbedoSpec, uvPos).rgb;
    float metallic  = texture(gAlbedoSpec, uvPos).a;
    float roughness = texture(gNormal, uvPos).a;

    float ao = 1.0;

    vec3 N = normalize(Normal);
    vec3 V = normalize(camPos - WorldPos);

    vec3 F0 = vec3(0.04);//
    F0 = mix(F0, albedo, metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    Lo = computeLO(WorldPos, 0, V, N, roughness, F0, albedo, metallic) * shadow;
    for(int i = 1; i < activeLights; ++i)
    {
        Lo += computeLO(WorldPos, i, V, N, roughness, F0, albedo, metallic);
    }
    if (metallic > -1) {

        vec3 L = normalize(reflect(-V, N));

        vec3 H = normalize(V + L);
        vec3 radiance = 0.01 * texture(skybox, L).rgb;

        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        vec3 nominator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 specular     = nominator / max(denominator, 0.001);

        // add to outgoing radiance Lo
        float NdotL = max(dot(N, L), 0.0);

        Lo += max((kD * albedo / PI + specular) * radiance * NdotL, vec3(0,0,0));
    }

    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + Lo;

    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    return vec4(color, texture(gPosition, uvPos).a);

}

vec4 getTColor(vec4 gColor, float shadow) {

    vec3 Normal = texture(tNormal, uvPos).rgb;

    vec3 camPos     = viewPos;
    vec3 WorldPos   = texture(tPosition, uvPos).rgb;
    vec3 albedo     = texture(tAlbedoSpec, uvPos).rgb;
    float metallic  = 0.0;
    float transparency = 1.0 - texture(tAlbedoSpec, uvPos).a;
    float roughness = texture(tNormal, uvPos).a;

    float ao = 1.0;

    vec3 N = normalize(Normal);
    vec3 V = normalize(camPos - WorldPos);

    vec3 F0 = vec3(0.04);//
    F0 = mix(F0, albedo, metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < activeLights; ++i)
    {
        // calculate per-light radiance
        vec3 L = normalize(positions[i] - WorldPos);
        if (types[i] == 2) L = -normalize(positions[i]);
        vec3 H = normalize(V + L);
        vec3 radiance = calculateRadiance(positions[i], colors[i], types[i], WorldPos, V, falloffs[i]);

        // cook-torrance brdf
        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        vec3 nominator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 specular     = nominator / max(denominator, 0.001);

        // add to outgoing radiance Lo
        float NdotL = max(dot(N, L), 0.0);
        Lo += max((kD * albedo / PI + specular) * radiance * NdotL, vec3(0,0,0));
    }
    if (metallic > -1) {

        vec3 L = normalize(reflect(-V, N));

        vec3 H = normalize(V + L);
        vec3 radiance = 0.01 * texture(skybox, L).rgb;

        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        vec3 nominator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 specular     = nominator / max(denominator, 0.001);

        // add to outgoing radiance Lo
        float NdotL = max(dot(N, L), 0.0);

        Lo += max((kD * albedo / PI + specular) * radiance * NdotL, vec3(0,0,0));
    }

    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + Lo;

    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    if (gColor.a >=0.999) {
        //gColor.rgb = texture(skybox, refract(-V, N, 1.48749)).rgb;
        gColor.rgb = texture(skybox, -V).rgb;
        //gColor.rgb *= vec3(0.5);
    }

    return vec4(mix(color, albedo * gColor.rgb, transparency), texture(tPosition, uvPos).a);
}

float linearizeDepth(float depth) {

    return near + (far-near) * depth;

}

void main() {

    //float scatter = 2.0 * (length(WorldPos-camPos)*length(WorldPos-camPos) * (WorldPos.x-camPos.x + WorldPos.y-camPos.y + WorldPos.z-camPos.z));

    vec3 color = vec3(0);

    vec4 WorldPos = lightViewMatrix * vec4(texture(gPosition, uvPos).rgb, 1.0);
    float lightDist = -WorldPos.z / WorldPos.w;
    vec4 lightSpacePos = (toLightSpace * WorldPos);

    vec3 lightPos = clamp(vec3(0.5) + lightSpacePos.xyz / 2, vec3(0), vec3(1));

    /*if (0 < x && 1 > x && 0 < y && 1 > y)
        shadowDist = linearizeDepth(texture(shadowMap, vec2(x, y)).r);

    float shadowMultiplier = 1.0;
    if (shadowDist < lightDist)
        shadowMultiplier = 0.1;*/

    float shadowMultiplier = texture(shadowMap, lightPos);

    vec4 gColor = getGColor(shadowMultiplier);
    vec4 tColor = getTColor(gColor, shadowMultiplier);

    if (tColor.a <= gColor.a) {
        color = tColor.rgb;
    } else  {
        color = gColor.rgb;
    }

    if (gColor.a >= 1 && tColor.a >= 0.999) discard;

    FragColor = vec4(color.rgb, 1.0);
    //FragColor = vec4(shadowMultiplier);
    //FragColor = texture(gNormal, uvPos);

    //FragColor = vec4(texture(shadowMap, vec3(uvPos, lightPos.z)));

    //FragColor = texture(shadowMap, vec2(0.5, 0.5) + lightSpacePos.xy / 2);
}
