#version 140

uniform mat4 proj_matrix;
uniform mat4 world_matrix;
uniform mat4 view_matrix;
uniform vec3 sunDirection;
/*uniform mat4 shadowTransform;
uniform float time;
uniform sampler2D normalMap;*/

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

out vec3 lightDirection;
out vec2 v_texcoord;
out vec3 v_pos;
out vec3 v_normal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 v_binormal;

out float visibility;
out float cameraDistance;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.1;

float distance(vec3 pos1, vec3 pos2) {

	return sqrt((pos2.x-pos1.x) * (pos2.x-pos1.x) + (pos2.y-pos1.y) * (pos2.y-pos1.y) + (pos2.z-pos1.z) * (pos2.z-pos1.z));

}

void main(void) {


	transformPos = (world_matrix * (vec4(position,1)));
	vec4 positionFromCamera = view_matrix * transformPos;
	vec4 tmpTangent = world_matrix * vec4(tangent, 0);
	vec4 tmpNormal = world_matrix *vec4(normal, 0);
	vec3 bitangent = normalize(cross(tmpTangent.xyz, tmpTangent.xyz));

	mat3 toTangentSpace = mat3(
		tmpTangent.x, bitangent.x, tmpNormal.x,
		tmpTangent.y, bitangent.y, tmpNormal.y,
		tmpTangent.z, bitangent.z, tmpNormal.z
	);

    lightDirection = toTangentSpace * -normalize(sunDirection);
	v_normal = toTangentSpace * normalize(world_matrix * vec4(normal,0)).xyz;

	cameraDirection = toTangentSpace * ((inverse(view_matrix) * vec4(0,0,0,0)).xyz - transformPos.xyz);

	cameraDistance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(cameraDistance * density, gradient)),0,1);
	//shadowCoord = shadowTransform * pos;

	v_binormal = bitangent;

	v_pos = transformPos.xyz;
	gl_Position = proj_matrix * positionFromCamera;
	v_texcoord = uv;

}

