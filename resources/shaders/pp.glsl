#version 330
uniform sampler2D colorRender;
uniform sampler3D lut;

in vec2 uvPos;
in vec4 pos;

out vec4 outColor;

void main(void) {

 	vec4 color = texture(colorRender, uvPos);
    outColor= texture(lut, color.rgb);

    if (color.r > 1.0 || color.g > 1.0 || color.b > 1.0)
        outColor = vec4(1,0,1,0);

}
