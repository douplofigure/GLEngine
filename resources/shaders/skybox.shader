#ASCII

string vertexShader = "skybox_vert.glsl"
string fragmentShader = "skybox_frag.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string transformationMatrix = "transform"
	string projectionMatrix = "projection"
	string viewMatrix = "view"

	# Variables for ambient light
	string ambientDirection = "sunDirection"

	# Variables for textures
	string diffuseMap = "tex"

	string time = "time"

	string fontColor = "color"

}

comp [1] textures = [

    {
        string name = "skybox"
        int32 slot = 0
    }

]

comp [4] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	}
]
