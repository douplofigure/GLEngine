#version 330 core

uniform sampler2D tex;

in vec2 uvPos;
in vec4 pos;

out vec4 outColor;

void main() {

    outColor = texture(tex, uvPos);

}
