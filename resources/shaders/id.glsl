#version 130

uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
uniform vec3 sunDirection;
uniform mat4 shadowTransform;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

out vec2 uvPos;
out vec4 pos;

void main(void) {

	pos = vec4(position, 1);
	gl_Position = pos;
	uvPos = vec2(1-uv.x, 1 - uv.y);

}
