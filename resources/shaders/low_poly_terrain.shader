#ASCII

string vertexShader = "low_poly_vertex.glsl"
string fragmentShader = "low_poly_terrain_fragment.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string transformationMatrix = "transform"
	string projectionMatrix = "projection"
	string viewMatrix = "view"

	# Variables for ambient light
	string ambientDirection = "sunDirection"

	string clipping = "useClipping"

	# Variables for textures

	string diffuseMap = "grass"

	string time = "time"

}

comp [4] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	}
]
