#version 330

uniform sampler2D tex;

uniform vec2 charPos;
uniform vec2 charEnd;

in vec2 uvPos;

out vec4 color;

void main(){

    //color = vec4(1,1,1,1);

    vec2 uv;

    uv.x = (charPos.x + charEnd.x) - uvPos.x * charEnd.x;
    uv.y = 1-(charPos.y + uvPos.y * charEnd.y);
    vec4 tmpcolor = texture(tex, uv);
    if (tmpcolor.a < 0.1) discard;
    color = mix(vec4(1,1,1,0), vec4(0,0,0,1), tmpcolor.a);

}
