#version 140

uniform mat4 projection;
uniform mat4 view;
uniform vec3 sunDirection;
/*uniform mat4 shadowTransform;
uniform float time;
uniform sampler2D normalMap;*/

uniform float useClipping;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

in vec4 transform_0;
in vec4 transform_1;
in vec4 transform_2;
in vec4 transform_3;

out vec3 lightDirection;
out vec2 uvPos;
out vec4 pos;
out vec3 faceNormal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 passTangent;

out float visibility;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.1;

float distance(vec3 pos1, vec3 pos2) {

	return sqrt((pos2.x-pos1.x) * (pos2.x-pos1.x) + (pos2.y-pos1.y) * (pos2.y-pos1.y) + (pos2.z-pos1.z) * (pos2.z-pos1.z));

}

void main(void) {

	mat4 transform = mat4(
		transform_0.x, transform_0.y, transform_0.z, transform_0.w,
		transform_1.x, transform_1.y, transform_1.z, transform_1.w,
		transform_2.x, transform_2.y, transform_2.z, transform_2.w,
		transform_3.x, transform_3.y, transform_3.z, transform_3.w
	);

	transformPos = (transform * (vec4(position,1)));
	vec4 positionFromCamera = view * transformPos;

    lightDirection = -normalize(sunDirection);
	faceNormal = normalize(transform * vec4(normal,0)).xyz;

	cameraDirection = ((inverse(view) * vec4(0,0,0,0)).xyz - pos.xyz);

	float distance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(distance * density, gradient)),0,1);
	//shadowCoord = shadowTransform * pos;

	passTangent = tangent;

	pos = transformPos;
	gl_Position = projection * positionFromCamera;
	if (abs(useClipping) > 0.5)
        gl_ClipDistance[0] = useClipping * pos.y + 1;
    else
        gl_ClipDistance[0] = 1;
	uvPos = uv;

}
