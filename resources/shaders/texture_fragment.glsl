#version 330

#define PI 3.141592

uniform sampler2D tex;
uniform sampler2D specularMap;
uniform sampler2D normalMap;

//const vec3 overlayColor = vec3(0xef/255.0, 0xa0/255.0, 0x43/255.0);

//uniform sampler2D shadowMap;

vec3 sunColor = vec3(1,1,1);
vec3 reflectivity = vec3(0.1,0.1,0.1);
float shineDampener = 0.001;
vec3 skyColor = vec3(0,0,0);
//uniform float hasNormalMap;

in vec2 uvPos;
in vec4 pos;
in vec3 faceNormal;
in vec3 lightDirection;
in vec3 cameraDirection;
in float visibility;
in float cameraDistance;
in vec4 shadowCoord;

in vec3 passTangent;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec4 outLight;

float distributionGGX(vec3 N, vec3 H, float a) {

    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;

    return nom / denom;
}

float geometrySchlickGGX(float NdotV, float k) {

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float geometrySmith(vec3 N, vec3 V, vec3 L, float k) {

    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx1 = geometrySchlickGGX(NdotV, k);
    float ggx2 = geometrySchlickGGX(NdotL, k);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

void main(void) {

    float gamma = 2.2;
    vec4 groundColor = vec4(pow(texture(tex, uvPos).rgb, vec3(gamma)), 1);

	if(cameraDistance < 400.0) {

		vec3 normal = normalize(texture(normalMap, uvPos).xyz * 2.0 - 1.0);

	//	if(groundColor.a < 0.1) discard;

		float lightLevel = min(dot(normal, lightDirection),1);

		vec3 finalSpecular = vec3(0,0,0);
		/*if (dot(lightDirection, normal) > 0) {
			vec3 reflected = normalize(reflect(-lightDirection, normal));

			float specularFactor = dot(reflected, normalize(cameraDirection));
			float damped = pow(specularFactor, shineDampener);

			finalSpecular = reflectivity * max(damped * sunColor,0);

		}*/

		//vec4 sMapColor = texture(tex, uvPos) + groundColor + texture(normalMap, uvPos);

		//finalSpecular *= texture(specularMap, uvPos).xyz;

		outColor = mix(vec4(skyColor,1), lightLevel * vec4(sunColor, 1) * groundColor + vec4(finalSpecular, 1), visibility);
		outColor.a = 1;
		//outLight = vec4(0.0);

		//outColor = vec4(lightDirection-cameraDirection, 1.0);//texture(normalMap, uvPos);(passTangent + vec3(1.0, 1.0, 1.0)) * 0.5



	}
	else {
		vec3 normal = faceNormal;
		float lightLevel = min(normal.x * lightDirection.x + normal.y * lightDirection.y + normal.z * lightDirection.z, 1);
		outColor = mix(vec4(skyColor,1), lightLevel * vec4(sunColor, 1) * groundColor, visibility);
		outColor.a = 1;
		outLight = vec4(0.0);
	}
//	outColor = vec4(0.0, 0.0, 0.0, 1.0);

	//outColor = texture(specularMap, uvPos);

	//outColor = vec4(1,1,1,1);

	//outColor = groundColor;

}
