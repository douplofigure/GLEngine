#version 440 core
layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

in vec2 uvPos;
in vec4 pos;
in vec3 faceNormal;
in vec3 lightDirection;
in vec3 cameraDirection;
in float visibility;
in float cameraDistance;
in vec4 shadowCoord;

in vec3 passTangent;

in mat3 tangentToWorld;

void main() {

    gPosition = vec4(pos.xyz, gl_FragCoord.z);
    float roughness = 0.0;
    gNormal = vec4(tangentToWorld * normalize(vec3(0.5) * 2.0 - 1.0), roughness);
    gAlbedoSpec.rgb = pow(vec3(1,1,1), vec3(2.2));
    gAlbedoSpec.a = 1.0;//texture(specularMap, uvPos).r;

}
