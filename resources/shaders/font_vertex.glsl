#version 140

uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
uniform vec3 sunDirection;
/*uniform mat4 shadowTransform;
uniform float time;
uniform sampler2D normalMap;*/

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

out vec3 lightDirection;
out vec2 uvPos;
out vec4 pos;
out vec3 faceNormal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 passTangent;

out float visibility;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.1;

float distance(vec3 pos1, vec3 pos2) {

	return sqrt((pos2.x-pos1.x) * (pos2.x-pos1.x) + (pos2.y-pos1.y) * (pos2.y-pos1.y) + (pos2.z-pos1.z) * (pos2.z-pos1.z));

}

void main(void) {

	transformPos = (transform * (vec4(position,1)));
	vec4 positionFromCamera = view * transformPos;

    lightDirection = -normalize(sunDirection);
	faceNormal = normalize(transform * vec4(normal,0)).xyz;

	cameraDirection = ((inverse(view) * vec4(0,0,0,0)).xyz - pos.xyz);

	float distance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(distance * density, gradient)),0,1);
	//shadowCoord = shadowTransform * pos;

	passTangent = tangent;

	pos = transformPos;
	gl_Position = projection * positionFromCamera;
	uvPos = uv;

}
