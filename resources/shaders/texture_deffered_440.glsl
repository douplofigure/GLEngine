#version 440 core
layout (location = 0) out vec4 gPosition;
layout (location = 1) out vec4 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

layout (location = 0) uniform sampler2D diffuse;
layout (location = 2) uniform sampler2D specularMap;
layout (location = 1) uniform sampler2D normalMap;

in vec2 uvPos;
in vec4 pos;
in vec3 faceNormal;
in vec3 lightDirection;
in vec3 cameraDirection;
in float visibility;
in float cameraDistance;
in vec4 shadowCoord;

in vec3 passTangent;

in mat3 tangentToWorld;

void main() {

    gPosition = vec4(pos.xyz, gl_FragCoord.z);
    float roughness = texture(specularMap, uvPos).g;
    gNormal = vec4(tangentToWorld * normalize(texture(normalMap, uvPos).xyz * 2.0 - 1.0), roughness);
    gAlbedoSpec.rgb = pow(texture(diffuse, uvPos).rgb, vec3(2.2));
    gAlbedoSpec.a = texture(specularMap, uvPos).r;

}
