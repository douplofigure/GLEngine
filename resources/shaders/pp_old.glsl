#version 330
uniform sampler2D colorRender;
uniform sampler2D depthRender;

uniform vec3 cameraPos;
uniform float time;

vec3 colorMixer = vec3(1,1,1);
vec2 screenSize = vec2(1280, 720);
float depthBlurAmount = 300;

in vec2 uvPos;
in vec4 pos;

out vec4 outColor;

const float zNear = 0.1;
const float zFar = 2000;
const float sigma = 0.1;


vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {

  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.411764705882353) * direction;
  vec2 off2 = vec2(3.2941176470588234) * direction;
  vec2 off3 = vec2(5.176470588235294) * direction;
  color += texture2D(image, uv) * 0.1964825501511404;
  color += texture2D(image, uv + (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv - (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv + (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv - (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv + (off3 / resolution)) * 0.010381362401148057;
  color += texture2D(image, uv - (off3 / resolution)) * 0.010381362401148057;
  return color;

}

float linearizeDepth(float depth) {

    return 2.0 * zNear / (zFar + zNear - depth * (zFar - zNear));

}

vec3 rgb2hsv(vec3 c) {

    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);

}

vec3 hsv2rgb(vec3 c) {

    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);

}

/*float bloom(vec4 color) {

    float value = (color.x+color.y+color.y)/3;
    if (value < 0.7) return 0;
    return float(value);

}*/

void main(void) {

	vec2 uv = uvPos + 0.001* vec2(sin(3*time+11*uvPos.x), cos(7*time+uvPos.y));
	if (uv.x < 0) uv.x = 0;
	if (uv.x > 1) uv.x = 1;
	if (uv.y < 0) uv.y = 0;
	if (uv.y > 1) uv.y = 1;


 	vec4 color = texture(colorRender, uvPos);
	vec4 overlay = texture(colorRender, uv);
	float depth = linearizeDepth(texture(depthRender, uvPos).x);
	float centerDepth = linearizeDepth(texture(depthRender, vec2(0.5, 0.5)).x);
	float b = 0.4;

	if (b > 0) {
        color = vec4(rgb2hsv(color.xyz), 1);
        color.y *= 1.19;
        color.z *= 1.5;
        color = vec4(hsv2rgb(color.xyz), 1);
    	//outColor = mix(blur13(colorRender, uvPos, screenSize, 2*(uvPos - vec2(0.5, 0.5))) , , abs(depth-centerDepth));
    	outColor = vec4(colorMixer,1) * mix(color, vec4(0.7,0.8,0.9,1), max((depth-.5F) * 2, 0));
    }
    else
        outColor = vec4(colorMixer,1) * color;
    if (cameraPos.y < 0) {
        overlay = vec4(rgb2hsv(overlay.xyz), 1);
        overlay.y *= 0.6;
        overlay.z *= max(0.2, min(1.0 / abs(cameraPos.y), 1));
        overlay = vec4(hsv2rgb(overlay.xyz), 1);

        depth = linearizeDepth(texture(depthRender, uv).x);
        outColor = vec4(colorMixer,1) * mix(overlay, vec4(0.7,0.8,0.9,1), max((depth-.5F) * 2, 0));

    }

}
