#version 330
uniform sampler2D tex;

uniform vec3 color;

/*uniform sampler2D specularMap;
uniform sampler2D normalMap;*/

//const vec3 overlayColor = vec3(0xef/255.0, 0xa0/255.0, 0x43/255.0);

//uniform sampler2D shadowMap;

vec3 sunColor = vec3(1,1,1);
vec3 reflectivity = vec3(0.1,0.1,0.1);
float shineDampener = 0.01;
vec3 skyColor = vec3(0,0,0);
//uniform float hasNormalMap;

const float width = 0.5;
const float edge = 0.1;

in vec2 uvPos;
in vec4 pos;
in vec3 faceNormal;
in vec3 lightDirection;
in vec3 cameraDirection;
in float visibility;
in vec4 shadowCoord;

in vec3 passTangent;

out vec4 outColor;

void main(void) {

	vec4 groundColor = texture(tex, uvPos);
	vec3 normal = faceNormal;

	float distance = 1.0 - groundColor.a;

	if (distance > width + edge * 0.5) discard;

//	if(groundColor.a < 0.1) discard;

	float lightLevel = (normal.x * lightDirection.x + normal.y * lightDirection.y + normal.z * lightDirection.z);
	//vec4 sMapColor = texture(tex, uvPos) + groundColor + texture(normalMap, uvPos);

	/*if (groundColor.x > 0.9 && groundColor.y > 0.9 && groundColor.z > 0.9) {

        groundColor *= vec4(overlayColor, 1);

	}*/
	

	outColor = vec4(color, 1) * mix(vec4(skyColor,1), max(lightLevel,0.15) * vec4(sunColor, 1) * groundColor, visibility);
	outColor.a = 1 - smoothstep(width, width + edge, distance);
	if (outColor.a == 0) discard;

	//outColor = texture(tex, uvPos);

	//outColor = vec4(1,1,1,1);

	//outColor = groundColor;

}
