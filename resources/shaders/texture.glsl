#version 330
uniform sampler2D tex;
uniform sampler2D specularMap;
uniform sampler2D normalMap;
//uniform sampler2D shadowMap;

vec3 sunColor = vec3(1,1,1);
vec3 reflectivity = vec3(0.1,0.1,0.1);
float shineDampener = 0.01;
vec3 skyColor = vec3(0,0,0);
uniform float hasNormalMap;

in vec2 uvPos;
in vec4 pos;
in vec3 faceNormal;
in vec3 lightDirection;
in vec3 cameraDirection;
in float visibility;
in vec4 shadowCoord;

in vec3 passTangent;

out vec4 outColor;

void main(void) {

	vec4 groundColor = texture(tex, uvPos);
	vec3 normal = texture(normalMap, uvPos).xyz;
	if (groundColor.a < 0.5) {

		discard;

	}

	/*if (hasNormalMap > 0) {
		normal = texture(normalMap, uvPos).xyz;
	}*/
	float lightLevel = (normal.x * lightDirection.x + normal.y * lightDirection.y + normal.z * lightDirection.z);
	vec3 finalSpecular = vec3(0,0,0);
	if (dot(lightDirection, normal) > 0) {
		vec3 reflected = reflect(-lightDirection, normal);

		float specularFactor = dot(reflected, cameraDirection);
		float damped = pow(specularFactor, shineDampener);

		finalSpecular = texture(specularMap, uvPos).xyz * max(damped * sunColor,0);

	}

	vec4 sMapColor = texture(tex, uvPos) + groundColor + texture(normalMap, uvPos);

	outColor = mix(vec4(skyColor,1), max(lightLevel,0.05) * vec4(sunColor, 1) * groundColor + vec4(finalSpecular, 1), visibility);
	//outColor = vec4(normal, 1);
	//outColor = max(lightLevel, 0.1) * vec4(1,1,1, 1);
	//outColor = max(lightLevel, 0.1) * texture(tex, uvPos);
	//outColor = vec4(lightLevel, lightLevel, lightLevel, 1);
	//outColor = vec4(finalSpecular,1);
	//outColor = pos;
	//outColor = vec4(faceNormal,1);
	//outColor = vec4(gl_FragCoord.z,gl_FragCoord.z,gl_FragCoord.z, 1);
}
