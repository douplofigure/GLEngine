#version 140

uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
uniform vec3 sunDirection;
/*uniform mat4 shadowTransform;
uniform float time;
uniform sampler2D normalMap;*/

uniform float useClipping;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

out vec3 lightDirection;
out vec2 uvPos;
out vec4 pos;
out vec3 faceNormal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 passTangent;

out mat3 tangentToWorld;

out float visibility;
out float cameraDistance;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.1;

float distance(vec3 pos1, vec3 pos2) {

	return sqrt((pos2.x-pos1.x) * (pos2.x-pos1.x) + (pos2.y-pos1.y) * (pos2.y-pos1.y) + (pos2.z-pos1.z) * (pos2.z-pos1.z));

}

void main(void) {

    //mat4 toObjectSpace = inverse(transform);

	transformPos = (transform * (vec4(position,1)));
	vec4 positionFromCamera = view * transformPos;

	vec3 t = (transform * vec4(tangent, 0.0)).xyz;
	vec3 n = (transform * vec4(normal, 0.0)).xyz;

	vec3 bitangent = -cross(t, n);

	mat3 toTangent = (

        mat3(
            t.x, t.y, t.z,
            bitangent.x, bitangent.y, bitangent.z,
            n.x, n.y, n.z
        )

	);

	tangentToWorld = toTangent;

    lightDirection = -normalize(sunDirection).xyz;
	faceNormal = n;

	cameraDirection = ((inverse(view) * vec4(0,0,0,0)) - transformPos).xyz;

	cameraDistance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(cameraDistance * density, gradient)),0,1);
	//shadowCoord = shadowTransform * pos;

	passTangent = t;

	pos = transformPos;
	gl_Position = projection * positionFromCamera;
	if (abs(useClipping) > 0.5)
        gl_ClipDistance[0] = pos.y;
    else
        gl_ClipDistance[0] = 1;
	uvPos = uv;

}

