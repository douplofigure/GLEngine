#version 330

uniform sampler2D tex;
uniform sampler2D background;

in vec2 uvPos;

out vec4 color;

void main(){

    //color = vec4(1,1,1,1);
    vec4 tmpcolor = texture(tex, uvPos);
    if (tmpcolor.a < 0.2) discard;
    color = tmpcolor;

}
