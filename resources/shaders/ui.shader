#ASCII

string vertexShader = "ui_vertex.glsl"
string fragmentShader = "ui_fragment.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string transformationMatrix = "transform"
	string projectionMatrix = "projection"
	string viewMatrix = "view"

	# Variables for ambient light
	string ambientDirection = "sunDirection"
	string ambientColor = "sunColor"

	# Variables for textures
	string diffuseMap = "tex"
	string normalMap = ""
	string specularMap = ""
	string shadowMap = ""

	string time = "time"

}

comp [1] textures = [

    {
        string name = "tex"
        int32 slot = 0
    },
    {
        string name = "background"
        int32 slot = 1
    }

]

comp [4] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	}
]
