#ASCII

string vertexShader = "pbr_vertex.glsl"
string fragmentShader = "pbr_fragment.glsl"
string geometryShader = ""

comp uniforms {

	# Matrices for model Positioning
	string transformationMatrix = "world_matrix"
	string projectionMatrix = "proj_matrix"
	string viewMatrix = "view_matrix"

	# Variables for ambient light
	string ambientDirection = "sunDirection"
	string ambientColor = "sunColor"

	# Variables for textures
	string diffuseMap = "tex"
	string normalMap = "norm"
	string specularMap = "spec"

	string time = "time"

}

comp [4] inputVars = [
	{
		int32 id = 0
		string name = "position"
	},
	{
		int32 id = 1
		string name = "normal"
	},
	{
		int32 id = 2
		string name = "uv"
	},
	{
		int32 id = 3
		string name = "tangent"
	}
]

