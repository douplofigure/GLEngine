#version 440 core
out vec4 FragColor;

#define M_PI 3.14159265358979323846

in vec2 uvPos;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

struct Light {
    vec3 Position;
    vec3 Color;
    int type;
    float fallOffPower;
    float PADDING1;
    float PADDING2;
    float PADDING3;
};

const int NR_LIGHTS = 128;
//uniform Light lights[NR_LIGHTS];
uniform vec3 viewPos;

uniform vec3 positions[128];
uniform vec3 colors[128];
uniform int types[128];
uniform float falloffs[128];

uniform float time;

/*layout (std140) uniform lightData {

    Light lights[128];

};*/

const float shineDampener = 11.0;

void main()
{
    // retrieve data from G-buffer
    /*lights[0].Position = vec3(1, 2, 3);
    lights[0].Color = vec3(10, 10, 10);
    lights[0].type = 1;
    lights[0].fallOffPower = 2.0;*/
    vec3 FragPos = texture(gPosition, uvPos).rgb;
    vec3 Normal = texture(gNormal, uvPos).rgb;
    vec3 Albedo = texture(gAlbedoSpec, uvPos).rgb;
    float Specular = texture(gAlbedoSpec, uvPos).a;

    if (texture(gPosition, uvPos).a >= 1.0) discard;

    // then calculate lighting as usual
    vec3 lighting = Albedo * 0.005; // hard-coded ambient component
    vec3 viewDir = normalize(viewPos - FragPos);
    for(int i = 0; i < 128; ++i)
    {
        // diffuse
        if (types[i] == 0) continue;
        else if (types[i] == 1) {

            vec3 lightDir = normalize(positions[i] - FragPos);
            vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Albedo * colors[i];

            vec3 finalSpecular = vec3(0,0,0);
            if (dot(lightDir, Normal) > 0) {
                vec3 reflected = normalize(reflect(-lightDir, Normal));

                float specularFactor = dot(reflected, normalize(viewDir));
                float damped = pow(specularFactor, shineDampener);

                finalSpecular = Specular * max(damped * colors[i],0);

            }

            lighting += diffuse / (pow(length(positions[i] - FragPos), falloffs[i]));
            lighting += finalSpecular / (pow(length(positions[i] - FragPos), falloffs[i]));

        } else if (types[i] == 2) {

            vec3 lightDir = -normalize(positions[i]);
            vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Albedo * colors[i];

            vec3 finalSpecular = vec3(0,0,0);
            if (dot(lightDir, Normal) > 0) {
                vec3 reflected = normalize(reflect(-lightDir, Normal));

                float specularFactor = dot(reflected, normalize(viewDir));
                float damped = pow(specularFactor, shineDampener);

                finalSpecular = Specular * max(damped * colors[i],0);

            }

            lighting += diffuse;
            lighting += finalSpecular;

        } else if (types[i] == 3) {

            vec3 lightDir = normalize(positions[i] - FragPos);
            vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Albedo * colors[i] * (0.5*sin(falloffs[i]*time)+1+0.3*cos(falloffs[i]*time*3));

            vec3 finalSpecular = vec3(0,0,0);
            if (dot(lightDir, Normal) > 0) {
                vec3 reflected = normalize(reflect(-lightDir, Normal));

                float specularFactor = dot(reflected, normalize(viewDir));
                float damped = pow(specularFactor, shineDampener);

                finalSpecular = Specular * max(damped * colors[i],0);

            }

            lighting += diffuse / (pow(length(positions[i] - FragPos), 2.0));
            lighting += finalSpecular / (pow(length(positions[i] - FragPos), 2.0));

        } else if (types[i] == 4) {

            vec3 lightDir = normalize(positions[i] - FragPos);
            vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Albedo * 8;

            float intensity = (cos(M_PI * clamp(acos(dot(lightDir, normalize(-colors[i]))) / falloffs[i], 0, 1))+1)*0.5;
            //float intensity = clamp((theta - falloffs[i]) / epsilon, 0.0, 1.0);

            vec3 finalSpecular = vec3(0,0,0);
            if (dot(lightDir, Normal) > 0) {
                vec3 reflected = normalize(reflect(-lightDir, Normal));

                float specularFactor = dot(reflected, normalize(viewDir));
                float damped = pow(specularFactor, shineDampener);

                finalSpecular = Specular * max(damped * vec3(1,1,1), 0);

            }

            lighting += diffuse / (pow(length(positions[i] - FragPos), 1)) * intensity;
            lighting += finalSpecular / (pow(length(positions[i] - FragPos), 1)) * intensity;

        }

    }

    FragColor = pow(vec4(lighting, 1.0), vec4(1/2.2));

}
