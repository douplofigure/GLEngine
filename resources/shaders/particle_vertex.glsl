#version 140

uniform mat4 projection;
uniform mat4 view;
uniform vec3 sunDirection;
/*uniform mat4 shadowTransform;
uniform float time;
uniform sampler2D normalMap;*/

uniform float useClipping;
uniform float time;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

in vec4 transform_0;
in vec4 transform_1;
in vec4 transform_2;
in vec4 transform_3;

out vec3 lightDirection;
out vec2 uvPos;
out vec4 pos;
out vec3 faceNormal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 passTangent;

out float visibility;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.1;

const float gravity = -9.81;

void main(void) {

    vec3 v0 = vec3(transform_0.x, transform_0.y, transform_0.z);
    //v0 = vec3(1, 10, 0);
    float dTime = (time - transform_0.w);
    float gamma = transform_1.x * gravity;
    vec3 initPos = vec3(transform_1.y, transform_1.z, transform_1.w);
    //initPos = vec3(0, 100, 0);
    vec3 dPos = v0 * dTime + vec3(0, 0.5 * gamma * dTime * dTime, 0) + initPos + position;

    mat4 tMat = mat4 (
        1, 0, 0, -dPos.x,
        0, 1, 0, -dPos.y,
        0, 0, 1, -dPos.z,
        0, 0, 0, 1
    );

    mat4 vMat = mat4(
        1, 0, 0, view[2][0],
        0, 1, 0, view[2][1],
        0, 0, 1, view[2][2],
        view[0][3], view[1][3], view[2][3], 1
    );

    transformPos = vec4(dPos,1);
	vec4 positionFromCamera = view * transformPos;

    lightDirection = -normalize(sunDirection);
	faceNormal = normalize(vec4(normal,0)).xyz;

	cameraDirection = ((inverse(view) * vec4(0,0,0,0)).xyz - transformPos.xyz);

	float distance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(distance * density, gradient)),0,1);
	//shadowCoord = shadowTransform * pos;

	passTangent = tangent;

	pos = transformPos;
	gl_Position = projection * positionFromCamera;
	if (abs(useClipping) > 0.5)
        gl_ClipDistance[0] = useClipping * pos.y + 1;
    else
        gl_ClipDistance[0] = 1;
	uvPos = uv;

}
