#version 140

uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
uniform vec3 sunDirection;
uniform mat4 shadowTransform;
uniform float time;
uniform sampler2D normalMap;

uniform float useClipping;

in vec3 position;
in vec2 uv;
in vec3 normal;
in vec3 tangent;

out vec3 lightDirection;
out vec2 uvPos;
out vec4 pos;
out vec3 faceNormal;
out vec3 cameraDirection;
out vec4 shadowCoord;
out vec4 transformPos;

out vec3 passTangent;

out float visibility;
out float iTime;

out vec4 clipSpace;
out vec4 undistorted;

const float density = 0.001;
const float gradient = 1;
const float amplitude = 0.75;

float distance(vec3 pos1, vec3 pos2) {

	return sqrt((pos2.x-pos1.x) * (pos2.x-pos1.x) + (pos2.y-pos1.y) * (pos2.y-pos1.y) + (pos2.z-pos1.z) * (pos2.z-pos1.z));

}

void main(void) {

	vec4 tmpPos = vec4(position, 1);
	tmpPos.y += amplitude * sin(13*time + 10*position.x) + amplitude * 1.5 * cos(3*time + 8*position.z);

	vec3 grad = vec3(0.15*cos(10*position.x + time), -0.2 * sin(8*position.z+3*time), 0.6*cos(time+10*position.x)-2.4*sin(3*time+8*position.z));

	vec3 calcNormal = cross(vec3(1, grad.x, 0), vec3(0, grad.z, 1));
//	calcNormal.y = 1;

    clipSpace = projection * view * transform * vec4(position, 1);
    undistorted = projection * view * transform * tmpPos;

	transformPos = (transform * tmpPos);
	vec4 positionFromCamera = view * transformPos;

    lightDirection = -normalize(sunDirection);
	faceNormal = normalize(transform * vec4(calcNormal,0)).xyz;

	cameraDirection = ((inverse(view) * vec4(0,0,0,0)).xyz - pos.xyz);

	float distance = length(positionFromCamera.xyz);

	visibility = clamp(exp(-pow(distance * density, gradient)),0,1);
	shadowCoord = shadowTransform * pos;

	passTangent = tangent;

	pos = transformPos;
	//clipSpace = projection * positionFromCamera;
	gl_Position = projection * positionFromCamera;
	if (abs(useClipping) > 0.5)
        gl_ClipDistance[0] = pos.y + 1;
    else
        gl_ClipDistance[0] = 1;
	uvPos = pos.xz + vec2(time, time);
	iTime = time;

}
