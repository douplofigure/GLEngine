#version 330

uniform mat4 transform;

in vec3 position;
in vec3 normal;
in vec2 uv;
in vec3 tangent;

out vec2 uvPos;

void main() {

    uvPos = uv;
    vec4 elemPos = (transform * vec4(position, 1));
    vec2 outPos = elemPos.xy;
    gl_Position = elemPos;

}
