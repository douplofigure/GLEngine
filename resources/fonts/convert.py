import struct
import sys
import os

class CharData():
	
	def __init__(self, lineData):
		
		self.id = 0
		self.x = 0
		self.y = 0
		self.width = 0
		self.height = 0
		
		self.xOffset = 0
		self.yOffset = 0
		self.xAdvance = 0
		self.page = 0
		self.chanel = 0
		
		for data in lineData:
			ls = data.split("=")
			if ls[0] == "id":
				self.id = int(ls[1])
			elif ls[0] == "x":
				self.x = int(ls[1])
			elif ls[0] == "y":
				self.y = int(ls[1])
			elif ls[0] == "width":
				self.width = int(ls[1])
			elif ls[0] == "height":
				self.height = int(ls[1])
			elif ls[0] == "xoffset":
				self.xOffset = int(ls[1])
			elif ls[0] == "yoffset":
				self.yOffset = int(ls[1])
			elif ls[0] == "xadvance":
				self.xAdvance = int(ls[1])
			elif ls[0] == "page":
				self.page = int(ls[1])
			elif ls[0] == "chnl":
				self.chanel = int(ls[1])
				
	def writeToBin(self, outFile):
		outFile.write(struct.pack("<i", self.id))
		outFile.write(struct.pack("<i", self.x))
		outFile.write(struct.pack("<i", self.y))
		outFile.write(struct.pack("<i", self.width))
		outFile.write(struct.pack("<i", self.height))
		outFile.write(struct.pack("<i", self.xOffset))
		outFile.write(struct.pack("<i", self.yOffset))
		outFile.write(struct.pack("<i", self.xAdvance))
		outFile.write(struct.pack("<i", self.page))
		outFile.write(struct.pack("<i", self.chanel))
		
class Page():
	
	def __init__(self, lineData):
		self.id = 0
		self.fname = ""
		
		for data in lineData:
			ls = data.split("=")
			if ls[0] == "id":
				self.id = int(ls[1])
			elif ls[0] == "file":
				self.fname = ls[1].split(".")[0][1:]
				
	def writeToBin(self, outFile):
		
		outFile.write(struct.pack("<i", self.id))
		
		outFile.write(struct.pack("<h", len(self.fname)))
		for char in self.fname:
			outFile.write(struct.pack("b", ord(char)))

def main():
	if len(sys.argv) < 3:
		print("Please specify input and output files")
		return
	f = open(sys.argv[1], "r")
	chars = []
	pages = []
	lineHeight = 0
	scaleW = 0
	scaleH = 0
	base = 0
	for l in f.readlines():
		line = l.split(" ")
		if line[0] == "char":
			chars.append(CharData(line))
		if line[0] == "page":
			pages.append(Page(line))
		if line[0] == "common":
			for data in line:
				ls = data.split("=")
				if ls[0] == "lineHeight":
					lineHeight = int(ls[1])
				elif ls[0] == "scaleW":
					scaleW = int(ls[1])
				elif ls[0] == "scaleH":
					scaleH = int(ls[1])
				elif ls[0] == "base":
					base = int(ls[1])
		
	f.close()
	out = open(sys.argv[2], "wb")
	
	out.write(struct.pack("<i", lineHeight))
	out.write(struct.pack("<i", base))
	out.write(struct.pack("<i", scaleW))
	out.write(struct.pack("<i", scaleH))
	
	out.write(struct.pack("<i", len(pages)))
	for p in pages:
		p.writeToBin(out)
		os.system("convert %s.png ../textures/fonts/%s.tga" % (p.fname, p.fname))

	out.write(struct.pack("<i", len(chars)))
	for c in chars:
		c.writeToBin(out)
	
	out.close()
			
			
if __name__ == "__main__":
	main()
	
