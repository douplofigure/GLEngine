CFILES := $(wildcard src/*.cc) $(wildcard src/*.cpp) $(wildcard src/*/*.cc) $(wildcard src/*/*.cpp) $(wildcard src/*/*/*.cc) $(wildcard src/*/*/*.cpp) $(wildcard src/*/*/*/*.cc) $(wildcard src/*/*/*/*.cpp)
HFILES := lib.h
BUILD_NAME := GLEngine
MAKEFILES :=
TEST_NAME := GLEngine

LINUX_CC = gcc
LINUX_CXX = g++
LINUX_AR = ar

WIN32_CC = i686-w64-mingw32-gcc
WIN32_CXX = i686-w64-mingw32-g++
WIN32_AR = i686-w64-mingw32-ar

WIN64_CC = x86_64-w64-mingw32-gcc
WIN64_CXX = x86_64-w64-mingw32-g++
WIN64_AR = x86_64-w64-mingw32-ar

CFLAGS =
CXXFLAGS = -std=c++11
AR_MODE = -rcs

INCLUDE_DIRS := . src

LINUX_DIR :=linux
WIN32_DIR :=win32
WIN64_DIR :=win64

LINUX_LIBS := -lGLEngine -lGL -lglfw -lply -lconfigloader -lpthread -ldl -lopenal -lalut -lBulletDynamics -lBulletCollision -lLinearMath -ltga -lstack -lm -lutil
WIN32_LIBS := -lGLEngine -lopengl32 -lglew32 -lglfw3 -lm -lconfigloader -lply_win32 -luser32 -lgdi32 -lpthread -lOpenAL32 -lalut -lBulletDynamics -lBulletCollision -lLinearMath -ltga -lstack
WIN64_LIBS := -lGLEngine -lopengl32 -lglew32 -lglfw3 -lm -lconfigloader -lply_win64 -luser32 -lgdi32 -lpthread -lOpenAL32 -lalut -lBulletDynamics -lBulletCollision -lLinearMath -ltga -lstack

#INTERNAL : DON NOT CHANGE

obj_target = obj/${2}/$(addsuffix .o,$(basename $(notdir ${1})))	#$(patsubst %.c,%.o,$(notdir ${1}))
test_target = test/${2}/$(basename ${1})		#$(patsubst %.c,%.o,$(notdir ${1}))
lib_target = lib/${2}/lib${1}.a
hdr_target = include/${2}/${1}
build_target = build/${2}/${1}
include_dir = -I./include/${2}/${1}

objlinux.c :=
objlinux.cpp :=
objlinux.cc :=
liblinux.a :=
objwin32.o :=
objwin64.o :=
libwin32.a :=
libwin64.a :=
hdr.h :=

define OFILES
  $(foreach src,${CFILES},$(call obj_target,${src},${1}))
endef

define obj
  $(call obj_target,${1},${2}) : ${1} | obj/${2}/
  obj${2}$(suffix ${1}) += $(call obj_target,${1},${2})
endef

define test
  $(call test_target,${1},${2}) : ${1} | lib/${2}/lib$(LIB_NAME).a
  test${2}$(suffix ${1}) += $(call test_target,${1},${2})
endef

define lib
  $(call lib_target,${1},${2}) : $(foreach src,${CFILES},$(call obj_target,${src},${2})) | lib/${2}/
  lib${2}.a += $(call lib_target,${1},${2})
endef

define hdr
  $(call hdr_target,${1},${2}) : ${1} | include/${2}/
  hdr.h += $(call hdr_target,${1},${2})
endef

define build
  $(call build_target,${1},${2}) : $(call OFILES,${2}) | build/${2}/
  final_${2} += $(call build_target,${1},${2})
endef

define SOURCES
  $(foreach src,${1},$(eval $(call obj,${src},${2})))
endef

define HEADERS
  $(foreach src,${1},$(eval $(call hdr,${src},${2})))
endef

define FINALS
  $(eval $(call build,${1},${2}))
endef

all : linux win32 win64 Makefile #headers

linux : $(call build_target,${BUILD_NAME},${LINUX_DIR})

win32 : $(call build_target,${BUILD_NAME},${WIN32_DIR})

win64 : $(call build_target,${BUILD_NAME},${WIN64_DIR})

include $(MAKEFILES)

headers: $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${LINUX_DIR}))\
         $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${WIN32_DIR}))\
         $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${WIN64_DIR}))


$(eval $(call SOURCES,${CFILES},${LINUX_DIR}))
$(eval $(call FINALS,${BUILD_NAME},${LINUX_DIR}))
#$(eval $(call HEADERS,${HFILES},${LINUX_DIR}))
#$(eval $(call lib,${LIB_NAME},${LINUX_DIR}))

$(eval $(call SOURCES,${CFILES},${WIN32_DIR}))
$(eval $(call FINALS,${BUILD_NAME},${WIN32_DIR}))
#$(eval $(call HEADERS,${HFILES},${WIN32_DIR}))
#$(eval $(call lib,${LIB_NAME},${WIN32_DIR}))

$(eval $(call SOURCES,${CFILES},${WIN64_DIR}))
$(eval $(call FINALS,${BUILD_NAME},${WIN64_DIR}))
#$(eval $(call HEADERS,${HFILES},${WIN64_DIR}))
#$(eval $(call lib,${LIB_NAME},${WIN64_DIR}))

${final_linux} : % :
	@echo Linking project
	${LINUX_CXX} -o $@ $^ $(CXXFLAGS) -L./lib/linux/ ${LINUX_LIBS}

${final_win32} : % :
	@echo Linking project
	@${WIN32_CXX} -o $@ $^ $(CXXFLAGS) -L./lib/win32/ ${WIN32_LIBS}

${final_win63} : % :
	@echo Linking project
	@${WIN64_CXX} -o $@ $^ $(CXXFLAGS) -L./lib/win64/ ${WIN64_LIBS}

${objlinux.c} : % :
	@echo Compiling $@
	@$(LINUX_CC) -c -o $@ $< $(CFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${LINUX_DIR}))

${objlinux.cpp} : % :
	echo Compiling $@
	$(LINUX_CXX) -c -o $@ $< $(CXXFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${LINUX_DIR}))

${objlinux.cc} : % :
	echo Compiling $@
	$(LINUX_CXX) -c -o $@ $< $(CXXFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${LINUX_DIR}))

${liblinux.a} : % :
	@echo Creating library $@
	$(LINUX_AR) $(AR_MODE) $@ $^ 

${hdr.h}: % :
	@echo Copying header $@
	@cp $< $@

${objwin32.c} : % :
	@echo Compiling $@
	@$(WIN32_CC) -c -o $@ $< $(CFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${WIN32_DIR}))

${objwin32.cpp} : % :
	@echo Compiling $@
	@$(WIN32_CXX) -c -o $@ $< $(CXXFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${WIN32_DIR}))

${objwin32.cc} : % :
	@echo Compiling $@
	@$(WIN32_CXX) -c -o $@ $< $(CXXFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${WIN32_DIR}))

${libwin32.a} : % :
	@echo Creating library $@
	$(WIN32_AR) $(AR_MODE) $@ $^

${objwin64.c} : % :
	@echo Compiling $@
	@$(WIN64_CC) -c -o $@ $< $(CFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${WIN64_DIR}))

${objwin64.cpp} : % :
	@echo Compiling $@
	@$(WIN64_CXX) -c -o $@ $< $(CXXFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${WIN64_DIR}))

${objwin64.cc} : % :
	@echo Compiling $@
	@$(WIN64_CXX) -c -o $@ $< $(CXXFLAGS) $(foreach dir,${INCLUDE_DIRS},$(call include_dir,${dir},${WIN64_DIR}))

${libwin64.a} : % :
	@echo Creating library $@
	$(WIN64_AR) $(AR_MODE) $@ $^

${testlinux.c} : % :
	@echo Building $@
	$(LINUX_CC) -o $@ $< $(CFLAGS) -L./lib/linux/

${testlinux.cpp} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CFLAGS) -L./lib/linux/

${testlinux.cc} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CFLAGS) -L./lib/linux/

${testwin32.c} : % :
	@echo Building $@
	$(LINUX_CC) -o $@ $< $(CFLAGS) -L./lib/win32/

${testwin32.cpp} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CFLAGS) -L./lib/win32/

${testwin32.cc} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CFLAGS) -L./lib/win32/

${testwin64.c} : % :
	@echo Building $@
	$(LINUX_CC) -o $@ $< $(CFLAGS) -L./lib/win64/

${testwin64.cpp} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CFLAGS) -L./lib/win64/

${testwin64.cc} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CFLAGS) -L./lib/win64/

include/win32/: include/
	@mkdir -p include/win32

include/win64/: include/
	@mkdir -p include/win64

include/linux/: include/
	@mkdir -p include/linux

include/:
	@mkdir -p include

lib/linux/: lib/
	@mkdir -p lib/linux

lib/win32/: lib/
	@mkdir -p lib/win32

lib/win64/: lib/
	@mkdir -p lib/win64

lib/:
	@mkdir -p lib

build/linux/: build/
	@mkdir -p build/linux

build/win32/: build/
	@mkdir -p build/win32

build/win64/: build/
	@mkdir -p build/win64

build/:
	@mkdir -p build

obj/win64/: obj/
	@mkdir -p obj/win64

obj/win32/: obj/
	@mkdir -p obj/win32

obj/linux/: obj/
	@mkdir -p obj/linux

obj/:
	@mkdir -p obj

test/linux/:
	@mkdir -p test/linux

test/win32/:
	@mkdir -p test/win32

test/win64/:
	@mkdir -p test/win64

test/:
	@mkdir -p test

.PHONY: all
