#include "gameengine.h"
//#include "allheaders.h"

#ifndef RELEASE_BUILD

#define MWORLD_SIZE 4096
#define WORLD_SIZE_1 MWORLD_SIZE * sqrt(2)
#define WORLD_SIZE_2 (WORLD_SIZE_1 / 2)
#define WORLD_RES 128

#define LOAD_WORLD

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>

#include "structure/level.h"
#include "render/viewportdeffered.h"
#include "render/staticrenderelement.h"
#include "render/animrenderelement.h"
#include "ui/uibutton.h"
#include "input/uiinputmanager.h"
#include "world/flatterrain.h"
#include "world/player.h"
#include "structure/structure.h"
#include "structure/structureloader.h"

#include "structure/daeloader.h"

#include "plugin/plugin.h"
#include "animation/animationrig.h"

Level * lvl;
using namespace Anim;

void preloadFunction() {


}

void loadingFunction(ResourceContainer * resources) {

    LoadingStage * fontResources = new LoadingStage();

    GameEngine::setLoadingMessage("Loading Models");

    resources->load<Texture>("ui/testbtn");
    resources->load<Texture>("ui/crosshair");

    resources->load<Shader>("font");
    resources->load<Shader>("particle");
    resources->load<Shader>("low_poly");
    resources->load<Shader>("blit");
    resources->load<Shader>("testpp");

    resources->load<Shader>("skybox");
    resources->load<Model>("skybox");

    resources->load<Model>("bone");
    resources->load<Shader>("anim_texture");
    resources->load<Shader>("anim_transparent");
    resources->load<Material>("planks");

    GameEngine::setLoadingMessage("Loading Level");

    lvl = Level::loadFromFile("resources/levels/test.lvl");
    lvl->loadRequirements(resources);

    resources->load<Structure>("projectile");

    resources->load<Structure>("test");
    resources->load<Structure>("crate");

    resources->load<Structure>("bin_test");

    /*resources->load<AnimationRig>("mausofant");*/
    resources->load<AnimationRig>("Armature");
    //resources->load<AnimationRig>("SheepAnim");

}

int main(int argc, char ** argv) {

    GameEngine::setResourceLocation("resources/");

    GameEngine::setLoadingFunc(loadingFunction);
    GameEngine::setPreloadFunc(preloadFunction);

    GameEngine::init(false, PBR, true);

    std::vector<int> renderSize = GameEngine::getRenderSize();

    ViewportDeffered * viewport = new ViewportDeffered(renderSize[0], renderSize[1], 70.0, 0.1, 2000, 128);

    viewport->setClearColor(0, 0, 0);

    viewport->addLayer();
    #ifdef LOAD_WORLD_
    GameEngine::world = World::loadFromFile("world_txt.save", viewport);
    #endif // LOAD_WORLD

    Viewport * menuView = new Viewport(renderSize[0], renderSize[1], 70, 0.1, 100);

    GameEngine::addViewport("menu", menuView);
    GameEngine::addViewport("standard", viewport);

    Texture * btnTexture = GameEngine::resources->get<Texture>("ui/testbtn");

    menuView->addUIElement(std::shared_ptr<UIButton>(new UIButton(0.5, 0.5, (double) btnTexture->getWidth() / renderSize[0], (double) btnTexture->getHeight() / renderSize[0],
                                        GameEngine::resources->get<Texture>("ui/testbtn"))));
    GameEngine::setViewport("standard");

    /**
        Loading Test-stuff
    **/

    Font * fnt = GameEngine::resources->get<Font>("distance");

    std::shared_ptr<UIButton> button(new UIButton(0.5, 1, 0.09, 0.045, GameEngine::resources->get<Texture>("ui/testbtn"), NORTH));
    std::shared_ptr<UIInputManager> uiInput(new UIInputManager());
    uiInput->addElement(button);
    viewport->addUIElement(button);

    std::shared_ptr<UIElement> crosshair(new UIElement(0.5, 0.5, 0.015, 0.015, GameEngine::resources->get<Texture>("ui/crosshair")));
    viewport->addUIElement(crosshair);

    GameEngine::world->setTerrain(new FlatTerrain(0.0));

    lvl->applyToWorld(GameEngine::world, viewport);

    std::cout << "level applied to world" << std::endl;

    std::shared_ptr<Player> player(new Player(Vector3(0,2.0,0), Vector3(0,0,0), 70.0, viewport->getCamera(), viewport));
    GameEngine::world->addPlayer(player, true);

    std::cout << "Creating AnimRenderElement" << std::endl;
    //std::shared_ptr<AnimRenderElement> animElem(new AnimRenderElement(GameEngine::resources->get<AnimationRig>("Armature"), GameEngine::resources->get<Material>("metall"), 5, 0, -12));
    //AnimRenderElement * animElem2 = new AnimRenderElement(GameEngine::resources->get<AnimationRig>("Armature"), GameEngine::resources->get<Material>("planks"), 5, 0, 12);
    std::shared_ptr<RenderElement> testElem(new RenderElement(GameEngine::resources->get<Structure>("bin_test"), 5, 0, -12));
    viewport->addElement(0, testElem);
    std::cout << "Adding element to viewport" << std::endl;
    //animElem->changeRotation(0, 0, 90);
    //viewport->addElement(0, animElem);
    //viewport->addElement(0, animElem2);

    GameEngine::addInputManager(player);
    GameEngine::addInputManager(uiInput);

    #ifdef _DEBUG
    GameEngine::resources->saveLoadedToFile("loadedResources.txt");
    #endif // _DEBUG

    GameEngine::setViewport("standard");
    //viewport->prerenderReflectionTextures();
    std::cout << "Starting main loop" << std::endl;
    GameEngine::mainloop();

    int majorVersion;
    int minorVersion;

    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

    int openglVersion = majorVersion * 100 + minorVersion * 10;

    std::cout << "Opengl Version " << openglVersion << " " << std::string((char*)glGetString(GL_VERSION)) <<  std::endl;

    //delete pymodule;
    GameEngine::terminate();

    return 0;

}

#endif
