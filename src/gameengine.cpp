#include "gameengine.h"

#include <iostream>
#include <math.h>

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include <GLFW/glfw3.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include "world/lowpolyterrain.h"
#include "ui/fontuielement.h"
#include "render/lut.h"
#include "audio/audiolistener.h"
#include "util/terraingenerator.h"
#include "plugin/plugin.h"

Window * GameEngine::window;
std::list<std::shared_ptr<InputManager>> GameEngine::inputManagers;
bool GameEngine::shouldStop;
bool GameEngine::cursorVisible = true;
bool GameEngine::loading;
RENDERING_MODE GameEngine::renderMode;

std::thread * GameEngine::pollThread;

void onError(int code, const char * msg);
void * pollThreadFunc();

ResourceContainer * GameEngine::resources;

double GameEngine::oldTime;
std::string GameEngine::loadingMessage = "Loading";

World * GameEngine::world;
std::vector<int> GameEngine::renderSize;

int GameEngine::openglVersion;

std::function<void(ResourceContainer *)> GameEngine::loadingFunc;
std::function<void()> GameEngine::preLoadFunc;

std::shared_ptr<UIElement> GameEngine::loadingElem;
std::shared_ptr<FontUIElement> GameEngine::message;

namespace Loading {

    bool done = false;

    void * keepAlive(void * ptr);

}

void GameEngine::loadInit(std::function<void(ResourceContainer *)> loadingFunc) {

    loading = true;
    loadingFunc(resources);
    loading = false;
    std::cout << "Loading Done!" << std::endl;

}

void GameEngine::setLoadingFunc(std::function<void(ResourceContainer *)> f) {
    loadingFunc = f;
}

void GameEngine::setPreloadFunc(std::function<void()> f) {
    preLoadFunc = f;
}

void GameEngine::loadWithScreen(std::function<void(ResourceContainer *)> f) {

    Viewport * oldView = window->currentViewport;

    setViewport("__loading");

    loading = true;
    std::thread loadThread = std::thread(loadInit, f);

    std::cout << "Starting loading loop" << std::endl;

    while (GameEngine::loading) {
        glEnable(GL_BLEND);
        //if (createWindow)
            window->updateContents();
        glDisable(GL_BLEND);
        resources->uploadPending();
        message->setText(loadingMessage);
        #ifdef _WIN32
        //if (createWindow)
        glfwPollEvents();
        #endif

        //std::cout << "loading loop" << std::endl;

        //std::this_thread::sleep_for(std::chrono::seconds(1));

    }
    if (loadThread.joinable())
        loadThread.join();

    resources->uploadPending();

    window->currentViewport = oldView;

}

void GameEngine::init(bool vSync, RENDERING_MODE rMode, bool createWindow, bool loadPlugins) {

    #ifdef _WIN32
    glewExperimental = GL_TRUE;
    #endif // _WIN32

    if (loadPlugins)
        Plugin::loadAll();

    Entity::registerEntityType("Entity", Entity::create);

    preLoadFunc();
    Plugin::onPreInit();

    if (!glfwInit()) {

        std::cerr << "Could not initialize GLFW\n";
        glfwTerminate();
        exit(-1);

    }

    glfwSetErrorCallback(onError);

    /**
        Setting up Rendering context
    **/

    CompoundNode * viewConfig = ConfigLoader::loadFileTree("viewport.conf");
    renderSize = viewConfig->getArray<int>("renderSize")->getValue();
    std::string windowName(viewConfig->get<const char *>("windowName"));

    if (createWindow)
        window = new Window(renderSize[0], renderSize[1], windowName);
    else
        window = nullptr;

    int majorVersion;
    int minorVersion;

    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

    openglVersion = majorVersion * 100 + minorVersion * 10;

    std::cout << "Opengl Version " << openglVersion << " " << std::string((char*)glGetString(GL_VERSION)) <<  std::endl;

    //exit(0);

    if (createWindow) glfwSwapInterval(vSync ? 1 : 0);

    initSoundEngine();
    AudioListener::init(0,0,0);
    world = new World(nullptr, new PhysicsConfig("physics.conf"));
    std::cout << "Loading world" << std::endl;

    //pthread_create(&pollThread, NULL, pollThreadFunc, NULL);
    if (createWindow)
        pollThread = new std::thread(pollThreadFunc);

    renderMode = rMode;

    std::cout << resources << std::endl;

    resources->load<Model>("quad");
    resources->load<Shader>("ui");
    resources->load<Texture>("fonts/distance");
    resources->load<Shader>("uifont");
    resources->load<Shader>("light");
    resources->load<Shader>("blit");
    resources->load<Texture>("logo");
    resources->uploadPending();

    resources->load<Font>("distance");
    resources->load<LUT>("neutral");
    resources->load<Model>("cube");

    resources->uploadPending();

    Viewport * loadingView = new Viewport(renderSize[0], renderSize[1], 70.0, 0.1, 100);
    loadingView->addLayer();

    if (createWindow) {
        addViewport("__loading", loadingView);
        setViewport("__loading");
    }

    loadingElem = std::shared_ptr<UIElement>(new UIElement(0.5, 0.5, 0.75, 0.75, resources->get<Texture>("logo")));
    loadingView->addElement(0, loadingElem);

    message = std::shared_ptr<FontUIElement>(new FontUIElement(0.5, 0.1, 0.5, 0.5, loadingMessage, resources->get<Font>("distance")));
    message->setTextColor(0.3, 0.3, 0.3);
    loadingView->addElement(0, message);

    loadingView->setClearColor(0.7, 0.7, 0.7);

    if (createWindow) window->updateSize(renderSize[0], renderSize[1]);

    std::cout << "Going to start loadThread" << std::endl;

    loadWithScreen(loadingFunc);

    std::cout << "Init done" << std::endl;

}

std::vector<int> GameEngine::getRenderSize() {

    if (!window) {
        std::vector<int> s(2);
        s[0] = 1600;
        s[1] = 900;
        return s;
    }

    return renderSize;
}

void GameEngine::setLoadingMessage(std::string msg) {

    loadingMessage = msg;

}

void GameEngine::addViewport(std::string name, Viewport * port) {

    if (window)
        window->addViewport(name, port);

}

void GameEngine::mainloop() {

    loading = false;
    shouldStop = false;

    Plugin::onPostInit();

    std::cout << "Updating window size" << std::endl;
    window->updateSize(window->getWidth(), window->getHeight());

    std::cout << "Done updating size" << std::endl;
    while (!window->shouldClose() && !shouldStop) {
        onTick();
        window->updateContents();

        #ifdef _WIN32
        glfwPollEvents();
        #endif
	}
	shouldStop = 1;
	std::cout << "Mainloop exit" << std::endl;

}

void GameEngine::addInputManager(std::shared_ptr<InputManager> manager) {

    std::cout << "adding manager " << manager << std::endl;
    inputManagers.push_back(manager);

    manager->onTick(8.0);

}

void GameEngine::terminate() {

    //ResourceManager::cleanup();

    //world->saveToFile("world.save");

    world->stopPhysics();

    std::cout << "Physics stopped" << std::endl;

    delete world;

    std::cout << "stopping py interpreter" << std::endl;
    std::cout << "Interpreter halted" << std::endl;
    delete window;

    std::cout << "deleting resources" << std::endl;
    //delete resources;
    std::cout << "Resources deleted" << std::endl;

    glfwTerminate();
    std::cout << "glfw terminated" << std::endl;

    std::cout << "MeshCount: " << Math::Mesh::activeMeshes << std::endl;

}

void GameEngine::stop() {

    shouldStop = true;

}

void GameEngine::setResourceLocation(std::string dir) {

    shouldStop = false;
    resources = new ResourceContainer(dir);


}

void GameEngine::onTick() {

    double time = getTime();
    double dTime = time-oldTime;
    for (std::list<std::shared_ptr<InputManager>>::iterator it = inputManagers.begin(); it != inputManagers.end(); ++it) {

        (*it)->onTick(dTime);

    }
    world->update(dTime);

    oldTime = time;

}

void GameEngine::onMouseMotion(double dx, double dy) {

    for (std::list<std::shared_ptr<InputManager>>::iterator it = inputManagers.begin(); it != inputManagers.end(); ++it) {

        (*it)->onMouseMotion(dx, dy);

    }

}

void GameEngine::onMouseButton(int button, int action, int mods, double x, double y) {

    for (std::list<std::shared_ptr<InputManager>>::iterator it = inputManagers.begin(); it != inputManagers.end(); ++it) {

        (*it)->onMouseButton(button, action, mods, x, y);

    }

}

void GameEngine::onKeyboardUpdate(int key, int scancode, int action, int mods) {

    for (std::list<std::shared_ptr<InputManager>>::iterator it = inputManagers.begin(); it != inputManagers.end(); ++it) {

        (*it)->onKeyboard(key, scancode, action, mods);

    }

}

void GameEngine::hideCursor() {

    window->hideCursor();

}

void GameEngine::showCursor() {

    window->showCursor();

}

void GameEngine::togleCursor() {

    if (cursorVisible) hideCursor();
    else showCursor();

    cursorVisible = !cursorVisible;

}

void GameEngine::setViewport(std::string name) {

    window->setCurrentViewport(name);

}

Viewport * GameEngine::getActiveViewport() {
    return window->currentViewport;
}

double GameEngine::getTime() {

    return glfwGetTime();

}

Window * GameEngine::getActiveWindow() {
    return window;
}

bool GameEngine::isRunning() {
    return !shouldStop;
}

void onError(int code, const char * msg) {

    std::cerr << "GLFWERROR: " << std::string(msg) << "\n";
    GameEngine::stop();
    exit(code);

}

void * pollThreadFunc() {

    std::cout << "Starting Poll Thread" << std::endl;
    while (GameEngine::isRunning()) {

        glfwPollEvents();

    }
    std::cout << "Poll Thread terminated" << std::endl;

}

int GameEngine::getCPUCount() {

    return std::thread::hardware_concurrency();

}

void GameEngine::displayError(std::string msg) {

    int w = GameEngine::getActiveWindow()->getWidth();
    int h = GameEngine::getActiveWindow()->getHeight();
    Viewport * view = new Viewport(w, h, 7.0, 0.1, 100);

    std::shared_ptr<FontUIElement> msgBox(new FontUIElement(0.5, 0.5, 1, 1, msg, GameEngine::resources->get<Font>("distance")));

    view->addUIElement(msgBox);
    GameEngine::addViewport("error", view);
    GameEngine::setViewport("error");

}

bool GameEngine::getFullScreen() {
    return false;
}

RENDERING_MODE GameEngine::getRenderMode() {
    return renderMode;
}

int GameEngine::getGLVersion() {
    return openglVersion;
}

void GameEngine::useLevel(Level * level) {

    GameEngine::window->currentViewport->clearElements();
    GameEngine::window->currentViewport->clearLights();
    world->clearEntities();
    level->applyToWorld(world, GameEngine::window->currentViewport);

}
