#ifndef TESTINPUTHANDLER_H
#define TESTINPUTHANDLER_H

#include "inputmanager.h"
#include "render/renderelement.h"
#include "render/camera.h"
#include "render/viewport.h"
#include "world/world.h"
#include "ui/fontuielement.h"
#include "render/staticrenderelement.h"

#define KEY_COUNT 16

typedef enum mvt_key_e {

    MVT_FWD = 0,
    MVT_BACK,
    MVT_LEFT,
    MVT_RIGHT,
    MVT_UP,
    MVT_DOWN,
    MVT_FWD_OBJ,
    MVT_BACK_OBJ,
    MVT_LEFT_OBJ,
    MVT_RIGHT_OBJ,
    MVT_UP_OBJ,
    MVT_DOWN_OBJ,
    MVT_RP_OBJ,
    MVT_RN_OBJ,
    MVT_THROW,
    MVT_THROW_SPHERE

} MVT_KEY;

class TestInputHandler : public InputManager
{
    public:
        TestInputHandler(std::shared_ptr<RenderElement> elem, Camera * cam, std::shared_ptr<FontUIElement> fpsDisplay, Viewport * view, World * world, int light);
        virtual ~TestInputHandler();

        void onTick(double dTime);
        void onMouseMotion(double dx, double dy);

        void onMouseButton(int button, int action, int mods, double x, double y);

        void onKeyboard(int key, int scancode, int action, int mods);

        void createPhysicsObject();
        void createPhysicsObject2();

    protected:

        std::shared_ptr<RenderElement> elem;
        Camera * camera;
        Viewport * view;
        World * world;

        bool keys[KEY_COUNT];

        bool moveCam;
        bool touchGround;
        std::shared_ptr<FontUIElement> fpsDisplay;
        double fpsUpdateTime;
        int frameCount;

    private:

        void endBuilding();
        void moveRelative(glm::vec3 dir, double dist);
        void rotateObj(double dr);

        std::shared_ptr<StaticRenderElement> treeElement;
        std::shared_ptr<StaticRenderElement> spruceElement;
        std::shared_ptr<StaticRenderElement> stoneElement;
        std::shared_ptr<StaticRenderElement> bushElement;

        int currentObjectType;
        int currentInstanceId;

        int physObjectCount;

        int lightId;
        float lightValue;

        bool isBuilding;
        bool hasInstance;

        Vector3 currentPos;
        Vector3 currentRot;
        double currentScale;

};

#endif // TESTINPUTHANDLER_H
