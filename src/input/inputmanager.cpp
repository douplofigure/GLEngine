#include "inputmanager.h"

#include <iostream>

InputManager::InputManager()
{
    //ctor
}

InputManager::~InputManager()
{
    std::cout << "destroyed input manager" << std::endl;
    exit(1);
}

void InputManager::onKeyboard(int key, int scancode, int action, int mods) {

}

void InputManager::onMouseButton(int button, int action, int mods, double x, double y) {

}

void InputManager::onMouseMotion(double dx, double dy) {

}

void InputManager::onTick(double dTime) {

}
