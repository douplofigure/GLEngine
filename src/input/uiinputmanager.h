#ifndef UIINPUTMANAGER_H
#define UIINPUTMANAGER_H

#include "inputmanager.h"

#include <list>
#include <memory>

#include "ui/uiinteractionelement.h"

class UIInputManager : public InputManager
{
    public:
        /** Default constructor */
        UIInputManager();
        /** Default destructor */
        virtual ~UIInputManager();

        void onMouseButton(int button, int action, int mods, double x, double y);

        void addElement(std::shared_ptr<UIInteractionElement> element);

    protected:

    private:

        std::list<std::shared_ptr<UIInteractionElement>> elements;

};

#endif // UIINPUTMANAGER_H
