#ifndef MULTIMODEINPUT_H
#define MULTIMODEINPUT_H

#include "inputmanager.h"

#include <vector>


class MultiModeInput : public InputManager
{
    public:
        /** Default constructor */
        MultiModeInput();
        MultiModeInput(std::vector<InputManager*> modes, int current);
        /** Default destructor */
        virtual ~MultiModeInput();

        void onMouseMotion(double dx, double dy) override;
        void onMouseButton(int button, int action, int mods, double x, double y) override;

        void onKeyboard(int key, int scancode, int action, int mods) override;

        void onTick(double dTime) override;

        void setMode(int mode);
        int getMode();

        void addMode(InputManager * mode);

    protected:

    private:

        std::vector<InputManager*> modes;
        int currentMode;

};

#endif // MULTIMODEINPUT_H
