#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H


class InputManager
{
    public:
        InputManager();
        virtual ~InputManager();

        virtual void onMouseMotion(double dx, double dy);
        virtual void onMouseButton(int button, int action, int mods, double x, double y);

        virtual void onKeyboard(int key, int scancode, int action, int mods);

        virtual void onTick(double dTime);

    protected:

    private:
};

#endif // INPUTMANAGER_H
