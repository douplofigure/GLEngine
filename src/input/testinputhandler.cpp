#include "testinputhandler.h"

#include <GLFW/glfw3.h>

#include "gameengine.h"
#include "audio/audiolistener.h"

#include "world/entitystatic.h"
#include "world/meshterrain.h"
#include "render/particle/particlesystem.h"
#include "render/viewportdeffered.h"

#include "util/vecutils.h"

#include "util/math/constants.h"

#define THROW_DENSITY 2700.0
#define THROW_DENSITY_SPHERE 200.0

extern ParticleSystem * particles;

TestInputHandler::TestInputHandler(std::shared_ptr<RenderElement> testElem, Camera * camera, std::shared_ptr<FontUIElement> fps, Viewport * view, World * world, int light) {

    this->elem = testElem;
    this->camera = camera;
    this->view = view;
    this->world = world;

    keys[MVT_FWD] = false;
    keys[MVT_BACK] = false;
    keys[MVT_LEFT] = false;
    keys[MVT_RIGHT] = false;
    keys[MVT_UP] = false;
    keys[MVT_DOWN] = false;
    keys[MVT_FWD_OBJ] = false;
    keys[MVT_BACK_OBJ] = false;
    keys[MVT_LEFT_OBJ] = false;
    keys[MVT_RIGHT_OBJ] = false;
    keys[MVT_UP_OBJ] = false;
    keys[MVT_DOWN_OBJ] = false;

    keys[MVT_RP_OBJ] = false;
    keys[MVT_RN_OBJ] = false;

    keys[MVT_THROW] = false;
    keys[MVT_THROW_SPHERE] = false;

    this->touchGround = false;

    moveCam = false;

    fpsDisplay = fps;

    /*this->treeElement = world->getRenderElement("tree_new") ? (StaticRenderElement*) world->getRenderElement("tree_new") : new StaticRenderElement("tree_new", "low_poly_static", 1000000);
    this->spruceElement = world->getRenderElement("low_poly_tree") ? (StaticRenderElement*) world->getRenderElement("low_poly_tree") : new StaticRenderElement("low_poly_tree", "low_poly_static", 1000000);
    this->stoneElement = world->getRenderElement("rock") ? (StaticRenderElement*) world->getRenderElement("rock") : new StaticRenderElement("rock", "low_poly_static", 1000000);
    this->bushElement = world->getRenderElement("bush") ? (StaticRenderElement*) world->getRenderElement("bush") : new StaticRenderElement("bush", "low_poly_static", 1000000);*/

    /*view->addElement(0, treeElement);
    view->addElement(0, spruceElement);
    view->addElement(0, stoneElement);
    view->addElement(0, bushElement);*/

    this->isBuilding = false;
    this->hasInstance = false;

    this->physObjectCount = 0;


    this->lightId = light;
    this->lightValue = 1.0;


}

TestInputHandler::~TestInputHandler()
{
    //dtor
}

void TestInputHandler::moveRelative(glm::vec3 dir, double distance) {

    double dx = (-dir.x * camera->facing.z+ dir.z * camera->facing.x) * distance;
    double dy = (dir.y * distance);
    double dz = (dir.x * camera->facing.x + dir.z * camera->facing.z) * distance;

    currentPos.x += dx;
    currentPos.z += dz;

    currentPos.y = GameEngine::world->getTerrain()->getHeight(currentPos.x, currentPos.z);

    currentScale += dy;

    switch(this->currentObjectType) {

        /// TODO : rechange quaternion to carry over functionality
        case 0:
            this->treeElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), Math::Quaternion(1, 0, 0, 0), currentScale);
            break;

        case 1:
            this->spruceElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), Math::Quaternion(1, 0, 0, 0), currentScale);
            break;

        case 2:
            this->stoneElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), Math::Quaternion(1, 0, 0, 0), currentScale);
            break;

        case 3:
            this->bushElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), Math::Quaternion(1, 0, 0, 0), currentScale);
            break;

    }

}

void TestInputHandler::rotateObj(double dr) {

    currentRot.y += dr;

    /*switch (this->currentObjectType) {

        case 0:
            this->treeElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), glm::vec3(currentRot.x,currentRot.y,currentRot.z), currentScale);
            break;
        case 1:
            this->spruceElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), glm::vec3(currentRot.x,currentRot.y,currentRot.z), currentScale);
            break;
        case 2:
            this->stoneElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), glm::vec3(currentRot.x,currentRot.y,currentRot.z), currentScale);
            break;
        case 3:
            this->bushElement->updateInstance(currentInstanceId, glm::vec3(currentPos.x, currentPos.y, currentPos.z), glm::vec3(currentRot.x,currentRot.y,currentRot.z), currentScale);
            break;

    }*/

}

void TestInputHandler::onTick(double dTime) {

    this->elem->changeRotation(0, dTime, 0);

    if (keys[MVT_FWD]) this->camera->moveRelative(glm::vec3(0,0,1), 3 * dTime);
    if (keys[MVT_BACK]) this->camera->moveRelative(glm::vec3(0,0,-1), 3 * dTime);
    if (keys[MVT_RIGHT]) this->camera->moveRelative(glm::vec3(1,0,0), 3 * dTime);
    if (keys[MVT_LEFT]) this->camera->moveRelative(glm::vec3(-1,0,0), 3 * dTime);
    if (keys[MVT_UP]) this->camera->moveRelative(glm::vec3(0,1,0), 3 * dTime);
    if (keys[MVT_DOWN]) this->camera->moveRelative(glm::vec3(0,-1,0), 3 * dTime);

    if (keys[MVT_FWD_OBJ]) this->moveRelative(glm::vec3(0,0,1), 0.03);
    if (keys[MVT_BACK_OBJ]) this->moveRelative(glm::vec3(0,0,-1), 0.03);
    if (keys[MVT_RIGHT_OBJ]) this->moveRelative(glm::vec3(1,0,0), 0.03);
    if (keys[MVT_LEFT_OBJ]) this->moveRelative(glm::vec3(-1,0,0), 0.03);
    if (keys[MVT_UP_OBJ]) this->moveRelative(glm::vec3(0,1,0), 0.01);
        //this->view->setFov(0.99*camera->getFov());
    if (keys[MVT_DOWN_OBJ]) this->moveRelative(glm::vec3(0,-1,0), 0.01);
        //this->view->setFov(1.01111*camera->getFov());

    if (keys[MVT_RP_OBJ]) this->rotateObj(0.01);
    if (keys[MVT_RN_OBJ]) this->rotateObj(-0.01);

    if (touchGround) {
        this->camera->position.y = GameEngine::world->getTerrain()->getHeight(camera->position.x, camera->position.z) + 1.75;
        this->camera->updateViewMatrix();
    }

    //particles->spawnParticle(glm::vec3(0,0,0), glm::vec3(randomFloatInBounds(-2, 2), 10.0, randomFloatInBounds(-2, 2)), .4, 8.0);
    AudioListener::setPosition(this->camera->position.x, this->camera->position.y, this->camera->position.z);
    frameCount++;
    if (fpsUpdateTime > 1000){
        fpsDisplay->setText(std::string("fps: ").append(std::to_string(this->frameCount / this->fpsUpdateTime)).append("\nPhysics Objects: ").append(std::to_string(this->physObjectCount)));
        fpsUpdateTime = 0;
        frameCount = 0;
    }

    if (keys[MVT_THROW]) {
        this->createPhysicsObject();
        keys[MVT_THROW] = false;
    }
    if (keys[MVT_THROW_SPHERE]) {
        this->createPhysicsObject2();
        keys[MVT_THROW_SPHERE] = false;
    }

    fpsUpdateTime += dTime;

    ((ViewportDeffered *)this->view)->updateLight(lightId, camera->position, camera->facing, L_SPOT, M_PI/4.0 * lightValue);

}

void TestInputHandler::onMouseMotion(double dx, double dy) {

    if (moveCam) this->camera->changeRotation(-0.005*dy, -0.005 * dx, 0);
    AudioListener::setFacing(this->camera->facing.x, this->camera->facing.y, this->camera->facing.z);

}

void TestInputHandler::createPhysicsObject() {

    std::shared_ptr<RenderElement> rElem(new RenderElement(GameEngine::resources->get<Structure>("crate"), this->camera->position.x, this->camera->position.y, this->camera->position.z));
    std::shared_ptr<Entity> entity(new Entity(GameEngine::resources->get<Structure>("crate"), Vector3(this->camera->position.x, this->camera->position.y, this->camera->position.z), Vector3(0,0,0), 500, rElem));
    PhysicsObject * obj = entity->getPhysicsObject();
    Vector3 dir = Vector3(this->camera->facing.x, this->camera->facing.y, this->camera->facing.z);
    //obj->addForce(dir * obj->getMass() * 120, Vector3(0,0,0), 0.1);
    entity->getPhysicsObject()->velocity = dir * 12;
    world->addEntity(entity, true);
    view->addElement(0, rElem);

    this->physObjectCount++;

}

void TestInputHandler::createPhysicsObject2() {

    std::shared_ptr<RenderElement> rElem(new RenderElement(GameEngine::resources->get<Structure>("projectile"), this->camera->position.x, this->camera->position.y, this->camera->position.z));
    std::shared_ptr<Entity> entity(new Entity(GameEngine::resources->get<Structure>("projectile"), Vector3(this->camera->position.x, this->camera->position.y, this->camera->position.z), Vector3(0,0,0), 200, rElem));
    PhysicsObject * obj = entity->getPhysicsObject();
    Vector3 dir = Vector3(this->camera->facing.x, this->camera->facing.y, this->camera->facing.z);
    //obj->addForce(dir * obj->getMass() * 120, Vector3(0,0,0), 0.1);
    entity->getPhysicsObject()->velocity = dir * 12;
    world->addEntity(entity, true);
    view->addElement(0, rElem);

    this->physObjectCount++;

}

void TestInputHandler::endBuilding() {

    switch(this->currentObjectType) {

        case 0:
            world->addEntity(std::shared_ptr<EntityStatic>(new EntityStatic(treeElement, this->currentInstanceId)), false);
            break;

        case 1:
            world->addEntity(std::shared_ptr<EntityStatic>(new EntityStatic(spruceElement, this->currentInstanceId)), false);
            break;

        case 2:
            world->addEntity(std::shared_ptr<EntityStatic>(new EntityStatic(stoneElement, this->currentInstanceId)), false);
            break;

    }

    isBuilding = false;

}

void TestInputHandler::onKeyboard(int key, int scancode, int action, int mods) {

    switch(key){

        case GLFW_KEY_W :
            if (action == GLFW_PRESS) keys[MVT_FWD] = true;
            else if (action == GLFW_RELEASE) keys[MVT_FWD] = false;
            break;

        case GLFW_KEY_S :
            if (action == GLFW_PRESS) keys[MVT_BACK] = true;
            else if (action == GLFW_RELEASE) keys[MVT_BACK] = false;
            break;

        case GLFW_KEY_A :
            if (action == GLFW_PRESS) keys[MVT_LEFT] = true;
            else if (action == GLFW_RELEASE) keys[MVT_LEFT] = false;
            break;

        case GLFW_KEY_D :
            if (action == GLFW_PRESS) keys[MVT_RIGHT] = true;
            else if (action == GLFW_RELEASE) keys[MVT_RIGHT] = false;
            break;

        case GLFW_KEY_SPACE :
            if (action == GLFW_PRESS) keys[MVT_UP] = true;
            else if (action == GLFW_RELEASE) keys[MVT_UP] = false;
            break;

        case GLFW_KEY_LEFT_SHIFT :
            if (action == GLFW_PRESS) keys[MVT_DOWN] = true;
            else if (action == GLFW_RELEASE) keys[MVT_DOWN] = false;
            break;

        case GLFW_KEY_UP :
            if (action == GLFW_PRESS) keys[MVT_FWD_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_FWD_OBJ] = false;
            break;

        case GLFW_KEY_DOWN :
            if (action == GLFW_PRESS) keys[MVT_BACK_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_BACK_OBJ] = false;
            break;

        case GLFW_KEY_LEFT :
            if (action == GLFW_PRESS) keys[MVT_LEFT_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_LEFT_OBJ] = false;
            break;

        case GLFW_KEY_RIGHT :
            if (action == GLFW_PRESS) keys[MVT_RIGHT_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_RIGHT_OBJ] = false;
            break;

        case GLFW_KEY_PAGE_UP:
            if (action == GLFW_PRESS) keys[MVT_UP_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_UP_OBJ] = false;
            break;

        case GLFW_KEY_PAGE_DOWN :
            if (action == GLFW_PRESS) keys[MVT_DOWN_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_DOWN_OBJ] = false;
            break;

        case GLFW_KEY_COMMA:
            if (action == GLFW_PRESS) keys[MVT_RN_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_RN_OBJ] = false;
            break;

        case GLFW_KEY_PERIOD:
            if (action == GLFW_PRESS) keys[MVT_RP_OBJ] = true;
            else if (action == GLFW_RELEASE) keys[MVT_RP_OBJ] = false;
            break;

        case GLFW_KEY_C :
            if (action == GLFW_PRESS){

                moveCam = !moveCam;
                GameEngine::togleCursor();

            }
            break;

        case GLFW_KEY_M :
            if (action == GLFW_PRESS)
                GameEngine::setViewport("menu");
            break;

        case GLFW_KEY_E :
            if (action == GLFW_PRESS) {
                this->keys[MVT_THROW] = true;
            }
            else if (action == GLFW_RELEASE) {
                //this->keys[MVT_THROW] = false;
            }
            break;

        case GLFW_KEY_R :
            if (action == GLFW_PRESS) {
                this->keys[MVT_THROW_SPHERE] = true;
            }
            else if (action == GLFW_RELEASE) {
                //this->keys[MVT_THROW] = false;
            }
            break;

        case GLFW_KEY_Q :
            if (action == GLFW_PRESS) {
                this->camera->position.y = GameEngine::world->getTerrain()->getHeight(camera->position.x, camera->position.z) + 1.75;
                this->camera->updateViewMatrix();
                touchGround = !touchGround;
            }
            break;

        case GLFW_KEY_N :
            if (action == GLFW_PRESS)
                GameEngine::setViewport("standard");
            break;

        case GLFW_KEY_1 :
            if (action == GLFW_PRESS && !isBuilding) {
                this->currentObjectType = 0;
                std::cout << "Adding instance " << this->treeElement << std::endl;
                this->hasInstance = false;
                this->currentPos = Vector3(camera->position.x, camera->position.y, camera->position.z);
                this->currentRot = Vector3(0,0,0);
                this->currentScale = 1.0;
                //this->currentInstanceId = this->treeElement->addInstance(camera->position.x, TerrainGenerator::getHeight(camera->position.x, camera->position.z), camera->position.z);
                std::cout << "OK" << std::endl;
                isBuilding = true;
            }
            break;

        case GLFW_KEY_2:
            if (action == GLFW_PRESS && !isBuilding) {
                this->currentObjectType = 1;
                this->hasInstance = false;
                this->currentPos = Vector3(camera->position.x, camera->position.y, camera->position.z);
                this->currentRot = Vector3(0,0,0);
                this->currentScale = 1.0;
                //this->currentInstanceId = this->spruceElement->addInstance(camera->position.x, TerrainGenerator::getHeight(camera->position.x, camera->position.z), camera->position.z);
                isBuilding = true;
            }
            break;

        case GLFW_KEY_3:
            if (action == GLFW_PRESS && !isBuilding) {
                this->currentObjectType = 2;
                this->hasInstance = false;
                this->currentPos = Vector3(camera->position.x, camera->position.y, camera->position.z);
                this->currentRot = Vector3(0,0,0);
                this->currentScale = 1.0;
                //this->currentInstanceId = this->stoneElement->addInstance(camera->position.x, TerrainGenerator::getHeight(camera->position.x, camera->position.z), camera->position.z);
                isBuilding = true;
            }
            break;

        case GLFW_KEY_4:
            if (action == GLFW_PRESS && !isBuilding) {
                this->currentObjectType = 3;
                this->hasInstance = false;
                this->currentPos = Vector3(camera->position.x, camera->position.y, camera->position.z);
                this->currentRot = Vector3(0,0,0);
                this->currentScale = 1.0;
                //this->currentInstanceId = this->stoneElement->addInstance(camera->position.x, TerrainGenerator::getHeight(camera->position.x, camera->position.z), camera->position.z);
                isBuilding = true;
            }
            break;

        case GLFW_KEY_ENTER:
            if (action == GLFW_PRESS && isBuilding) {
                endBuilding();
            }
            break;

        case GLFW_KEY_J:
            if (action == GLFW_PRESS) {
                GameEngine::world->startPhysics();
            }
            break;

        case GLFW_KEY_K:
            if (action == GLFW_PRESS) {
                MeshTerrain * t = ((MeshTerrain*)GameEngine::world->getTerrain());
                std::cout << "(" << camera->position.x / t->getMeshScale() << ", " << camera->position.y << ", " << camera->position.z  / t->getMeshScale() << ")" << std::endl;
                std::vector<double> face = t->getFace(camera->position.x, camera->position.z);
                std::cout << "Face ok" << std::endl;
                for (int i = 0; i < face.size(); ++i) {
                    std::cout << face[i] << std::endl;
                }

            }
            break;

        case GLFW_KEY_Y:
            if (action == GLFW_PRESS) {
                if (lightValue < 0.5) {
                    lightValue = 1;
                }else {
                    lightValue = 0;
                }
            }
            break;


    }

}

void TestInputHandler::onMouseButton(int button, int action, int mods, double x, double y) {

    std::cout << "Click " << button << " at (" << x << ", " << y << ")" << std::endl;

    switch (button) {

        case GLFW_MOUSE_BUTTON_1:
            if (action == GLFW_PRESS) {
                this->keys[MVT_THROW] = true;
            }
            else if (action == GLFW_RELEASE) {
                //this->keys[MVT_THROW] = false;
            }
            break;

        case GLFW_MOUSE_BUTTON_2:
            if (action == GLFW_PRESS) {
                this->keys[MVT_THROW_SPHERE] = true;
            }
            else if (action == GLFW_RELEASE) {
                //this->keys[MVT_THROW] = false;
            }
            break;

    }

}
