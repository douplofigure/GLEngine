#include "multimodeinput.h"

MultiModeInput::MultiModeInput() {

    this->modes = std::vector<InputManager*>();
    this->currentMode = 0;

}

MultiModeInput::MultiModeInput(std::vector<InputManager*> modes, int current) {
    this->modes = modes;
    this->currentMode = current;
}

MultiModeInput::~MultiModeInput()
{
    //dtor
}

void MultiModeInput::addMode(InputManager * mode) {
    this->modes.push_back(mode);
}

void MultiModeInput::setMode(int mode) {
    this->currentMode = mode;
}

int MultiModeInput::getMode() {
    return currentMode;
}

void MultiModeInput::onMouseMotion(double dx, double dy) {
    this->modes[currentMode]->onMouseMotion(dx, dy);
}

void MultiModeInput::onMouseButton(int button, int action, int mods, double x, double y) {
    this->modes[currentMode]->onMouseButton(button, action, mods, x, y);
}

void MultiModeInput::onKeyboard(int key, int scancode, int action, int mods) {
    this->modes[currentMode]->onKeyboard(key, scancode, action, mods);
}

void MultiModeInput::onTick(double dTime) {
    this->modes[currentMode]->onTick(dTime);
}
