#include "uiinputmanager.h"

#include <iostream>

UIInputManager::UIInputManager() {

    this->elements = std::list<std::shared_ptr<UIInteractionElement>>();

}

UIInputManager::~UIInputManager()
{
    //dtor
}

void UIInputManager::addElement(std::shared_ptr<UIInteractionElement> elem) {
    this->elements.push_back(elem);
}

void UIInputManager::onMouseButton(int button, int action, int mods, double x, double y) {

    std::cout << "Checking ui inputs" << std::endl;

    for (std::list<std::shared_ptr<UIInteractionElement>>::iterator it = elements.begin(); it != elements.end(); ++it) {
        if ((*it)->mouseOver(x, y)) {
            (*it)->onClick(button);
        }
    }

}
