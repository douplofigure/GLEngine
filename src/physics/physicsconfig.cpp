#include "physicsconfig.h"

#include <configloader.h>
#include "gameengine.h"

PhysicsConfig::PhysicsConfig(std::string filename) {

    if (!fileExists(filename)) {
        std::cerr << "Could not find Physics configuration '" << filename << "'" << std::endl;
        exit(1);
    }

    CompoundNode * rootNode = ConfigLoader::loadFileTree(filename);

    if (rootNode->getChildNode("gravity"))
        this->gravity = rootNode->get<double>("gravity");
    else
        this->gravity = -9.81;

    if (rootNode->getChildNode("maxThreadCount")) {
        this->maxThreadCount = rootNode->get<int>("maxThreadCount");
    }
    else this->maxThreadCount = -1;

    this->filename = filename;

}

PhysicsConfig::~PhysicsConfig()
{
    //dtor
}


double PhysicsConfig::getGravity() {
    return gravity;
}

int PhysicsConfig::getMaxThreads() {

    if (maxThreadCount < 1) return GameEngine::getCPUCount() / 2;

    return maxThreadCount;

}

std::string PhysicsConfig::getFileName() {
    return filename;
}
