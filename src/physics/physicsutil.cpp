#include "physicsutil.h"

using namespace PhysicsUtil;
using namespace Math;

btCollisionShape * PhysicsUtil::createShapeFromMesh(Mesh * mesh) {

    btTriangleMesh * tMesh = new btTriangleMesh();

    std::vector<Vector3> tData = mesh->getTriangles();

    for (int i = 0; i < tData.size()/3; ++i) {
        tMesh->addTriangle(btVector3(tData[i*3].x,tData[i*3].y,tData[i*3].z),
                            btVector3(tData[i*3+1].x,tData[i*3+1].y,tData[i*3+1].z),
                            btVector3(tData[i*3+2].x,tData[i*3+2].y,tData[i*3+2].z),
                            true);

    }

    return new btBvhTriangleMeshShape(tMesh, true);

}

