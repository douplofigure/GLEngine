#include "collisionobject.h"

CollisionObject::CollisionObject()
{
    //ctor
}

CollisionObject::~CollisionObject()
{
    //dtor
}

double CollisionObject::getRadius() {
    return radius;
}

void CollisionObject::setRadius(double rad) {
    radius = rad;
}

Math::Vector3 CollisionObject::getPos() {
    return position;
}

void CollisionObject::setPos(Math::Vector3 pos){

    position = pos;

}

bool CollisionObject::collidesWith(CollisionObject * obj) {

    double distance = (this->position - obj->getPos()).length();
    return distance < (this->radius + obj->getRadius());

}
