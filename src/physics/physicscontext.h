#ifndef PHYSICSCONTEXT_H
#define PHYSICSCONTEXT_H

#include <vector>
#include <list>

#include <btBulletDynamicsCommon.h>

#ifndef _WIN32
    #include <thread>
#else
    #include <mingw.thread.h>
#endif

#include "collisionobject.h"
#include "physicsobject.h"
#include "world/terrain.h"

#define MAX_PHYSICS_THREADS 8

typedef struct physics_thread_config_t PHYSICS_THREAD_CONFIG;

class PhysicsContext
{
    public:
        PhysicsContext(double gravity, Terrain * terrain);
        PhysicsContext(double gravity, Terrain * terrain, int maxThreads);
        virtual ~PhysicsContext();

        //virtual Math::Vector3 getForces(Math::Vector3 position, double mass);


        virtual void simulateStep(double dt);

        void start();
        void stop();

        void clearObjects();


        void addObject(PhysicsObject * obj);
        void setTerrain(Terrain * t);

        btDiscreteDynamicsWorld * getWorld();

    protected:

        virtual void simulateObject(double dt, PhysicsObject * obj);
        virtual void simulateWorld(double dt);

    private:

        btDefaultCollisionConfiguration * collisionConfig;
        btCollisionDispatcher * collisionDispacher;
        btBroadphaseInterface * broadphaseInterface;
        btSequentialImpulseConstraintSolver * solver;

        btDiscreteDynamicsWorld * dynamicsWorld;

        btAlignedObjectArray<btCollisionShape *> collisionShapes;


        double gravity;

        Terrain * terrain;

        /// All objects that will never be removed nor added to the scene
        std::vector<CollisionObject*> staticEntities;
        /// All Objects capabale of beeing added or removed while  running
        std::list<CollisionObject*> dynamicEntites;

        std::vector<PhysicsObject*> objects;

        int threadCount;
        int maxThreadCount;
        bool isRunning;

        //pthread_t * threads;
        std::thread ** nThreads;
        double * updateTimes;
        PHYSICS_THREAD_CONFIG * threadConfigs;

        bool simulating;

        static void * threadStart(void * ctxt);


};

#endif // PHYSICSCONTEXT_H
