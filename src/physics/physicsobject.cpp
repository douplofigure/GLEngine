#include "physicsobject.h"

#define FOREVER 7.988465674e307

btCollisionShape * stdShape = new btSphereShape(1.0); //new btBoxShape(btVector3(1.0, 1.0, 1.0));

using namespace Math;

PhysicsObject::PhysicsObject(double mass) {

    this->mass = mass;
    this->position = Vector3(0,0,0);
    this->velocity = Vector3(0,0,0);
    this->omega = Vector3(0,0,0);

    this->forces = SmartArray<Vector3>(10);
    this->applicationPoints = SmartArray<Vector3>(10);
    this->forceTimeouts = SmartArray<double>(10);

    this->rotation = Quaternion(1, 0, 0 ,0);

    this->collisionShape = stdShape;

    this->angularFactor = 1.0;

}

PhysicsObject::PhysicsObject(double mass, Vector3 pos) {

    this->mass = mass;
    this->position = pos;
    this->velocity = Vector3(0,0,0);
    this->omega = Vector3(0,0,0);


    this->forces = SmartArray<Vector3>(10);
    this->applicationPoints = SmartArray<Vector3>(10);
    this->forceTimeouts = SmartArray<double>(10);

    this->rotation = Quaternion(1, 0, 0 ,0);

    this->collisionShape = stdShape;
    this->angularFactor = 1.0;

}

PhysicsObject::PhysicsObject(double mass, Vector3 pos, double vol) {

    this->mass = mass;
    this->position = pos;
    this->velocity = Vector3(0,0,0);
    this->omega = Vector3(0,0,0);


    this->forces = SmartArray<Vector3>(10);
    this->applicationPoints = SmartArray<Vector3>(10);
    this->forceTimeouts = SmartArray<double>(10);

    this->rotation = Quaternion(1, 0, 0 ,0);

    this->volume = vol;

    this->collisionShape = stdShape;
    this->angularFactor = 1.0;

}

PhysicsObject::~PhysicsObject()
{
    //dtor
}

double PhysicsObject::getMass() {
    return mass;
}

void PhysicsObject::setPitch(double p) {
    //this->rotation.x = p;
}

void PhysicsObject::setRoll(double r) {
    //this->rotation.y = r;
}

void PhysicsObject::setYaw(double y) {
    //this->rotation.z = y;
}

void PhysicsObject::setRotation(double yaw, double pitch, double roll) {
    //this->rotation = Vector3(pitch, roll, yaw);

}

void PhysicsObject::precomputeForces(double dTime) {

    globalForce = Vector3(0,0,0);
    globalMoment = Vector3(0,0,0);

    for (int i = 0; i < forces.size(); ++i) {
        if (forceTimeouts[i] < dTime) {


            globalForce += forces[i];
            globalMoment += Vector3::cross(applicationPoints[i], forces[i]);

            //std::cout << "removing force " << forces[i] << std::endl;

            forces.removeElement(i);
            applicationPoints.removeElement(i);
            forceTimeouts.removeElement(i);

        }
        forceTimeouts[i] -= dTime;
        globalForce += forces[i];
        globalMoment += Vector3::cross(applicationPoints[i], forces[i]);

    }

    //std::cout << "Object has " << globalMoment << " forces" << std::endl;

}

void PhysicsObject::addForce(Vector3 force, Vector3 point) {

    this->addForce(force, point, FOREVER);

}

void PhysicsObject::addForce(Vector3 force, Vector3 point, double timeout) {

    this->forces.addElement(force);
    this->applicationPoints.addElement(point);
    this->forceTimeouts.addElement(timeout);

}

Vector3 PhysicsObject::getSumOfForces() {
    return globalForce;
}

Vector3 PhysicsObject::getSumOfMoments() {
    return globalMoment;
}

double PhysicsObject::getYaw() {
    //return this->rotation.z;
}
double PhysicsObject::getPitch() {
    //return this->rotation.x;
}
double PhysicsObject::getRoll() {
    //return this->rotation.y;
}

void PhysicsObject::setAngle(double ang) {
    //this->angle = ang;
}

void PhysicsObject::addAngle(double omega) {
    this->angle += omega;
}

void PhysicsObject::setAxis(Vector3 axis) {
    //this->rotation = axis;
}

Vector3 PhysicsObject::getAxis() {
    //return rotation;
}

double PhysicsObject::getAngle() {
    return angle;
}

double PhysicsObject::getVolume() {
    return volume;
}

btCollisionShape * PhysicsObject::getCollisionShape() {

    return collisionShape;

}

void PhysicsObject::setCollisionShape(btCollisionShape * shape) {
    this->collisionShape = shape;
}

double PhysicsObject::getAngularFactor() {

    return angularFactor;

}

void PhysicsObject::setAngularFactor(double fac) {
    angularFactor = fac;
}

btRigidBody * PhysicsObject::getRigidBody() {
    return rigidBody;
}

Math::Quaternion PhysicsObject::getRotation() {
    return this->rotation;
}

void PhysicsObject::setRotation(Math::Quaternion q) {
    this->rotation = q;
}

void PhysicsObject::setRigidBody(btRigidBody * body) {
    rigidBody = body;
}

void PhysicsObject::setScale(double scale) {

    this->collisionShape->setLocalScaling(btVector3(scale, scale, scale));

}
