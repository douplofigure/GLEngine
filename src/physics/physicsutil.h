#ifndef PHYSICSUTIL_H
#define PHYSICSUTIL_H

#include <btBulletDynamicsCommon.h>
#include "util/math/mesh.h"

namespace PhysicsUtil {


btCollisionShape * createShapeFromMesh(Math::Mesh * mesh);


}

#endif // PHYSICSUTIL_H
