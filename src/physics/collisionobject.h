#ifndef COLLISIONOBJECT_H
#define COLLISIONOBJECT_H

#include "util/math/vector3.h"

class CollisionObject
{
    public:
        CollisionObject();
        virtual ~CollisionObject();

        virtual bool collidesWith(CollisionObject * obj);

        double getRadius();
        void setRadius(double radius);

        Math::Vector3 getPos();
        void setPos(Math::Vector3 position);

    protected:

        Math::Vector3 position;

        double radius;

    private:
};

#endif // COLLISIONOBJECT_H
