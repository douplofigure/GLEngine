#ifndef PHYSICSOBJECT_H
#define PHYSICSOBJECT_H

#include <vector>
#include <list>

#include <btBulletDynamicsCommon.h>

#include "collisionobject.h"
#include "util/math/vector3.h"
#include "util/math/quaternion.h"
#include "util/smartarray.h"

class PhysicsObject
{
    public:
        PhysicsObject(double mass);
        PhysicsObject(double mass, Math::Vector3 position);
        PhysicsObject(double mass, Math::Vector3 pos, double vol);
        virtual ~PhysicsObject();

        void setRotation(double yaw, double pitch, double roll);
        void setAngle(double angle);
        void addAngle(double omega);
        void setAxis(Math::Vector3 axis);
        void setPitch(double pitch);
        void setYaw(double yaw);
        void setRoll(double roll);

        void setCollisionShape(btCollisionShape * shape);

        Math::Vector3 getAxis();
        double getAngle();

        Math::Quaternion getRotation();
        void setRotation(Math::Quaternion rot);

        double getMass();
        double getVolume();

        double getPitch();
        double getRoll();
        double getYaw();

        double getAngularFactor();
        void setAngularFactor(double fac);

        void setScale(double scale);

        btRigidBody * getRigidBody();
        void setRigidBody(btRigidBody * body);

        void addForce(Math::Vector3 force, Math::Vector3 position);
        void addForce(Math::Vector3 force, Math::Vector3 position, double timeOut);

        void precomputeForces(double dTime);

        btCollisionShape * getCollisionShape();

        Math::Vector3 getSumOfForces();
        Math::Vector3 getSumOfMoments();

        Math::Vector3 position;
        Math::Vector3 velocity;
        Math::Vector3 omega;

    protected:

        double mass;
        double volume;
        double angularFactor;
        CollisionObject * collisionBox;

    private:

        Math::Quaternion rotation;

        btCollisionShape * collisionShape;
        btRigidBody * rigidBody;

        double angle;

        Math::Vector3 globalForce;
        Math::Vector3 globalMoment;

        SmartArray<Math::Vector3> forces;
        SmartArray<Math::Vector3> applicationPoints;
        SmartArray<double> forceTimeouts;



};

#endif // PHYSICSOBJECT_H
