#include "physicscontext.h"

#include "util/math/conversions.h"
#include "util/math/constants.h"
#include "gameengine.h"

using namespace Math;

struct physics_thread_config_t {

    int id;
    int padding;
    PhysicsContext * context;
    int lBound;
    int uBound;

};

PhysicsContext::PhysicsContext(double gravity, Terrain * terrain) : PhysicsContext(gravity, terrain, MAX_PHYSICS_THREADS)
{

}

PhysicsContext::PhysicsContext(double gravity, Terrain * terrain, int maxThreads)
{
    collisionConfig = new btDefaultCollisionConfiguration();
    collisionDispacher = new btCollisionDispatcher(collisionConfig);
    broadphaseInterface = new btDbvtBroadphase();
    solver = new btSequentialImpulseConstraintSolver();

    dynamicsWorld = new btDiscreteDynamicsWorld(collisionDispacher, broadphaseInterface, solver, collisionConfig);

    dynamicsWorld->setGravity(btVector3(0, gravity, 0));

    btCollisionShape * groundShape = new btBoxShape(btVector3(0.1, 0.1, 0.1)); //new btBoxShape(btVector3(50.0, 50.0, 50.0));

	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(0,-50,0));
	//btQuaternion quat;
	//quat.setRotation(btVector3(1, 0, 0), M_PI_4);
//	groundTransform.setRotation(quat);

	{
        double mass = 0;
        bool isDynamic = false;

        btVector3 localInertia(0,10,0);

        if (isDynamic)
            groundShape->calculateLocalInertia(mass, localInertia);

        btDefaultMotionState * motionState = new btDefaultMotionState(groundTransform);
        btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, groundShape, localInertia);

        btRigidBody * body = new btRigidBody(rbInfo);
        body->setFriction(1.0);
        body->setRestitution(0.1);

        dynamicsWorld->addRigidBody(body);


	}

	this->objects.push_back(nullptr);

    this->gravity = gravity;
    this->terrain = terrain;

    //this->threads = (pthread_t*) malloc(sizeof(pthread_t) * MAX_PHYSICS_THREADS);
    this->nThreads = (std::thread **) malloc(sizeof(std::thread*) * MAX_PHYSICS_THREADS);
    this->updateTimes = (double*) malloc(sizeof(double) * MAX_PHYSICS_THREADS);
    this->threadConfigs = (PHYSICS_THREAD_CONFIG *) malloc(sizeof(PHYSICS_THREAD_CONFIG) * MAX_PHYSICS_THREADS);

    this->threadCount = 0;

    for (int i = 0; i < maxThreads; ++i) {

        updateTimes[i] = 0.0;

    }

    this->maxThreadCount = maxThreads;

}

PhysicsContext::~PhysicsContext()
{

    delete this->dynamicsWorld;
    delete this->broadphaseInterface;
    delete this->collisionConfig;
    delete this->collisionDispacher;
    delete this->solver;
    delete this->terrain;
    free(this->updateTimes);

}

void PhysicsContext::simulateObject(double dt, PhysicsObject * obj) {

    Vector3 forces;
    Vector3 moments;

    glm::vec3 position = asRender(obj->position);

    double height = terrain->getHeight(position.x, position.z);

    double radius = 1.0;

    Vector3 normal = Vector3(0, 0, 1);
    Vector3 grad = Vector3(1, 0, 0);

    /// Collision with Ground
    if (height + radius > obj->position.z + obj->velocity.z * dt) {

        //std::cout << "GROUND COLLISION" << std::endl;
        obj->velocity.z *= 0.2;
        obj->position.z = height + radius;
        obj->omega = Vector3::cross(Vector3(0,0,-radius), obj->velocity);

        double dhX = (terrain->getHeight(position.x + radius, position.z) - height);
        double dhY = (terrain->getHeight(position.x, position.z + radius) - height);

        normal = Vector3::cross(Vector3(1, 0, dhX), Vector3(0, 1, dhY));
        grad = Vector3(1, 1,terrain->getHeight(position.x + dhX, position.z + dhY) - height);

        normal = normal * ((normal.z > 0 ? -1: 1)/normal.length());
        grad = grad * ((normal.z > 0 ? -1: 1)/ grad.length());

        obj->addForce(normal * gravity * obj->getMass(), Vector3(0,0,-radius), DOUBLE_0);
        obj->addForce(obj->velocity * gravity * obj->getMass() * 0.02 / obj->velocity.length(), Vector3(0, 0, -radius), DOUBLE_0);

        //return;

    }

    /// In Water, adding Archimedes
    if (obj->position.z < 0) {

        obj->addForce(Vector3(0, 0, -gravity * obj->getVolume() * 1000), Vector3(0,0,-1), DOUBLE_0);
        obj->addForce(obj->velocity * -obj->velocity.length() * 1000 * 0.05 * M_PI, Vector3(0,0,0), DOUBLE_0);

    }

    obj->precomputeForces(dt);

    forces = obj->getSumOfForces();
    moments = obj->getSumOfMoments();

    //std::cout << "forces: " << moments << std::endl;

    /// F = ma

    obj->position += obj->velocity * dt;
    obj->velocity += forces * dt / obj->getMass();

    /// L = iw

    if (obj->omega.length() > DOUBLE_0) {
        obj->setAxis(obj->omega * (1/obj->omega.length()));
        obj->addAngle(obj->omega.length() * dt);
    }

    double I = (7.0/5.0) * obj->getMass();
    Vector3 omegaDot = moments * (1 / I);

    obj->omega += omegaDot * dt;

    double yaw = obj->getYaw();
    double pitch = obj->getPitch();
    double roll = obj->getRoll();

    //std::cout << "omega: " << omegaDot << std::endl;

    //obj->setRotation(yaw + omega.z * dt, pitch + omega.x * dt, roll + omega.y * dt);

}

#define MAX_UPDATE_TIME 0.032

void PhysicsContext::simulateWorld(double dt) {

    simulating = true;
    dynamicsWorld->stepSimulation(dt, 1);
    simulating = false;

}

void PhysicsContext::simulateStep(double dt) {

    #ifdef _WIN32
    dynamicsWorld->stepSimulation(dt, 10);
    #endif

    while(simulating);

    for (int i = 1; i < this->objects.size(); ++i) {

        btCollisionObject * obj = dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody * body = btRigidBody::upcast(obj);
        if (body && body->getMotionState()) {
            btTransform trans;
            //body->getMotionState()->getWorldTransform(trans);
            trans = body->getCenterOfMassTransform();
            btMatrix3x3 mat = trans.getBasis();

            btQuaternion quat;
            mat.getRotation(quat);

            objects[i]->position = Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());
            objects[i]->setRotation(Quaternion(trans.getRotation().getW(), trans.getRotation().getX(), trans.getRotation().getY(), trans.getRotation().getZ()));

        }

    }

    int collisionCount = dynamicsWorld->getDispatcher()->getNumManifolds();
    for (int i = 0; i < collisionCount; ++i) {

        btPersistentManifold * manifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
        const btCollisionObject * objA = manifold->getBody0();
        const btCollisionObject * objB = manifold->getBody1();

        Entity::processCollision((btCollisionObject *)objA, (btCollisionObject *)objB);

    }

}

void PhysicsContext::start() {

    this->isRunning = true;
    this->threadConfigs[0].lBound = 0;
    this->threadConfigs[0].uBound = this->objects.size();
    this->threadConfigs[0].id = 0;
    this->threadConfigs[0].context = this;
    std::cout << "Starting physics simulation" << std::endl;
    this->nThreads[0] = new std::thread(threadStart, threadConfigs+threadCount);
    this->nThreads[0]->detach();
    std::cout << "Thread created" << std::endl;
    //pthread_create(this->threads, NULL, this->threadStart, (void*)this->threadConfigs);
    this->threadCount = 1;

}

void PhysicsContext::stop() {
    this->isRunning = false;
}

void PhysicsContext::addObject(PhysicsObject * obj) {

    btCollisionShape * collisionShape = obj->getCollisionShape();
    std::cout << "Collision Shape ok" << std::endl;
    this->collisionShapes.push_back(collisionShape);

    btTransform startTransform;
    startTransform.setIdentity();

    bool isDynamic = (obj->getMass() != 0.0);

    std::cout << "object is " << (isDynamic ? "" : "not ") << "dynamic " << std::endl;

    btVector3 localInertia(0,0,0);
    if (isDynamic) collisionShape->calculateLocalInertia(obj->getMass(), localInertia);

    Math::Quaternion rotation = obj->getRotation();
    startTransform.setRotation(btQuaternion(rotation.b, rotation.c, rotation.d, rotation.a));
    startTransform.setOrigin(btVector3(obj->position.x,obj->position.y,obj->position.z));

    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(obj->getMass(),myMotionState,collisionShape,localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	body->setLinearVelocity(btVector3(obj->velocity.x, obj->velocity.y, obj->velocity.z));
	body->setAngularVelocity(btVector3(obj->omega.x, obj->omega.y, obj->omega.z));
	body->setFriction(1.0);

	body->setRestitution(0.1);

	body->setAngularFactor(obj->getAngularFactor());

	body->activate(true);
	//body->setLinearFactor(btVector3(1,1,1));

	std::cout << "Body configured " << dynamicsWorld << std::endl;

    dynamicsWorld->addRigidBody(body);
    obj->setRigidBody(body);

    std::cout << "Body added " << std::endl;


    this->objects.push_back(obj);
    /*obj->addForce(Vector3(0, 0, gravity * obj->getMass()), Vector3(0,0,0));
    for (int i = 0; i < threadCount; ++i) {
        this->threadConfigs[i].uBound++;
        this->threadConfigs[i].lBound++;
    }
    this->threadConfigs[0].lBound = 0;
    this->threadConfigs[threadCount-1].uBound = objects.size();*/

}

void PhysicsContext::clearObjects() {

    while(simulating);

    for (int i = 0; i < objects.size(); ++i)
        if (objects[i])
            dynamicsWorld->removeRigidBody(objects[i]->getRigidBody());

}

void PhysicsContext::setTerrain(Terrain * t) {
    this->terrain = t;
}

void * PhysicsContext::threadStart(void * ctxt) {

    PHYSICS_THREAD_CONFIG * conf = (PHYSICS_THREAD_CONFIG *) ctxt;
    PhysicsContext * context = conf->context;
    double time = GameEngine::getTime();
    double nTime = time;
    double dt = 0.016;

    int tickNum = 0;

    while (context->isRunning) {
        /*for (int i = conf->lBound; i < conf->uBound; ++i) {
            //context->simulateObject(dt, context->objects[i]);
        }*/

        #ifndef _WIN32
        context->simulateWorld(dt);
        #endif // _WIN32

        //std::cout << "Simulation tick " << tickNum++ << std::endl;

        nTime = GameEngine::getTime();
        dt = nTime - time;
        time = nTime;

        //std::this_thread::sleep_for(std::chrono::microseconds((int)(16000 - 1000000*dt)));

        //context->updateTimes[conf->id] = dt;

    }

    return nullptr;

}

btDiscreteDynamicsWorld * PhysicsContext::getWorld() {
    return dynamicsWorld;
}
