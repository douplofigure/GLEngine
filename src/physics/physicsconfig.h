#ifndef PHYSICSCONFIG_H
#define PHYSICSCONFIG_H

#include <string>

class PhysicsConfig
{
    public:
        /** Default constructor */
        PhysicsConfig(std::string filename);
        /** Default destructor */
        virtual ~PhysicsConfig();

        double getGravity();
        int getMaxThreads();

        std::string getFileName();

    protected:

    private:

        double gravity;

        bool hasThreadCount;
        int  maxThreadCount;

        std::string filename;

};

#endif // PHYSICSCONFIG_H
