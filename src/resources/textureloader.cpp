#include "textureloader.h"

#include <assert.h>
#include <stdio.h>

#include <tga.h>



struct PPM_PIXEL {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

void saveImagePPM(float * data, int width, int height, std::string fname) {

    TGA_PIXEL * pixels = (TGA_PIXEL *) data;

    FILE * file = fopen(fname.c_str(), "w");

    fprintf(file, "P6\n%d %d\n%d\n", width, height, 255);

    #ifdef _WIN32
    fseek(file, -1, SEEK_CUR);
    #endif

    for (int i = 0; i < width * height; ++i) {

        PPM_PIXEL pix;
        pix.r = (uint8_t) (255.0 * pixels[i].r);
        pix.g = (uint8_t) (255.0 * pixels[i].g);
        pix.b = (uint8_t) (255.0 * pixels[i].b);

        fwrite(&pix, sizeof(uint8_t) * 3, 1, file);

    }

    fclose(file);

}

ResourceInjector<Texture> * TGALoader::create(std::string dircetory, std::string name) {

    int width, height;
    std::string fname = dircetory;
    fname.append(name).append(".tga");

    float * data = getImageAsFloatArray(fname.c_str(), &width, &height);

    //saveImagePPM(data, width, height, "loaded_images/" + name + ".ppm");

	std::cout << "Images has " << width * height * 4 << " Floats\n";

	std::vector<float> vecData(data, data + width * height * 4);
    std::cout << "Vector Created" << std::endl;
    //Texture * tex = Texture::createTexture2D(vecData, width, height, 4);

    TextureInjector * injector = new TextureInjector(vecData, width, height, 4);

    std::cout << "Done loading " << fname << std::endl;
    free(data);
    return injector;

}

bool TGALoader::canCreate(std::string dircetory, std::string name) {

    std::string fname = dircetory;
    fname.append(name).append(".tga");

    std::cout << "Checking for " << fname << "\n";

    return fileExists(fname);

}

bool CubeMapLoader::canCreate(std::string dir, std::string name) {

    std::string frontName = dir;
    frontName.append(name).append("_ft.tga");

    std::string backName = dir;
    backName.append(name).append("_bk.tga");

    std::string downName = dir;
    downName.append(name).append("_dn.tga");

    std::string upName = dir;
    upName.append(name).append("_up.tga");

    std::string leftName = dir;
    leftName.append(name).append("_lf.tga");

    std::string rightName = dir;
    rightName.append(name).append("_rt.tga");

    return fileExists(frontName) && fileExists(backName) && fileExists(upName) && fileExists(downName) && fileExists(leftName) && fileExists(rightName);

}

ResourceInjector<CubeMap> * CubeMapLoader::create(std::string dir, std::string name) {

    int width, height;

    std::string frontName = dir;
    frontName.append(name).append("_ft.tga");

    std::string backName = dir;
    backName.append(name).append("_bk.tga");

    std::string downName = dir;
    downName.append(name).append("_dn.tga");

    std::string upName = dir;
    upName.append(name).append("_up.tga");

    std::string leftName = dir;
    leftName.append(name).append("_lf.tga");

    std::string rightName = dir;
    rightName.append(name).append("_rt.tga");

    float * data = getImageAsFloatArray(frontName.c_str(), &width, &height);
    std::vector<float> frontData(data, data + width * height * 4);

    data = getImageAsFloatArray(backName.c_str(), &width, &height);
    std::vector<float> backData(data, data + width * height * 4);

    data = getImageAsFloatArray(leftName.c_str(), &width, &height);
    std::vector<float> leftData(data, data + width * height * 4);

    data = getImageAsFloatArray(rightName.c_str(), &width, &height);
    std::vector<float> rightData(data, data + width * height * 4);

    data = getImageAsFloatArray(upName.c_str(), &width, &height);
    std::vector<float> upData(data, data + width * height * 4);

    data = getImageAsFloatArray(downName.c_str(), &width, &height);
    std::vector<float> downData(data, data + width * height * 4);

    CubeMapInjector * injector = new CubeMapInjector(width, height);
    injector->addFaceData(CubeMap::CUBEMAP_FRONT, frontData);
    injector->addFaceData(CubeMap::CUBEMAP_BACK, backData);
    injector->addFaceData(CubeMap::CUBEMAP_LEFT, leftData);
    injector->addFaceData(CubeMap::CUBEMAP_RIGHT, rightData);
    injector->addFaceData(CubeMap::CUBEMAP_TOP, upData);
    injector->addFaceData(CubeMap::CUBEMAP_BOTTOM, downData);

    return injector;

}

std::vector<std::string> CubeMapLoader::getFilenames(std::string dir, std::string name) {

    std::vector<std::string> files(6);

    std::string frontName = dir;
    frontName.append(name).append("_ft.tga");
    files[0] = frontName;

    std::string backName = dir;
    backName.append(name).append("_bk.tga");
    files[1] = backName;

    std::string downName = dir;
    downName.append(name).append("_dn.tga");
    files[2] = downName;

    std::string upName = dir;
    upName.append(name).append("_up.tga");
    files[3] = upName;

    std::string leftName = dir;
    leftName.append(name).append("_lf.tga");
    files[4] = leftName;

    std::string rightName = dir;
    rightName.append(name).append("_rt.tga");
    files[5] = rightName;

    return files;

}

CubeMapLoader * CubeMapLoader::buildLoader() {
    return new CubeMapLoader();
}

TGA_PIXEL * readTgaRGB(FILE * file, TGA_HEADER * head, int pixelSize) {

	int i, j;
	uint8_t * rawData = (uint8_t *) malloc(head->width * head->height * pixelSize * sizeof(uint8_t));
	assert(rawData);
	TGA_PIXEL * pixels = (TGA_PIXEL *) malloc(head->width * head->height * sizeof(TGA_PIXEL));
	assert(pixels);
	int colorDepth = 1;

	//fseek(file, 2, SEEK_CUR);
	fread(rawData, pixelSize, head->width * head->height,file);
	printf("Starting to load %d pixels\n", head->width * head->height);

	int imageSize = head->width * head->height;

	if (pixelSize > 3) {
		for (i = 0; i < head->width * head->height; ++i) {

			pixels[i].b = (float) rawData[i*pixelSize] / 255.0;
			pixels[i].g = (float) rawData[i*pixelSize + 1] / 255.0;
			pixels[i].r = (float) rawData[i*pixelSize + 2] / 255.0;
			pixels[i].a = (float) rawData[i*pixelSize + 3] / 255.0;

		}
	}
	else {

		for (i = 0; i < head->width * head->height;	 ++i) {

			pixels[i].b = (float) rawData[i*pixelSize] / 255.0;
			pixels[i].g = (float) rawData[i*pixelSize + 1] / 255.0;
			pixels[i].r = (float) rawData[i*pixelSize + 2] / 255.0;
			pixels[i].a = 1;

		}
	}
	printf("loading Done\n");
	free(rawData);
	return pixels;

}

float * getImageAsFloatArray(const char * fname, int * width, int * height) {


    TGA_FILE * imgFile = tgaOpen(fname);

    tgaGetSize(imgFile, width, height);

    std::cout << "Getting image Data " << *width << "x" << *height << std::endl;
    uint8_t * imgData = tgaGetColorDataRGBA(imgFile);
    float * data = (float*) malloc(sizeof(float) * (*width) * (*height) * 4);

    std::cout << "Converting to float" << std::endl;
    for (int i = 0; i < *width * *height * 4; ++i) {

        data[i] = (float) imgData[i] / 255.0;

    }

    tgaClose(imgFile);
    free(imgData);

    std::cout << "Returning image Data" << std::endl;
    return data;

    /*
	FILE * file = fopen(fname, "r");
	if (!file) fprintf(stderr, "Could not open %s\n", fname);
	TGA_HEADER fileHeader;
	int pixelSizeBytes;
	TGA_PIXEL * pixels;

	fseek(file, 0 , SEEK_SET);
	fread(&fileHeader.idLength, sizeof(uint8_t), 1, file);
	fread(&fileHeader.colorType, sizeof(uint8_t), 1, file);
	fread(&fileHeader.dataTypeCode, sizeof(uint8_t), 1, file);

	fread(&fileHeader.colorMapOrigin, sizeof(int16_t), 1, file);
	fread(&fileHeader.colorMapLength, sizeof(int16_t), 1, file);
	fread(&fileHeader.colorMapDepth, sizeof(uint8_t), 1, file);

	fread(&fileHeader.xOrigin, sizeof(int16_t), 1, file);
	fread(&fileHeader.yOrigin, sizeof(int16_t), 1, file);
	fread(&fileHeader.width, sizeof(int16_t), 1, file);
	fread(&fileHeader.height, sizeof(int16_t), 1, file);

	fread(&fileHeader.bitsPerPixel, sizeof(uint8_t), 1, file);
	fread(&fileHeader.imageDescriptor, sizeof(uint8_t), 1, file);

	pixelSizeBytes = fileHeader.bitsPerPixel/8;
	printf("Image file header read\n");
	if (pixelSizeBytes == 2) {
		fprintf(stderr, "Please use higher color-depth images as textures!\n");
		return 0;
	}

	switch (fileHeader.dataTypeCode) {

		case TGA_RGB:
			pixels = readTgaRGB(file, &fileHeader, pixelSizeBytes);
			break;

		default:
			fprintf(stderr, "The used image format is not implemented, chose another one\nplease\n");
			break;

	}

	assert(file);
	fclose(file);

	*width = fileHeader.width;
	*height = fileHeader.height;

	return (float*) pixels;
	*/

}

std::vector<std::string> TGALoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".tga");

    std::vector<std::string> files(1);
    files[0] = fname;
    return files;

}

TGALoader * TGALoader::buildLoader() {
    return new TGALoader();
}
