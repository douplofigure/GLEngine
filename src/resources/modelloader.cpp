#include "modelloader.h"

#include <map>
#include <configloader.h>
#include <math.h>
#include <iostream>
#include <vector>

#include <ply.hpp>

#include "render/model.h"
#include "resourcemanager.h"

std::string ModelLoader::modelDir = "resources/models/";
std::map<std::string, Model*> ModelLoader::modelsByName = std::map<std::string, Model*>();

typedef struct ply_vertex_t {

    float x;
    float y;
    float z;
    float nx;
    float ny;
    float nz;
    float u;
    float v;
    float r;
    float g;
    float b;

} PLY_VERTEX;

/*typedef struct raw_vertex_t {

    VECTOR3F position;
    VECTOR2F uv;
    VECTOR3F normal;

} RAW_VERTEX;

typedef struct ply_vertex_t {

    VECTOR3F position;
    VECTOR3F normal;
    VECTOR2F uv;
    VECTOR3F data;

} PLY_VERTEX;

typedef struct model_face_t {

    int v1;
    int v2;
    int v3;

} FACE;

std::vector<RAW_VERTEX> demangleLoadedData(RAW_VERTEX * rawData, int * indecies, int indexCount) {

    int i;
    std::vector<RAW_VERTEX> data;

    for (i = 0; i < indexCount; ++i) {

        data.push_back(rawData[i]);

    }

    return data;

}

VERTEX getVertexByIndex(std::vector<VERTEX> verts, int * indecies, int index) {

    int i;
    VERTEX out;
    bool isFirst = true;
    for (i = 0; i < verts.size(); ++i) {

        if (indecies[i] == index) return verts[i];


    }
    return out;

}


VERTEX * createTangentData(RAW_VERTEX * rawData, int * vertexCount, int * indecies, int * indexCount) {

    unsigned int i;
    std::vector<RAW_VERTEX> vertexData = demangleLoadedData(rawData, indecies, *indexCount);

    std::vector<VECTOR3D> tangents;
    std::vector<VECTOR3D> bitangents;

    std::vector<VERTEX> verts;

    VERTEX tmp;

    printf("Creating tangent data\n");

    for (i = 0; i < vertexData.size(); ++i) {

        VECTOR3D v0 = vertexData[i+0].position;
        VECTOR3D v1 = vertexData[i+1].position;
        VECTOR3D v2 = vertexData[i+2].position;

        // Shortcuts for UVs
        VECTOR2D uv0 = vertexData[i+0].uv;
        VECTOR2D uv1 = vertexData[i+1].uv;
        VECTOR2D uv2 = vertexData[i+2].uv;

        // Edges of the triangle : postion delta
        VECTOR3D deltaPos1 = v1-v0;
        VECTOR3D deltaPos2 = v2-v0;

        // UV delta
        VECTOR2D deltaUV1 = uv1-uv0;
        VECTOR2D deltaUV2 = uv2-uv0;

        float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);

        VECTOR3D tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
        VECTOR3D bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;

        printf("Accessing vector\n");

        tmp.position = vertexData[i].position;
        tmp.uv = vertexData[i].uv;
        tmp.normal = vertexData[i].normal;
        tmp.tangent = tangent;
        verts.push_back(tmp);
        tmp.position = vertexData[i+1].position;
        tmp.uv = vertexData[i+1].uv;
        tmp.normal = vertexData[i+1].normal;
        tmp.tangent = tangent;
        verts.push_back(tmp);
        tmp.position = vertexData[i+2].position;
        tmp.uv = vertexData[i+2].uv;
        tmp.normal = vertexData[i+2].normal;
        tmp.tangent = tangent;
        verts.push_back(tmp);


    }

    printf("Repacking data\n");

    VERTEX * outData = (VERTEX *) malloc(*vertexCount * sizeof(VERTEX));
    for (i = 0; i < *vertexCount; ++i) {

        printf("Vertex %d / %d\n", i, *vertexCount);
        outData[i] = getVertexByIndex(verts, indecies, i);

    }

    return outData;

}*/

Model * ModelLoader::loadModelPLY(std::string modelName) {

    float * rawVerts;
    int * indecies;
    int indexCount;
    int vertexCount;
    std::string fname = "resources/models";

    if (modelsByName[modelName] != NULL) {
        std::cout << "Model '" << modelName << "' is already loaded\n";
        std::cout << (void*)modelsByName[modelName] << "\n";
        return modelsByName[modelName];
    }

    fname.append(modelName).append(".ply");

    printf("Loading PLY Model %s\n", fname.c_str());

    PlyFile * plyFile = new PlyFile(fname);

    printf("retrieving data\n");
    rawVerts = plyFile->getVertexData(&vertexCount);
    printf("Model contains %d verts\n", vertexCount);
    indecies = plyFile->getIndexData(&indexCount);
    printf("Model contains %d indecies\n", indexCount);
    double len, maxRadius = 0;

    std::vector<float> modelData(rawVerts, rawVerts + vertexCount * 11);
    std::vector<int> indexData(indecies, indecies + indexCount);

    std::cout << "Creating dynamic model from data\n";

    const int offsetArray[4] = {0, 3, 6, 8};
    std::vector<int> offsets(offsetArray, offsetArray + 4);

    Model * model = createModel(modelData, vertexCount, indexData);
    //modelsByName[modelName] = model;

    free(rawVerts);
    free(indecies);

    return model;


}

ResourceInjector<Model> * ModelLoader::create(std::string dir, std::string modelName) {

    float * rawVerts;
    int * indecies;
    int indexCount;
    int vertexCount;
    std::string fname = dir;

    fname.append(modelName).append(".ply");

    PlyFile * plyFile = new PlyFile(fname);
    rawVerts = plyFile->getVertexData(&vertexCount);
    indecies = plyFile->getIndexData(&indexCount);
    double len, maxRadius = 0;

    std::vector<float> modelData(rawVerts, rawVerts + vertexCount * 11);
    std::vector<int> indexData(indecies, indecies + indexCount);

    const int offsetArray[4] = {0, 3, 6, 8};
    std::vector<int> offsets(offsetArray, offsetArray + 4);

    //Model * model = createModel(modelData, vertexCount, indexData);

    Math::Mesh * mesh = new Math::Mesh(modelData, vertexCount, indexData);


    //mesh->computeTangents();
    //if (vertexCount > 200) mesh->buildTree();

    return new ModelInjector(modelData, vertexCount, indexData, mesh, 0);


}

bool ModelLoader::canCreate(std::string dir, std::string modelName) {

    std::string fname = dir;

    fname.append(modelName).append(".ply");

    return fileExists(fname);

}

/*Model * ModelLoader::loadModelPLY(std::string modelName, bool isStatic) {

    PLY_VERTEX * rawVerts;
    int * indecies;
    int indexCount;
    int vertexCount;
    std::string fname = modelDir;

    if (modelsByName[modelName] != NULL) {
        std::cout << "Model '" << modelName << "' is already loaded\n";
        std::cout << (void*)modelsByName[modelName] << "\n";
        return modelsByName[modelName];
    }

    fname.append(modelName).append(".ply");

    printf("Loading PLY Model %s\n", fname.c_str());

    PlyFile * plyFile = new PlyFile(fname);

    printf("retrieving data\n");
    rawVerts = (PLY_VERTEX*) plyFile->getVertexData(&vertexCount);
    printf("Model contains %d verts\n", vertexCount);
    indecies = plyFile->getIndexData(&indexCount);
    printf("Model contains %d indecies\n", indexCount);
    double len, maxRadius = 0;

    VERTEX * verts = (VERTEX *) malloc(sizeof(VERTEX) * vertexCount);

    for (int i = 0; i < vertexCount; ++i) {

        verts[i].position = rawVerts[i].position;
        verts[i].uv = rawVerts[i].uv;
        verts[i].normal = rawVerts[i].normal;
        verts[i].tangent = rawVerts[i].data;

        len = sqrt(rawVerts[i].position.x * rawVerts[i].position.x + rawVerts[i].position.y * rawVerts[i].position.y + rawVerts[i].position.z * rawVerts[i].position.z);

        if (len > maxRadius) maxRadius = len;

    }

    Model * model;

    if (isStatic) {
        printf("Creating static model\n");
        model = new StaticModel(verts, vertexCount, indecies, indexCount, true, maxRadius);
        //exit(11);
        return model;
    }

    std::cout << "Creating dynamic model from data\n";
    model = new Model(verts, vertexCount, indecies, indexCount, true, maxRadius);
    modelsByName[modelName] = model;

    return model;

}

Model * ModelLoader::loadModel(std::string modelName) {

    int * indecies;
    std::string fname = modelDir;

    if (modelsByName[modelName] != NULL) {
        std::cout << (void*)modelsByName[modelName] << "\n";
        return modelsByName[modelName];
    }

    fname.append(modelName).append(".model");
    printf("Loading Model %s\n", fname.c_str());
	//indexCount = readObjFile(fname.c_str(), (float**)&verts, (float**)&uvs, (float**)&normals, &indecies, &vertexCount);

    ConfigNode * data = ConfigLoader::loadFile(fname);

    RAW_VERTEX * rawVerts;
    //printf("Loading Verts %p\n", data["verts"]);
    int vertexCount = ((ConfigValue*)data->getChildNode("verts"))->getValue((float**)&rawVerts)/8;
    printf("Model has %d verts:\n", vertexCount);
    int indexCount  = ((ConfigValue*)data->getChildNode("indecies"))->getValue(&indecies);
    double maxRadius = 0;
    double len;
    VERTEX * verts = (VERTEX*) malloc(sizeof(VERTEX) * vertexCount);//createTangentData(rawVerts, &vertexCount, indecies, &indexCount);
    for (int i = 0; i < vertexCount; ++i) {

        verts[i].position = rawVerts[i].position;
        verts[i].uv = rawVerts[i].uv;
        verts[i].normal = rawVerts[i].normal;
        verts[i].tangent = (VECTOR3F) {rawVerts[i].normal.z,rawVerts[i].normal.y, -rawVerts[i].normal.x};
        len = sqrt(verts[i].position.x * verts[i].position.x + verts[i].position.y * verts[i].position.y + verts[i].position.z * verts[i].position.z);

        if (len > maxRadius) maxRadius = len;
    }

    return new Model(verts, vertexCount, indecies, indexCount, true, maxRadius);


	//new ShaderModel(antModel, new Shader("resources/shaders/vertexShader.glsl", "resources/shaders/fragmentShader.glsl"))
}

Model * ModelLoader::loadModel(std::string modelName, bool isStatic) {

    int * indecies;
    std::string fname = modelDir;

    if (modelsByName[modelName] != NULL) {
        std::cout << (void*)modelsByName[modelName] << "\n";
        return modelsByName[modelName];
    }

    fname.append(modelName).append(".model");
    printf("Loading Model %s\n", fname.c_str());
	//indexCount = readObjFile(fname.c_str(), (float**)&verts, (float**)&uvs, (float**)&normals, &indecies, &vertexCount);

    ConfigNode * data = ConfigLoader::loadFile(fname);

    RAW_VERTEX * rawVerts;
    //printf("Loading Verts %p\n", data["verts"]);
    int vertexCount = ((ConfigValue*)data->getChildNode("verts"))->getValue((float**)&rawVerts)/8;
    printf("Model has %d verts:\n", vertexCount);
    int indexCount  = ((ConfigValue*)data->getChildNode("indecies"))->getValue(&indecies);
    double maxRadius = 0;
    double len;
    VERTEX * verts = (VERTEX*) malloc(sizeof(VERTEX) * vertexCount);//createTangentData(rawVerts, &vertexCount, indecies, &indexCount);
    for (int i = 0; i < vertexCount; ++i) {

        verts[i].position = rawVerts[i].position;
        verts[i].uv = rawVerts[i].uv;
        verts[i].normal = rawVerts[i].normal;
        verts[i].tangent = (VECTOR3F) {rawVerts[i].normal.z,rawVerts[i].normal.y, -rawVerts[i].normal.x};
        len = sqrt(verts[i].position.x * verts[i].position.x + verts[i].position.y * verts[i].position.y + verts[i].position.z * verts[i].position.z);

        if (len > maxRadius) maxRadius = len;
    }

    if (isStatic) return new StaticModel(verts, vertexCount, indecies, indexCount, true, maxRadius);
    return new Model(verts, vertexCount, indecies, indexCount, true, maxRadius);


	//new ShaderModel(antModel, new Shader("resources/shaders/vertexShader.glsl", "resources/shaders/fragmentShader.glsl"))
}
*/

std::vector<std::string> ModelLoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;

    fname.append(name).append(".ply");

    std::vector<std::string> files(1);
    files[0] = fname;
    return files;

}

ModelLoader * ModelLoader::buildLoader() {
    return new ModelLoader();
}
