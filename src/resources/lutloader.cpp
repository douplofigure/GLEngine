#include "lutloader.h"

#include <tga.h>
#include "util/math/spline.h"

using namespace Math;

LUTLoader::LUTLoader()
{
    //ctor
}

LUTLoader::~LUTLoader()
{
    //dtor
}

float * getLutAsFloatArray(const char * fname, int * width, int * height, int * depth) {

    TGA_FILE * imgFile = tgaOpen(fname);

    tgaGetSize(imgFile, width, height);

    *depth = 0;

    std::cout << "Getting image Data " << *width << "x" << *height << std::endl;
    uint8_t * imgData = tgaGetColorDataRGBA(imgFile);
    float * data = (float*) malloc(sizeof(float) * (*width) * (*height) * 4);

    std::cout << "Converting to float" << std::endl;
    for (int i = 0; i < *width * *height * 4; ++i) {

        data[i] = (float) imgData[i] / 255.0;

    }

    tgaClose(imgFile);
    free(imgData);

    std::cout << "Returning image Data" << std::endl;
    return data;

}

std::vector<int> getSamplePoints(char * line) {

    std::vector<int> samples;
    while(*line && *line != '\n') {

        int tmp;
        if (!sscanf(line, "%d", &tmp)) break;
        samples.push_back(tmp);
        while((*line != ' ' && *line != '\t')) line++;
        while((*line == ' ' || *line == '\t')) line++;

    }

    return samples;

}

std::vector<float> createImage3DfromLut(FILE * file, std::vector<int> samples) {

    int s = samples.size();
    double maxSample = samples[s - 1] * 4;

    std::vector<float> imgData(s * s * s * 4);
    int index = 0;


    for (int z = 0; z < s; ++z) {

        for (int y = 0; y < s; ++y) {

            for (int x = 0; x < s; ++x) {

                double r;
                double g;
                double b;

                int pos = (x * s * s + y * s + z)*4;

                fscanf(file, "%lf %lf %lf\n", &r, &g, &b);
                std::cout << "got Value " << (r/maxSample) << " " << (g/maxSample) << " " << (b/maxSample) << std::endl;
                imgData[pos] = r / maxSample;
                imgData[pos + 1] = g / maxSample;
                imgData[pos + 2] = b / maxSample;
                imgData[pos + 3] = 1;


            }

        }

    }

    return imgData;

}

ResourceInjector<LUT> * LUTLoader::create(std::string dir, std::string name) {

    int width, height, depth;
    //Texture * tex = Texture::createTexture2D(vecData, width, height, 4);

    std::string fname2 = dir;
    fname2.append(name).append(".3dl");

    char tmp[1024];
    FILE * file =  fopen(fname2.c_str(), "r");
    fgets(tmp, 1024, file);
    while(*tmp == '#' && fgets(tmp, 1024, file));

    std::vector<int> samplePoints = getSamplePoints(tmp);
    int deltaV = samplePoints[1] - samplePoints[0];
    for (int i = 2; i < samplePoints.size(); ++i) {
        std::cout << "sample[" << i << "] = " << samplePoints[i] << std::endl;
        int tmpDelta = samplePoints[i] - samplePoints[i-1];
        if (tmpDelta != deltaV) {


            /// TODO: add midway samples to create uniform sampling in output

            if (tmpDelta - deltaV == 1) {
                samplePoints[i]++;
                continue;
            } else if (tmpDelta - deltaV == -1) {
                samplePoints[i]--;
                continue;
            }

            std::cerr << "Error in 3d lut: sample spacing not uniform." << std::endl;
            std::cerr << (samplePoints[i] - samplePoints[i-1]) << " != " << deltaV << std::endl;
            exit(1);
        }

    }


    std::vector<float> generatedImg = createImage3DfromLut(file, samplePoints);


    LUTInjector * injector = new LUTInjector(generatedImg, samplePoints.size(), samplePoints.size(), samplePoints.size(), 4);

    return injector;

}

bool LUTLoader::canCreate(std::string dircetory, std::string name) {

    std::string fname = dircetory;
    fname.append(name).append(".3dl");

    std::cout << "Checking for " << fname << "\n";

    return fileExists(fname);

}

std::vector<std::string> LUTLoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".3dl");

    std::vector<std::string> files(1);
    files[0] = fname;
    return files;

}


LUTLoader * LUTLoader::buildLoader() {
    return new LUTLoader();
}
