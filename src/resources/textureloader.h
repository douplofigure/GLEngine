#ifndef TEXTURELOADER_H
#define TEXTURELOADER_H
#include "render/texture.h"
#include "render/cubemap.h"
#include "resourceregistry.h"
#include "render/lut.h"


float * getImageAsFloatArray(const char * fname, int * width, int * height);
void saveImagePPM(float * data, int width, int height, std::string fname);

typedef enum tga_data_types_e {

	TGA_NO_IMAGE,
	TGA_COLOR_MAPPED,
	TGA_RGB,
	TGA_BW,
	TGA_RUNLENGTH_COLLOR_MAPPED = 9,
	TGA_RUNLENGTH_RGB,
	TGA_COMPRESSED_BW,
	TGA_COMPRESSED_COLLOR_MAPPED = 32,
	TGA_COMPRESSED_COLLOR_MAPPED_4_PASS_QUAD_TREE_PROCESS,

} TGA_DATA_TYPE;

typedef struct tga_header {

	uint8_t idLength;
	uint8_t colorType;
	uint8_t dataTypeCode;
	int16_t colorMapOrigin;
	int16_t colorMapLength;
	uint8_t colorMapDepth;
	int16_t xOrigin;
	int16_t yOrigin;
	int16_t width;
	int16_t height;
	uint8_t bitsPerPixel;
	uint8_t imageDescriptor;

} TGA_HEADER;

typedef struct tga_pixel {

	float r;
	float g;
	float b;
	float a;

} TGA_PIXEL;

class TGALoader : public ResourceLoader<Texture> {

    public:

        ResourceInjector<Texture> * create(std::string dir, std::string name) override;
        bool canCreate(std::string dir, std::string name) override;
        std::vector<std::string> getFilenames(std::string dir, std::string name) override;

        static TGALoader * buildLoader();

};

class CubeMapLoader : public ResourceLoader<CubeMap> {

    public:

        ResourceInjector<CubeMap> * create(std::string dir, std::string name) override;
        bool canCreate(std::string dir, std::string name) override;
        std::vector<std::string> getFilenames(std::string dir, std::string name);

        static CubeMapLoader * buildLoader();

};

#endif // TEXTURELOADER_H
