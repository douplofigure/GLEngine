#ifndef RESOURCELOADER_H_INCLUDED
#define RESOURCELOADER_H_INCLUDED

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>

#include <assert.h>

#include "gpuresource.h"

class Resource {

    public:
        Resource(){

        };

        virtual void deleteData() = 0;

};

template<typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> class ResourceLoader {

    public:
        ResourceLoader(){}
        virtual ~ResourceLoader(){}

        virtual ResourceInjector<T> * create(std::string dir, std::string name) {return nullptr;};
        virtual bool canCreate(std::string dir, std::string name){return false;};
        virtual std::vector<std::string> getFilenames(std::string dir, std::string name) {return std::vector<std::string>(0);};

};

#endif // RESOURCELOADER_H_INCLUDED
