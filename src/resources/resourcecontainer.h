#ifndef RESOURCECONTAINER_H
#define RESOURCECONTAINER_H

#include <string>
#include <queue>

#include <configloader.h>

#include "resourceregistry.h"
#include "render/texture.h"
#include "render/model.h"
#include "render/shader.h"
#include "font/font.h"

#include "util/typeutil.h"

class ResourceContainer
{
    public:
        ResourceContainer(std::string dir);
        virtual ~ResourceContainer();

        std::string getDirectory();

        void saveLoadedToFile(std::string fname);

        std::vector<std::string> getLoadedFiles();

        bool hasPending();
        void uploadPending();

        Resource * get(std::string type, std::string name);

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> void addRegistry(ResourceRegistry<T> * registry) {
            std::string id = getTypeName<T>();
            this->registries[id] = (ResourceRegistry<Resource> *) registry;
        }

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> T * get(std::string name) {
            T * data = (T*) this->registries[getTypeName<T>()]->get(name);
            if (!data) {

                std::string msg = "Unable to load ";
                msg.append(getTypeName<T>());
                msg.append(" '").append(name).append("'");

                throw std::runtime_error(msg);
            }
            return data;
        }

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> void * load(std::string name) {
            while (uploading);
            std::cout << "Loading new resource " << name << std::endl;
            ResourceRegistry<T> * reg = ((ResourceRegistry<T>*) this->registries[getTypeName<T>()]);
            ResourceInjector<T> * injector = reg->load(name);
            injector->setName(name);
            injector->setType(getTypeName<T>());
            uploadList.push((ResourceInjector<void*>*)injector);
            stageList.push(nullptr);
            std::cout << "Returning the injector" << std::endl;
            return injector;
        }

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> void * load(std::string dir, std::string name) {
            while (uploading);
            ResourceRegistry<T> * reg = ((ResourceRegistry<T>*) this->registries[getTypeName<T>()]);
            ResourceInjector<T> * injector = reg->load(dir, name);
            injector->setName(name);
            injector->setType(getTypeName<T>());
            uploadList.push((ResourceInjector<void*>*)injector);
            stageList.push(nullptr);
            return injector;
        }

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> void * load(std::string name, LoadingStage * stage) {
            while (uploading);
            ResourceInjector<T> * injector = ((ResourceRegistry<T>*) this->registries[getTypeName<T>()])->load(name);
            injector->setName(name);
            injector->setType(getTypeName<T>());
            uploadList.push((ResourceInjector<void*>*)injector);
            stageList.push(stage);
            return injector;
        }

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> void addResource(ResourceInjector<T> * res) {

            while (uploading);
            uploadList.push((ResourceInjector<void*>*) res);
            stageList.push(nullptr);

        }

        template <typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> std::string getResourceLocation() {

            return registries[getTypeName<T>()]->getDirectory();

        }

    protected:

        std::string dirName;
        CompoundNode * rootNode;

        ResourceRegistry<Model> * modelReg;
        ResourceRegistry<Shader> * shaderReg;
        ResourceRegistry<Texture> * textureReg;
        ResourceRegistry<Font> * fontReg;

        std::map<std::string, ResourceRegistry<Resource> *> registries;

    private:

        std::queue<ResourceInjector<void*>*> uploadList;
        std::queue<LoadingStage *> stageList;
        bool uploading;

};

#endif // RESOURCECONTAINER_H
