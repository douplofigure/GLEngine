#ifndef GPURESOURCE_H_INCLUDED
#define GPURESOURCE_H_INCLUDED

#include "util/typeutil.h"

class GPUResource {

    public:
        GPUResource(){

        };

        virtual ~GPUResource(){

        };

        virtual void upload() {};

};

class LoadingStage {

    public:
        LoadingStage(){
            this->reached = false;
        };

        virtual ~LoadingStage(){};
        void trigger(){
            this->reached = true;
        };

        bool isReached() {
            return this->reached;
        };

    private:

        bool reached;


};

template <typename T> class ResourceInjector {

    public:
        ResourceInjector() {
            uploaded = nullptr;
        };

        virtual ~ResourceInjector() {

        };

        virtual T * upload() {return nullptr;};
        virtual void deleteTempData(){};

        std::string getType() {
            return this->type;
        };

        T * uploadInternal() {
            if (!this->uploaded) {
                this->uploaded = upload();
            }
            return this->uploaded;
        };

        void setType(std::string type) {
            this->type = type;
        };

        std::string getName() {
            return this->name;
        };

        void setName(std::string name) {
            this->name = name;
        };

    protected:

        std::string type;

    private:
        std::string name;
        T * uploaded;

};

#endif // GPURESOURCE_H_INCLUDED
