#ifndef LUTLOADER_H
#define LUTLOADER_H

#include "resourceloader.h"
#include "render/lut.h"

class LUTLoader : public ResourceLoader<LUT> {

    public:

        LUTLoader();
        virtual ~LUTLoader();

        ResourceInjector<LUT> * create(std::string dir, std::string name) override;
        bool canCreate(std::string dir, std::string name) override;
        std::vector<std::string> getFilenames(std::string dir, std::string name);

        static LUTLoader * buildLoader();

};

#endif // LUTLOADER_H
