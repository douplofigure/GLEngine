#ifndef RESOURCEREGISTRY_H
#define RESOURCEREGISTRY_H

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>

#include <assert.h>

#include "util/fileutils.h"
#include "gpuresource.h"
#include "resourceloader.h"

template<typename T, typename std::enable_if<std::is_base_of<Resource, T>::value>::type* = nullptr> class ResourceRegistry
{
    public:
        ResourceRegistry(ResourceLoader<T> * load){

            this->injectors = std::unordered_map<std::string, ResourceInjector<T> *>();
            loaders.push_back(load);
            resources = std::unordered_map<std::string, T*>();
        }
        ResourceRegistry(ResourceLoader<T> * load, std::string dir){
            loaders.push_back(load);
            resources = std::unordered_map<std::string, T*>();
            directory = dir;
            this->injectors = std::unordered_map<std::string, ResourceInjector<T> *>();
        }

        ResourceRegistry(std::vector<ResourceLoader<T> *> loads, std::string dir){
            loaders = loads;
            resources = std::unordered_map<std::string, T*>();
            directory = dir;
            this->injectors = std::unordered_map<std::string, ResourceInjector<T> *>();
        }

        ~ResourceRegistry(){

            for (auto it = resources.begin(); it != resources.end(); ++it) {
                std::cout << "deleting " << getTypeName<T>() << " " << it->first << " at " << it->second << std::endl;
                it->second->deleteData();
                resources[it->first] = nullptr;
                std::cout << "done deleting resource " << it->first << std::endl;
            }

        }

        T * get(std::string name) {

            if (!this->resources[name]) {

                /*for (int i = 0; i < loaders.size(); ++i) {
                    std::cout << "Checking loader " << i << " for Resouce " << name << "\n";
                    if (loaders[i]->canCreate(directory, name)) {
                        std::cout << "Loader " << i << " can load resource\n";
                        this->resources[name] = this->loaders[i]->create(directory, name);
                        std::cout << "Done Loading Resource '" << name << "'" << std::endl;
                        return this->resources[name];
                    }
                    std::cerr << "Could no load resource '" << name << "' in directory '" << directory << "'\n";
                }
                std::cerr << "Could no load resource '" << name << "' in directory '" << directory << "'\n";
                //exit(1);*/

                std::cerr << "No such " << getTypeName<T>() << " loaded '" << name << "'" << std::endl;
                return nullptr;

            }

            return this->resources[name];

        }

        ResourceInjector<T> * load(std::string name) {

            std::cout << "Loading " << name << std::endl;

            if (this->injectors[name]) return this->injectors[name];

            for (int i = 0; i < loaders.size(); ++i) {
                std::cout << "Checking loader " << i << std::endl;
                if (loaders[i]->canCreate(directory, name)) {
                    this->injectors[name] = this->loaders[i]->create(directory, name);
                    std::vector<std::string> filenames = this->loaders[i]->getFilenames(directory, name);
                    this->loadedFiles.insert(loadedFiles.end(), filenames.begin(), filenames.end());
                    return this->injectors[name];
                }
            }
            std::cerr << "Unable to load " << getTypeName<T>() << "/" << name << std::endl;
            exit(1);

        }

        ResourceInjector<T> * load(std::string dir, std::string name) {

            std::cout << "Loading " << name << std::endl;

            if (this->injectors[name]) return this->injectors[name];

            for (int i = 0; i < loaders.size(); ++i) {
                std::cout << "Checking loader " << i << std::endl;
                if (loaders[i]->canCreate(dir, name)) {
                    this->injectors[name] = this->loaders[i]->create(dir, name);
                    std::vector<std::string> filenames = this->loaders[i]->getFilenames(dir, name);
                    this->loadedFiles.insert(loadedFiles.end(), filenames.begin(), filenames.end());
                    return this->injectors[name];
                }
            }
            std::cerr << "Unable to load " << getTypeName<T>() << "/" << name << std::endl;
            exit(1);

        }

        void addResource(std::string name, T * res) {
            this->resources[name] = res;
            /*for (int i = 0; i < loaders.size(); ++i) {

                if (loaders[i]->canCreate(this->directory, name)) {
                    std::vector<std::string> filenames = this->loaders[i]->getFilenames(directory, name);
                    this->loadedFiles.insert(loadedFiles.end(), filenames.start(), filenames.end());
                    return;
                }

            }*/
        }

        void deleteResource(std::string name) {

            T * res = this->resources[name];

            this->resources.erase(name);
            res.deleteData();
            //delete res;

        }

        void setResourceDir(std::string dir) {
            this->directory = dir;
        }

        void addLoader(ResourceLoader<T> * loader) {

            this->loaders.push_back(loader);

        }

        void printLoaded(FILE * file) {

            for (typename std::unordered_map<std::string, T *>::iterator it = resources.begin(); it != resources.end(); ++it) {

                fprintf(file, "%s%s\n", this->directory.c_str(), it->first.c_str());

            }

        }

        std::vector<std::string> getLoadedFiles() {

            return loadedFiles;

        }

        std::string getDirectory() {
            return directory;
        }

    protected:

        std::vector<ResourceLoader<T> *> loaders;

    private:

        std::string directory;
        std::unordered_map<std::string, T*> resources;
        std::unordered_map<std::string, ResourceInjector<T>*> injectors;
        std::vector<std::string> loadedFiles;

};

#endif // RESOURCEREGISTRY_H
