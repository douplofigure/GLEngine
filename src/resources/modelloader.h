#ifndef MODELLOADER_H
#define MODELLOADER_H

#include <string>
#include <map>
#include "resourceregistry.h"
#include "render/model.h"

class ModelLoader : public ResourceLoader<Model> {


    public:
        ModelLoader(){};
        virtual ~ModelLoader(){};


        //static Model * loadModel(std::string modelName);
        static Model * loadModelPLY(std::string modelName);
        //static Model * loadModel(std::string modelName, bool isStatic);
        //static Model * loadModelPLY(std::string modelName, bool isStatic);
        static std::string modelDir;

        ResourceInjector<Model> * create(std::string dir, std::string name) override;
        bool canCreate(std::string dir, std::string name) override;
        std::vector<std::string> getFilenames(std::string dir, std::string name) override;

        static ModelLoader * buildLoader();

    private:
        static std::map<std::string, Model *> modelsByName;


};

#endif // MODELLOADER_H
