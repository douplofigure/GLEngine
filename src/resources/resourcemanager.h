#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <map>
#include <string>
#include <queue>

#include "render/shader.h"
#include "render/model.h"
#include "render/texture.h"
#include "font/font.h"
#include "gpuresource.h"

///TODO Replace with new System for managing resources 'ResourceRegistry' and 'ResourceLoader'
///     Only to be used as a wrapper class.
///
///     It will be used as a means to store all ResourceRegistry and ResourceLoader instances
///     when I know how to use it using 'namespaces' for different Resources. It wil only have
///     a single static method for getting to all resources.

class ResourceManager
{
    public:

        static void uploadResource();
        //static void addResource(ResourceInjector<void*> * injector);
        static bool isLoadingDone();


    private:

        //static std::queue<ResourceInjector<void*> *> resources;

};

#endif // RESOURCEMANAGER_H
