#include "resourcecontainer.h"

#ifndef _WIN32
#include <dlfcn.h>
#define OS_LIB_SUFFIX ".so"
#else
#include <windows.h>
#define OS_LIB_SUFFIX ".dll"
#endif
#include <iostream>

#include "modelloader.h"
#include "textureloader.h"
#include "render/material.h"
#include "structure/structureloader.h"
#include "plugin/plugin.h"
#include "audio/sound.h"
#include "lutloader.h"
#include "structure/daeloader.h"
#include "animation/animationloader.h"
#include "structure/binstructureloader.h"

std::map<std::string, ResourceLoader<Resource> * (*)()> builders;

ResourceLoader<Resource> * buildLoaderFromCompound(std::string dir, CompoundNode * config, std::string type) {

    bool isCustom = config->get<bool>("isCustom");

    if (!isCustom) {
        std::cout << "getting default loader for "<< std::endl;
        std::cout << type<< std::endl;

        if (config->getChildNode("name")) {
            std::string name = std::string(config->get<const char *>("name"));
            std::cout << "Getting builder for " << name << " and not " << type << std::endl;
            //exit(0);
            return builders[name]();
        }

        std::cout << builders[type] << std::endl;

        return builders[type]();
    }

    throw std::runtime_error("Custom Plugin system does no longer work this way...");

    std::string name(config->get<const char *>("file"));

    /*Plugin * plugin = new Plugin(name);
    plugin->load();

    ResourceLoader<Resource> * (*builder)() = (ResourceLoader<Resource> * (*)()) plugin->getSymbol("buildLoader");

    return builder();*/

}


std::vector<ResourceLoader<Resource> *> buildLoadersFromArray(std::string dir, std::vector<CompoundNode *> config, std::string type) {

    std::vector<ResourceLoader<Resource> *> loaders;

    std::cout << "Loading Loader Array" << std::endl;
    for (int i = 0; i < config.size(); ++i) {
        CompoundNode * node = config[i];
        loaders.push_back(buildLoaderFromCompound(dir, node, type));
    }

    return loaders;

}

void preloadDefaults() {

    std::cout << "Starting preload" << std::endl;
    builders["Model"] = (ResourceLoader<Resource> * (*)()) ModelLoader::buildLoader;
    std::cout << "Model OK" << std::endl;
    builders["Texture"] = (ResourceLoader<Resource> * (*)()) TGALoader::buildLoader;
    std::cout << "Texture OK" << std::endl;
    builders["Shader"] = (ResourceLoader<Resource> * (*)()) ShaderLoader::buildLoader;
    std::cout << "Shader OK" << std::endl;
    builders["Sound"] = (ResourceLoader<Resource> * (*)()) SoundLoader::buildLoader;
    std::cout << "Sound OK" << std::endl;
    builders["Material"] = (ResourceLoader<Resource> * (*)()) MaterialLoader::buildLoader;
    builders["Structure"] = (ResourceLoader<Resource> * (*)()) StructureLoader::buildLoader;
    builders["CubeMap"] = (ResourceLoader<Resource> * (*)()) CubeMapLoader::buildLoader;
    builders["LUT"] = (ResourceLoader<Resource> * (*)()) LUTLoader::buildLoader;
    builders["DAE"] = (ResourceLoader<Resource> * (*)()) DAELoader::buildLoader;
    builders["AnimationRig"] = (ResourceLoader<Resource> * (*)()) AnimationLoader::buildLoader;
    builders["BinStructure"] = (ResourceLoader<Resource> * (*)()) BinStructureLoader::buildLoader;

}

ResourceContainer::ResourceContainer(std::string dir)
{
    dirName = dir;

    this->registries = std::map<std::string, ResourceRegistry<Resource> *>();

    uploading = false;
    this->uploadList = std::queue<ResourceInjector<void*>*>();
    this->stageList = std::queue<LoadingStage *>();

    std::cout << "preloading Defaults" << std::endl;
    preloadDefaults();

    std::cout << "OK" << std::endl;
    std::string confFName = dir;
    confFName.append("resources.conf");

    //baseNode = ConfigLoader::loadFile(confFName);
    std::cout << "Loading file Tree" << std::endl;
    rootNode = ConfigLoader::loadFileTree(confFName);

    std::cout << "Config loaded\n";

    std::string textureDir = dir;

    std::cout << rootNode->get<const char *>("textureDir") << "\n";

    textureDir.append(std::string(rootNode->get<const char *>("textureDir")));
    if (textureDir.back() != '/') textureDir.append("/");

    std::string modelDir = dir;
    modelDir.append(std::string(rootNode->get<const char *>("modelDir")));
    if (modelDir.back() != '/') modelDir.append("/");

    std::string shaderDir = dir;
    shaderDir.append(std::string(rootNode->get<const char *>("shaderDir")));
    if (shaderDir.back() != '/') shaderDir.append("/");

    std::string fontDir = dir;
    fontDir.append(std::string(rootNode->get<const char *>("fontDir")));
    if (fontDir.back() != '/') fontDir.append("/");

    CompoundNode * shaderConfig = rootNode->getCompound("shaderConfig");

    std::string vertexSuffix = std::string(shaderConfig->get<const char *>("vertexSuffix"));
    std::string fragmentSuffix = std::string(shaderConfig->get<const char *>("fragmentSuffix"));

    ShaderLoader * sdrLoader = new ShaderLoader();
    sdrLoader->setSuffixes(vertexSuffix, fragmentSuffix);
    this->addRegistry<Shader>(new ResourceRegistry<Shader>(sdrLoader, shaderDir));
    this->addRegistry<Font>(new ResourceRegistry<Font>(new FontLoader(), fontDir));

    std::vector<CompoundNode*> regList = rootNode->getArray<CompoundNode*>("Registries")->getValue();

    std::cout << "=======================================================\n";

    for  (int i = 0; i < regList.size(); ++i) {

        std::cout << "Creating new Registry\n";

        std::string loaderName = std::string(regList[i]->get<const char*>("configName"));
        std::string typeName = std::string(regList[i]->get<const char*>("typename"));
        std::string tmpDir = std::string(regList[i]->get<const char *>("dirName"));
        std::vector<ResourceLoader<Resource> *> loaders;
        if(regList[i]->getChildNode("Loader")->isArray()) {

            std::cout << "Found Loader Array\n";

            loaders = buildLoadersFromArray(dir, regList[i]->getArray<CompoundNode *>("Loader")->getValue(), typeName);
        }
        else {

            std::cout << "Building Loader from compound" << std::endl;

            loaders.push_back(buildLoaderFromCompound(dir, regList[i]->getCompound("Loader"), typeName));

        }

        this->registries[typeName] = new ResourceRegistry<Resource>(loaders, dir + tmpDir);

    }

}


ResourceContainer::~ResourceContainer() {

    for (auto it = this->registries.begin(); it != this->registries.end(); ++it) {

        delete it->second;
        registries[it->first] = nullptr;

    }

}

std::string ResourceContainer::getDirectory() {

    return dirName;

}

void ResourceContainer::uploadPending() {
    this->uploading = true;
    while (!this->uploadList.empty()) {
        ResourceInjector<void *> * injector = this->uploadList.front();
        this->uploadList.pop();

        std::cout << "Uploading " << injector->getName() << " to " << injector->getType() << " Using " << this->registries[injector->getType()] << std::endl;
        Resource * dat = (Resource *) injector->uploadInternal();
        std::cout << "Result " << dat << std::endl;
        this->registries[injector->getType()]->addResource(injector->getName(), dat);
        std::cout << "Uploaded" << std::endl;
        LoadingStage * stage = this->stageList.front();
        this->stageList.pop();
        std::cout << "Trying to trigger stage " << stage << std::endl;
        if (stage) {
            stage->trigger();
        }
        std::cout << "Deleting temp data" << std::endl;
        injector->deleteTempData();

    }
    this->uploading = false;
    //std::cout << "Uploading done" << std::endl;
}

bool ResourceContainer::hasPending() {
    return !this->uploadList.empty();
}

void ResourceContainer::saveLoadedToFile(std::string fname) {

    FILE * file = fopen(fname.c_str(), "w");

    std::vector<std::string> loadedFiles = this->getLoadedFiles();

    for (int i = 0; i < loadedFiles.size(); ++i) {

        fprintf(file, "%s\n", loadedFiles[i].c_str());

    }

    fclose(file);

}

std::vector<std::string> ResourceContainer::getLoadedFiles() {

    std::vector<std::string> files;

    files.push_back("resources/shaders/vertex_structure.glsl");
    files.push_back("resources/shaders/vertex_structure_static.glsl");
    files.push_back("resources/resources.conf");
    files.push_back("resources/shaders/deffered_pbr.glsl");
    files.push_back("resources/terrain/terrain_gen.conf");

    files.push_back("physics.conf");
    files.push_back("viewport.conf");

    files.push_back("resources/levels/test.lvl");

    files.push_back("resources/textures/luts/neutral.3dl");

    for (auto it = registries.begin(); it != registries.end(); ++it) {

        std::vector<std::string> regFiles = it->second->getLoadedFiles();

        files.insert(files.end(), regFiles.begin(), regFiles.end());

    }

    return files;

}

Resource * ResourceContainer::get(std::string type, std::string name) {

    std::cout << "Getting resource " << name << " of type " << type << std::endl;
    return this->registries[type]->get(name);

}
