#ifndef PLUGIN_H
#define PLUGIN_H

#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

class PluginLoader;

class Plugin {

    public:
        Plugin(std::string name);
        virtual ~Plugin();

        virtual void preInit() = 0;
        virtual void postInit() = 0;

        std::string getName();

        static std::shared_ptr<Plugin> getByName(std::string name);
        static std::shared_ptr<Plugin> loadFromPath(std::string path);

        static void addLoader(std::shared_ptr<PluginLoader> loader);
        static void loadAll();

        static void onPreInit();
        static void onPostInit();

    protected:

        std::string name;

        static void registerPlugin(std::string name, std::shared_ptr<Plugin> plugin);

    private:

        static std::unordered_map<std::string, std::shared_ptr<Plugin>> pluginsByName;
        static std::vector<std::shared_ptr<PluginLoader>> loaders;


};

class PluginLoader {

    public:
        virtual std::shared_ptr<Plugin> load(std::string path) = 0;
        virtual bool canLoad(std::string path) = 0;

};

#endif // PLUGIN_H
