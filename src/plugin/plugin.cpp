#include "plugin.h"

#include <assert.h>
#include <iostream>
#include <algorithm>

std::unordered_map<std::string, std::shared_ptr<Plugin>> Plugin::pluginsByName;
std::vector<std::shared_ptr<PluginLoader>> Plugin::loaders;

Plugin::Plugin(std::string name) {

    this->name = name;

}

Plugin::~Plugin() {

}

std::string Plugin::getName() {
    return name;
}

void Plugin::registerPlugin(std::string name, std::shared_ptr<Plugin> plugin) {
    pluginsByName[name] = plugin;
}

void Plugin::addLoader(std::shared_ptr<PluginLoader> loader) {
    loaders.push_back(loader);
}

std::shared_ptr<Plugin> Plugin::getByName(std::string name) {

    if (pluginsByName.find(name) != pluginsByName.end()) {
        return pluginsByName[name];
    }

    return nullptr;

}

std::shared_ptr<Plugin> Plugin::loadFromPath(std::string path) {

    std::shared_ptr<Plugin> plugin = nullptr;
    bool foundLoader = false;

    for (std::shared_ptr<PluginLoader> & l: loaders) {

        if (l->canLoad(path)) {
            plugin = l->load(path);
            foundLoader = true;
            registerPlugin(plugin->getName(), plugin);
            break;
        }

    }

    if (!foundLoader)
        throw std::runtime_error(std::string("Unable to load plugin '").append(path).append("', no loader specidfied"));

    return plugin;

}

void Plugin::loadAll() {

    FILE * file = fopen("plugins.txt", "r");
    if (!file) {
        throw std::runtime_error("Unable to open plugins.txt");
    }
    char line[256];

    while (fgets(line, 256, file)) {

        std::string str(line);
        str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
        str.erase(std::remove(str.begin(), str.end(), '\r'), str.end());
        if (str.length() > 0 && str.c_str()[0] != '#')
            loadFromPath(str);

    }
}

void Plugin::onPreInit() {

    for (auto it = pluginsByName.begin(); it != pluginsByName.end(); ++it) {
        it->second->preInit();
    }

}

void Plugin::onPostInit() {

    for (auto it = pluginsByName.begin(); it != pluginsByName.end(); ++it) {
        it->second->postInit();
    }
}
