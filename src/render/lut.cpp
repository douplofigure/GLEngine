#include "lut.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "util/math/vector4.h"

using namespace Math;

LUT::LUT(unsigned int id, int width, int height) : Texture(id, width, height, GL_TEXTURE_2D) {

}

LUT::LUT(unsigned int id, int width, int height, int depth) : Texture(id, width, height, GL_TEXTURE_3D) {



}

LUT::~LUT()
{
    //dtor
}

void LUT::bindToUnit(int unit) {

    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(this->type, this->id);

}

LUT * LUT::createTexture2D(std::vector<float> values, int width, int height, int chanelCount) {

    unsigned int textureId;

    glGenTextures(1, &textureId);
    std::cout << "Texture has Id " << textureId << std::endl;
	glBindTexture(GL_TEXTURE_2D, textureId);
	std::cout << "Texture is Bound" << std::endl;
	glTexImage2D(GL_TEXTURE_2D, 0, (chanelCount > 3 ? GL_RGBA : GL_RGB), width, height, 0, chanelCount > 3 ? GL_RGBA : GL_RGB, GL_FLOAT, values.data());
	std::cout << "Data on GPU" << std::endl;
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	std::cout << "Params set" << std::endl;

	return new LUT(textureId, width, height);

}

float getValueFromTexture(std::vector<float> image, int width, int height, int chanelCount, int x, int y, int ch) {

    int pos = (y * width + x) * chanelCount + ch;
    return image[pos];

}

void getColorFrom2DAs3D(std::vector<float> image, int width, int height, int chanelCount, int x, int y, int z, int cellsPerLine, float * loc) {

    int XOffset = (z % cellsPerLine) * (width / cellsPerLine);
    int YOffset = ((z * height) / cellsPerLine) % cellsPerLine;


    x += XOffset;
    y += YOffset;

    int pos = (x * height + y) * chanelCount;

    if (pos+3 >= image.size()){

        std::cerr << "Looking outside of buffer " << x << " " << y << " " << z << " width:" << width << " height: " << height << std::endl;
        exit(-1);

    }

    loc[0] = image[pos];
    loc[1] = image[pos+1];
    loc[2] = image[pos+2];
    loc[3] = image[pos+3];

}

std::vector<float> LUTInjector::convert2DTo3D(std::vector<float> image, int width, int height, int chanelCount, int cellsPerLine) {


    std::vector<float> data(image.size());

    float tmpdata[4];

    int index = 0;

    for (int z = 0; z < cellsPerLine * cellsPerLine; ++z) {

        for (int x = 0; x < width / cellsPerLine; ++x) {

            for (int y = 0; y < height / cellsPerLine; ++y) {

                getColorFrom2DAs3D(image, width, height, chanelCount, x, y, z, cellsPerLine, tmpdata);

                data[index++] = tmpdata[0];
                data[index++] = tmpdata[1];
                data[index++] = tmpdata[2];
                data[index++] = tmpdata[3];

            }

        }

    }

    return data;

}

LUT * LUT::createTexture3D(std::vector<float> values, int width, int height, int depth, int chanelCount) {

    unsigned int textureId;

    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_3D, textureId);
    glTexImage3D(GL_TEXTURE_3D, 0, (chanelCount > 3 ? GL_RGBA : GL_RGB), width, height, depth, 0, chanelCount > 3 ? GL_RGBA : GL_RGB, GL_FLOAT, values.data());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    return new LUT(textureId, width, height, depth);

}

LUTInjector::LUTInjector(std::vector<float> values, int width, int height, int chanelCount) {
    this->values = values;//convert2DTo3D(values, width*8, height*8, chanelCount, 8);
    this->width = width;
    this->height = height;
    this->depth = 64;
    this->channelCount = chanelCount;
}

LUTInjector::LUTInjector(std::vector<float> values, int width, int height, int depth, int chanelCount) {
    this->values = values;
    this->width = width;
    this->height = height;
    this->depth = depth;
    this->channelCount = chanelCount;
}

LUTInjector::~LUTInjector() {

}

LUT * LUTInjector::upload() {

    LUT * tex = LUT::createTexture3D(values, width, height, depth, channelCount);
    return tex;

}

void LUTInjector::deleteTempData() {

    std::vector<float>().swap(values);

}
