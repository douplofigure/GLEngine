#include "shader.h"

#include <iostream>

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include <glm/gtc/type_ptr.hpp>
#include <configloader.h>

#include "resources/resourcemanager.h"
#include "gameengine.h"

std::string shaderDir = "resources/shaders/";

Shader::Shader(unsigned int programId, unsigned int vertexShaderId, unsigned int fragmentShaderId) {

    this->vertexShaderId = vertexShaderId;
    this->fragmentShaderId = fragmentShaderId;

    this->geometryShaderId = 0;

    this->programId = programId;

    this->hasGeometryShader = false;
    this->textureSlots = std::vector<TEXTURE_SLOT>(0);

}

Shader::Shader(unsigned int progamId, unsigned int vertexShaderId, unsigned int fragmentShaderId, std::unordered_map<std::string, std::string> uniformNameMap) :
    Shader(programId, vertexShaderId, fragmentShaderId)
{
    this->uniformNameMap = uniformNameMap;
}

Shader::~Shader() {

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);
    if (hasGeometryShader) glDeleteShader(geometryShaderId);

    glDeleteProgram(programId);

}

void Shader::bindForRender() {

    glUseProgram(this->programId);

}

void Shader::unbindForRender() {

    glUseProgram(0);

}

void Shader::loadMatrix(unsigned int location, glm::mat4 matrix) {

    glUniformMatrix4fv(location, 1, false, glm::value_ptr(matrix));

}

void Shader::loadVec3(unsigned int location, glm::vec3 vec) {

    glUniform3f(location, vec.x, vec.y, vec.z);

}

void Shader::loadInt(unsigned int location, int i) {

    glUniform1i(location, i);

}

void Shader::loadFloat(unsigned int location, float f) {

    glUniform1f(location, f);

}

unsigned int Shader::getUniformLocation(std::string name) {

    return glGetUniformLocation(programId, name.c_str());

}

unsigned int Shader::getProgramId() {
    return programId;
}

unsigned int loadShader(std::string fname, GLenum type){

    FILE * file = fopen(fname.c_str(), "r");
	if (!file) {
		printf("No Such file %s\n", fname.c_str());
		exit(1);
		return 0;
	}
	int fileSize;
	int shaderId;
	int isOK;
	char * buffer;
	int errorLength;
	char errorLog[500];
	fseek(file, 0, SEEK_END);
	fileSize = ftell(file);
	fseek(file, 0, SEEK_SET);

	buffer = (char*) malloc(fileSize+2);
	fread(buffer, 1, fileSize, file);
	buffer[fileSize] = 0;
	shaderId = glCreateShader(type);

	glShaderSource(shaderId, 1, (const char **)&buffer, &fileSize);
	glCompileShader(shaderId);
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &isOK);

	int logLength;

	if (isOK == GL_FALSE) {

		printf("PROBLEM WITH SHADER FOUND! in '%s'\n", fname.c_str());
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLength);
		glGetShaderInfoLog(shaderId, 500, &errorLength, errorLog);
		printf("%u: %s\n", logLength, errorLog);
		exit(1);

	}

	free(buffer);
	return shaderId;

}

void Shader::setTextureUniforms() {

    for (int i = 0; i < textureSlots.size(); ++i) {

        //std::cout << "Using texture slot " << textureSlots[i].textureUnit << std::endl;
        this->loadInt(textureSlots[i].uniformLoc, textureSlots[i].textureUnit);

    }

}

void Shader::addTextureUniform(std::string name, int unit) {

    unsigned int uniformLoc = this->getUniformLocation(name);
    this->textureSlots.push_back((TEXTURE_SLOT) {unit, uniformLoc});

}

/**
ResourceInjector<Shader> * ShaderLoader::create(std::string dir, std::string name) {

    unsigned int vertexId;
    unsigned int fragmentId;

    unsigned int programId;

    programId = glCreateProgram();

    Shader * shader;


    std::string filename = dir;
    filename.append(name).append(".shader");

    CompoundNode * shaderData = ConfigLoader::loadFileTree(filename);

    std::string vertexName = dir;
    vertexName.append(std::string(shaderData->get<const char *>("vertexShader")));
    vertexId = loadShader(vertexName, GL_VERTEX_SHADER);

    std::string fragmentName = dir;
    fragmentName.append(std::string(shaderData->get<const char *>("fragmentShader")));
    fragmentId = loadShader(fragmentName, GL_FRAGMENT_SHADER);

    glAttachShader(programId, vertexId);
    glAttachShader(programId, fragmentId);

    shader = new Shader(programId, vertexId, fragmentId);

    std::vector<CompoundNode *> inputVars = shaderData->getArray<CompoundNode *>("inputVars")->getValue();

    for (int i = 0; i < inputVars.size(); ++i) {

        glBindAttribLocation(programId, inputVars[i]->get<int>("id"), inputVars[i]->get<const char*>("name"));

    }

    /*glBindAttribLocation(programId, 0, (char*)"position");
    glBindAttribLocation(programId, 1, (char*)"normal");
    glBindAttribLocation(programId, 2, (char*)"uv");
    glBindAttribLocation(programId, 3, (char*)"tangent");

    glBindAttribLocation(programId, 4, (char*)"transform");

    glLinkProgram(programId);

    GLint status;

    glGetProgramiv(programId,GL_LINK_STATUS,&status);

    if (status == GL_FALSE) {

        std::cerr << "Could not link shader\n";
        int maxLength;
        int length;
        glGetProgramiv(programId,GL_INFO_LOG_LENGTH,&maxLength);
        char* log = new char[maxLength];
        glGetProgramInfoLog(programId,maxLength,&length,log);
        std::cout << log << std::endl;

    }

    glValidateProgram(programId);

    glGetProgramiv(programId,GL_VALIDATE_STATUS,&status);

    if (status == GL_FALSE) {

        std::cerr << "Could not validate shader\n";

    }

    int uniformCount;

    glGetProgramiv(programId, GL_ACTIVE_UNIFORMS, &uniformCount);

    shader = new Shader(programId, vertexId, fragmentId);

    CompoundNode * uniformData = shaderData->getCompound("uniforms");
    std::list<std::string> uniformNames = uniformData->getChildList();

    for (std::list<std::string>::iterator it = uniformNames.begin(); it != uniformNames.end(); ++it) {

        shader->setUniformName(*it, std::string(uniformData->get<const char *>(*it)));

    }

    return shader;

}*/

ShaderInjector::ShaderInjector(std::string vertex, std::string fragment) {

    this->vertexSource = vertex;
    this->fragmentSource = fragment;
    this->hasGeometryStage = false;

    this->type = getTypeName<Shader>();

}

ShaderInjector::ShaderInjector(std::string vertex, std::string geometry, std::string fragment) {

    this->vertexSource = vertex;
    this->fragmentSource = fragment;
    this->hasGeometryStage = true;
    this->geometrySource = geometry;

    this->type = getTypeName<Shader>();

}


Shader * ShaderInjector::upload() {

    Shader * shader;

    unsigned int programId = glCreateProgram();

    unsigned int vertexId = createStage(vertexSource, GL_VERTEX_SHADER);
    unsigned int fragmentId = createStage(fragmentSource, GL_FRAGMENT_SHADER);

    glAttachShader(programId, vertexId);
    glAttachShader(programId, fragmentId);

    shader = new Shader(programId, vertexId, fragmentId);

    for (std::map<int, std::string>::iterator it = this->attributes.begin(); it != this->attributes.end(); ++it) {

        std::cout << "Binding " << it->second << " to " << it->first << std::endl;
        glBindAttribLocation(programId, it->first, it->second.c_str());

    }

    for (std::unordered_map<std::string, std::string>::iterator it = this->uniformNameMap.begin(); it != this->uniformNameMap.end(); ++it) {

        shader->setUniformName(it->first, it->second);

    }

    glLinkProgram(programId);

    GLint status;

    glGetProgramiv(programId,GL_LINK_STATUS,&status);

    if (status == GL_FALSE) {

        std::cerr << "Could not link shader" << std::endl;
        int maxLength;
        int length;
        glGetProgramiv(programId,GL_INFO_LOG_LENGTH,&maxLength);
        char* log = new char[maxLength];
        glGetProgramInfoLog(programId,maxLength,&length,log);
        std::cout << log << std::endl;

    }

    glValidateProgram(programId);

    glGetProgramiv(programId,GL_VALIDATE_STATUS,&status);

    if (status == GL_FALSE) {

        std::cerr << "Could not validate shader" << std::endl;

    }

    int uniformCount;

    glGetProgramiv(programId, GL_ACTIVE_UNIFORMS, &uniformCount);

    for (std::map<std::string, int>::iterator it = textureUnits.begin(); it != textureUnits.end(); ++it) {

        shader->addTextureUniform(it->first, it->second);

    }

    return shader;

}

void ShaderInjector::setAttribute(int id, std::string name) {

    this->attributes[id] = name;

}

int ShaderInjector::createStage(std::string source, int type) {

    int isOK;
	int errorLength;
	char errorLog[500];

    unsigned int shaderId;

    uint8_t * tmpData = (uint8_t*) malloc(sizeof(uint8_t) * (source.size() + 1));
    for (int i = 0; i < source.size(); ++i) {
        tmpData[i] = source.c_str()[i];
    }

    const char * buffer = source.c_str();
    int length = source.size();

    shaderId = glCreateShader(type);

	glShaderSource(shaderId, 1, (const char **)&tmpData, &length);
	glCompileShader(shaderId);
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &isOK);

	int logLength;

	if (isOK == GL_FALSE) {

		printf("PROBLEM WITH SHADER FOUND! in '%s'\n", source.c_str());
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLength);
		glGetShaderInfoLog(shaderId, 500, &errorLength, errorLog);
		printf("%u: %s\n", logLength, errorLog);
		exit(1);

	}

	free(tmpData);

	return shaderId;

}

ResourceInjector<Shader> * ShaderLoader::create(std::string dir, std::string name) {

    std::cout << "Creating Shader" << std::endl;
    std::string filename = dir;
    filename.append(name).append(".shader");

    std::cout << "Loading config from" << filename << std::endl;
    CompoundNode * shaderData = ConfigLoader::loadFileTree(filename);

    std::cout << "Loading vertexName" << std::endl;
    std::string vertexName = dir;

    std::cout << "Loading fragmentName" << std::endl;
    std::string fragmentName = dir;

    std::string tmpFragName = std::string(shaderData->get<const char *>("fragmentShader"));
    std::string tmpVertName = std::string(shaderData->get<const char *>("vertexShader"));

    vertexName.append(tmpVertName);
    fragmentName.append(tmpFragName);

    std::string vertexSource = loadFile(vertexName);
    std::string fragmentSource = loadFile(fragmentName);

    std::cout << vertexSource << std::endl;

    ShaderInjector * injector = new ShaderInjector(vertexSource, fragmentSource);

    CompoundNode * uniformData = shaderData->getCompound("uniforms");
    std::list<std::string> uniformNames = uniformData->getChildList();

    for (std::list<std::string>::iterator it = uniformNames.begin(); it != uniformNames.end(); ++it) {

        injector->setUniformName(*it, std::string(uniformData->get<const char *>(*it)));

    }

    std::vector<CompoundNode *> inputVars = shaderData->getArray<CompoundNode *>("inputVars")->getValue();

    for (int i = 0; i < inputVars.size(); ++i) {

        injector->setAttribute(inputVars[i]->get<int>("id"), std::string(inputVars[i]->get<const char*>("name")));

    }

    std::vector<CompoundNode*> textures = shaderData->getArray<CompoundNode*>("textures")->getValue();

    for (int i = 0; i < textures.size(); ++i) {
        injector->setTextureSlot(std::string(textures[i]->get<const char *>("name")), textures[i]->get<int>("slot"));
    }

    delete shaderData;

    return injector;


}

void ShaderInjector::setUniformName(std::string uniform, std::string name) {

    this->uniformNameMap[uniform] = name;

}

void ShaderInjector::setTextureSlot(std::string name, int slot) {

    this->textureUnits[name] = slot;

}

void ShaderInjector::deleteTempData() {

    /*this->fragmentSource = "";
    this->vertexSource = "";
    this->geometrySource = "";*/

}

std::map<std::string, int> ShaderInjector::getTextureBindings() {
    return textureUnits;
}

std::string ShaderInjector::getFragmentSource() {
    return fragmentSource;
}

std::string ShaderInjector::getVertexSource() {
    return vertexSource;
}

std::string ShaderLoader::loadFile(std::string fname) {

    FILE * file = fopen(fname.c_str(), "r");

    std::cout << "File opened" << std::endl;
    if (!file) {
        std::cerr << "Could no open " << file << std::endl;
        exit(1);
    }

    int fileSize;

    std::cout << "Getting file size" << std::endl;
    fseek(file, 0, SEEK_END);
	fileSize = ftell(file);
	fseek(file, 0, SEEK_SET);

	std::cout << "Loading " << fileSize << " bytes from file" << std::endl;
	std::string data = "";
	char buffer[128];

	std::cout << "Loading file line by line" << std::endl;

	while (fgets(buffer, 128, file)) {
        data.append(std::string(buffer));
        buffer[0] = 0;
	}
	data.append(std::string(buffer));
	fclose(file);
	std::cout << "Loading done" << std::endl;
	return data;
}

ShaderLoader::ShaderLoader(){

    this->setSuffixes("_vertex", "_fragment");

}

ShaderLoader * ShaderLoader::setSuffixes(std::string vert, std::string frag) {

    this->vertexSuffix = vert;
    this->fragmentSuffix = frag;

    return this;

}

bool ShaderLoader::canCreate(std::string dir, std::string name) {

    std::string filename = dir;
    filename.append(name).append(".shader");
    return fileExists(filename);

}

void Shader::setUniformName(std::string uniform, std::string name) {

    this->uniformNameMap[uniform] = name;

}

std::string Shader::getUniformName(std::string uniform) {

    return this->uniformNameMap[uniform];

}

Shader * createShader(std::string vertexName, std::string fragmentName) {

    unsigned int vertexId;
    unsigned int fragmentId;

    unsigned int programId;

    Shader * shader;

    programId = glCreateProgram();

    std::string tmpName = "resources/shaders/";
    tmpName.append(vertexName).append(".glsl");
    vertexId = loadShader(tmpName, GL_VERTEX_SHADER);

    tmpName = shaderDir;
    tmpName.append(fragmentName).append(".glsl");
    fragmentId = loadShader(tmpName, GL_FRAGMENT_SHADER);

    glAttachShader(programId, vertexId);
    glAttachShader(programId, fragmentId);

    shader = new Shader(programId, vertexId, fragmentId);

    glBindAttribLocation(programId, 0, (char*)"position");
    glBindAttribLocation(programId, 1, (char*)"normal");
    glBindAttribLocation(programId, 2, (char*)"uv");
    glBindAttribLocation(programId, 3, (char*)"tangent");
    glBindAttribLocation(programId, 4, (char*)"transform");

    glLinkProgram(programId);

    GLint status;

    glGetProgramiv(programId,GL_LINK_STATUS,&status);

    if (status == GL_FALSE) {

        std::cerr << "Could not link shader\n";

    }

    glValidateProgram(programId);

    glGetProgramiv(programId,GL_VALIDATE_STATUS,&status);

    if (status == GL_FALSE) {

        std::cerr << "Could not validate shader\n";

    }

    int uniformCount;

    glGetProgramiv(programId, GL_ACTIVE_UNIFORMS, &uniformCount);

    return new Shader(programId, vertexId, fragmentId);

}

std::vector<std::string> ShaderLoader::getFilenames(std::string dir, std::string name) {

    std::vector<std::string> files;

    std::string filename = dir;
    filename.append(name).append(".shader");

    files.push_back(filename);

    CompoundNode * shaderData = ConfigLoader::loadFileTree(filename);

    std::cout << "Loading vertexName" << std::endl;
    std::string vertexName = dir;
    vertexName.append(std::string(shaderData->get<const char *>("vertexShader")));
    std::cout << vertexName << std::endl;

    std::cout << "Loading fragmentName" << std::endl;
    std::string fragmentName = dir;
    fragmentName.append(std::string(shaderData->get<const char *>("fragmentShader")));

    files.push_back(vertexName);
    files.push_back(fragmentName);

    return files;

}

ShaderLoader * ShaderLoader::buildLoader() {
    return new ShaderLoader();
}

void Shader::deleteData() {

}
