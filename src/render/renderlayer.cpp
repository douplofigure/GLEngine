#include "renderlayer.h"

#include <iostream>

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "gameengine.h"

RenderLayer::RenderLayer() {

    this->elements = std::vector<std::shared_ptr<RenderElement>>();

}

RenderLayer::~RenderLayer()
{
    for (int i = 0; i < this->elements.size(); ++i) {
        elements[i] = nullptr;
    }
}

void RenderLayer::addElement(std::shared_ptr<RenderElement> elem) {

    this->elements.push_back(elem);

}

void RenderLayer::clear() {

    for (int i = 0; i < elements.size(); ++i)
        std::cout << elements[i].use_count() << std::endl;

    elements = std::vector<std::shared_ptr<RenderElement>>();

}

void RenderLayer::updateSize(int width, int height) {

    std::cout << "Updating layer size " << elements.size() << std::endl;

    for (int i = 0; i < elements.size(); ++i) {
        elements[i]->updateScreenSize(width, height);
    }

    std::cout << "layer done" << std::endl;

}

std::vector<std::shared_ptr<RenderElement>> RenderLayer::getElements() {

    return this->elements;

}

void RenderLayer::render(Camera * camera) {

    Model * model;
    Shader * shader;

    for (std::vector<std::shared_ptr<RenderElement>>::iterator it = elements.begin(); it != elements.end(); ++it) {
        shader = (*it)->getShader();
        shader->bindForRender();
        (*it)->prepareUniforms(camera);
        (*it)->bindTextures();
        (*it)->choseLevelOfDetail(camera->position);
        if(camera->canSee((*it)->getPos(), (*it)->getScale()))
            (*it)->render(camera);

        shader->unbindForRender();

    }

}

void RenderLayer::render(Camera * camera, float clippingDirection) {

    Model * model;
    Shader * shader;

    for (std::vector<std::shared_ptr<RenderElement>>::iterator it = elements.begin(); it != elements.end(); ++it) {
        shader = (*it)->getShader();
        shader->bindForRender();
        (*it)->prepareUniforms(camera);
        int id = shader->getUniformLocation(shader->getUniformName("clipping"));
        shader->loadFloat(id, clippingDirection);
        (*it)->bindTextures();
        (*it)->choseLevelOfDetail(camera->position);
        if(camera->canSee((*it)->getPos(), (*it)->getScale()))
            (*it)->render(camera);

        shader->unbindForRender();

    }

}

void RenderLayer::render(Camera * camera, float clippingDirection, glm::vec3 sunDir) {

    Model * model;
    Shader * shader;

    for (std::vector<std::shared_ptr<RenderElement>>::iterator it = elements.begin(); it != elements.end(); ++it) {
        shader = (*it)->getShader();
        shader->bindForRender();
        (*it)->prepareUniforms(camera, sunDir);
        int id = shader->getUniformLocation(shader->getUniformName("clipping"));
        shader->loadFloat(id, clippingDirection);
        (*it)->bindTextures();
        (*it)->choseLevelOfDetail(camera->position);
        if(camera->canSee((*it)->getPos(), (*it)->getScale()))
            (*it)->render(camera);

        shader->unbindForRender();

    }

}
