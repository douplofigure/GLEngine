#ifndef POSTPROCESSINGSTACK_H
#define POSTPROCESSINGSTACK_H

#include "renderbuffer.h"
#include "texture.h"
#include "shader.h"
#include "model.h"
#include "postprocessingeffect.h"

class PostProcessingStack
{
    public:
        PostProcessingStack();
        virtual ~PostProcessingStack();

        void applyToBuffer(RenderBuffer * buffer);

        Texture * getResult(RenderBuffer * buffer);

        void addEffect(Shader * shader, int bufferWidth, int bufferHeight);
        void addEffect(PostProcessingEffect * effect);

        PostProcessingEffect * getEffect(int num);

    protected:

    private:

        std::vector<PostProcessingEffect *> effects;

        Model * quad;

};

#endif // POSTPROCESSINGSTACK_H
