#include "viewport.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include "resources/resourcemanager.h"

Viewport::Viewport(int width, int height, float fov, float zNear, float zFar) {

    this->layers = std::vector<RenderLayer*>(0);

    this->width = width;
    this->height = height;

    this->zNear = zNear;
    this->zFar = zFar;
    this->fov = fov;

    this->camera = new Camera(fov, (float) width/height, zNear, zFar);
    this->layers.push_back(new RenderLayer());
    this->layers.push_back(new RenderLayer());

}

Viewport::~Viewport()
{
    for (int i = 0; i < layers.size(); ++i) {
        delete layers[i];
    }
}

void Viewport::updateSize(int newWidth, int newHeight) {

    std::cout << "Updating viewport size " << layers.size() << std::endl;

    this->height = newHeight;
    this->width = newWidth;

    this->camera->updateProjection(fov, (float) newWidth / newHeight, zNear, zFar);

    for (int i = 0; i < layers.size(); ++i) {

        layers[i]->updateSize(newWidth, newHeight);

    }

}

Camera * Viewport::getCamera() {

    return camera;

}

void Viewport::addElement(int layer, std::shared_ptr<RenderElement> elem) {

    std::cout << "Adding element to layer " << layer << std::endl;

    this->layers[layer]->addElement(elem);

}

void Viewport::addUIElement(std::shared_ptr<RenderElement> elem) {

    this->layers[layers.size()-1]->addElement(elem);

}

void Viewport::addLayer() {

    RenderLayer * layer = new RenderLayer();
    this->layers.insert(layers.end()-1, layer);

}

void Viewport::clearElements() {

    for (int i = 0; i < this->layers.size(); ++i) {
        this->layers[i]->clear();
    }

}

void Viewport::addUILayer() {

    this->uiLayers.push_back(new RenderLayer());

}

void Viewport::renderElements() {

    glm::mat4 projection = this->camera->getProjectionMatrix();
    glm::mat4 view = this->camera->getViewMatrix();

    Model * model;
    Shader * shader;

    glViewport(0,0, width, height);

    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);


    glClearColor(cr, cg, cb, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < layers.size(); ++i) {

        layers[i]->render(camera);

    }

    for (int i = 0; i < uiLayers.size(); ++i) {

        uiLayers[i]->render(camera);

    }

    glDisable(GL_BLEND);

}

void Viewport::setFov(float fov) {
    this->camera->updateProjection(fov, camera->getAspect(), camera->getNear(), camera->getFar());
}

void Viewport::setClearColor(float r, float g, float b) {

    this->cr = r;
    this->cb = b;
    this->cg = g;

}

void Viewport::addStructureLights(Structure * strc, Math::Vector3 pos, Math::Quaternion rot, double scale) {

}

int Viewport::addLight(Math::Vector3 pos, Math::Vector3 col, LIGHT_TYPE type, double scale) {
    return addLight(glm::vec3(pos.x, pos.y, pos.z), glm::vec3(col.x, col.y, col.z), type, scale);
}

int Viewport::addLight(glm::vec3 pos, glm::vec3 color, LIGHT_TYPE type, float falloff) {

}

void Viewport::setLightAsSun(int index) {

}

void Viewport::clearLights() {

}
