#ifndef STATICRENDERELEMENT_H
#define STATICRENDERELEMENT_H

#define GLM_ENABLE_EXPERIMENTAL

#include "renderelement.h"
#include <glm/gtx/transform.hpp>

typedef struct render_instance_t RENDER_INSTANCE;

class StaticRenderElement : public RenderElement
{
    public:
        StaticRenderElement(Model * model, Shader * shader, int maxInstanceCount);
        StaticRenderElement(Model * model, Material * material, int maxInstanceCount);
        StaticRenderElement(std::string model, std::string shader, int maxInstanceCount);
        StaticRenderElement(Structure * strc, int maxInstances);
        virtual ~StaticRenderElement();

        virtual void render() override;
        virtual void render(Camera * camera) override;

        int addInstance(float x, float y, float z);
        void updateInstance(int id, glm::vec3 pos, Math::Quaternion rot, float scale);
        int getInstanceCount();

        glm::vec3 getInstancePosition(int instanceId);
        Math::Quaternion getInstanceRotation(int instanceId);
        double getInstanceScale(int instanceId);

        virtual void prepareUniforms(Camera * camera) override;
        virtual void prepareUniforms(Camera * camera, glm::vec3 sunDirection) override;
        virtual CompoundNode * asCompound() override;

        Math::Mesh * getCombinedMesh();

    protected:

        virtual void linkWithShader() override;
        int removeInstance(int id);

        unsigned int transformBufferId;
        int instanceCount;

        unsigned int vaoId;
        int maxInstanceCount;

        std::vector<RENDER_INSTANCE> instances;

    private:

};

#endif // STATICRENDERELEMENT_H
