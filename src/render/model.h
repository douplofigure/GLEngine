#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <string>

#include "util/math/mesh.h"
#include "resources/resourceregistry.h"

class Model : Resource {
    public:

        Model();

        Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount);
        Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount, Math::Mesh * mesh);
        Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount, int primitiveType);
        Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount, int primitiveType, int attributeCount);
        virtual ~Model();

        unsigned int getVaoId();
        unsigned int getIndexBufferId();
        unsigned int getDataBufferId();

        int getPrimitiveType();

        int getIndexCount();
        void activateAttributes(int surplus);
        void deactivateAttributes(int surplus);

        Math::Mesh * getMeshData();
        void setMeshData(Math::Mesh * data);
        void deleteData();

        bool isReady();

    protected:

        unsigned int vaoId;
        unsigned int indexBufferId;
        unsigned int dataBufferId;

        int indexCount;
        int attributeCount;

        int primitiveType;

    private:

        Math::Mesh * meshData;

        bool hasVao;
        bool hasIndexBuffer;
        bool hasDataBuffer;

};

class ModelInjector : public ResourceInjector<Model> {

    public:
        ModelInjector(std::vector<float> data, int vertexCount, std::vector<int> indecies, Math::Mesh * meshData, unsigned int primType);
        ModelInjector(std::vector<float> data, int vertexCount, std::vector<int> indecies, Math::Mesh * meshData, unsigned int primType, bool material);

        ModelInjector() {};

        Model * upload();
        Math::Mesh * getMeshData();

        void deleteTempData();


    private:
        std::vector<float> data;
        int vertexCount;
        std::vector<int> indecies;
        unsigned int primType;
        Math::Mesh * mesh;
        bool hasMaterialAttribute;

};

Model * createModel(std::vector<float> data, int vertexCount, std::vector<int> indecies);
Model * createModel(std::vector<float> data, int vertexCount, std::vector<int> indecies, Math::Mesh * meshData);
Model * createModel(std::vector<float> data, int vertexCount, std::vector<int> indecies, unsigned int primType);

#endif // MODEL_H
