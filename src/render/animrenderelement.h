#ifndef ANIMRENDERELEMENT_H
#define ANIMRENDERELEMENT_H

#include "renderelement.h"
#include "animation/armature.h"
#include "animation/animation.h"
#include "animation/animationrig.h"

class AnimRenderElement : public RenderElement
{
    public:
        AnimRenderElement(AnimationRig * rig, Material * mat, float x, float y, float z);
        virtual ~AnimRenderElement();

        void render(Camera * cam);
        void render();


        virtual void prepareUniforms(Camera * camera);
        virtual void prepareUniforms(Camera * camera, glm::vec3 sunDir);

    protected:

    private:

        void createBoneModel();

        float * rotationData;
        float * offsetData;

        unsigned int boneRotationLoc;
        unsigned int boneOffsetLoc;

        Anim::Armature * armature;
        std::map<std::string, Anim::Animation*> animations;
        Anim::Animation * currentAnimation;

};

#endif // ANIMRENDERELEMENT_H
