#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include "../staticrenderelement.h"

typedef struct particle_t Particle;

class ParticleSystem : public StaticRenderElement
{
    public:
        /** Default constructor */
        ParticleSystem(int maxCount, glm::vec3 position, double particleLifeTime);
        /** Default destructor */
        virtual ~ParticleSystem();

        virtual void render() override;
        virtual void render(Camera * camera) override;

        void spawnParticle(glm::vec3 relPos, glm::vec3 speed, float gravityCoef, float lifeTime);

    protected:

    private:

        void update(double dt);

        int addToBuffer(glm::vec3 pos, glm::vec3 speed, float gCoef);
        void resetParticle(int id);

        glm::vec3 worldPos;

        double emitTime;

        std::vector<Particle> particles;
        double lastRenderTime;
        double lastSpawnTime;

};

#endif // PARTICLESYSTEM_H
