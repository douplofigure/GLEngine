#ifndef COMPUTESHADER_H
#define COMPUTESHADER_H

#include <vector>

class ComputeShader
{
    public:
        /** Default constructor */
        ComputeShader(unsigned int programId, unsigned int stageId, std::vector<float> data);
        /** Default destructor */
        virtual ~ComputeShader();

        void execute();

    protected:

        unsigned int programId;
        unsigned int stageId;

        unsigned int bufferId;

    private:

        int sizeX;
        int sizeY;
        int sizeZ;

};

#endif // COMPUTESHADER_H
