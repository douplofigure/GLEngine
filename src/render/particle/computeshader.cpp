#include "computeshader.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

ComputeShader::ComputeShader(unsigned int pId, unsigned int sId, std::vector<float> initData) {

    this->programId = pId;
    this->stageId = sId;

    glGenBuffers(GL_SHADER_STORAGE_BUFFER, &bufferId);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferId);

    glBufferData(GL_SHADER_STORAGE_BUFFER, initData.size() * sizeof(float), initData.data(), GL_DYNAMIC_DRAW);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufferId);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

}

ComputeShader::~ComputeShader()
{
    //dtor
}

void ComputeShader::execute() {

    glUseProgram(programId);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufferId);
    glDispatchCompute(sizeX, sizeY, sizeZ);

    glUseProgram(0);

}
