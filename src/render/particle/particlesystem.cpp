#include "particlesystem.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <glm/gtc/matrix_access.hpp>

#include "gameengine.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#include "util/vecutils.h"
#define GRAVITY -9.81

struct particle_t {

    glm::vec3 pos;
    glm::vec3 speed;
    float gravityCoef;
    float lifeTime;
    float lifeDuration;

};

ParticleSystem::ParticleSystem(int maxCount, glm::vec3 position, double lifeDuration) : StaticRenderElement(GameEngine::resources->get<Model>("sheep"), GameEngine::resources->get<Shader>("particle"), maxCount) {

    this->particles = std::vector<Particle>(maxCount);

    this->position = position;
    this->worldPos = position;
    emitTime = lifeDuration;

}

ParticleSystem::~ParticleSystem()
{
    //dtor
}

void ParticleSystem::spawnParticle(glm::vec3 relPos, glm::vec3 speed, float gravityCoef, float lifeTime) {

    if (instanceCount >= maxInstanceCount) return;

    this->particles[instanceCount].pos = relPos;
    this->particles[instanceCount].speed = speed;
    this->particles[instanceCount].gravityCoef = gravityCoef;
    this->particles[instanceCount].lifeTime = lifeTime;
    this->particles[instanceCount].lifeDuration = lifeTime;

    glm::vec3 absPos = relPos + worldPos;

    this->addToBuffer(position, speed, gravityCoef);

}

void ParticleSystem::update(double dt) {

    if (instanceCount < maxInstanceCount) {

        if (GameEngine::getTime() - lastSpawnTime > (emitTime / maxInstanceCount)) {
            this->spawnParticle(glm::vec3(0,0,0), glm::vec3(randomFloatInBounds(-2, 2), 10.0, randomFloatInBounds(-2, 2)), .4, emitTime);
            this->lastSpawnTime = GameEngine::getTime();
        }

    }

    for (int i = 0; i < instanceCount; ++i) {

        if (particles[i].lifeTime < 0) {
            //this->particles[i] = particles[instanceCount-1];
            this->resetParticle(i);
            this->particles[i].lifeTime = particles[i].lifeDuration;
            //this->removeInstance(i);
        }

        //this->particles[i].pos += (float) dt * particles[i].speed;
        //this->particles[i].speed.y += dt * GRAVITY * particles[i].gravityCoef;

        //this->updateInstance(i, particles[i].pos + worldPos, glm::vec3(0,0,0), 1);

        this->particles[i].lifeTime -= dt;

    }

}

void ParticleSystem::render() {

    double dt = GameEngine::getTime() - lastRenderTime;
    lastRenderTime = GameEngine::getTime();

    this->update(dt);

    glBindVertexArray(vaoId);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
    glEnableVertexAttribArray(6);
    glEnableVertexAttribArray(7);

    glDrawElementsInstanced(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0, instanceCount);

    glDisableVertexAttribArray(4);
    glDisableVertexAttribArray(5);
    glDisableVertexAttribArray(6);
    glDisableVertexAttribArray(7);

    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    glBindVertexArray(0);

}

void ParticleSystem::render(Camera * camera) {

    double dt = GameEngine::getTime() - lastRenderTime;
    lastRenderTime = GameEngine::getTime();

    this->update(dt);

    glBindVertexArray(vaoId);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
    glEnableVertexAttribArray(6);
    glEnableVertexAttribArray(7);

    glDrawElementsInstanced(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0, instanceCount);

    glDisableVertexAttribArray(4);
    glDisableVertexAttribArray(5);
    glDisableVertexAttribArray(6);
    glDisableVertexAttribArray(7);

    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    glBindVertexArray(0);

}

int ParticleSystem::addToBuffer(glm::vec3 pos, glm::vec3 speed, float gCoef) {

    if(instanceCount > maxInstanceCount-1) return -1;

    glm::mat4 trans = getTransformationMatrix();

    //this->instances[instanceCount] = {glm::vec3(0, 0, 0), glm::vec3(0,0,0), 1.0, trans};

    glm::vec4 col1 = glm::column(trans, 0);
    glm::vec4 col2 = glm::column(trans, 1);
    glm::vec4 col3 = glm::column(trans, 2);
    glm::vec4 col4 = glm::column(trans, 3);

    pos += this->position * 0.5f;

    std::vector<float> data(16);
    int index = 0;
    data[index++] = speed.x;
    data[index++] = speed.y;
    data[index++] = speed.z;
    data[index++] = GameEngine::getTime();
    data[index++] = gCoef;
    data[index++] = pos.x;
    data[index++] = pos.y;
    data[index++] = pos.z;
    data[index++] = col3.x;
    data[index++] = col3.y;
    data[index++] = col3.z;
    data[index++] = col3.w;
    data[index++] = col4.x;
    data[index++] = col4.y;
    data[index++] = col4.z;
    data[index++] = col4.w;

    glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

    glBufferSubData(GL_ARRAY_BUFFER, 16 * instanceCount * sizeof(float), 16 * sizeof(float), data.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return instanceCount++;

}

void ParticleSystem::resetParticle(int id) {

    std::vector<float> data(16);
    int index = 0;
    data[index++] = particles[id].speed.x;
    data[index++] = particles[id].speed.y;
    data[index++] = particles[id].speed.z;
    data[index++] = GameEngine::getTime();
    data[index++] = particles[id].gravityCoef;
    data[index++] = particles[id].pos.x + position.x;
    data[index++] = particles[id].pos.y + position.y;
    data[index++] = particles[id].pos.z + position.z;
    data[index++] = 0;
    data[index++] = 0;
    data[index++] = 0;
    data[index++] = 0;
    data[index++] = 0;
    data[index++] = 0;
    data[index++] = 0;
    data[index++] = 0;

    glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

    glBufferSubData(GL_ARRAY_BUFFER, 16 * id * sizeof(float), 16 * sizeof(float), data.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);

}
