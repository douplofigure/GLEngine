#ifndef SHADERMATERIAL_H
#define SHADERMATERIAL_H

#include "shader.h"


class ShaderMaterial : public Shader
{
    public:
        ShaderMaterial(unsigned int programId, unsigned int vertexShaderId, unsigned int fragmentShaderId);
        virtual ~ShaderMaterial();

    protected:



    private:
};

#endif // SHADERMATERIAL_H
