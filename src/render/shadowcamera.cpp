#include "shadowcamera.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "util/math/conversions.h"

#define SHADOW_DISTANCE 20.0f
#define OFFSET 10
#define UP glm::vec4(0,1,0,0)
#define FORWARD glm::vec4(0,0,-1,0)

using namespace Math;

float projMat[16] = {

    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0,

};

ShadowCamera::ShadowCamera(Camera * viewCam) {
    this->viewCam = viewCam;

    this->projectionMatrix = glm::ortho(-SHADOW_DISTANCE/2, SHADOW_DISTANCE/2, -SHADOW_DISTANCE/2, SHADOW_DISTANCE/2, 0.0f, 200.0f);
    this->viewMatrix = glm::make_mat4(projMat);
}

ShadowCamera::~ShadowCamera()
{
    //dtor
}

void ShadowCamera::setSunDirection(glm::vec3 dir) {
    this->sunDirection = dir;
}

glm::mat4 calculateCameraRotationMatrix(Camera * cam) {

    glm::mat4 rot = glm::rotate((float)-cam->getYaw(), glm::vec3(0,1,0));
    rot = glm::rotate(rot, (float)-cam->rotation.x, glm::vec3(1,0,0));
    return rot;

}

glm::vec4 ShadowCamera::calculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction, float width) {
    glm::vec3 point = startPoint + width * direction;
    glm::vec4 point4f = glm::vec4(point.x, point.y, point.z, 1.0);
    point4f = this->viewMatrix * point4f;
    return point4f;
}

std::vector<glm::vec4> ShadowCamera::calculateFrustumVertices(glm::mat4 rotation, glm::vec3 fwdVec, glm::vec3 cN, glm::vec3 cF) {

    glm::vec4 tmpUp = rotation * UP;
    glm::vec3 upVec = glm::vec3(tmpUp.x, tmpUp.y, tmpUp.z);

    glm::vec3 rightVec = glm::cross(fwdVec, upVec);
    glm::vec3 downVec = -1.0f * upVec;
    glm::vec3 leftVec = -1.0f * rightVec;

    glm::vec3 farTop = cF + farHeight * upVec;
    glm::vec3 farBottom = cF + farHeight * downVec;

    glm::vec3 nearTop = cN + nearHeight * upVec;
    glm::vec3 nearBottom = cN + nearHeight * downVec;

    std::vector<glm::vec4> points = std::vector<glm::vec4>(8);

    points[0] = calculateLightSpaceFrustumCorner(farTop, rightVec, farWidth);
    points[1] = calculateLightSpaceFrustumCorner(farTop, leftVec, farWidth);
    points[2] = calculateLightSpaceFrustumCorner(farBottom, rightVec, farWidth);
    points[3] = calculateLightSpaceFrustumCorner(farBottom, leftVec, farWidth);
    points[4] = calculateLightSpaceFrustumCorner(nearTop, rightVec, nearWidth);
    points[5] = calculateLightSpaceFrustumCorner(nearTop, leftVec, nearWidth);
    points[6] = calculateLightSpaceFrustumCorner(nearBottom, rightVec, nearWidth);
    points[7] = calculateLightSpaceFrustumCorner(nearBottom, leftVec, nearWidth);
    return points;

}

void ShadowCamera::calculateWidthsAndHeights() {
    farWidth = (float) (SHADOW_DISTANCE * tan(radians(this->viewCam->getFov())));
    nearWidth = (float) (this->viewCam->getNear() * tan(radians(this->viewCam->getFov())));
    farHeight = farWidth / this->viewCam->getAspect();
    nearHeight = nearWidth / this->viewCam->getAspect();
}

void ShadowCamera::updateBox() {

    glm::mat4 rotMat = calculateCameraRotationMatrix(viewCam);
    glm::vec4 tmp = rotMat * FORWARD;
    glm::vec3 forwardVec = glm::vec3(tmp.x, tmp.y, tmp.z);

    glm::vec3 toFar = SHADOW_DISTANCE * forwardVec;
    glm::vec3 toNear = viewCam->getNear() * forwardVec;

    glm::vec3 centerNear = toNear + viewCam->position;
    glm::vec3 centerFar = toFar + viewCam->position;

    std::vector<glm::vec4> points = calculateFrustumVertices(rotMat, forwardVec, centerNear, centerFar);

    bool first = true;
    for (glm::vec4 point : points) {
        if (first) {
            minX = point.x;
            maxX = point.x;
            minY = point.y;
            maxY = point.y;
            minZ = point.z;
            maxZ = point.z;
            first = false;
            continue;
        }
        if (point.x > maxX) {
            maxX = point.x;
        } else if (point.x < minX) {
            minX = point.x;
        }
        if (point.y > maxY) {
            maxY = point.y;
        } else if (point.y < minY) {
            minY = point.y;
        }
        if (point.z > maxZ) {
            maxZ = point.z;
        } else if (point.z < minZ) {
           minZ = point.z;
        }
    }

    maxZ += OFFSET;

}

glm::vec3 ShadowCamera::getCenter() {

    updateBox();

    float x = (minX + maxX) / 2.0;
    float y = (minY + maxY) / 2.0;
    float z = (minZ + maxZ) / 2.0;
    glm::vec4 cen = glm::vec4(x, y, z, 1);
    glm::mat4 invertedLight = glm::inverse(viewMatrix);

    glm::vec4 tmp = invertedLight * cen;
    return glm::vec3(tmp.x, tmp.y, tmp.z);
}

void ShadowCamera::updateViewMatrix() {

    /*glm::vec3 center = -getCenter();

    float pitch = acos(sqrt(sunDirection.x * sunDirection.x + sunDirection.y * sunDirection.y));
    viewMatrix = glm::rotate(pitch, glm::vec3(1,0,0));
    float yaw = atan(sunDirection.x / sunDirection.z);
    yaw = sunDirection.z > 0 ? yaw - M_PI : yaw;
    viewMatrix = glm::rotate(viewMatrix, yaw, glm::vec3(0,1,0));

    viewMatrix = glm::translate(viewMatrix, center);*/

    this->position = viewCam->position - SHADOW_DISTANCE * this->sunDirection + (SHADOW_DISTANCE / 2.0f) * glm::vec3(viewCam->facing.x, 0, viewCam->facing.z);
    this->facing = sunDirection;

    //this->projectionMatrix = glm::perspective((float)radians(70.0), 1.0f, 0.1f, SHADOW_DISTANCE);

    this->viewMatrix = glm::lookAt(position, position + facing, glm::vec3(0,1,0));

}
