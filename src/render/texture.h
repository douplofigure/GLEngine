#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include "resources/resourceregistry.h"

class Texture : Resource
{
    public:
        Texture(unsigned int id, int width, int height, int type);
        virtual ~Texture();

        unsigned int getID();

        virtual void bindToUnit(int unit);
        virtual void deleteData();
        int getWidth();
        int getHeight();
        static Texture * createTexture2D(std::vector<float> values, int width, int height, int chanelCount);
        static Texture * createTexture2D(std::vector<float> values, int width, int height, int chanelCount, bool isMap);

    protected:

        unsigned int id;
        int width;
        int height;
        int type;

    private:
};

class TextureInjector : public ResourceInjector<Texture> {

    public:
        TextureInjector(std::vector<float> values, int width, int height, int chanelCount);
        TextureInjector(std::vector<float> values, int width, int height, int chanelCount, bool isMap);
        virtual ~TextureInjector();

        void setMap(bool b);

        Texture * upload();
        void deleteTempData();

        std::vector<float> getValues();

    private:

        std::vector<float> values;
        int width;
        int height;
        int chanelCount;

        bool isMap;

};

#endif // TEXTURE_H
