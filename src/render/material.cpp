#include "material.h"

#include <configloader.h>

#include "util/fileutils.h"
#include "gameengine.h"
#include "resources/textureloader.h"

Material::Material(Shader * shader, Shader * sShader, std::vector<TEXTURE_BINDING> bindings) {

    this->textures = bindings;
    this->shader = shader;
    this->staticShader = sShader;

}

Material::~Material()
{
    //dtor
}

Shader * Material::getShader() {
    return shader;
}

Shader * Material::getStaticShader() {
    return staticShader;
}

std::vector<TEXTURE_BINDING> Material::getBindings() {
    return textures;
}

void Material::addTexture(int slot, Texture * texture) {
    this->textures.push_back((TEXTURE_BINDING){slot, texture});
}

bool MaterialLoader::canCreate(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".mat");

    std::cout << "Checking for " << fname << "\n";

    return fileExists(fname);

}

std::vector<float> mergeTextures(std::vector<float *> data, int width, int height) {

    int channelCount = data.size();
    std::vector<float> tData(width * height * channelCount);

    for (int i = 0; i < channelCount; ++i) {

        if (!data[i]) {
            for (int j = 0; j < width * height; ++j) {

                tData[j*channelCount+i] = 0;

            }
            continue;
        }

        for (int j = 0; j < width * height; ++j) {

            tData[j*channelCount+i] = (data[i][j*4+1] + data[i][j*4+2] + data[i][j*4+3]) / 3.0f;

        }
    }

    return tData;

}

TextureInjector * createCompositeTexture(CompoundNode * tNode) {

    static int genCount;

    std::vector<std::string> names(4);

    if (tNode->getChildNode("red"))
        names[0] = std::string(tNode->get<const char *>("red"));
    if (tNode->getChildNode("green"))
        names[1] = std::string(tNode->get<const char *>("green"));
    if (tNode->getChildNode("blue"))
        names[2] = std::string(tNode->get<const char *>("blue"));
    if (tNode->getChildNode("alpha"))
        names[3] = std::string(tNode->get<const char *>("alpha"));


    int width, height;
    std::vector<float *> tData(4);
    TGALoader * loader = TGALoader::buildLoader();

    for (int i = 0; i < names.size(); ++i) {


        std::cout << "Texture Name '" << names[i] << "'" << std::endl;

        if (!names[i].compare("")) {

            std::cout << "Skipping texture" << std::endl;
            tData[i] = nullptr;
            continue;

        }

        std::vector<std::string> nVec = loader->getFilenames(GameEngine::resources->getResourceLocation<Texture>(), names[i]);
        std::cout << "Opening Image " << nVec[0] << std::endl;
        tData[i] = getImageAsFloatArray(nVec[0].c_str(), &width, &height);
        std::cout << "Image " << nVec[0] << "Could be read" << std::endl;

    }

    std::vector<float> mData = mergeTextures(tData, width, height);

    for (int i = 0; i < tData.size(); ++i) {
        free(tData[i]);
    }

    TextureInjector * injector = new TextureInjector(mData, width, height, 4, true);

    injector->setName("__generated__" + std::to_string(genCount++));
    injector->setType(getTypeName<Texture>());

    return injector;

}

std::vector<T_INJECTOR_BINDING> getPBRTextures(CompoundNode * rootNode) {

    CompoundNode * metallNode = rootNode->getCompound("metallic");
    std::vector<CompoundNode *> tData = metallNode->getArray<CompoundNode*>("textures")->getValue();

    std::cout << "tData has size " << tData.size() << std::endl;
    std::vector<T_INJECTOR_BINDING> textures(tData.size());

    for (int i = 0; i < tData.size(); ++i) {


        if (tData[i]->getChildNode("name"))
            textures[i].injector = (TextureInjector *) GameEngine::resources->load<Texture>(std::string(tData[i]->get<const char *>("name")));
        else {

            textures[i].injector = createCompositeTexture(tData[i]->getCompound("composite"));
            GameEngine::resources->addResource<Texture>(textures[i].injector);

        }
        textures[i].slot = tData[i]->get<int>("slot");
        textures[i].isMap = tData[i]->get<bool>("isMap");

        std::cout << "Texture " << textures[i].injector->getName() << " slot " << textures[i].slot << std::endl;

    }

    return textures;

}
std::vector<T_INJECTOR_BINDING> getDefaultTextures(CompoundNode * rootNode) {

    std::vector<CompoundNode *> tData = rootNode->getCompound("default")->getArray<CompoundNode*>("textures")->getValue();

    std::vector<T_INJECTOR_BINDING> textures(tData.size());

    for (int i = 0; i < tData.size(); ++i) {

        textures[i].injector = (TextureInjector *) GameEngine::resources->load<Texture>(std::string(tData[i]->get<const char *>("name")));
        textures[i].slot = tData[i]->get<int>("slot");
        textures[i].isMap = tData[i]->get<bool>("isMap");

    }

    return textures;

}

int MaterialInjector::getTextureCount() {

    return bindings.size();

}

MaterialInjector * MaterialLoader::create(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".mat");

    std::cout << "loading " << fname << std::endl;

    CompoundNode * rootNode = ConfigLoader::loadFileTree(fname);

    std::vector<T_INJECTOR_BINDING> textures;
    std::string shaderName;
    std::string staticShaderName;

    std::cout << "config loaded" << std::endl;

    switch (GameEngine::getRenderMode()) {

        case PBR:
            std::cout << "Using PBR textures" << std::endl;
            textures = getPBRTextures(rootNode);
            shaderName = std::string(rootNode->getCompound("metallic")->get<const char *>("shader"));
            staticShaderName = std::string(rootNode->getCompound("metallic")->get<const char *>("staticShader"));
            break;

        default:
            std::cout << "Using Default textures" << std::endl;
            textures = getDefaultTextures(rootNode);
            shaderName = std::string(rootNode->getCompound("default")->get<const char *>("shader"));
            staticShaderName = std::string(rootNode->getCompound("default")->get<const char *>("staticShader"));
            break;

    }

    MaterialInjector * injector = new MaterialInjector((ShaderInjector*)GameEngine::resources->load<Shader>(shaderName), (ShaderInjector*)GameEngine::resources->load<Shader>(staticShaderName));

    for (int i = 0; i < textures.size(); ++i) {

        textures[i].injector->setMap(textures[i].isMap);
        injector->addTexture(textures[i].slot, textures[i].injector);

    }

    delete rootNode;

    return injector;

}

std::vector<std::string> extractCompositeTextureFiles(CompoundNode * tNode) {

    std::vector<std::string> names;

    if (tNode->getChildNode("red"))
        names.push_back("resources/textures/" + std::string(tNode->get<const char *>("red")) + ".tga");
    if (tNode->getChildNode("green"))
        names.push_back("resources/textures/" + std::string(tNode->get<const char *>("green")) + ".tga");
    if (tNode->getChildNode("blue"))
        names.push_back("resources/textures/" + std::string(tNode->get<const char *>("blue")) + ".tga");
    if (tNode->getChildNode("alpha"))
        names.push_back("resources/textures/" + std::string(tNode->get<const char *>("alpha")) + ".tga");

    return names;


}

std::vector<std::string> getCompositeTextureFiles(CompoundNode * rootNode) {

    std::vector<std::string> files;
    std::vector<std::string> names;

    switch(GameEngine::getRenderMode()) {

        case PBR:

            CompoundNode * metallNode = rootNode->getCompound("metallic");
            std::vector<CompoundNode *> tData = metallNode->getArray<CompoundNode*>("textures")->getValue();

            for (int i = 0; i < tData.size(); ++i) {


                if (!tData[i]->getChildNode("name")) {
                    names = extractCompositeTextureFiles(tData[i]->getCompound("composite"));
                    files.insert(files.end(), names.begin(), names.end());
                }

            }
        return files;

    }

}

std::vector<std::string> MaterialLoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".mat");
    std::vector<std::string> files;
    files.push_back(fname);

    CompoundNode * rootNode = ConfigLoader::loadFileTree(fname);
    std::vector<std::string> compText = getCompositeTextureFiles(rootNode);

    files.insert(files.end(), compText.begin(), compText.end());

    return files;

}

MaterialLoader * MaterialLoader::buildLoader() {
    return new MaterialLoader();
}

MaterialInjector::MaterialInjector(ShaderInjector * shader, ShaderInjector * sShader) {

    this->shaderInjector = shader;
    this->staticInjector = sShader;

}

void MaterialInjector::deleteTempData() {


}

MaterialInjector::~MaterialInjector(){};

Material * MaterialInjector::upload() {

    std::vector<TEXTURE_BINDING> textures(bindings.size());

    for (int i = 0; i < this->bindings.size(); ++i) {

        textures[i].texture = bindings[i].injector->uploadInternal();
        textures[i].index = bindings[i].slot;

        std::cout << "TextureInjector " << bindings[i].injector->getName() << " slot " << bindings[i].slot << std::endl;

    }

    Shader * shader = shaderInjector->uploadInternal();
    Shader * sShader = staticInjector->uploadInternal();

    Material * mat = new Material(shader, sShader, textures);

    return mat;

}

void Material::deleteData() {



}

TextureInjector * MaterialInjector::getTexture(int slot) {

    for (int i = this->bindings.size()-1; i >= 0; --i) {
        if (bindings[i].slot == slot) return bindings[i].injector;
    }
    return nullptr;

}

void MaterialInjector::addTexture(int slot, TextureInjector * injector) {

    this->bindings.push_back((T_INJECTOR_BINDING) {slot, injector});

}

ShaderInjector * MaterialInjector::getShader() {
    return shaderInjector;
}
