#include "model.h"

#include <iostream>
#include <stdlib.h>


#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include "resources/resourcemanager.h"

Model::Model() {

    hasDataBuffer = false;
    hasIndexBuffer = false;
    hasVao = false;
    this->attributeCount = 3;
    this->meshData = nullptr;

}

Model::Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount) {

    this->vaoId = vaoId;
    this->indexBufferId = indexBufferId;
    this->dataBufferId = dataBufferId;

    this->indexCount = indexCount;
    this->primitiveType = GL_TRIANGLES;

    hasDataBuffer = true;
    hasIndexBuffer = true;
    hasVao = true;
    this->attributeCount = 3;
    this->meshData = nullptr;

}

Model::Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount, Math::Mesh * mesh) {

    this->vaoId = vaoId;
    this->indexBufferId = indexBufferId;
    this->dataBufferId = dataBufferId;

    this->indexCount = indexCount;
    this->primitiveType = GL_TRIANGLES;

    hasDataBuffer = true;
    hasIndexBuffer = true;
    hasVao = true;
    this->attributeCount = 3;

    meshData = mesh;

}

Model::Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int indexCount, int primitiveType) {

    this->vaoId = vaoId;
    this->indexBufferId = indexBufferId;
    this->dataBufferId = dataBufferId;

    this->indexCount = indexCount;

    this->primitiveType = primitiveType;

    hasDataBuffer = true;
    hasIndexBuffer = true;
    hasVao = true;
    this->attributeCount = 3;

    this->meshData = nullptr;

}

Model::Model(unsigned int vaoId, unsigned int indexBufferId, unsigned int dataBufferId, int vertexCount, int primType, int attrCount) :
    Model (vaoId, indexBufferId, dataBufferId, vertexCount, primType){

    this->attributeCount = attrCount;

}

Model::~Model() {

    glDeleteBuffers(1, &indexBufferId);
    glDeleteBuffers(1, &dataBufferId);

    glDeleteVertexArrays(1, &vaoId);

    if (this->meshData)
        delete this->meshData;

}

void Model::deleteData() {

    glDeleteBuffers(1, &indexBufferId);
    glDeleteBuffers(1, &dataBufferId);

    glDeleteVertexArrays(1, &vaoId);

    if (this->meshData)
        delete this->meshData;

}

unsigned int Model::getVaoId() {
    return vaoId;
}

int Model::getIndexCount() {
    return indexCount;
}

int Model::getPrimitiveType() {
    return primitiveType;
}

Math::Mesh * Model::getMeshData() {
    return meshData;
}

void Model::setMeshData(Math::Mesh * data) {
    meshData = data;
}

bool Model::isReady() {
    return hasVao && hasIndexBuffer && hasDataBuffer;
}

void Model::activateAttributes(int surplus) {

    for (int i = 0; i < attributeCount+surplus; ++i) {

        glEnableVertexAttribArray(i);

    }

}

void Model::deactivateAttributes(int surplus) {

    for (int i = attributeCount+surplus-1; i >= 0; --i) {

        glDisableVertexAttribArray(i);

    }

}

unsigned int Model::getDataBufferId() {
    return dataBufferId;
}

unsigned int Model::getIndexBufferId() {
    return indexBufferId;
}

Model * createModel(std::vector<float> data, int vertexCount, std::vector<int> indecies) {

    unsigned int vaoId;
    unsigned int dataId;
    unsigned int indexId;

    glGenVertexArrays(1, &vaoId);

    if (!vaoId) {

        std::cerr << "Could not create VAO\n";
        exit(1);

    }

    glBindVertexArray(vaoId);

    glGenBuffers(1, &indexId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indecies.size() * sizeof(int), &indecies[0], GL_STATIC_DRAW);

    glGenBuffers(1, &dataId);
    glBindBuffer(GL_ARRAY_BUFFER, dataId);

    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);

    int vertexSize = data.size()/vertexCount;

    glVertexAttribPointer(0, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(6 * sizeof(float)));
    glVertexAttribPointer(3, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(8 * sizeof(float)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    Model * model = new Model(vaoId, indexId, dataId, indecies.size());

    model->setMeshData(new Math::Mesh(data, vertexCount, indecies));

    return model;

}

Model * createModel(std::vector<float> data, int vertexCount, std::vector<int> indecies, Math::Mesh * mesh) {

    unsigned int vaoId;
    unsigned int dataId;
    unsigned int indexId;

    glGenVertexArrays(1, &vaoId);

    if (!vaoId) {

        std::cerr << "Could not create VAO\n";
        exit(1);

    }

    glBindVertexArray(vaoId);

    glGenBuffers(1, &indexId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indecies.size() * sizeof(int), &indecies[0], GL_STATIC_DRAW);

    glGenBuffers(1, &dataId);
    glBindBuffer(GL_ARRAY_BUFFER, dataId);

    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);

    int vertexSize = data.size()/vertexCount;

    glVertexAttribPointer(0, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(6 * sizeof(float)));
    glVertexAttribPointer(3, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(8 * sizeof(float)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    Model * model = new Model(vaoId, indexId, dataId, indecies.size(), mesh);

    return model;

}

Model * createModel(std::vector<float> data, int vertexCount, std::vector<int> indecies, unsigned int primType) {

    unsigned int vaoId;
    unsigned int dataId;
    unsigned int indexId;

    glGenVertexArrays(1, &vaoId);

    if (!vaoId) {

        std::cerr << "Could not create VAO\n";
        exit(1);

    }

    glBindVertexArray(vaoId);

    glGenBuffers(1, &indexId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indecies.size() * sizeof(int), &indecies[0], GL_STATIC_DRAW);

    glGenBuffers(1, &dataId);
    glBindBuffer(GL_ARRAY_BUFFER, dataId);

    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);

    int vertexSize = data.size()/vertexCount;

    glVertexAttribPointer(0, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(6 * sizeof(float)));
    glVertexAttribPointer(3, 3, GL_FLOAT, 0, vertexSize * sizeof(float), (void*)(8 * sizeof(float)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    return new Model(vaoId, indexId, dataId, indecies.size(), primType);

}

ModelInjector::ModelInjector(std::vector<float> data, int vertexCount, std::vector<int> indecies, Math::Mesh * meshData, unsigned int primType) {

    this->data = data;
    this->vertexCount = vertexCount;

    this->indecies = indecies;
    this->primType = (!primType ? GL_TRIANGLES : primType);

    this->mesh = meshData;
    this->type = getTypeName<Model>();
    this->hasMaterialAttribute = false;

}

ModelInjector::ModelInjector(std::vector<float> data, int vertexCount, std::vector<int> indecies, Math::Mesh * meshData, unsigned int primType, bool material)
    : ModelInjector(data, vertexCount, indecies, meshData, primType)
 {
    this->hasMaterialAttribute = material;
    std::cout << "Vector has " << data.size() << " floats" << std::endl;
 }

Model * ModelInjector::upload() {

    std::cout << "Starting to upload " << getName() << std::endl;
    unsigned int vaoId;
    unsigned int dataId;
    unsigned int indexId;

    std::cout << "Creating Vertex Array Object" << std::endl;
    glGenVertexArrays(1, &vaoId);
    std::cout << "Got adress " << vaoId << std::endl;

    if (!vaoId) {

        std::cerr << "Could not create VAO" << std::endl;
        exit(1);

    }

    std::cout << "Created VAO " << vaoId << std::endl;

    glBindVertexArray(vaoId);

    glGenBuffers(1, &indexId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indecies.size() * sizeof(int), &indecies[0], GL_STATIC_DRAW);

    std::cout << "Index buffer OK" << std::endl;

    int vertCount = vertexCount;
    std::vector<float> modelData = data;//(hasMaterialAttribute) ? data : mesh->getModelData(&vertCount);

    glGenBuffers(1, &dataId);
    glBindBuffer(GL_ARRAY_BUFFER, dataId);

    glBufferData(GL_ARRAY_BUFFER, modelData.size() * sizeof(float), modelData.data(), GL_STATIC_DRAW);

    std::cout << "Data buffer OK" << std::endl;

    int vertexSize = modelData.size()/vertexCount;
    //if (hasMaterialAttribute) vertexSize++;

    std::cout << "Verts are " << modelData.size() << " / " << vertexCount << " = " << vertexSize <<  " floats long" << std::endl;

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertexSize * sizeof(float), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertexSize * sizeof(float), (void*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, vertexSize * sizeof(float), (void*)(6 * sizeof(float)));
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, vertexSize * sizeof(float), (void*)(8 * sizeof(float)));

    int inputCount = 4;

    if (this->hasMaterialAttribute) {
        std::cout << "Adding Material attribute" << std::endl;
        glVertexAttribIPointer(4, 1, GL_UNSIGNED_INT, vertexSize * sizeof(float), (int*)(11 * sizeof(float)));
        inputCount++;

    } else if (vertexSize > 11) {

        glVertexAttribIPointer(4, 4, GL_INT, vertexSize * sizeof(float), (void*)(11 * sizeof(float)));
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, vertexSize * sizeof(float), (void*)(15 * sizeof(float)));
        inputCount = 6;
        std::cout << "Added pointers for animation" << std::endl;

        //exit(1);

    }

    std::cout << "Pointers OK" << std::endl;

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    Model * model = new Model(vaoId, indexId, dataId, indecies.size(), primType, inputCount);

    std::cout << "Model " << model << std::endl;

    model->setMeshData(mesh);

    return model;

}

Math::Mesh * ModelInjector::getMeshData() {
    return mesh;
}

void ModelInjector::deleteTempData() {

    std::vector<float>().swap(data);
    std::vector<int>().swap(this->indecies);

}
