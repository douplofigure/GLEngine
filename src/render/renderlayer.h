#ifndef RENDERLAYER_H
#define RENDERLAYER_H

#include <list>
#include <memory>

#include "camera.h"
#include "renderelement.h"


class RenderLayer
{
    public:
        RenderLayer();
        virtual ~RenderLayer();

        virtual void render(Camera * camera);
        virtual void render(Camera * camera, float clippingDirection);
        virtual void render(Camera * camera, float clippingDirection, glm::vec3 sunDirection);

        void addElement(std::shared_ptr<RenderElement> elem);
        void clear();
        void updateSize(int width, int height);
        std::vector<std::shared_ptr<RenderElement>> getElements();

    protected:

        std::vector<std::shared_ptr<RenderElement>> elements;

    private:
};

#endif // RENDERLAYER_H
