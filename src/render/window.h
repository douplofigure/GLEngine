#ifndef WINDOW_H
#define WINDOW_H

#include <map>
#include <string>

#include "viewport.h"

typedef enum event_type_e {

    EVENT_KEYBOARD,
    EVENT_MOUSE_MOTION,
    EVENT_MOUSE_BUTTON

} EVENT_TYPE;

class Window
{
    public:
        Window(int width, int height, std::string title);
        virtual ~Window();

        bool shouldClose();

        void pollEvents();
        void swapBuffers();

        void updateContents();

        void addViewport(std::string name, Viewport * view);
        void setCurrentViewport(std::string name);
        void updateSize(int width, int height);

        void updateCursor(double x, double y);

        void hideCursor();
        void showCursor();

        int getWidth();
        int getHeight();

        Viewport * currentViewport;

    protected:

        void * window;

        int width;
        int height;

        double cursorX;
        double cursorY;

        std::map<std::string, Viewport *> viewports;

    private:
};

#endif // WINDOW_H
