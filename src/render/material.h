#ifndef MATERIAL_H
#define MATERIAL_H

#include "texture.h"
#include "shader.h"

typedef struct texture_injector_binding_t {

    int slot;
    TextureInjector * injector;
    bool isMap;

} T_INJECTOR_BINDING;

typedef struct texture_binding_t {

    int index;
    Texture * texture;

} TEXTURE_BINDING;

class Material : Resource
{
    public:
        Material(Shader * shader, Shader * staticShader, std::vector<TEXTURE_BINDING> bindings);
        virtual ~Material();

        void addTexture(int slot, Texture * texture);

        std::vector<TEXTURE_BINDING> getBindings();
        Shader * getShader();
        Shader * getStaticShader();

        void deleteData();

    protected:

    private:

        std::vector<TEXTURE_BINDING> textures;
        Shader * shader;
        Shader * staticShader;

};

class MaterialInjector : public ResourceInjector<Material> {

    public:
        MaterialInjector(ShaderInjector * shader, ShaderInjector * sShader);
        virtual ~MaterialInjector();

        virtual Material * upload();
        void deleteTempData();

        void addTexture(int slot, TextureInjector * injector);
        int getTextureCount();
        TextureInjector * getTexture(int slot);

        ShaderInjector * getShader();

    private:

        std::vector<T_INJECTOR_BINDING> bindings;

        ShaderInjector * shaderInjector;
        ShaderInjector * staticInjector;

};

class MaterialLoader : public ResourceLoader<Material> {

    public:

        MaterialLoader(){};
        virtual ~MaterialLoader(){};

        MaterialInjector * create(std::string dir, std::string name);
        bool canCreate(std::string dir, std::string name);
        std::vector<std::string> getFilenames(std::string dir, std::string name);

        static MaterialLoader * buildLoader();

    private:

};

#endif // MATERIAL_H
