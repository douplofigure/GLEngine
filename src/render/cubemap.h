#ifndef CUBEMAP_H
#define CUBEMAP_H

#include "texture.h"

class CubeMap : public Texture
{
    public:

        typedef enum faces_e {

            CUBEMAP_RIGHT,
            CUBEMAP_LEFT,
            CUBEMAP_TOP,
            CUBEMAP_BOTTOM,
            CUBEMAP_BACK,
            CUBEMAP_FRONT

        } FACE;

        CubeMap(unsigned int id);
        virtual ~CubeMap();

        void bindToUnit(int unit);

    protected:

    private:
};

class CubeMapInjector : public ResourceInjector<CubeMap> {

    public:

        CubeMapInjector(int width, int height);
        virtual ~CubeMapInjector();

        CubeMap * upload();

        void addFaceData(CubeMap::FACE face, std::vector<float> data);
        void deleteTempData();

    private:

        std::vector<float> data[6];

        int width;
        int height;


};

#endif // CUBEMAP_H
