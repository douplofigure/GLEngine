#include "staticrenderelement.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <glm/gtc/matrix_access.hpp>

#include "gameengine.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using namespace Math;

struct render_instance_t {

    glm::vec3 position;
    Math::Quaternion rotation;
    double scale;
    glm::mat4 transformationMatrix;

};

StaticRenderElement::StaticRenderElement(Model * model, Shader * shader, int maxInstanceCount) {

    vaoId = model->getVaoId();

    this->shader = shader;
    this->model = model;

    instanceCount = 0;

    glBindVertexArray(vaoId);
    glGenBuffers(1, &transformBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

    glBufferData(GL_ARRAY_BUFFER, 16 * maxInstanceCount, NULL, GL_STREAM_DRAW);

    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)0);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)(4*sizeof(float)));
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)(8*sizeof(float)));
    glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)(12*sizeof(float)));
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);
    glVertexAttribDivisor(7, 1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    this->maxInstanceCount = maxInstanceCount;

    this->linkWithShader();
    this->setScale(1);

    instances = std::vector<RENDER_INSTANCE>(maxInstanceCount);


}

StaticRenderElement::StaticRenderElement(Model * model, Material * material, int maxInstanceCount) : StaticRenderElement(model, material->getStaticShader(), maxInstanceCount) {

    std::vector<TEXTURE_BINDING> tmpBinds = material->getBindings();
    this->textureBindings = std::vector<TEXTURE_BINDING>(tmpBinds.size());
    for (int i = 0; i < tmpBinds.size(); ++i) {

        this->textureBindings[i] = tmpBinds[i];

    }

}

StaticRenderElement::StaticRenderElement(std::string model, std::string shader, int maxInstanceCount) :StaticRenderElement(GameEngine::resources->get<Model>(model), GameEngine::resources->get<Shader>(shader), maxInstanceCount) {

    this->shaderName = shader;
    this->modelName = model;

}

StaticRenderElement::StaticRenderElement(Structure * strc, int maxInstances) {

    this->shader = strc->getMaterial()->getStaticShader();
    this->model = strc->getModel();

    std::vector<TEXTURE_BINDING> tmpBinds = strc->getMaterial()->getBindings();
    this->textureBindings = std::vector<TEXTURE_BINDING>(tmpBinds.size());
    for (int i = 0; i < tmpBinds.size(); ++i) {

        this->textureBindings[i] = tmpBinds[i];

    }

    vaoId = model->getVaoId();

    instanceCount = 0;

    glBindVertexArray(vaoId);
    glGenBuffers(1, &transformBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

    glBufferData(GL_ARRAY_BUFFER, 16 * maxInstances, NULL, GL_STREAM_DRAW);

    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)0);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)(4*sizeof(float)));
    glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)(8*sizeof(float)));
    glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(float), (void*)(12*sizeof(float)));
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);
    glVertexAttribDivisor(7, 1);
    glVertexAttribDivisor(8, 1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    this->maxInstanceCount = maxInstances;

    this->linkWithShader();
    this->setScale(1);

    instances = std::vector<RENDER_INSTANCE>(maxInstances);


}

StaticRenderElement::~StaticRenderElement()
{
    //dtor
}

void StaticRenderElement::linkWithShader() {

    //transformLocation = shader->getUniformLocation(shader->getUniformName("transformationMatrix"));
    projectionLocation = shader->getUniformLocation(shader->getUniformName("projectionMatrix"));
    viewLocation = shader->getUniformLocation(shader->getUniformName("viewMatrix"));

    sunColorLoc = shader->getUniformLocation(shader->getUniformName("ambientColor"));
    sunDirectionLoc = shader->getUniformLocation(shader->getUniformName("ambientDirection"));

    timeLoc = shader->getUniformLocation(shader->getUniformName("time"));

    diffuseMapLoc = shader->getUniformLocation(shader->getUniformName("diffuseMap"));
    normalMapLoc = shader->getUniformLocation(shader->getUniformName("normalMap"));
    specularMapLoc = shader->getUniformLocation(shader->getUniformName("specularMap"));

}

Math::Quaternion StaticRenderElement::getInstanceRotation(int id) {

    return instances[id].rotation;

}

int StaticRenderElement::addInstance(float x, float y, float z) {

    if(instanceCount > maxInstanceCount-1) return -1;

    std::cout << "All ok for adding" << std::endl;

    this->position = glm::vec3(x,y,z);
    this->rotation = Math::Quaternion(1, 0, 0, 0);
    this->scale = glm::vec3(1,1,1);

    glm::mat4 trans = getTransformationMatrix();

    this->instances[instanceCount] = {glm::vec3(x, y, z), Math::Quaternion(1, 0, 0, 0), 1.0, trans};

    glm::vec4 col1 = glm::column(trans, 0);
    glm::vec4 col2 = glm::column(trans, 1);
    glm::vec4 col3 = glm::column(trans, 2);
    glm::vec4 col4 = glm::column(trans, 3);

    std::vector<float> data(16);
    int index = 0;
    data[index++] = col1.x;
    data[index++] = col1.y;
    data[index++] = col1.z;
    data[index++] = col1.w;
    data[index++] = col2.x;
    data[index++] = col2.y;
    data[index++] = col2.z;
    data[index++] = col2.w;
    data[index++] = col3.x;
    data[index++] = col3.y;
    data[index++] = col3.z;
    data[index++] = col3.w;
    data[index++] = col4.x;
    data[index++] = col4.y;
    data[index++] = col4.z;
    data[index++] = col4.w;

    glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

    glBufferSubData(GL_ARRAY_BUFFER, 16 * instanceCount * sizeof(float), 16 * sizeof(float), data.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    instanceCount++;

    std::cout << "Instance count : " << instanceCount << std::endl;

    return instanceCount-1;

}

int StaticRenderElement::removeInstance(int id) {

    RENDER_INSTANCE lastInstance = this->instances[instanceCount-1];

    this->updateInstance(id, lastInstance.position, lastInstance.rotation, lastInstance.scale);

    instanceCount--;

    return instanceCount;

}

void StaticRenderElement::updateInstance(int id, glm::vec3 pos, Math::Quaternion rot, float scale) {

    this->position = glm::vec3(pos.x, pos.y, pos.z);
    this->rotation = rot;

    this->setScale(scale);

    glm::mat4 trans = getTransformationMatrix();

    glm::vec4 col1 = glm::column(trans, 0);
    glm::vec4 col2 = glm::column(trans, 1);
    glm::vec4 col3 = glm::column(trans, 2);
    glm::vec4 col4 = glm::column(trans, 3);

    this->instances[id] = {pos, rot, scale, trans};

    std::vector<float> data(16);
    int index = 0;
    data[index++] = col1.x;
    data[index++] = col1.y;
    data[index++] = col1.z;
    data[index++] = col1.w;
    data[index++] = col2.x;
    data[index++] = col2.y;
    data[index++] = col2.z;
    data[index++] = col2.w;
    data[index++] = col3.x;
    data[index++] = col3.y;
    data[index++] = col3.z;
    data[index++] = col3.w;
    data[index++] = col4.x;
    data[index++] = col4.y;
    data[index++] = col4.z;
    data[index++] = col4.w;

    glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

    glBufferSubData(GL_ARRAY_BUFFER, 16 * id * sizeof(float), 16 * sizeof(float), data.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);

}

void StaticRenderElement::render() {

    glBindVertexArray(vaoId);

    model->activateAttributes(4);

    glDrawElementsInstanced(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0, instanceCount);

    model->deactivateAttributes(4);

    glBindVertexArray(0);

}

void StaticRenderElement::render(Camera * camera) {

    int toRender = 0;
    std::vector<float> data(16);
    //std::cout << instanceCount << std::endl;
    /*for (int i = 0; i < instanceCount; ++i) {

        glm::vec3 v = (instances[i].position - camera->position);
        glm::vec3 f = camera->facing;

        //std::cout << instances[i].position.x << " " << instances[i].position.y << " " << instances[i].position.z << std::endl;

        if ( camera->canSee(instances[i].position, 20)) {

            glm::vec4 col1 = glm::column(instances[i].transformationMatrix, 0);
            glm::vec4 col2 = glm::column(instances[i].transformationMatrix, 1);
            glm::vec4 col3 = glm::column(instances[i].transformationMatrix, 2);
            glm::vec4 col4 = glm::column(instances[i].transformationMatrix, 3);

            int index = 0;
            data[index++] = col1.x;
            data[index++] = col1.y;
            data[index++] = col1.z;
            data[index++] = col1.w;
            data[index++] = col2.x;
            data[index++] = col2.y;
            data[index++] = col2.z;
            data[index++] = col2.w;
            data[index++] = col3.x;
            data[index++] = col3.y;
            data[index++] = col3.z;
            data[index++] = col3.w;
            data[index++] = col4.x;
            data[index++] = col4.y;
            data[index++] = col4.z;
            data[index++] = col4.w;

            glBindBuffer(GL_ARRAY_BUFFER, transformBufferId);

            glBufferSubData(GL_ARRAY_BUFFER, 16 * toRender * sizeof(float), 16 * sizeof(float), data.data());

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            toRender++;

        }

    }*/
    glBindVertexArray(vaoId);

    model->activateAttributes(4);

    glDrawElementsInstanced(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0, instanceCount);

    model->deactivateAttributes(4);

    glBindVertexArray(0);

}

int StaticRenderElement::getInstanceCount() {
    return instanceCount;
}

void StaticRenderElement::prepareUniforms(Camera * camera) {

    /*shader->loadInt(diffuseMapLoc, 0);
    shader->loadInt(normalMapLoc, 1);
    shader->loadInt(specularMapLoc, 2);*/

    shader->setTextureUniforms();

    this->shader->loadMatrix(projectionLocation, camera->getProjectionMatrix());
    this->shader->loadMatrix(viewLocation, camera->getViewMatrix());

    shader->loadVec3(sunDirectionLoc, glm::vec3(-1,-1,0));
    shader->loadFloat(timeLoc, GameEngine::getTime());

}

void StaticRenderElement::prepareUniforms(Camera * camera, glm::vec3 sunDir) {

    /*shader->loadInt(diffuseMapLoc, 0);
    shader->loadInt(normalMapLoc, 1);
    shader->loadInt(specularMapLoc, 2);*/

    shader->setTextureUniforms();

    this->shader->loadMatrix(projectionLocation, camera->getProjectionMatrix());
    this->shader->loadMatrix(viewLocation, camera->getViewMatrix());

    shader->loadVec3(sunDirectionLoc, sunDir);
    shader->loadFloat(timeLoc, GameEngine::getTime());

}

Mesh * StaticRenderElement::getCombinedMesh() {

    Matrix4 mat = Matrix4::fromGLM(instances[0].transformationMatrix);
    Mesh * res = Mesh::withTransform(model->getMeshData(), mat);
    for (int i = 1; i < instanceCount; ++i) {

        mat = Matrix4::fromGLM(instances[i].transformationMatrix);
        Mesh * mesh = Mesh::withTransform(model->getMeshData(), mat);
        res->mergeWith(mesh);
        delete mesh;
        std::cout << "added " << i << " / " << instanceCount << "\n";

    }
    return res;

}

glm::vec3 StaticRenderElement::getInstancePosition(int id) {
    return this->instances[id].position;
}

double StaticRenderElement::getInstanceScale(int id) {
    return this->instances[id].scale;
}

CompoundNode * StaticRenderElement::asCompound() {

    CompoundNode * rootNode = new CompoundNode();

    rootNode->addChildNode((Node<void *>*) new Node<const char *>("model", (const char *) this->modelName.c_str()));
    rootNode->addChildNode((Node<void *>*) new Node<const char *>("shader", (const char *) this->shaderName.c_str()));
    rootNode->addChildNode((Node<void *>*) new Node<int>("type", RenderElementType::STATIC));
    rootNode->addChildNode((Node<void *>*) new Node<int>("maxCount", this->maxInstanceCount));

    return rootNode;

}
