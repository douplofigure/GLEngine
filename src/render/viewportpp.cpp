#include "viewportpp.h"

#include <iostream>

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include "resources/resourcemanager.h"
#include "gameengine.h"
// TODO (david#2#): Add Shadow mapping
#include "ui/uielement.h"

//const float skyColor[3] = {0.521568627, 0.760784314, 0.858823529};

const float skyColor[3] = {0.171711, 0.61184, 0.811765};

ViewportPP::ViewportPP(int width, int height, float fov, float zNear, float zFar, int bufferWidth, int bufferHeight, int shadowRes) : Viewport(width, height, fov, zNear, zFar) {

    unsigned int colorId;
    unsigned int lightId;
    unsigned int depthId;

    /*unsigned int rcolorId;
    unsigned int rlightId;
    unsigned int rdepthId;*/

    unsigned int reflectionId;
    unsigned int reflectionDepthId;

    unsigned int refractionId;
    unsigned int refractionDepthId;

    unsigned int shadowId;
    unsigned int shadowColorId;

    this->bufferWidth = bufferWidth;
    this->bufferHeight = bufferHeight;

    this->reflectionWidth = bufferWidth;
    this->reflectionHeight = bufferHeight;

    /// 3D Effect Start
    /*glGenFramebuffers(1, &rightBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, rightBuffer);

    glGenTextures(1, &rcolorId);
    glBindTexture(GL_TEXTURE_2D, rcolorId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenTextures(1, &rdepthId);
	glBindTexture(GL_TEXTURE_2D, rdepthId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->bufferWidth, this->bufferHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rcolorId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, rdepthId, 0);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_NONE);

	this->rightColor = new Texture(rcolorId, bufferWidth, bufferHeight, GL_TEXTURE_2D);
	this->rightDepth = new Texture(rdepthId, bufferWidth, bufferHeight, GL_TEXTURE_2D);*/

	/// END OF 3D effect
	glGenFramebuffers(1, &ppBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, ppBuffer);

    glGenTextures(1, &colorId);
    glBindTexture(GL_TEXTURE_2D, colorId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenTextures(1, &lightId);
    glBindTexture(GL_TEXTURE_2D, lightId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenTextures(1, &depthId);
	glBindTexture(GL_TEXTURE_2D, depthId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->bufferWidth, this->bufferHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, lightId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthId, 0);

	GLenum buffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

	glDrawBuffers(2, buffers);
	glReadBuffer(GL_NONE);

	glGenFramebuffers(1, &reflectionBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, reflectionBuffer);

	glGenTextures(1, &reflectionId);
	glBindTexture(GL_TEXTURE_2D, reflectionId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, reflectionWidth, reflectionHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenTextures(1, &reflectionDepthId);
	glBindTexture(GL_TEXTURE_2D, reflectionDepthId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, reflectionWidth, reflectionHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, reflectionId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, reflectionDepthId, 0);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_NONE);

	glGenFramebuffers(1, &refractionBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, refractionBuffer);

	glGenTextures(1, &refractionId);
	glBindTexture(GL_TEXTURE_2D, refractionId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, reflectionWidth, reflectionHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenTextures(1, &refractionDepthId);
	glBindTexture(GL_TEXTURE_2D, refractionDepthId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, reflectionWidth, reflectionHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, refractionId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, refractionDepthId, 0);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_NONE);

	this->ppColor = new Texture(colorId, bufferWidth, bufferHeight, GL_TEXTURE_2D);
	this->ppDepth = new Texture(depthId, bufferWidth, bufferHeight, GL_TEXTURE_2D);
	this->ppLight = new Texture(lightId, bufferWidth, bufferHeight, GL_TEXTURE_2D);

	std::cout << "Loading pp shader" << std::endl;
	this->ppShader = GameEngine::resources->get<Shader>("post_processing");
	//this->ppShader = ResourceManager::getShader("low_poly");
	std::cout << "Loading pp model" << std::endl;
	this->ppModel = GameEngine::resources->get<Model>("quad");

	this->reflectionTexture = new Texture(reflectionId, bufferWidth, bufferHeight, GL_TEXTURE_2D);
	this->refractionTexture = new Texture(refractionId, bufferWidth, bufferHeight, GL_TEXTURE_2D);
	this->refractionDepth = new Texture(refractionDepthId, bufferWidth, bufferHeight, GL_TEXTURE_2D);

	std::cout << "Setting Uniforms" << std::endl;

    ppShader->loadInt(ppShader->getUniformLocation("colorRender"), 0);
    ppShader->loadInt(ppShader->getUniformLocation("depthRender"), 1);
    ppShader->loadInt(ppShader->getUniformLocation("lightRender"), 2);

    ///Setting up shadow map
    std::cout << "Creating shadow map" << std::endl;
    this->shadowBufferHeight = shadowRes;
    this->shadowBufferWidth = shadowRes;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenFramebuffers(1, &shadowBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowBuffer);

    glGenTextures(1, &shadowColorId);
	glBindTexture(GL_TEXTURE_2D, shadowColorId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, shadowBufferWidth, shadowBufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glGenTextures(1, &shadowId);
    glBindTexture(GL_TEXTURE_2D, shadowId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->shadowBufferWidth, this->shadowBufferHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, shadowColorId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowId, 0);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	std::cout << "Shadow Buffer OK" << std::endl;

	this->shadowMap = new Texture(shadowId, shadowRes, shadowRes, GL_TEXTURE_2D);
	this->shadowColor = new Texture(shadowColorId, shadowRes, shadowRes, GL_TEXTURE_2D);

	this->reflectionCamera = new Camera(this->camera->getFov(), this->camera->getAspect(), this->camera->getNear(), this->camera->getFar());

	this->reflectionPass = new RenderPass(reflectionWidth, reflectionHeight);
	this->reflectionPass->setCamera(reflectionCamera);
	this->reflectionPass->addLayer(this->layers[0]);

	this->reflectionPass->activateOption(GL_CLIP_DISTANCE0);
	this->reflectionPass->activateOption(GL_DEPTH_TEST);
	this->reflectionPass->activateOption(GL_TEXTURE_2D);
	this->reflectionPass->activateOption(GL_CULL_FACE);

	this->refractionPass = new RenderPass(reflectionWidth, reflectionHeight);
	this->refractionPass->setCamera(this->camera);
	this->refractionPass->addLayer(layers[0]);

	this->refractionPass->activateOption(GL_CLIP_DISTANCE0);
	this->refractionPass->activateOption(GL_DEPTH_TEST);
	this->refractionPass->activateOption(GL_TEXTURE_2D);
	this->refractionPass->activateOption(GL_CULL_FACE);

	sunDirection = glm::vec3(0.5, -1, -1);

	this->shadowCam = new ShadowCamera(camera);
	//this->rightEye = new Camera(this->camera->getFov(), this->camera->getAspect(), this->camera->getNear(), this->camera->getFar());

    std::cout << "Viewport constructor OK" << std::endl;

}

ViewportPP::~ViewportPP()
{
    //dtor
}

void ViewportPP::addLayer() {

    RenderLayer * layer = new RenderLayer();
    this->layers.insert(layers.end()-1, layer);
    this->reflectionPass->addLayer(layer);
    this->refractionPass->addLayer(layer);

}

void ViewportPP::addWaterElement(std::shared_ptr<RenderElement> elem) {

    if (!hasWaterLayer) {
        this->addLayer();
    }

    //elem->setDiffuseMap(reflectionPass->getColorTexture());
    //elem->setNormalMap(refractionPass->getDepthTexture());
    elem->setDiffuseMap(this->reflectionTexture);
    elem->setSpecularMap(this->refractionTexture);
    elem->setNormalMap(this->refractionDepth);

    this->addElement(this->layers.size()-2, elem);

}

void ViewportPP::addElement(int layer, std::shared_ptr<RenderElement> elem) {

    elem->setShadowMap(shadowMap);
    this->layers[layer]->addElement(elem);

}

const double plane[4] = {0,1,0,0};

void ViewportPP::renderElements() {

    Model * model;
    Shader * shader;

    //this->sunDirection = glm::vec3(-cos(GameEngine::getTime() * 0.1), -sin(GameEngine::getTime() * 0.1), -0.5);

    this->reflectionCamera->position = this->camera->position;
    this->reflectionCamera->position.y *= -1;

    glm::vec3 target = this->camera->position + this->camera->facing;
    target.y *= -1;
    this->reflectionCamera->facing = target - this->reflectionCamera->position;
    //this->reflectionCamera->facing.x = -1.0;

    this->reflectionCamera->updateViewMatrix();

    /*this->rightEye->position = this->camera->position;
    glm::vec3 delta = glm::vec3(camera->facing.z, 0, -camera->facing.x);
    this->rightEye->position.x += 0.06 * delta.x;
    this->rightEye->position.z += 0.06 * delta.z;
    this->rightEye->facing = camera->facing;
    this->rightEye->updateViewMatrix();*/

    this->shadowCam->setSunDirection(sunDirection);
    this->shadowCam->updateViewMatrix();

    glm::mat4 projection = this->reflectionCamera->getProjectionMatrix();
    glm::mat4 view = this->reflectionCamera->getViewMatrix();

    /*std::vector<RenderLayer*> tmpLayers = reflectionPass->getLayers();

    reflectionPass->prepareRender();

    for (int i = 0; i < tmpLayers.size(); ++i) {
        tmpLayers[i]->render(reflectionPass->getCamera(), 1.0);
    }

    reflectionPass->endRender();*/

    ///Starting ShadowPass
    /*std::cout << "Shadow pass" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, shadowBuffer);
    glViewport(0, 0, shadowBufferWidth, shadowBufferWidth);
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glEnable(GL_CLIP_DISTANCE0);

    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < layers.size()-2; ++i) {

        layers[i]->render(shadowCam);

    }

    glDisable(GL_BLEND);
    glDisable(GL_CLIP_DISTANCE0);*/


    ///Starting Reflection Pass

    glBindFramebuffer(GL_FRAMEBUFFER, reflectionBuffer);
    glViewport(0, 0, reflectionWidth, reflectionHeight);
    glClearColor(this->cr, this->cg, this->cb, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glClipPlane(GL_CLIP_PLANE0, (GLdouble*)&plane);
    glEnable(GL_CLIP_DISTANCE0);

    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < layers.size()-2; ++i) {

        layers[i]->render(reflectionCamera, 1.0, sunDirection);

    }

    glDisable(GL_BLEND);
    glDisable(GL_CLIP_DISTANCE0);

    /*tmpLayers = refractionPass->getLayers();

    refractionPass->prepareRender();

    for (int i = 0; i < tmpLayers.size(); ++i) {
        tmpLayers[i]->render(refractionPass->getCamera(), -1.0);
    }

    refractionPass->endRender();*/

    ///Starting Refraction Pass

    projection = this->camera->getProjectionMatrix();
    view = this->camera->getViewMatrix();

    glBindFramebuffer(GL_FRAMEBUFFER, refractionBuffer);
    glViewport(0, 0, reflectionWidth, reflectionHeight);
    glClearColor(this->cr, this->cg, this->cb, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glClipPlane(GL_CLIP_PLANE0, (GLdouble*)&plane);
    glEnable(GL_CLIP_DISTANCE0);

    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < layers.size()-2; ++i) {

        layers[i]->render(camera, -1.0, sunDirection);

    }

    glDisable(GL_BLEND);
    glDisable(GL_CLIP_DISTANCE0);

    ///Starting World Pass

    glBindFramebuffer(GL_FRAMEBUFFER, ppBuffer);
    glViewport(0,0,bufferWidth, bufferHeight);
    glClearColor(this->cr, this->cg, this->cb, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //glViewport(0,0, bufferWidth, bufferHeight);

    //glEnable(GL_CLIP_DISTANCE0);

    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < layers.size()-1; ++i) {

        layers[i]->render(camera, 0.0, sunDirection);

    }

    glDisable(GL_BLEND);
    //glDisable(GL_CLIP_DISTANCE0);

    /*glBindFramebuffer(GL_FRAMEBUFFER, rightBuffer);
    glViewport(0,0,bufferWidth, bufferHeight);
    glClearColor(skyColor[0],skyColor[1],skyColor[2],1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //glViewport(0,0, bufferWidth, bufferHeight);

    glEnable(GL_CLIP_DISTANCE0);

    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < layers.size()-1; ++i) {

        layers[i]->render(rightEye, 0.0, sunDirection);

    }*/

    /// Blitting to screen

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0,0, width, height);
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glDisable(GL_CLIP_DISTANCE0);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);


    ppShader->bindForRender();

    ppShader->loadVec3(ppShader->getUniformLocation("cameraPos"), camera->position);

    ppShader->loadInt(ppShader->getUniformLocation("depthRender"), 1);
    ppShader->loadInt(ppShader->getUniformLocation("lightRender"), 2);

    ppColor->bindToUnit(0);
    ppDepth->bindToUnit(1);
//    rightColor->bindToUnit(2);
    //ppLight->bindToUnit(2);

    //reflectionTexture->bindToUnit(0);

    //shadowColor->bindToUnit(0);

    glBindVertexArray(ppModel->getVaoId());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glDrawElements(ppModel->getPrimitiveType(), ppModel->getIndexCount(), GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);

    ppShader->unbindForRender();

    layers[layers.size()-1]->render(camera);

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

}

void ViewportPP::setFov(float fov) {
    this->camera->updateProjection(fov, camera->getAspect(), camera->getNear(), camera->getFar());
    this->reflectionCamera->updateProjection(fov, reflectionCamera->getAspect(), reflectionCamera->getNear(), reflectionCamera->getFar());
}
