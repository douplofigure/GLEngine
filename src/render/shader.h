#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <map>
#include <glm/matrix.hpp>

#include "resources/resourceregistry.h"

typedef struct texture_slot_t {

    int textureUnit;
    unsigned int uniformLoc;

} TEXTURE_SLOT;

class Shader : Resource
{
    public:
        Shader(unsigned int programId, unsigned int vertexShaderId, unsigned int fragmentShaderId);
        Shader(unsigned int programId, unsigned int vertexShaderId, unsigned int fragmentShaderId, std::unordered_map<std::string, std::string> uniformNameMap);
        virtual ~Shader();

        unsigned int getProgramId();

        virtual void bindForRender();
        virtual void unbindForRender();

        unsigned int getUniformLocation(std::string name);

        void loadMatrix(unsigned int location, glm::mat4 matrix);
        void loadVec3(unsigned int location, glm::vec3 vec);
        void loadInt(unsigned int location, int i);
        void loadFloat(unsigned int location, float f);

        void addTextureUniform(std::string name, int unit);

        void setTextureUniforms();

        void deleteData();

        void setUniformName(std::string uniform, std::string name);
        std::string getUniformName(std::string uniform);

    protected:

        unsigned int programId;
        unsigned int vertexShaderId;
        unsigned int geometryShaderId;
        unsigned int fragmentShaderId;

        bool hasGeometryShader;

        std::vector<TEXTURE_SLOT> textureSlots;

        std::unordered_map<std::string, std::string> uniformNameMap;

    private:
};

class ShaderInjector : public ResourceInjector<Shader> {

    public:
        ShaderInjector(std::string vertexSource, std::string fragmentSource);
        ShaderInjector(std::string vertexSource, std::string geometrySource, std::string fragmentSource);

        ShaderInjector(){};

        void setUniformName(std::string uniform, std::string name);
        void setAttribute(int id, std::string name);
        void setTextureSlot(std::string name, int slot);

        void deleteTempData();

        std::map<std::string, int> getTextureBindings();
        std::string getFragmentSource();
        std::string getVertexSource();

        Shader * upload();

    private:

        int createStage(std::string source, int type);

        std::string vertexSource;
        std::string fragmentSource;
        std::string geometrySource;

        bool hasGeometryStage;

        std::unordered_map<std::string, std::string> uniformNameMap;
        std::map<int, std::string> attributes;
        std::map<std::string, int> textureUnits;

};

class ShaderLoader : public ResourceLoader<Shader> {

    public:

        ShaderLoader();

        ResourceInjector<Shader> * create(std::string dir, std::string name) override;
        bool canCreate(std::string dir, std::string name) override;

        ShaderLoader * setSuffixes(std::string vert, std::string frag);
        std::vector<std::string> getFilenames(std::string dir, std::string name);

        static ShaderLoader * buildLoader();

    private:

        std::string loadFile(std::string fname);

        std::string vertexSuffix;
        std::string fragmentSuffix;

};

Shader * createShader(std::string vertexName, std::string fragmentName);

#endif // SHADER_H
