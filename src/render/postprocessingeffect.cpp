#include "postprocessingeffect.h"

PostProcessingEffect::PostProcessingEffect(Shader * shader, int width, int height) {

    std::vector<unsigned int> attachments(1);
    attachments[0] = RenderBuffer::ATTACHMENT_COLOR_8;

    this->shader = shader;
    this->renderBuffer = new RenderBuffer(width, height, attachments);

}

PostProcessingEffect::~PostProcessingEffect()
{
    //dtor
}

Shader * PostProcessingEffect::getShader() {
    return this->shader;
}

RenderBuffer * PostProcessingEffect::getRenderBuffer() {
    return renderBuffer;
}

Texture * PostProcessingEffect::getTexture() {
    return renderBuffer->getTexture(0);
}

void PostProcessingEffect::bindTextures(Texture * tex) {

    tex->bindToUnit(0);

    for (int i = 0; i < rTextures.size(); ++i) {

        rTextures[i]->bindToUnit(i+1);

    }

}

void PostProcessingEffect::addTexture(Texture * tex) {

    this->rTextures.push_back(tex);

}

void PostProcessingEffect::changeTexture(int slot, Texture * tex) {

    if (rTextures.size() <= slot) return;
    rTextures[slot] = tex;

}
