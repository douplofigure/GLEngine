#ifndef CAMERA_H
#define CAMERA_H

#include <glm/vec3.hpp>
#include <glm/matrix.hpp>

class Camera {

    public:
        Camera(float fov, float aspect, float nearPlane, float farPlane);
        virtual ~Camera();

        glm::mat4 getProjectionMatrix();
        glm::mat4 getViewMatrix();

        virtual void moveRelative(glm::vec3 dir, double distance);
        virtual void moveAbsolute(glm::vec3 vec);

        virtual void changeRotation(double dPitch, double dYaw, double dRoll);
        virtual void updateProjection(float fov, float aspect, float nearPlane, float farPlane);
        virtual void updateViewMatrix();

        void endFrame();

        float getFov();
        float getAspect();
        float getNear();
        float getFar();

        virtual double getYaw();

        bool canSee(glm::vec3 position, float radius);

        float getDepthBlur();

        glm::vec3 positionAsFloat();
        glm::vec3 facing;


        glm::vec3 position;
        glm::vec3 rotation;

    protected:

        Camera();

        glm::mat4 projectionMatrix;
        glm::mat4 viewMatrix;

        double yaw;

    private:

        float fov;
        float aspect;
        float nearPlane;
        float farPlane;

};

#endif // CAMERA_H
