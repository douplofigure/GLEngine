#include "camera.h"

#include <math.h>

#include <glm/gtc/matrix_transform.hpp>

#define radians(x) (x/180 * M_PI)
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

Camera::Camera() {



}

Camera::Camera(float fov, float aspect, float nearPlane, float farPlane) {

	this->position = glm::vec3(0,0,0);
	this->rotation = glm::vec3(0,0,0);
	this->facing = glm::vec3(0,0,1);

	this->yaw = 0;

	this->fov = fov;
	this->aspect = aspect;
	this->nearPlane = nearPlane;
	this->farPlane = farPlane;

	this->updateProjection(fov, aspect, nearPlane, farPlane);

	updateViewMatrix();


}

Camera::~Camera()
{
    //dtor
}

glm::mat4 Camera::getProjectionMatrix() {

    return this->projectionMatrix;

}

glm::mat4 Camera::getViewMatrix(){

    return this->viewMatrix;

}

double Camera::getYaw() {
    return yaw;
}

glm::vec3 Camera::positionAsFloat(){

    return position;

}

void Camera::updateViewMatrix(){

    this->viewMatrix = glm::lookAt(position, position + facing, glm::vec3(0,1,0));

}

void Camera::changeRotation(double dPitch, double dYaw, double dRoll) {


    this->rotation.x += dPitch;

    if (rotation.x > M_PI / 2) rotation.x = M_PI / 2;
    if (rotation.x < - M_PI / 2) rotation.x = -M_PI / 2;
    this->rotation.y += dYaw;
    this->rotation.z += dRoll;

    this->facing.y = sin(rotation.x) * sqrt(facing.x*facing.x + facing.y*facing.y + facing.z*facing.z);

    this->facing.x = cos(dYaw) * facing.x + sin(dYaw) * facing.z;
    this->facing.z = -sin(dYaw) * facing.x + cos(dYaw) * facing.z;

    double length = sqrt(facing.x*facing.x + facing.y*facing.y + facing.z*facing.z);

    facing.x /= length;
    facing.y /= length;
    facing.z /= length;

    this->updateViewMatrix();

}

void Camera::moveRelative(glm::vec3 dir, double distance) {

    double dx = (-dir.x * facing.z+ dir.z * facing.x) * distance;
    double dy = (dir.y * distance);
    double dz = (dir.x * facing.x + dir.z * facing.z) * distance;
    moveAbsolute(glm::vec3(dx, dy, dz));

}

void Camera::updateProjection(float fov, float aspect, float nearPlane, float farPlane) {

    this->fov = fov;
	this->aspect = aspect;
	this->nearPlane = nearPlane;
	this->farPlane = farPlane;

	this->projectionMatrix = glm::perspective((float)radians(fov), aspect, nearPlane, farPlane);

}

void Camera::moveAbsolute(glm::vec3 vec) {

    this->position.x += vec.x;
    this->position.y += vec.y;
    this->position.z += vec.z;

    this->updateViewMatrix();

}

void Camera::endFrame() {

    //this->updateProjection();
    this->updateViewMatrix();

}

float Camera::getDepthBlur() {

    return 0.125;

}

bool Camera::canSee(glm::vec3 pos, float radius) {

    glm::vec3 toElem = pos - this->position;
    return true;
    return (toElem.x * facing.x + toElem.y * facing.y + toElem.z * facing.z) > 0;

}

float Camera::getAspect() {
    return aspect;
}

float Camera::getFov() {
    return fov;
}

float Camera::getFar(){
    return farPlane;
}

float Camera::getNear() {
    return nearPlane;
}
