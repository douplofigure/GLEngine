#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <list>

#include "camera.h"
#include "renderelement.h"
#include "renderlayer.h"

typedef enum light_type_e {

    L_DISABLED,
    L_POINT,
    L_SUN,
    L_FLICKER,
    L_SPOT

} LIGHT_TYPE;

class Viewport
{
    public:
        Viewport(int width, int height, float fov, float zNear, float zFar);
        virtual ~Viewport();

        virtual void updateSize(int newWidth, int newHeight);
        virtual void setFov(float fov);
        virtual void renderElements();

        Camera * getCamera();
        void addElement(int layer, std::shared_ptr<RenderElement> elem);
        void clearElements();
        void addUIElement(std::shared_ptr<RenderElement> elem);

        virtual void addLayer();
        void addUILayer();

        void setClearColor(float r, float g, float b);

        virtual void addStructureLights(Structure * strc, Math::Vector3 pos, Math::Quaternion rot, double scale);
        virtual int addLight(Math::Vector3 pos, Math::Vector3 color, LIGHT_TYPE type, double falloff);
        virtual int addLight(glm::vec3 pos, glm::vec3 rot, LIGHT_TYPE type, float falloff);
        virtual void setLightAsSun(int lightIndex);

        virtual void clearLights();


    protected:

        int width;
        int height;

        float fov;
        float zNear;
        float zFar;

        Camera * camera;

        std::vector<RenderLayer *> layers;
        std::vector<RenderLayer *> uiLayers;

        float cr;
        float cg;
        float cb;

    private:
};

#endif // VIEWPORT_H
