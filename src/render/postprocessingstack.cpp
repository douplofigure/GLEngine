#include "postprocessingstack.h"

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include "gameengine.h"

PostProcessingStack::PostProcessingStack() {

    this->quad = GameEngine::resources->get<Model>("quad");

}

PostProcessingStack::~PostProcessingStack()
{
    //dtor
}


Texture * PostProcessingStack::getResult(RenderBuffer * buffer) {

    if (effects.size() <= 0) {
        return buffer->getTexture(0);
    }

    this->applyToBuffer(buffer);
    return this->effects[effects.size()-1]->getTexture();

}

void PostProcessingStack::applyToBuffer(RenderBuffer * buffer) {

    for (int i = 0; i < effects.size(); ++i) {

        effects[i]->getRenderBuffer()->bindForRender();
        glViewport(0,0, effects[i]->getRenderBuffer()->getWidth(), effects[i]->getRenderBuffer()->getHeight());

        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

        effects[i]->getShader()->bindForRender();

        effects[i]->getShader()->setTextureUniforms();

        Texture * tex;

        if (!i) {

            tex = buffer->getTexture(0);//->bindToUnit(0);
            //tex = GameEngine::resources->get<Texture>("luts/neutral");

        } else {

            tex = effects[i-1]->getTexture();//->bindToUnit(0);

        }

        effects[i]->bindTextures(tex);

        glBindVertexArray(quad->getVaoId());
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(3);

        glDrawElements(quad->getPrimitiveType(), quad->getIndexCount(), GL_UNSIGNED_INT, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(3);

        effects[i]->getShader()->unbindForRender();

    }

}

void PostProcessingStack::addEffect(PostProcessingEffect * effect) {

    this->effects.push_back(effect);

}

void PostProcessingStack::addEffect(Shader * shader, int width, int height) {

    this->effects.push_back(new PostProcessingEffect(shader, width, height));

}

PostProcessingEffect * PostProcessingStack::getEffect(int num) {
    return this->effects[num];
}
