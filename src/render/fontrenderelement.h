#ifndef FONTRENDERELEMENT_H
#define FONTRENDERELEMENT_H

#include "renderelement.h"
#include "font/font.h"

class FontRenderElement : public RenderElement
{
    public:

        FontRenderElement(std::string text, Font * font);
        FontRenderElement(std::string text, Font * font, bool isUI);
        virtual ~FontRenderElement();

        void setTextColor(float r, float g, float b);
        void setText(std::string text);

        virtual void prepareUniforms(Camera * camera);

    protected:

        void constructModel();
        void linkWithShader();

        Font * font;
        std::string text;
        bool hasUpdated;
        bool isUI;

        glm::vec3 txtColor;

        int colorLocation;

    private:
};

#endif // FONTRENDERELEMENT_H
