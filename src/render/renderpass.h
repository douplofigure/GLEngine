#ifndef RENDERPASS_H
#define RENDERPASS_H

#include <vector>
#include "texture.h"
#include "camera.h"
#include "renderlayer.h"

class RenderPass {

    public:
        /** Default constructor */
        RenderPass(int bufferWidth, int bufferHeight);
        /** Default destructor */
        virtual ~RenderPass();

        void prepareRender();
        void endRender();

        void activateOption(int option);

        void updateBufferSize(int width, int height);

        Texture * getColorTexture();
        Texture * getDepthTexture();

        Camera * getCamera();
        void setCamera(Camera * camera);

        void addLayer(RenderLayer * layer);
        std::vector<RenderLayer *> getLayers();

    protected:

        std::vector<RenderLayer*> layers;

        glm::vec3 bgColor;

    private:

        unsigned int bufferId;

        int bufferWidth;
        int bufferHeight;

        Texture * colorOutput;
        Texture * depthOutput;

        std::vector<int> glOptions;

        Camera * camera;

};

#endif // RENDERPASS_H
