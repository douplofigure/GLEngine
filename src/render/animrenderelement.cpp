#include "animrenderelement.h"

#include <configloader.h>

#include "gameengine.h"
#include "resources/modelloader.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "animation/animationrig.h"

#define MAX_BONE_COUNT 128

using namespace Math;
using namespace Anim;

AnimRenderElement::AnimRenderElement(AnimationRig * rig, Material * mat, float x, float y, float z) : RenderElement(rig->getModel(), mat, x, y,z){//} : RenderElement(GameEngine::resources->get<Model>("cube"), GameEngine::resources->get<Material>("planks"), 0, 0, 0) {

    this->animations = rig->getAnimations();
    this->armature = rig->getArmature();

    this->currentAnimation = animations["animation"];

    this->shader = GameEngine::resources->get<Shader>("anim_texture");

    ///Actual usefull stuff

    this->rotationData = (float*) malloc(sizeof(float) * MAX_BONE_COUNT * 4);
    this->offsetData = (float*) malloc(sizeof(float) * MAX_BONE_COUNT * 4);

    this->boneRotationLoc = shader->getUniformLocation("boneRotations");
    this->boneOffsetLoc = shader->getUniformLocation("boneOffsets");

    rotationData[0] = 1;
    rotationData[1] = 0;
    rotationData[2] = 0;
    rotationData[3] = 0;

    offsetData[0] = 0;
    offsetData[1] = 0;
    offsetData[2] = 0;
    offsetData[3] = 0;

    this->createBoneModel();

}

AnimRenderElement::~AnimRenderElement()
{
    free(rotationData);
    free(offsetData);
}

void AnimRenderElement::createBoneModel() {

    this->armature->reset();

    double t = fmod(GameEngine::getTime(), this->currentAnimation->getDuration());
    this->currentAnimation->applyTo(this->armature, t);

    for (int i = 1; i < armature->getBoneCount()+1; ++i) {

        Quaternion q = armature->getBoneRotation(i-1);
        Vector3 o = armature->getBoneOffset(i-1);
        Vector3 p = armature->getBonePosition(i-1);

        rotationData[i*4] = q.a;
        rotationData[i*4+1] = q.b;
        rotationData[i*4+2] = q.c;
        rotationData[i*4+3] = q.d;

        offsetData[i*4] = o.x;
        offsetData[i*4+1] = o.y;
        offsetData[i*4+2] = o.z;
        offsetData[i*4+3] = 0;

    }

}

void AnimRenderElement::render() {

    if (!model->isReady()) return;
    glBindVertexArray(model->getVaoId());

    model->activateAttributes(0);

    glDrawElements(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0);

    model->deactivateAttributes(0);

    glBindVertexArray(0);

}

void AnimRenderElement::render(Camera * camera) {

    if (!model->isReady()) return;

    glBindVertexArray(model->getVaoId());

    model->activateAttributes(0);

    glDrawElements(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0);

    model->deactivateAttributes(0);

    glBindVertexArray(0);

}
void AnimRenderElement::prepareUniforms(Camera * camera) {

    this->createBoneModel();
    if (!isReady) return;
    if (!isLinkedWithShader) linkWithShader();

    this->shader->setTextureUniforms();

    this->shader->loadMatrix(transformLocation, this->getTransformationMatrix());
    this->shader->loadMatrix(projectionLocation, camera->getProjectionMatrix());
    this->shader->loadMatrix(viewLocation, camera->getViewMatrix());

    shader->loadVec3(sunDirectionLoc, glm::vec3(-1,-1,0));
    shader->loadFloat(timeLoc, GameEngine::getTime());

    glUniform4fv(boneRotationLoc, MAX_BONE_COUNT, rotationData);
    glUniform4fv(boneOffsetLoc, MAX_BONE_COUNT, offsetData);

}

void AnimRenderElement::prepareUniforms(Camera * camera, glm::vec3 sunDir) {

    this->createBoneModel();
    if (!isReady) return;
    if (!isLinkedWithShader) linkWithShader();

    shader->setTextureUniforms();

    this->shader->loadMatrix(transformLocation, this->getTransformationMatrix());
    this->shader->loadMatrix(projectionLocation, camera->getProjectionMatrix());
    this->shader->loadMatrix(viewLocation, camera->getViewMatrix());

    shader->loadVec3(sunDirectionLoc, sunDir);
    shader->loadFloat(timeLoc, GameEngine::getTime());

    glUniform4fv(boneRotationLoc, MAX_BONE_COUNT, rotationData);
    glUniform4fv(boneOffsetLoc, MAX_BONE_COUNT, offsetData);

}
