#include "renderbuffer.h"

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>
RenderBuffer::RenderBuffer(int width, int height, std::vector<unsigned int> attachments) {

    std::vector<unsigned int> renderAttachments(attachments.size());

    this->textures = std::vector<Texture*>(attachments.size());

    int colorCount = 0;

    glGenFramebuffers(1, &bufferId);
    glBindFramebuffer(GL_FRAMEBUFFER, bufferId);

    this->attachments = attachments;

    for (int i = 0; i < attachments.size(); ++i) {

        unsigned int textureId;

        glGenTextures(1, &textureId);
        glBindTexture(GL_TEXTURE_2D, textureId);

        switch(attachments[i]) {

            case ATTACHMENT_COLOR_8:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_FLOAT, NULL);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + colorCount, GL_TEXTURE_2D, textureId, 0);
                renderAttachments[colorCount] = GL_COLOR_ATTACHMENT0+colorCount;
                colorCount++;
                break;

            case ATTACHMENT_COLOR_16:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + colorCount, GL_TEXTURE_2D, textureId, 0);
                renderAttachments[colorCount] = GL_COLOR_ATTACHMENT0+colorCount;
                colorCount++;
                break;

            case ATTACHMENT_DEPTH:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureId, 0);

                break;


        }

        textures[i] = new Texture(textureId, width, height, GL_TEXTURE_2D);

    }

    glDrawBuffers(colorCount, renderAttachments.data());

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    this->width = width;
    this->height = height;

}

RenderBuffer::~RenderBuffer()
{
    //dtor
}

void RenderBuffer::bindForRender() {
    glBindFramebuffer(GL_FRAMEBUFFER, bufferId);
}

void RenderBuffer::unbindForRender() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Texture * RenderBuffer::getTexture(int id) {
    return textures[id];
}

void RenderBuffer::updateSize(int width, int height) {

    int colorCount = 0;


    for (int i = 0; i < textures.size(); ++i) {

        delete textures[i];
        textures[i] = nullptr;

    }

    std::vector<unsigned int> renderAttachments(attachments.size());

    glBindFramebuffer(GL_FRAMEBUFFER, bufferId);

    this->attachments = attachments;

    for (int i = 0; i < attachments.size(); ++i) {

        unsigned int textureId;

        glGenTextures(1, &textureId);
        glBindTexture(GL_TEXTURE_2D, textureId);

        switch(attachments[i]) {

            case ATTACHMENT_COLOR_8:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_FLOAT, NULL);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + colorCount, GL_TEXTURE_2D, textureId, 0);
                renderAttachments[colorCount] = GL_COLOR_ATTACHMENT0+colorCount;
                colorCount++;
                break;

            case ATTACHMENT_COLOR_16:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + colorCount, GL_TEXTURE_2D, textureId, 0);
                renderAttachments[colorCount] = GL_COLOR_ATTACHMENT0+colorCount;
                colorCount++;
                break;

            case ATTACHMENT_DEPTH:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureId, 0);

                break;


        }

        textures[i] = new Texture(textureId, width, height, GL_TEXTURE_2D);

    }

    glDrawBuffers(colorCount, renderAttachments.data());

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    this->width = width;
    this->height = height;

}

int RenderBuffer::getWidth() {

    return this->width;

}

int RenderBuffer::getHeight() {

    return this->height;

}
