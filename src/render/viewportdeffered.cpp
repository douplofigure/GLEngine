#include "viewportdeffered.h"

#include <stdlib.h>
#include <iostream>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>


#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include "gameengine.h"
#include "render/lut.h"
#include "shadowcamera.h"

#define SHADOW_BUFFER_SIZE 2048

ViewportDeffered::ViewportDeffered(int width, int height, float fov, float zNear, float zFar, int maxLights) : Viewport(width, height, fov, zNear, zFar) {

    this->maxLightCount = maxLights;
    this->currentLights = 0;

    std::vector<unsigned int> vattachments(4);
    vattachments[0] = RenderBuffer::ATTACHMENT_COLOR_16;
    vattachments[1] = RenderBuffer::ATTACHMENT_COLOR_16;
    vattachments[2] = RenderBuffer::ATTACHMENT_COLOR_8;
    vattachments[3] = RenderBuffer::ATTACHMENT_DEPTH;

    this->renderBuffer = new RenderBuffer(width, height, vattachments);
    this->transparentBuffer = new RenderBuffer(width, height, vattachments);

    std::vector<unsigned int> sattachments(1);
    sattachments[0] = RenderBuffer::ATTACHMENT_DEPTH;

    this->shadowBuffer = new RenderBuffer(SHADOW_BUFFER_SIZE, SHADOW_BUFFER_SIZE, sattachments);
    this->shadowCamera = new ShadowCamera(this->camera);

    std::vector<unsigned int> rattachments(2);
    rattachments[0] = RenderBuffer::ATTACHMENT_COLOR_8;
    rattachments[1] = RenderBuffer::ATTACHMENT_DEPTH;

    this->blitShader = GameEngine::resources->get<Shader>("blit");

    this->resultBuffer = new RenderBuffer(width, height, rattachments);

    lightShader = GameEngine::resources->get<Shader>("light");

    lightShader->loadInt(lightShader->getUniformLocation("gPosition"), 0);
    lightShader->loadInt(lightShader->getUniformLocation("gNormal"), 1);
    lightShader->loadInt(lightShader->getUniformLocation("gAlbedoSpec"), 2);

    lightShader->loadInt(lightShader->getUniformLocation("tPosition"), 3);
    lightShader->loadInt(lightShader->getUniformLocation("tNormal"), 4);
    lightShader->loadInt(lightShader->getUniformLocation("tAlbedoSpec"), 5);

    lightShader->loadInt(lightShader->getUniformLocation("shadowMap"), 7);

    this->lightData = (DEFFERED_LIGHT *) malloc(sizeof(DEFFERED_LIGHT) * maxLights);

    for (int i = 0; i < maxLights; ++i) {
        lightData[i] = (DEFFERED_LIGHT) {0, 0, 0, 0, 0, 0, L_POINT, 0};
    }

    this->positions = std::vector<glm::vec3>(maxLights);
    this->colors = std::vector<glm::vec3>(maxLights);
    this->types = std::vector<int>(maxLights);
    this->falloffs = std::vector<float>(maxLights);
    this->ids = std::vector<int>(maxLights);

    int uboSize;
//    glGetActiveUniformBlockiv(lightShader->getProgramId(), lightDataIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &uboSize);
    std::cout << "uboSize " << uboSize << std::endl;
    std::cout << "memSize " << sizeof(DEFFERED_LIGHT) * maxLights << std::endl;
    //exit(0);

    this->quadModel = GameEngine::resources->get<Model>("quad");

    this->sizeUpdate = false;

    this->skybox = nullptr;

    this->boxModel = GameEngine::resources->get<Model>("cube");
    this->boxShader = GameEngine::resources->get<Shader>("skybox");

    this->resolutionX = width;
    this->resolutionY = height;


    this->ppStack = new PostProcessingStack();

    this->hasReflections = false;

}

ViewportDeffered::~ViewportDeffered() {

    std::cout << "deleting deffered rendering view" << std::endl;

    delete ppStack;
    free(lightData);
}

void ViewportDeffered::updateSize(int newWidth, int newHeight) {

    Viewport::updateSize(newWidth, newHeight);

    sizeUpdate = true;

    std::cout << "Done" << std::endl;

}

void ViewportDeffered::updateFramebuffer() {

    renderBuffer->updateSize(resolutionX, resolutionY);
    transparentBuffer->updateSize(resolutionX, resolutionY);

    resultBuffer->updateSize(resolutionX, resolutionY);

    sizeUpdate = false;

}

int ViewportDeffered::addLight(glm::vec3 pos, glm::vec3 color, LIGHT_TYPE type, float falloff) {

    DEFFERED_LIGHT light;

    light.x = pos.x;
    light.y = pos.y;
    light.z = pos.z;

    light.r = color.x;
    light.g = color.y;
    light.b = color.z;

    light.type = type;
    light.falloff = falloff;

    /*glBindBuffer(GL_UNIFORM_BUFFER, lightBuffer);

    glBufferSubData(GL_UNIFORM_BUFFER, 0, maxLightCount * sizeof(DEFFERED_LIGHT), &lightData);

    //glBindBufferRange(GL_UNIFORM_BUFFER, 0, lightBuffer, 0, currentLights * sizeof(DEFFERED_LIGHT));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);*/

    positions[currentLights] = pos;
    colors[currentLights] = color;
    types[currentLights] = type;
    falloffs[currentLights] = falloff;
    ids[currentLights] = currentLights;

    lightData[currentLights] = light;

    return currentLights++;

}

void ViewportDeffered::addStructureLights(Structure * strc, Vector3 pos, Quaternion rot, double s) {

    glm::vec3 position(pos.x, pos.y, pos.z);

    glm::mat4 smat = glm::scale(glm::vec3(s, s, s));

    Matrix4 mat;
    mat.fromQuaternion(rot, pos);

    glm::mat4 rmat = mat.toGLM();

    glm::mat4 res = rmat * smat;

    std::vector<glstructure::STRUCTURE_LIGHT> lights = strc->getLights();

    Matrix4 matrix = Matrix4::fromGLM(res);

    for (int i = 0; i < lights.size(); ++i) {

        std::cout << "Adding light at " << lights[i].pos << std::endl;
        Vector4 pos4 = Vector4(lights[i].pos, 1.0);

        pos4 = matrix * pos4;

        Vector3 rPos = Vector3(pos4.x, pos4.y, pos4.z) / pos4.w;

        this->addLight(glm::vec3(rPos.x, rPos.y, rPos.z), glm::vec3(lights[i].color.x,lights[i].color.y,lights[i].color.z), (LIGHT_TYPE) lights[i].type, lights[i].falloff);

    }

}

void ViewportDeffered::clearLights() {

    free(lightData);
    delete ppStack;
    this->currentLights = 0;
    this->lightData = (DEFFERED_LIGHT *) malloc(sizeof(DEFFERED_LIGHT) * maxLightCount);

    ppStack = new PostProcessingStack();

}

void ViewportDeffered::updateLight(int id, glm::vec3 pos, glm::vec3 color, LIGHT_TYPE type, float falloff) {

    DEFFERED_LIGHT light;

    light.x = pos.x;
    light.y = pos.y;
    light.z = pos.z;

    light.r = color.x;
    light.g = color.y;
    light.b = color.z;

    light.type = type;
    light.falloff = falloff;

    positions[ids[id]] = pos;
    colors[ids[id]] = color;
    types[ids[id]] = type;
    falloffs[ids[id]] = falloff;

    lightData[id] = light;

}

double distance2(glm::vec3 a, glm::vec3 b) {

    return (a.x-b.x) * (a.x-b.x) + (a.y-b.y) * (a.y-b.y) + (a.z-b.z) * (a.z-b.z);

}

ostream& operator<<(ostream& stream, glm::vec3 vec) {

    stream << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
    return stream;

}

void ViewportDeffered::sortLights() {

    //int sortedCount = 1;
    //std::cout << "sorting lights" << std::endl;


    /*positions[0] = glm::vec3(lightData[0].x, lightData[0].y, lightData[0].z);
    colors[0]    = glm::vec3(lightData[0].r, lightData[0].g, lightData[0].b);
    falloffs[0]  = lightData[0].falloff;
    types[0]     = lightData[0].type;

    int j;
    for (int i = 1; i < maxLightCount; ++i) {

        j = 0;
        while (distance2(positions[j++], camera->position) < distance2(glm::vec3(lightData[i].x, lightData[i].y, lightData[i].z), camera->position));
        //j--;
        positions.insert(positions.begin()+j, glm::vec3(lightData[i].x, lightData[i].y, lightData[i].z));
        colors.insert(colors.begin()+j, glm::vec3(lightData[i].r, lightData[i].g, lightData[i].b));
        falloffs.insert(falloffs.begin()+j, lightData[i].falloff);
        types.insert(types.begin()+j, lightData[i].type);

    }*/

    //std::cout << "Done sorting" << std::endl;


}

void ViewportDeffered::setSkybox(CubeMap * box) {

    this->skybox = box;

}

void ViewportDeffered::setLUT(LUT * lut) {

    this->ppStack->getEffect(0)->changeTexture(0, lut);

}

std::vector<float> ViewportDeffered::getContent(int * width, int * height) {

    *width = this->ppStack->getEffect(0)->getRenderBuffer()->getWidth();
    *height = this->ppStack->getEffect(0)->getRenderBuffer()->getHeight();

    std::vector<float> data(*width * *height * 4);

    this->ppresult->bindToUnit(0);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, data.data());

    return data;

}

void ViewportDeffered::addPostProcessingEffect(PostProcessingEffect * effect) {

    this->ppStack->addEffect(effect);

}

extern void saveImagePPM(float * data, int width, int height, std::string fname);

void ViewportDeffered::prerenderReflectionTextures() {

    #define REFLECT_RES 256

    Camera * tmpCam = new Camera(90.0, 1, 0.01, 10.0);
    std::vector<float> imgData(REFLECT_RES * REFLECT_RES * 4 * 6);

    std::vector<float> singleImg(REFLECT_RES * REFLECT_RES * 4);
    std::vector<float> depthData(REFLECT_RES * REFLECT_RES);

    std::vector<unsigned int> vattachments(4);
    vattachments[0] = RenderBuffer::ATTACHMENT_COLOR_16;
    vattachments[1] = RenderBuffer::ATTACHMENT_COLOR_16;
    vattachments[2] = RenderBuffer::ATTACHMENT_COLOR_8;
    vattachments[3] = RenderBuffer::ATTACHMENT_DEPTH;

    RenderBuffer * texBuffer = new RenderBuffer(REFLECT_RES, REFLECT_RES, vattachments);

    for (int i = 0; i < this->layers[1]->getElements().size(); ++i) {

        ///Render reflection map for element.
        std::shared_ptr<RenderElement> rElem(this->layers[1]->getElements()[i]);
        glm::vec3 pos = rElem->getPos();
        tmpCam->position = pos;
        tmpCam->facing = glm::vec3(1, 0, 0);

        texBuffer->bindForRender();
        glViewport(0, 0, REFLECT_RES, REFLECT_RES);
        glClearColor(cr, cg, cb, 1);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        this->layers[0]->render(this->camera, 1.0, glm::vec3(-0.5, -1, -1));
        texBuffer->unbindForRender();

        texBuffer->getTexture(2)->bindToUnit(0);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, singleImg.data());

        texBuffer->getTexture(3)->bindToUnit(0);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH, GL_FLOAT, depthData.data());

        saveImagePPM(singleImg.data(), REFLECT_RES, REFLECT_RES, "reflect.ppm");

    }

    delete texBuffer;
    delete tmpCam;

    hasReflections = true;

}

void ViewportDeffered::setLightAsSun(int index) {
    this->sunLightIndex = index;
    if (index >= 0)
        this->shadowCamera->setSunDirection(this->positions[sunLightIndex]);
}

void ViewportDeffered::renderElements() {

    //sortLights();

    if (sizeUpdate) {
        updateFramebuffer();
    }

    if (!hasReflections) {
        prerenderReflectionTextures();
    }

    shadowBuffer->bindForRender();
    this->shadowCamera->updateViewMatrix();

    glm::mat4 toLightSpace = shadowCamera->getProjectionMatrix() * shadowCamera->getViewMatrix();

    glViewport(0, 0, SHADOW_BUFFER_SIZE, SHADOW_BUFFER_SIZE);
    glClear(GL_DEPTH_BUFFER_BIT);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    layers[0]->render(shadowCamera, 1.0);

    glCullFace(GL_BACK);
    shadowBuffer->unbindForRender();

    //glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
    renderBuffer->bindForRender();
    glViewport(0, 0, resolutionX, resolutionY);
    glClearColor(cr, cg, cb, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    layers[0]->render(camera, 1.0, glm::vec3(-0.5, -1, -1));

    transparentBuffer->bindForRender();
    glViewport(0, 0, resolutionX, resolutionY);
    glClearColor(cr, cg, cb, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    layers[1]->render(camera, 1.0, glm::vec3(-0.5, -1, -1));

    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    this->resultBuffer->bindForRender();

    glViewport(0, 0, resolutionX, resolutionY);
    //glClearColor(cr, cg, cb, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderBuffer->getTexture(0)->bindToUnit(0);
    renderBuffer->getTexture(1)->bindToUnit(1);
    renderBuffer->getTexture(2)->bindToUnit(2);

    transparentBuffer->getTexture(0)->bindToUnit(3);
    transparentBuffer->getTexture(1)->bindToUnit(4);
    transparentBuffer->getTexture(2)->bindToUnit(5);

    //renderBuffer->getTexture(3)->bindToUnit(6);
    //transparentBuffer->getTexture(3)->bindToUnit(7);

    shadowBuffer->getTexture(0)->bindToUnit(7);

    lightShader->bindForRender();
    glUniform1i(lightShader->getUniformLocation("shadowMap"), 7);

    //lightShader->loadInt(lightShader->getUniformLocation("gPosition"), 0);
    //lightShader->loadInt(lightShader->getUniformLocation("gNormal"), 1);
    //lightShader->loadInt(lightShader->getUniformLocation("gAlbedoSpec"), 2);

    lightShader->setTextureUniforms();

    lightShader->loadVec3(lightShader->getUniformLocation("viewPos"), camera->position);

    lightShader->loadMatrix(lightShader->getUniformLocation("toLightSpace"), shadowCamera->getProjectionMatrix());
    lightShader->loadMatrix(lightShader->getUniformLocation("lightViewMatrix"), shadowCamera->getViewMatrix());
    lightShader->loadVec3(lightShader->getUniformLocation("lightPos"), shadowCamera->position);

    lightShader->loadFloat(lightShader->getUniformLocation("time"), GameEngine::getTime());
    lightShader->loadInt(lightShader->getUniformLocation("activeLights"), this->currentLights);

    //glUniform1fv(lightShader->getUniformLocation("lights"), maxLightCount * (sizeof(DEFFERED_LIGHT)/4), (float*)lightData);

    glUniform1fv(lightShader->getUniformLocation("falloffs"), maxLightCount, falloffs.data());
    glUniform3fv(lightShader->getUniformLocation("positions"), maxLightCount, (float*)positions.data());
    glUniform3fv(lightShader->getUniformLocation("colors"), maxLightCount, (float*)colors.data());
    glUniform1iv(lightShader->getUniformLocation("types"), maxLightCount, types.data());

    if (this->skybox) {
        skybox->bindToUnit(6);
        glUniform1i(lightShader->getUniformLocation("skybox"), 6);

        glUniform1f(lightShader->getUniformLocation("fov"), camera->getFov());
        glUniform1f(lightShader->getUniformLocation("aspect"), camera->getAspect());
        glUniform3f(lightShader->getUniformLocation("viewDir"), camera->facing.x, camera->facing.y, camera->facing.z);

    }


    glBindVertexArray(quadModel->getVaoId());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glDrawElements(quadModel->getPrimitiveType(), quadModel->getIndexCount(), GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);

    lightShader->unbindForRender();

    if (this->skybox) {
        glDepthMask(GL_FALSE);
        this->boxShader->bindForRender();

        boxShader->loadMatrix(boxShader->getUniformLocation("projection"), camera->getProjectionMatrix());

        glm::mat4 view = glm::mat4(glm::mat3(camera->getViewMatrix()));

        boxShader->loadMatrix(boxShader->getUniformLocation("view"), view);

        boxShader->loadInt(boxShader->getUniformLocation("skybox"), 0);

        skybox->bindToUnit(0);

        glBindVertexArray(boxModel->getVaoId());
        glEnableVertexAttribArray(0);

        glDrawElements(boxModel->getPrimitiveType(), boxModel->getIndexCount(), GL_UNSIGNED_INT, 0);

        glDisableVertexAttribArray(0);

        this->boxShader->unbindForRender();

        glDepthMask(GL_TRUE);

    }

    this->ppresult = this->ppStack->getResult(this->resultBuffer);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, width, height);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    this->blitShader->bindForRender();
    this->blitShader->setTextureUniforms();

    ppresult->bindToUnit(0);

    glBindVertexArray(quadModel->getVaoId());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glDrawElements(quadModel->getPrimitiveType(), quadModel->getIndexCount(), GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);

    this->blitShader->unbindForRender();

    layers[layers.size()-1]->render(this->getCamera());

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);


}
