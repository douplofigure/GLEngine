#include "renderpass.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>


RenderPass::RenderPass(int bufferWidth, int bufferHeight) {

    unsigned int colorId;
    unsigned int depthId;

    this->bufferWidth = bufferWidth;
    this->bufferHeight = bufferHeight;

    glGenFramebuffers(1, &bufferId);
    glBindFramebuffer(GL_FRAMEBUFFER, bufferId);

    glGenTextures(1, &colorId);
    glBindTexture(GL_TEXTURE_2D, colorId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glGenTextures(1, &depthId);
	glBindTexture(GL_TEXTURE_2D, depthId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->bufferWidth, this->bufferHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthId, 0);

	GLenum buffers[1] = {GL_COLOR_ATTACHMENT0};

	glDrawBuffers(1, buffers);
	glReadBuffer(GL_NONE);

    this->colorOutput = new Texture(colorId, bufferWidth, bufferHeight, GL_TEXTURE_2D);
    this->depthOutput = new Texture(depthId, bufferWidth, bufferHeight, GL_TEXTURE_2D);


}

RenderPass::~RenderPass()
{
    //dtor
}

void RenderPass::prepareRender() {

    glBindFramebuffer(GL_FRAMEBUFFER, bufferId);
    glViewport(0, 0, bufferWidth, bufferHeight);
    glClearColor(bgColor.x, bgColor.y, bgColor.z, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (int i = 0; i < glOptions.size(); ++i) {
        glEnable(glOptions[i]);
    }


}

void RenderPass::endRender() {

    for (int i = glOptions.size() - 1; i > 0; --i) {
        glEnable(glOptions[i]);
    }

}

void RenderPass::activateOption(int option) {
    this->glOptions.push_back(option);
}

void RenderPass::addLayer(RenderLayer * layer) {
    this->layers.push_back(layer);
}

std::vector<RenderLayer *> RenderPass::getLayers() {
    return layers;
}

Texture * RenderPass::getColorTexture() {
    return colorOutput;
}

Texture * RenderPass::getDepthTexture() {
    return depthOutput;
}

Camera * RenderPass::getCamera() {

    return camera;

}

void RenderPass::setCamera(Camera * camera) {
    this->camera = camera;
}

void RenderPass::updateBufferSize(int w, int h) {

}
