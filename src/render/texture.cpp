#include "texture.h"

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "resources/resourcemanager.h"

Texture::Texture(unsigned int id, int width, int height, int type) {

    this->id = id;
    this->width = width;
    this->height = height;
    this->type = type;
    std::cout << "Texture instance Created" << std::endl;

}

Texture::~Texture() {

    glDeleteTextures(1, &id);

}

unsigned int Texture::getID() {
    return id;
}

void Texture::bindToUnit(int unit) {

    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(this->type, this->id);

}

Texture * Texture::createTexture2D(std::vector<float> values, int width, int height, int chanelCount) {

    unsigned int textureId;

    glGenTextures(1, &textureId);
    std::cout << "Texture has Id " << textureId << std::endl;
	glBindTexture(GL_TEXTURE_2D, textureId);
	std::cout << "Texture is Bound" << std::endl;
	glTexImage2D(GL_TEXTURE_2D, 0, (chanelCount > 3 ? GL_RGBA : GL_RGB), width, height, 0, chanelCount > 3 ? GL_RGBA : GL_RGB, GL_FLOAT, values.data());
	std::cout << "Data on GPU" << std::endl;
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	std::cout << "Params set" << std::endl;

	return new Texture(textureId, width, height, GL_TEXTURE_2D);

}

Texture * Texture::createTexture2D(std::vector<float> values, int width, int height, int chanelCount, bool isMap) {

    unsigned int textureId;

    glGenTextures(1, &textureId);
    std::cout << "Texture has Id " << textureId << std::endl;
	glBindTexture(GL_TEXTURE_2D, textureId);
	std::cout << "Texture is Bound" << std::endl;
	glTexImage2D(GL_TEXTURE_2D, 0, (chanelCount > 3 ? GL_RGBA : GL_RGB), width, height, 0, chanelCount > 3 ? GL_RGBA : GL_RGB, GL_FLOAT, values.data());
	std::cout << "Data on GPU" << std::endl;
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	std::cout << "Params set" << std::endl;

	return new Texture(textureId, width, height, GL_TEXTURE_2D);

}

TextureInjector::TextureInjector(std::vector<float> values, int width, int height, int chanelCount) {
    this->values = values;
    this->width = width;
    this->height = height;
    this->chanelCount = chanelCount;
    this->type = getTypeName<Texture>();
    isMap = false;
}

TextureInjector::TextureInjector(std::vector<float> values, int width, int height, int chanelCount, bool isMap) : TextureInjector(values, width, height, chanelCount) {
    this->isMap = isMap;
}

TextureInjector::~TextureInjector(){

}

void TextureInjector::setMap(bool b) {
    isMap = b;
}

Texture * TextureInjector::upload() {

    Texture * tex = Texture::createTexture2D(values, width, height, chanelCount, isMap);
    return tex;

}

void TextureInjector::deleteTempData() {

    std::vector<float>().swap(values);

}

std::vector<float> TextureInjector::getValues() {
    return values;
}

void Texture::deleteData() {

    glDeleteTextures(1, &id);

}

int Texture::getWidth() {
    return width;
}

int Texture::getHeight() {
    return height;
}
