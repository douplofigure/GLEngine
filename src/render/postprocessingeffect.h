#ifndef POSTPROCESSINGEFFECT_H
#define POSTPROCESSINGEFFECT_H

#include <vector>

#include "shader.h"
#include "texture.h"
#include "renderbuffer.h"

class PostProcessingEffect
{
    public:
        PostProcessingEffect(Shader * shader, int width, int height);
        virtual ~PostProcessingEffect();

        Shader * getShader();
        RenderBuffer * getRenderBuffer();
        Texture * getTexture();

        void bindTextures(Texture * input);

        void addTexture(Texture * extraInput);
        void changeTexture(int slot, Texture * extraInput);

    protected:

    private:

        Shader * shader;
        RenderBuffer * renderBuffer;

        std::vector<Texture *> rTextures;

};

#endif // POSTPROCESSINGEFFECT_H
