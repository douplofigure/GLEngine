#ifndef RENDERBUFFER_H
#define RENDERBUFFER_H

#include <vector>
#include "render/texture.h"

class RenderBuffer
{
    public:

        static enum attachment_type_e {

            ATTACHMENT_COLOR_8,
            ATTACHMENT_COLOR_16,
            ATTACHMENT_DEPTH

        } ATTACHMENT_TYPE;

        RenderBuffer(int width, int height, std::vector<unsigned int> attachments);
        virtual ~RenderBuffer();

        void bindForRender();
        void unbindForRender();

        void updateSize(int width, int height);

        Texture * getTexture(int id);

        int getWidth();
        int getHeight();

        std::vector<Texture *> textures;

    protected:

    private:

        unsigned int bufferId;

        std::vector<unsigned int> attachments;

        int width;
        int height;

};

#endif // RENDERBUFFER_H
