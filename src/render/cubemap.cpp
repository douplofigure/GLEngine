#include "cubemap.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

CubeMap::CubeMap(unsigned int id) : Texture(id, 0, 0, GL_TEXTURE_CUBE_MAP) {



}

CubeMap::~CubeMap()
{
    //dtor
}

void CubeMap::bindToUnit(int unit) {

    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, this->id);

}

CubeMapInjector::CubeMapInjector(int width, int height) {

    this->width = width;
    this->height = height;

}

CubeMapInjector::~CubeMapInjector() {

}

void CubeMapInjector::addFaceData(CubeMap::FACE face, std::vector<float> data) {

    this->data[face] = data;

}

CubeMap * CubeMapInjector::upload() {

    unsigned int textureId;

    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);

    for (int i = 0; i < 6; ++i) {

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, data[i].data());

    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return new CubeMap(textureId);

}

void CubeMapInjector::deleteTempData() {

    for (int i = 0; i < 6; ++i) {

        std::cout << "freeing cubemap data: " << (sizeof(float) * data[i].size()) << std::endl;
        std::vector<float>().swap(data[i]);

    }

}
