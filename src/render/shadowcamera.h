#ifndef SHADOWCAMERA_H
#define SHADOWCAMERA_H

#include "camera.h"

#include <vector>


class ShadowCamera : public Camera
{
    public:
        /** Default constructor */
        ShadowCamera(Camera * camera);
        /** Default destructor */
        virtual ~ShadowCamera();

        virtual void updateViewMatrix();
        void setSunDirection(glm::vec3 dir);

    protected:

        Camera * viewCam;

        glm::vec3 sunDirection;

    private:

        void updateBox();
        glm::vec3 getCenter();
        void calculateWidthsAndHeights();
        glm::vec4 calculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction, float width);
        std::vector<glm::vec4> calculateFrustumVertices(glm::mat4 rotation, glm::vec3 fwdVec, glm::vec3 cN, glm::vec3 cF);

        float farWidth;
        float farHeight;
        float nearWidth;
        float nearHeight;

        float minX;
        float maxX;
        float minY;
        float maxY;
        float minZ;
        float maxZ;

};

#endif // SHADOWCAMERA_H
