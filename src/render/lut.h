#ifndef LUT_H
#define LUT_H

#include "texture.h"


class LUT : public Texture
{
    public:
        LUT(unsigned int id, int width, int height);
        LUT(unsigned int id, int width, int height, int depth);
        virtual ~LUT();

        void bindToUnit(int unit);


        static LUT * createTexture2D(std::vector<float> values, int width, int height, int chanelCount);
        static LUT * createTexture3D(std::vector<float> values, int width, int height, int depth, int chanelCount);

    protected:


    private:

};

class LUTInjector : public ResourceInjector<LUT> {

    public:

        LUTInjector(std::vector<float> values, int width, int height, int chanelCount);
        LUTInjector(std::vector<float> values, int width, int height, int depth, int chanelCount);
        virtual ~LUTInjector();

        LUT * upload();
        void deleteTempData();

    private:

        std::vector<float> values;

        int width;
        int height;
        int depth;
        int channelCount;


        static std::vector<float> convert2DTo3D(std::vector<float> image, int width, int height, int chanelCount, int cellsPerLine);



};

#endif // LUT_H
