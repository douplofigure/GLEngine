#include "fontrenderelement.h"

#include <iostream>

#include "resources/resourcemanager.h"
#include "gameengine.h"

FontRenderElement::FontRenderElement(std::string text, Font * font) : FontRenderElement(text, font, false) {

}

FontRenderElement::FontRenderElement(std::string text, Font * font, bool isUI) {

    this->text = text;
    this->font = font;

    this->model = nullptr;

    this->constructModel();

    this->isUI = isUI;

    if (isUI) {
        this->shader = GameEngine::resources->get<Shader>("uifont");
    }
    else
        this->shader = GameEngine::resources->get<Shader>("font");

    for (int i = 0; i < font->getPageCount(); ++i) {
        this->addTextureBinding(i, font->getPageTexture(i));
    }

    this->normalMap = NULL;
    this->specularMap = NULL;

    this->txtColor = glm::vec3(0,0,0);

    this->linkWithShader();

}

FontRenderElement::~FontRenderElement() {



}

void FontRenderElement::linkWithShader() {

    RenderElement::linkWithShader();
    colorLocation = shader->getUniformLocation(shader->getUniformName("fontColor"));

}

void FontRenderElement::constructModel() {
    if (this->model) delete this->model;
    this->model = this->font->renderStringToModel(this->text);

}

void FontRenderElement::setTextColor(float r, float g, float b) {

    this->txtColor = glm::vec3(r,g,b);

}

void FontRenderElement::prepareUniforms(Camera * camera) {

    RenderElement::prepareUniforms(camera);
    this->shader->loadVec3(colorLocation, this->txtColor);

}

void FontRenderElement::setText(std::string text) {
    this->text = text;
    this->constructModel();
}
