#ifndef VIEWPORTPP_H
#define VIEWPORTPP_H

#include "viewport.h"
#include "renderpass.h"
#include "shadowcamera.h"


class ViewportPP : public Viewport
{
    public:
        ViewportPP(int width, int height, float fov, float zNear, float zFar, int bufferWidth, int bufferHeight, int shadowRes);
        virtual ~ViewportPP();

        void addWaterElement(std::shared_ptr<RenderElement> elem);
        void setFov(float fov);

        void renderElements();
        void addElement(int layer, std::shared_ptr<RenderElement> elem);

        virtual void addLayer();

    protected:

        Texture * ppColor;
        Texture * ppDepth;
        Texture * ppLight;

        Texture * reflectionTexture;
        Texture * reflectionDepth;

        Texture * refractionTexture;
        Texture * refractionDepth;

        unsigned int ppBuffer;
        unsigned int reflectionBuffer;
        unsigned int refractionBuffer;

        int bufferWidth;
        int bufferHeight;

        int reflectionWidth;
        int reflectionHeight;

        Shader * ppShader;
        Model * ppModel;


        unsigned int shadowBuffer;

        int shadowBufferWidth;
        int shadowBufferHeight;

        Texture * shadowMap;
        Texture * shadowColor;
        ShadowCamera * shadowCam;

        glm::vec3 sunDirection;

        RenderPass * reflectionPass;
        RenderPass * refractionPass;

    private:

        bool hasWaterLayer;

        Camera * reflectionCamera;
        /*Camera * rightEye;
        Texture * rightColor;
        Texture * rightDepth;
        unsigned int rightBuffer;*/

};

#endif // VIEWPORTPP_H
