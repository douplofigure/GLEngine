#include "window.h"

#include <iostream>
#include <map>

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#ifdef _WIN32

#include <GL/glew.h>

#endif
#include <GL/gl.h>

#include <GLFW/glfw3.h>

#include "gameengine.h"

std::map<GLFWwindow *, Window *> windowsByPointer;

void onSizeUpdate(GLFWwindow * window, int width, int height);
void onCursorPosUpdate(GLFWwindow * window, double xpos, double ypos);
void onKeyboardUpdate(GLFWwindow * window, int key, int scancode, int action, int mods);
void onMouseButton(GLFWwindow * window, int button, int action, int mods);

Window::Window(int width, int height, std::string title) {

    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);  /// yes, 3 and 2!!!
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	//glViewport(0,0,0,0);

    if (GameEngine::getFullScreen()) {
        GLFWmonitor * monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode * videoMode = glfwGetVideoMode(monitor);

        window = glfwCreateWindow(videoMode->width, videoMode->height, title.c_str(), NULL, NULL);
	} else {

        window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);

	}

	if (!window) {
		std::cerr << "Could not create window\n";
		glfwTerminate();
		exit(1);
	}


	glfwMakeContextCurrent((GLFWwindow *)window);


	#ifdef _WIN32
    GLenum err = glewInit();
	if (GLEW_OK != err) {
        fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
        exit(1);
    }
    #endif


	glfwSetWindowSizeCallback((GLFWwindow *) window, onSizeUpdate);


	windowsByPointer[(GLFWwindow *)window] = this;

	glfwGetCursorPos((GLFWwindow*)window, &cursorX, &cursorY);

	glfwSetCursorPosCallback((GLFWwindow*)window, onCursorPosUpdate);
	glfwSetKeyCallback((GLFWwindow*)window, onKeyboardUpdate);

	glfwSetMouseButtonCallback((GLFWwindow*) window, onMouseButton);

}

Window::~Window()
{

    for (std::map<std::string, Viewport*>::iterator it = viewports.begin(); it != viewports.end(); ++it) {

        delete it->second;

    }

    glfwDestroyWindow((GLFWwindow*)this->window);
}

bool Window::shouldClose() {

    return glfwWindowShouldClose((GLFWwindow*)window);

}

void Window::pollEvents() {

    glfwPollEvents();

}

void Window::swapBuffers() {

    glfwSwapBuffers((GLFWwindow*)window);

}

void Window::updateContents() {

    currentViewport->renderElements();

    swapBuffers();

}

void Window::addViewport(std::string name, Viewport * view) {

    this->viewports[name] = view;
    this->currentViewport = view;

}

void Window::updateCursor(double x, double y) {

    GameEngine::onMouseMotion(x - cursorX, y - cursorY);

    cursorX = x;
    cursorY = y;

}

void Window::setCurrentViewport(std::string name) {

    if (viewports[name])
        currentViewport = viewports[name];

}

void Window::updateSize(int width, int height) {

    for (std::map<std::string, Viewport*>::iterator it = viewports.begin(); it != viewports.end(); ++it) {

        it->second->updateSize(width, height);

    }

}

void Window::hideCursor() {

    //glfwSetCursor((GLFWwindow*) window, NULL);
    glfwSetInputMode((GLFWwindow*)window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

}

void Window::showCursor() {

    glfwSetInputMode((GLFWwindow*) window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    glfwSetCursorPos((GLFWwindow*) window, 0.5 * getWidth(), 0.5 * getHeight());

}

void onSizeUpdate(GLFWwindow * window, int  width, int height) {

    windowsByPointer[window]->updateSize(width, height);

}

void onCursorPosUpdate(GLFWwindow * window, double xpos, double ypos) {

    windowsByPointer[window]->updateCursor(xpos, ypos);

}

void onKeyboardUpdate(GLFWwindow * window, int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_ESCAPE) GameEngine::stop();

    GameEngine::onKeyboardUpdate(key, scancode, action, mods);

}

void onMouseButton(GLFWwindow * window, int button, int action, int mods) {

    double x,y;
    int width, height;
    glfwGetFramebufferSize((GLFWwindow *)window, &width, &height);
    glfwGetCursorPos(window, &x, &y);

    GameEngine::onMouseButton(button, action, mods, x/width, 1-y/height);
}

int Window::getWidth() {

    int width, height;
    glfwGetFramebufferSize((GLFWwindow *)window, &width, &height);
    return width;

}

int Window::getHeight() {

    int width, height;
    glfwGetFramebufferSize((GLFWwindow *)window, &width, &height);
    return height;

}
