#ifndef RENDERELEMENT_H
#define RENDERELEMENT_H

#include <glm/vec3.hpp>
#include <glm/matrix.hpp>

#include <configloader.h>

#include "model.h"
#include "shader.h"
#include "texture.h"
#include "camera.h"
#include "material.h"

#include "structure/structure.h"

typedef enum render_elem_type_e {

    DEFAULT,
    STATIC,

} RenderElementType;

class RenderElement
{
    public:
        RenderElement(Model * model, Shader * shader, float x, float y, float z);
        RenderElement(Model * model, Material * material, float x, float y, float z);
        RenderElement(std::string model, std::string shader, float x, float y, float z);
        RenderElement(Structure * strc, float x, float y, float z);
        virtual ~RenderElement();

        virtual glm::mat4 getTransformationMatrix();

        Shader * getShader();
        Model * getModel();

        void bindTextures();

        /// DEPRECATED
        void setDiffuseMap(Texture * texture);
        void setSpecularMap(Texture * texture);
        void setNormalMap(Texture * texture);
        void setShadowMap(Texture * texture);

        void addTextureBinding(int index, Texture * texture);

        void setPosition(float x, float y, float z);
        void setRotation(Math::Quaternion rot);
        void setScale(float s);

        void changeRotation(float rx, float ry, float rz);
        void setRotationAxis(float x, float y, float z);
        void changePosition(float dx, float dy, float dz);

        void setLevelOfDetail(int i);
        int  addLevelOfDetail(Model * model);

        virtual void choseLevelOfDetail(glm::vec3 cameraPos);

        virtual void updateScreenSize(int width, int height);
        virtual void prepareUniforms(Camera * camera);
        virtual void prepareUniforms(Camera * camera, glm::vec3 sunDir);

        virtual void render();
        virtual void render(Camera * camera);

        virtual CompoundNode * asCompound();
        static RenderElement * fromCompound(CompoundNode * rootNode);

        glm::vec3 getPos();
        glm::vec3 getRotationAxis();
        Math::Quaternion getRotation();
        float getScale();

        std::string getModelName();

    protected:

        RenderElement();

        Shader * shader;
        Model * model;

        Texture * diffuseMap;
        Texture * specularMap;
        Texture * normalMap;

        Texture * shadowMap;

        glm::vec3 position;
        Math::Quaternion rotation;
        glm::vec3 rotAxis;
        glm::vec3 scale;

        int transformLocation;
        int viewLocation;
        int projectionLocation;

        int sunDirectionLoc;
        int sunColorLoc;

        int timeLoc;

        int diffuseMapLoc;
        int normalMapLoc;
        int specularMapLoc;
        int shadowMapLoc;

        std::vector<Model*> levelsOfDetail;

        virtual void linkWithShader();

        std::string modelName;
        std::string shaderName;

        std::vector<TEXTURE_BINDING> textureBindings;
        bool isReady;
        bool isLinkedWithShader;

    private:

};

#endif // RENDERELEMENT_H

