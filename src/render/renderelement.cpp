#include "renderelement.h"

#include <iostream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include "gameengine.h"
#include "render/staticrenderelement.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

using namespace Math;

RenderElement::RenderElement(Model * model, Shader * shader, float x, float y, float z) {


    this->isReady = false;
    this->model = model;
    this->shader = shader;

    this->position = glm::vec3(x,y,z);

    this->rotation = Quaternion(1, 0, 0, 0);
    this->rotAxis = glm::vec3(0,0,1);
    this->scale = glm::vec3(1,1,1);

    diffuseMap = NULL;
    specularMap = NULL;
    normalMap = NULL;

    shadowMap = NULL;

    this->isLinkedWithShader = false;

    //this->linkWithShader();

    this->levelsOfDetail = std::vector<Model*>();

    addLevelOfDetail(model);
    this->isReady = true;

    //this->textureBindings = std::vector<TEXTURE_BINDING>(0);

}

RenderElement::RenderElement(Model * model, Material * material, float x, float y, float z) : RenderElement(model, material->getShader(), x, y, z) {

    std::vector<TEXTURE_BINDING> tmpBinds = material->getBindings();
    this->textureBindings = std::vector<TEXTURE_BINDING>(tmpBinds.size());
    for (int i = 0; i < tmpBinds.size(); ++i) {

        this->textureBindings[i] = tmpBinds[i];

    }

}

RenderElement::RenderElement(Structure * strc, float x, float y, float z) : RenderElement(strc->getModel(), strc->getMaterial(), x, y, z) {


};

RenderElement::RenderElement(std::string model, std::string shader, float x, float y, float z) {


    this->isReady = false;
    this->model = GameEngine::resources->get<Model>(model);
    this->shader = GameEngine::resources->get<Shader>(shader);

    this->modelName = model;
    this->shaderName = shader;

    this->position = glm::vec3(x,y,z);

    this->rotation = Quaternion(1, 0, 0, 0);
    this->rotAxis = glm::vec3(0,0,1);
    this->scale = glm::vec3(1,1,1);

    diffuseMap = NULL;
    specularMap = NULL;
    normalMap = NULL;

    shadowMap = NULL;

    this->isLinkedWithShader = false;

    //this->linkWithShader();

    this->levelsOfDetail = std::vector<Model*>();

    addLevelOfDetail(this->model);
    this->isReady = true;

    this->textureBindings = std::vector<TEXTURE_BINDING>(0);

}

RenderElement::RenderElement() {

    diffuseMap = NULL;
    specularMap = NULL;
    normalMap = NULL;
    shadowMap = NULL;
    this->rotAxis = glm::vec3(0,1,0);
    this->isReady = true;
    this->isLinkedWithShader = false;

    this->textureBindings = std::vector<TEXTURE_BINDING>(0);

}

RenderElement::~RenderElement() {
    std::cout << "deleting RenderElement" << std::endl;
}

void RenderElement::linkWithShader() {

    transformLocation = shader->getUniformLocation(shader->getUniformName("transformationMatrix"));
    projectionLocation = shader->getUniformLocation(shader->getUniformName("projectionMatrix"));
    viewLocation = shader->getUniformLocation(shader->getUniformName("viewMatrix"));

    sunColorLoc = shader->getUniformLocation(shader->getUniformName("ambientColor"));
    sunDirectionLoc = shader->getUniformLocation(shader->getUniformName("ambientDirection"));

    timeLoc = shader->getUniformLocation(shader->getUniformName("time"));

    diffuseMapLoc = shader->getUniformLocation(shader->getUniformName("diffuseMap"));
    normalMapLoc = shader->getUniformLocation(shader->getUniformName("normalMap"));
    specularMapLoc = shader->getUniformLocation(shader->getUniformName("specularMap"));
    shadowMapLoc = shader->getUniformLocation(shader->getUniformName("shadowMap"));

}

glm::mat4 RenderElement::getTransformationMatrix() {

    glm::mat4 smat = glm::scale(scale);

    Matrix4 mat;

    mat.fromQuaternion(this->rotation, this->position);

    return mat.toGLM() * smat;

}

void RenderElement::bindTextures() {

    for (int i = 0; i < textureBindings.size(); ++i) {
        textureBindings[i].texture->bindToUnit(textureBindings[i].index);
    }

}

Model * RenderElement::getModel() {

    return model;

}

void RenderElement::setDiffuseMap(Texture * texture) {

    diffuseMap = texture;
    this->textureBindings.push_back((TEXTURE_BINDING){0, texture});

}

void RenderElement::setSpecularMap(Texture * texture) {

    specularMap = texture;
    this->textureBindings.push_back((TEXTURE_BINDING){2, texture});

}

void RenderElement::setNormalMap(Texture * texture) {

    normalMap = texture;
    this->textureBindings.push_back((TEXTURE_BINDING){1, texture});

}

void RenderElement::setShadowMap(Texture * texture) {

    //shadowMap = texture;
    this->textureBindings.push_back((TEXTURE_BINDING){3, texture});

}

void RenderElement::addTextureBinding(int index, Texture * texture) {

    this->textureBindings.push_back((TEXTURE_BINDING){index, texture});

}

void RenderElement::updateScreenSize(int width, int height) {

}

Shader * RenderElement::getShader() {

    return shader;

}

void RenderElement::changeRotation(float rx, float ry, float rz) {

    /*this->rotation.x += rx;
    this->rotation.y += ry;
    this->rotation.z += rz;*/

}

void RenderElement::changePosition(float dx, float dy, float dz) {

    /*this->position.x += dx;
    this->position.y += dy;
    this->position.z += dz;*/

}

void RenderElement::setPosition(float x, float y, float z) {

    this->position = glm::vec3(x,y,z);

}

void RenderElement::setRotation(Quaternion rot) {

    this->rotation = rot;

}

void RenderElement::setScale(float s) {

    this->scale = glm::vec3(s,s,s);

}

void RenderElement::prepareUniforms(Camera * camera) {

    //if (!shader->isReady()) return;
    if (!isReady) return;
    if (!isLinkedWithShader) linkWithShader();

    /*shader->loadInt(diffuseMapLoc, 0);
    shader->loadInt(normalMapLoc, 1);
    shader->loadInt(shader->getUniformLocation("specularMap"), 2);*/

    this->shader->setTextureUniforms();

    this->shader->loadMatrix(transformLocation, this->getTransformationMatrix());
    this->shader->loadMatrix(projectionLocation, camera->getProjectionMatrix());
    this->shader->loadMatrix(viewLocation, camera->getViewMatrix());

    shader->loadVec3(sunDirectionLoc, glm::vec3(-1,-1,0));
    shader->loadFloat(timeLoc, GameEngine::getTime());

}

void RenderElement::prepareUniforms(Camera * camera, glm::vec3 sunDir) {

    //if (!shader->isReady()) return;
    if (!isReady) return;
    if (!isLinkedWithShader) linkWithShader();

    /*shader->loadInt(diffuseMapLoc, 0);
    shader->loadInt(normalMapLoc, 1);
    shader->loadInt(shader->getUniformLocation("specularMap"), 2);*/

    shader->setTextureUniforms();

    this->shader->loadMatrix(transformLocation, this->getTransformationMatrix());
    this->shader->loadMatrix(projectionLocation, camera->getProjectionMatrix());
    this->shader->loadMatrix(viewLocation, camera->getViewMatrix());

    shader->loadVec3(sunDirectionLoc, sunDir);
    shader->loadFloat(timeLoc, GameEngine::getTime());

}

void RenderElement::setLevelOfDetail(int lvl) {

    this->model = levelsOfDetail[lvl];

}

int RenderElement::addLevelOfDetail(Model * model) {

    int index = levelsOfDetail.size();

    levelsOfDetail.push_back(model);
    return index;

}

void RenderElement::choseLevelOfDetail(glm::vec3 camerapos) {

    float dist = sqrt((camerapos.x - position.x)*(camerapos.x - position.x) + (camerapos.y - position.y)*(camerapos.y - position.y) + (camerapos.z - position.z)*(camerapos.z - position.z));

    if (dist < 100 && levelsOfDetail.size()) {
        this->model = levelsOfDetail[0];
    } else if (levelsOfDetail.size() >= 2) {
        this->model = levelsOfDetail[1];
    }

}

void RenderElement::render() {

    if (!model->isReady()) return;

    glBindVertexArray(model->getVaoId());

    model->activateAttributes(0);

    glDrawElements(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0);

    model->deactivateAttributes(0);

    glBindVertexArray(0);

}

void RenderElement::render(Camera * camera) {

    if (!model->isReady()) return;

    glBindVertexArray(model->getVaoId());

    model->activateAttributes(0);

    glDrawElements(model->getPrimitiveType(), model->getIndexCount(), GL_UNSIGNED_INT, 0);

    model->deactivateAttributes(0);

    glBindVertexArray(0);

}

CompoundNode * RenderElement::asCompound() {

    CompoundNode * rootNode = new CompoundNode();

    rootNode->addChildNode((Node<void *>*) new Node<const char *>("model", (const char *) this->modelName.c_str()));
    rootNode->addChildNode((Node<void *>*) new Node<const char *>("shader", (const char *) this->shaderName.c_str()));
    rootNode->addChildNode((Node<void *>*) new Node<int>("type", RenderElementType::DEFAULT));

    return rootNode;

}

glm::vec3 RenderElement::getPos() {

    return position;
}

float RenderElement::getScale() {
    return scale.x;
}

void RenderElement::setRotationAxis(float x, float y, float z) {
    this->rotAxis = glm::vec3(x, y, z);
}

StaticRenderElement * createStaticRenderElement(CompoundNode * rootNode) {

    std::string model = std::string(rootNode->get<const char*>("model"));
    std::string shader = std::string(rootNode->get<const char*>("shader"));
    int maxCount = rootNode->get<int>("maxCount");

    return new StaticRenderElement(model, shader, maxCount);

}

RenderElement * createRenderElement(CompoundNode * rootNode) {

    std::string model = std::string(rootNode->get<const char*>("model"));
    std::string shader = std::string(rootNode->get<const char*>("shader"));

    return new RenderElement(model, shader, 0, 0 ,0);

}

std::string RenderElement::getModelName() {
    return this->modelName;
}

RenderElement * RenderElement::fromCompound(CompoundNode * rootNode) {

    RenderElementType type = (RenderElementType) rootNode->get<int>("type");

    switch(type) {

        case STATIC:
            return createStaticRenderElement(rootNode);
            break;

        default:
            return createRenderElement(rootNode);

    }

}

glm::vec3 RenderElement::getRotationAxis() {
    return rotAxis;
}

Quaternion RenderElement::getRotation() {
    return rotation;
}
