#ifndef VIEWPORTDEFFERED_H
#define VIEWPORTDEFFERED_H

#include "viewport.h"
#include "cubemap.h"

#include "util/math/vector3.h"

#include "structure/structure.h"
#include "renderbuffer.h"
#include "postprocessingstack.h"
#include "lut.h"
#include "shadowcamera.h"

typedef struct deffered_light_t {

    float x;
    float y;
    float z;

    float r;
    float g;
    float b;

    int type;
    float falloff;

} DEFFERED_LIGHT;

class ViewportDeffered : public Viewport
{
    public:
        ViewportDeffered(int width, int height, float fov, float zNear, float zFar, int maxLights);
        virtual ~ViewportDeffered();

        void renderElements();

        void updateSize(int newWidth, int newHeight) override;

        int addLight(glm::vec3 pos, glm::vec3 color, LIGHT_TYPE type, float falloff);
        void setLightAsSun(int lightIndex);
        void updateLight(int id, glm::vec3 pos, glm::vec3 color, LIGHT_TYPE type, float falloff);
        void addStructureLights(Structure * strc, Math::Vector3 pos, Math::Quaternion rot, double scale);
        void clearLights();

        void setSkybox(CubeMap * box);
        void setLUT(LUT * lut);
        void addPostProcessingEffect(PostProcessingEffect * effect);
        void prerenderReflectionTextures();
        std::vector<float> getContent(int * width, int * height);

    protected:

    private:

        unsigned int gBuffer;
        unsigned int gPosition;
        unsigned int gNormal;
        unsigned int gColorSpec;

        RenderBuffer * renderBuffer;
        RenderBuffer * transparentBuffer;
        RenderBuffer * resultBuffer;
        RenderBuffer * shadowBuffer;

        ShadowCamera * shadowCamera;

        PostProcessingStack * ppStack;

        Shader * blitShader;

        unsigned int depthBuffer;

        unsigned int lightBuffer;

        Shader * lightShader;
        Model * quadModel;
        Texture * ppresult;

        CubeMap * skybox;
        Model * boxModel;
        Shader * boxShader;

        int maxLightCount;
        int currentLights;

        DEFFERED_LIGHT * lightData;

        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> colors;
        std::vector<int> types;
        std::vector<float> falloffs;

        std::vector<int> ids;

        bool sizeUpdate;
        bool hasReflections;

        void updateFramebuffer();
        void sortLights();

        int resolutionX;
        int resolutionY;
        int sunLightIndex;

};

#endif // VIEWPORTDEFFERED_H
