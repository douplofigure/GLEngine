#include "font.h"

#include <iostream>

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "resources/resourcemanager.h"
#include "util/fileutils.h"
#include "gameengine.h"

typedef struct font_vertex_t {

    float x;
    float y;
    float z;

    float nx;
    float ny;
    float nz;

    float u;
    float v;

    float r;
    float g;
    float b;

} FONT_VERTEX;

float basicQuad[44] {

    0, 0, 0,    0,0,1,  0,1,     0,0,0,
    1, 0, 0,    0,0,1,  1,1,     0,0,0,
    1, 1, 0,    0,0,1,  1,0,     0,0,0,
    0, 1, 0,    0,0,1,  0,0,     0,0,0

};

int basicIndecies[6] = {0,1,2,0,2,3};

std::vector<FONT_VERTEX> createQuadWithUV(float minX, float minY, float maxX, float maxY);
void writeToPLYFile(std::string name, std::vector<float> vertexData, std::vector<int> indexData);

Font::Font(std::vector<GLYPH_INFO> data, std::vector<std::string> pageNames)  {

    for (int i = 0; i < data.size(); ++i) {

        this->charData[data[i].id] = data[i];

    }

    for (int i = 0; i < pageNames.size(); ++i) {
        std::string fontBaseDir = "fonts/";
        this->pages.push_back(GameEngine::resources->get<Texture>(fontBaseDir.append(pageNames[i])));
        std::cout << "Page " << i << " loaded" << std::endl;
    }
    std::cout << "Font Constructor End" << std::endl;

}

Font::~Font()
{
    //dtor
}

Model * Font::renderStringToModel(std::string str) {

    const char * text = str.c_str();
    char currentChar;

    int i = 0;

    int currentVertIndex = 0;

    float cursorX = 0.0;
    float cursorY = 0.0;

    float Xmin = 1e32;
    float Xmax = -1e32;
    float Ymin = 1e32;
    float Ymax = -1e32;

    std::vector<float> modelData;
    std::vector<int>   modelIndecies;

    while(currentChar = text[i++]) {

        if (currentChar == '\n') {

            cursorX = 0.0;
            cursorY -= (float) lineHeight / pageHeight;
            continue;
        }

        GLYPH_INFO currentInfo = charData[(int)currentChar];

        float minX = (float) currentInfo.x / pageWidth;
        float maxY = 1 - (float) currentInfo.y / pageHeight;

        float maxX = (float)(currentInfo.x + currentInfo.width) / pageWidth;
        float minY = 1 - (float)(currentInfo.y + currentInfo.height) / pageHeight;

        float offsetx = (float) currentInfo.xOffset / pageWidth;
        float offsety = (float) currentInfo.yOffset / pageHeight;

        std::vector<FONT_VERTEX> quadData = createQuadWithUV(minX, maxY, maxX, minY);

        std::vector<int> vertIndexData = std::vector<int>(6);

        for (int j = 0; j < 4; ++j) {


            quadData[j].x *= (float) currentInfo.width / pageWidth;
            quadData[j].y *= (float) currentInfo.height / pageHeight;

            quadData[j].x += cursorX + offsetx;
            quadData[j].y += cursorY - (offsety + (float)currentInfo.height / pageHeight);

            if (quadData[j].x < Xmin) Xmin = quadData[j].x;
            if (quadData[j].x > Xmax) Xmax = quadData[j].x;
            if (quadData[j].y < Ymin) Ymin = quadData[j].y;
            if (quadData[j].y > Ymax) Ymax = quadData[j].y;

            quadData[j].r = currentInfo.page;

            //vertIndexData[j] = currentVertIndex++;

            float * start = (float *) &quadData[j];

            modelData.insert(modelData.end(), start, start + 11);

        }
        for (int j = 0; j < 6; ++j) {
            vertIndexData[j] = basicIndecies[j] + currentVertIndex;
        }
        currentVertIndex += 4;
        modelIndecies.insert(modelIndecies.end(), vertIndexData.begin(), vertIndexData.end());

        cursorX += (float) currentInfo.xAdvance / pageWidth;

    }


    double length;

    if (Xmax-Xmin > Ymax -Ymin) {
        length = Xmax - Xmin;
    } else {
        length = Ymax - Ymin;
    }

    float dx = -(Xmin + Xmax) / (length);
    float dy = -(Ymin + Ymax) / (length);

    FONT_VERTEX * verts = (FONT_VERTEX *) modelData.data();

    for (int i = 0; i < modelData.size() / 11; ++i) {

        verts[i].x *= 2 / length;
        verts[i].y *= 2 / length;

        verts[i].x += dx;
        verts[i].y += dy;

    }

    ///TODO write output to ply file for testing

    //writeToPLYFile("font_test.ply", modelData, modelIndecies);

    Model * model = createModel(modelData, modelData.size() / 11, modelIndecies, GL_TRIANGLES);
    //free(verts);

    return model;

}

std::vector<FONT_VERTEX> createQuadWithUV(float minX, float minY, float maxX, float maxY) {

    FONT_VERTEX * verts = (FONT_VERTEX*) malloc(sizeof(FONT_VERTEX) * 4);

    for (int i = 0; i < 4; ++i) {

        verts[i] = ((FONT_VERTEX*) basicQuad)[i];

    }

    verts[0].u = minX;
    verts[0].v = maxY;

    verts[1].u = maxX;
    verts[1].v = maxY;

    verts[2].u = maxX;
    verts[2].v = minY;

    verts[3].u = minX;
    verts[3].v = minY;

    return std::vector<FONT_VERTEX>(verts, verts + 4);

}

void Font::setPageSize(int width, int height) {

    this->pageWidth = width;
    this->pageHeight = height;

}

void Font::setLineHeight(int height) {

    this->lineHeight = height;

}

int Font::getPageCount(){
    return this->pages.size();
}

Texture * Font::getPageTexture(int pageNum) {

    return this->pages[pageNum];

}

/*Font * FontLoader::createFont(std::string name) {

    std::string dirName = ResourceManager::getFontDir();
    dirName.append(name).append(".bin");
    FILE * file = fopen(dirName.c_str(), "r");

    if (!file)
        std::cerr << "Could not find '" << dirName << "'\n";

    int lineHeight;
    int base;
    int pageWidth;
    int pageHeight;
    int pageCount;
    int charCount;

    fread(&lineHeight, 1, sizeof(int), file);
    fread(&base, 1, sizeof(int), file);
    fread(&pageWidth, 1, sizeof(int), file);
    fread(&pageHeight, 1, sizeof(int), file);

    fread(&pageCount, 1, sizeof(int), file);

    std::vector<std::string> pNames;
    std::cout << "Reading " << pageCount << " pages\n";
    for (int i = 0; i < pageCount; ++i) {

        int id;
        short nameLen;

        fread(&id, 1, sizeof(int), file);
        fread(&nameLen, 1, sizeof(short), file);

        char * pName = (char *) malloc(sizeof(char) * nameLen + 1);
        fread(pName, nameLen, 1, file);
        pName[nameLen] = 0;

        pNames.push_back(std::string(pName));

    }

    fread(&charCount, 1, sizeof(int), file);
    std::cout << "Reading " << charCount << " chars\n";
    CHAR_INFO * info = (CHAR_INFO *) malloc(sizeof(CHAR_INFO) * charCount);

    fread(info, charCount, sizeof(CHAR_INFO), file);

    std::vector<CHAR_INFO> charInfo = std::vector<CHAR_INFO>(info, info+charCount);

    Font * font = new Font(charInfo, pNames);

    font->setPageSize(pageWidth, pageHeight);
    font->setLineHeight(lineHeight);

    return font;

}*/

FontInjector::FontInjector(std::vector<GLYPH_INFO> info, std::vector<std::string> pages, int pageWidth, int pageHeight, int lineHeight) {
    this->info = info;
    this->pageNames = pages;

    this->pgWidth = pageWidth;
    this->pgHeight = pageHeight;
    this->lineHeight = lineHeight;
    this->type = getTypeName<Font>();
}

Font * FontInjector::upload() {
    Font * font = new Font(info, pageNames);

    font->setPageSize(pgWidth, pgHeight);
    font->setLineHeight(lineHeight);

    return font;
}

void FontInjector::deleteTempData() {



}

ResourceInjector<Font> * FontLoader::create(std::string dir, std::string name) {

    std::string dirName = dir;
    dirName.append(name).append(".bin");
    FILE * file = fopen(dirName.c_str(), "rb");

    if (!file)
        std::cerr << "Could not find '" << dirName << "'\n";

    int lineHeight;
    int base;
    int pageWidth;
    int pageHeight;
    int pageCount;
    int charCount;

    fread(&lineHeight, 1, sizeof(int), file);
    fread(&base, 1, sizeof(int), file);
    fread(&pageWidth, 1, sizeof(int), file);
    fread(&pageHeight, 1, sizeof(int), file);

    fread(&pageCount, 1, sizeof(int), file);

    std::vector<std::string> pNames;
    std::cout << "Reading " << pageCount << " pages\n";
    for (int i = 0; i < pageCount; ++i) {

        int id;
        short nameLen;

        fread(&id, 1, sizeof(int), file);
        fread(&nameLen, 1, sizeof(short), file);

        char * pName = (char *) malloc(sizeof(char) * nameLen + 1);
        for (int j = 0; j < nameLen; ++j)  {
            fread(pName+j, 1, 1, file);
        }
        pName[nameLen] = 0;

        pNames.push_back(std::string(pName));

    }

    fread(&charCount, 1, sizeof(int), file);
    std::cout << "Reading " << charCount << " chars\n";
    GLYPH_INFO * info = (GLYPH_INFO *) malloc(sizeof(GLYPH_INFO) * charCount);

    fread(info, charCount, sizeof(GLYPH_INFO), file);

    std::vector<GLYPH_INFO> charInfo = std::vector<GLYPH_INFO>(info, info+charCount);

    return new FontInjector(charInfo, pNames, pageWidth, pageHeight, lineHeight);

}

bool FontLoader::canCreate(std::string dir, std::string name) {

    std::string dirName = dir;
    dirName.append(name).append(".bin");
    return fileExists(dirName);

}

std::vector<std::string> FontLoader::getFilenames(std::string dir, std::string name) {

    std::string dirName = dir;
    dirName.append(name).append(".bin");

    std::vector<std::string> files(1);
    files[0] = dirName;
    return files;

}

#define PLY_FACE_VERT_COUNT 3

void writeToPLYFile(std::string name, std::vector<float> vertexData, std::vector<int> indexData) {

    FILE * file = fopen(name.c_str(), "w");

    fprintf(file, "ply\nformat ascii 1.0\n");

    fprintf(file, "element vertex %lu\n", vertexData.size() / 11);

    fprintf(file, "property float x\nproperty float y\nproperty float z\nproperty float nx\nproperty float ny\nproperty float nz\nproperty float s\nproperty float t\nproperty float red\nproperty float green\nproperty float blue\n");

    fprintf(file, "element face %lu\n", indexData.size() / PLY_FACE_VERT_COUNT);
    fprintf(file, "property list uchar uint vertex_indices\n");
    fprintf(file, "end_header\n");

    for (int i = 0; i < vertexData.size()/11; ++i) {

        for (int j = 0; j < 11; ++j) {

            fprintf(file, "%f ", vertexData[i*11 + j]);

        }
        fprintf(file, "\n");

    }

    for (int i = 0; i < indexData.size()/PLY_FACE_VERT_COUNT; ++i) {

        fprintf(file, "%d ", PLY_FACE_VERT_COUNT);

        for (int j = 0; j < PLY_FACE_VERT_COUNT; ++j) {

            fprintf(file, "%d ", indexData[i*PLY_FACE_VERT_COUNT + j]);

        }
        fprintf(file, "\n");

    }

    fclose(file);

}

void Font::deleteData() {

}
