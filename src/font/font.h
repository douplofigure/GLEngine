#ifndef FONT_H
#define FONT_H

#include <map>
#include <vector>
#include <string>

#include "render/texture.h"
#include "render/model.h"

typedef struct char_info_t {

    int id;
    int x;
    int y;
    int width;
    int height;
    int xOffset;
    int yOffset;
    int xAdvance;
    int page;
    int chanel;

} GLYPH_INFO;

class Font : Resource {

    public:
        Font(std::vector<GLYPH_INFO> info, std::vector<std::string> pageNames);
        virtual ~Font();

        static Font * createFont(std::string name);

        Model * renderStringToModel(std::string text);
        Texture * getPageTexture(int pageNum);

        void setPageSize(int width, int height);
        void setLineHeight(int height);
        void deleteData();

        int getPageCount();

    protected:

        std::map<int, GLYPH_INFO> charData;
        std::vector<Texture *> pages;


        int lineHeight;
        int base;

        int pageHeight;
        int pageWidth;

    private:

};

class FontInjector : public ResourceInjector<Font> {

    public:
        FontInjector(std::vector<GLYPH_INFO> info, std::vector<std::string> pageNames, int pageWidth, int pageHeight, int lineHeight);

        Font * upload();
        void deleteTempData();

    private:
        std::vector<GLYPH_INFO> info;
        std::vector<std::string> pageNames;

        int pgWidth;
        int pgHeight;
        int lineHeight;

};

class FontLoader : public ResourceLoader<Font> {

    public:

    ResourceInjector<Font> * create(std::string dir, std::string name) override;
    bool canCreate(std::string dir, std::string name) override;

    std::vector<std::string> getFilenames(std::string dir, std::string name);

    /// DEPRECATED
    //static Font * createFont(std::string name);

};

#endif // FONT_H
