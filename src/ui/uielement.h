#ifndef UIELEMENT_H
#define UIELEMENT_H

#include "render/renderelement.h"

typedef enum anchor_e {

    NORTH = 1,
    SOUTH = 2,
    WEST  = 4,
    EAST  = 8

} ANCHOR;

inline ANCHOR operator|(ANCHOR a, ANCHOR b)
{return static_cast<ANCHOR>(static_cast<int>(a) | static_cast<int>(b));}

class UIElement : public RenderElement
{
    public:
        ///Coords ad scale are relative to screen, (0,0) is bottom-left
        UIElement(float x, float y, float width, float height, Texture * texture);
        UIElement(float x, float y, float width, float height, Texture * texture, ANCHOR anchor);
        virtual ~UIElement();

        glm::mat4 getTransformationMatrix();

        void updateScreenSize(int width, int height);

    protected:

        float width;
        float height;

        float x;
        float y;

        float aspect;

    private:
};

#endif // UIELEMENT_H
