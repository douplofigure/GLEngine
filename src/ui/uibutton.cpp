#include "uibutton.h"

#include <iostream>

UIButton::UIButton(float x, float y, float width, float height, Texture * texture) : UIElement(x, y, width, height, texture){

    bounds[0] = x - width/2;
    bounds[1] = y - height/2;

    bounds[2] = x + width/2;
    bounds[3] = y + height/2;

}

UIButton::UIButton(float x, float y, float width, float height, Texture * texture, ANCHOR anchor) : UIElement(x,y,width, height, texture, anchor) {

    bounds[0] = x - width/2;
    bounds[1] = y - height/2;

    bounds[2] = x + width/2;
    bounds[3] = y + height/2;

    if (anchor & NORTH){
        this->bounds[1] -= this->height/2;
        this->bounds[3] -= this->height/2;
    }
    if (anchor & SOUTH) {
        this->bounds[1] += this->height/2;
        this->bounds[3] += this->height/2;
    }

    if (anchor & EAST){
        this->bounds[0] -= this->width/2;
        this->bounds[2] -= this->width/2;
    }
    if (anchor & WEST) {
        this->bounds[0] += this->width/2;
        this->bounds[2] += this->width/2;
    }

}

UIButton::~UIButton()
{
    //dtor
}

bool UIButton::isActive() {
    return true;
}

bool UIButton::mouseOver(double x, double y) {

    std::cout << x << " : " << y << " (" << bounds[0] << ", " << bounds[1] << ") to (" << bounds[2] << ", " << bounds[3] << ")" << std::endl;
    return x > bounds[0] && x < bounds[2] && y > bounds[1] && y < bounds[3];

}

void UIButton::onClick(int button) {
    std::cout << "Clicked Button" <<std::endl;
}

void UIButton::onMouseOver() {

}

void UIButton::onKeyboard(int key) {

}
