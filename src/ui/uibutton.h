#ifndef UIBUTTON_H
#define UIBUTTON_H

#include "uielement.h"
#include "uiinteractionelement.h"

class UIButton : public UIElement, public UIInteractionElement
{
    public:
        /** Default constructor */
        UIButton(float x, float y, float width, float height, Texture * texture);
        UIButton(float x, float y, float width, float height, Texture * texture, ANCHOR anchor);
        /** Default destructor */
        virtual ~UIButton();

        void onClick(int button);
        void onMouseOver();

        void onKeyboard(int key);

        bool isActive();
        bool mouseOver(double x, double y);

    protected:

    private:

        double bounds[4];


};

#endif // UIBUTTON_H
