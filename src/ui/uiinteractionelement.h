#ifndef UIINTERACTIONELEMENT_H
#define UIINTERACTIONELEMENT_H


class UIInteractionElement
{
    public:
        /** Default constructor */
        UIInteractionElement();
        /** Default destructor */
        virtual ~UIInteractionElement();

        virtual void onClick(int button) = 0;
        virtual void onMouseOver() = 0;

        virtual void onKeyboard(int key) = 0;

        virtual bool isActive() = 0;
        virtual bool mouseOver(double x, double y) = 0;

    protected:

    private:
};

#endif // UIINTERACTIONELEMENT_H
