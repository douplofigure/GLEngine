#include "fontuielement.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

FontUIElement::FontUIElement(float x, float y, float width, float height, std::string text, Font * font) : FontRenderElement(text, font, true){

    this->width = width;
    this->height = height;

    this->x = x * 2 -1;
    this->y = y * 2 -1;

}

FontUIElement::FontUIElement(float x, float y, float width, float height, std::string text, Font * font, ANCHOR anchor) : FontRenderElement(text, font, true){

    this->width = width;
    this->height = height;

    this->x = x * 2 -1;
    this->y = y * 2 -1;

    if (anchor & NORTH) this->y -= this->height;
    if (anchor & SOUTH) this->y += this->height;

    if (anchor & EAST) this->x -= this->width;
    if (anchor & WEST) this->x += this->width;

}

FontUIElement::~FontUIElement()
{
    //dtor
}


glm::mat4 FontUIElement::getTransformationMatrix() {

    glm::mat4 smat = glm::scale(glm::vec3(width, height*aspect, 1));

    glm::mat4 tmat = glm::translate(glm::vec3(x, y*(aspect/2), 0));

    return tmat * smat;

}

void FontUIElement::updateScreenSize(int width, int height) {

    this->aspect = (float) width / (float) height;

}
