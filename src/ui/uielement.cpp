#include "uielement.h"

#include <iostream>

#include "resources/resourcemanager.h"
#include "gameengine.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

UIElement::UIElement(float x, float y, float width, float height, Texture * texture) : RenderElement(GameEngine::resources->get<Model>("quad"), GameEngine::resources->get<Shader>("ui"), 0, 0, 0) {

    this->width = width;
    this->height = height;

    this->x = x * 2 -1;
    this->y = y * 2 -1;

    this->setDiffuseMap(texture);

}

UIElement::UIElement(float x, float y, float width, float height, Texture * texture, ANCHOR anchor) : RenderElement(GameEngine::resources->get<Model>("quad"), GameEngine::resources->get<Shader>("ui"), 0, 0, 0) {

    this->width = width;
    this->height = height;
    this->setDiffuseMap(texture);

    this->x = x * 2 -1;
    this->y = y * 2 -1;

    if (anchor & NORTH) this->y -= this->height;
    if (anchor & SOUTH) this->y += this->height;

    if (anchor & EAST) this->x -= this->width;
    if (anchor & WEST) this->x += this->width;


}

UIElement::~UIElement()
{
    //dtor
}

glm::mat4 UIElement::getTransformationMatrix() {

    glm::mat4 smat = glm::scale(glm::vec3(width / aspect, height, 1));

    glm::mat4 tmat = glm::translate(glm::vec3(2*x/aspect, y, 0));

    return tmat * smat;

}

void UIElement::updateScreenSize(int width, int height) {

    this->aspect = (float) width / (float) height;

}
