#ifndef FONTUIELEMENT_H
#define FONTUIELEMENT_H

#include "render/fontrenderelement.h"
#include "uielement.h"

class FontUIElement : public FontRenderElement
{
    public:
        FontUIElement(float x, float y, float width, float height, std::string text, Font * font);
        FontUIElement(float x, float y, float width, float height, std::string text, Font * font, ANCHOR anchor);
        virtual ~FontUIElement();

        virtual glm::mat4 getTransformationMatrix();
        void updateScreenSize(int width, int height);

    protected:

        float width;
        float height;

        float x;
        float y;

        float aspect;

    private:
};

#endif // FONTUIELEMENT_H
