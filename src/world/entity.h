#ifndef ENTITY_H
#define ENTITY_H

#include <configloader.h>
#include <functional>

#include <btBulletDynamicsCommon.h>

#include "render/renderelement.h"
#include "physics/physicsobject.h"
#include "util/math/vector3.h"
#include "util/math/quaternion.h"
#include "audio/audiosource.h"

#include "structure/level.h"

using namespace std;
using namespace Math;

class Entity;

typedef std::function<std::shared_ptr<Entity>(Structure * strc, Vector3 pos, Quaternion rot, double mass, std::shared_ptr<RenderElement> rElem, bool pickUp, CompoundNode * data)> EntityCreator;

typedef enum entity_type_e {

    DEFAULT_ENTITY,
    STATIC_ENTITY

} EntityType;

class Entity
{
    public:

        Entity(Structure * strc, Vector3 pos, Vector3 rot, double mass, std::shared_ptr<RenderElement> rElem);
        Entity(Structure * strc, Vector3 pos, Vector3 rot, double mass, std::shared_ptr<RenderElement> rElem, bool pickup);
        Entity(Structure * strc, Vector3 pos, Vector3 rot, double mass, std::shared_ptr<RenderElement> rElem, Sound * sound);
        Entity(Structure * strc, Vector3 pos, Quaternion rot, double mass, std::shared_ptr<RenderElement> rElem, bool pickUp);

        Entity(btCollisionShape * shape, Vector3 pos, Quaternion rot, double mass, std::shared_ptr<RenderElement> rElem, bool pickUp);

        Entity(std::shared_ptr<RenderElement> elem);
        Entity(std::shared_ptr<RenderElement> elem, double density);
        Entity(std::shared_ptr<RenderElement> elem, Vector3 pos);
        virtual ~Entity();

        virtual void onUpdate(double dTime);

        virtual void onPhysicsAdded();
        virtual void onPhysicsStart();
        virtual void onWorldAdded(World * world);

        virtual void setScale(double scale);
        virtual void setRotation(double x, double y, double z, double angle);

        virtual void onCollision(std::shared_ptr<Entity> entity);

        /// These are the internal methods calling callbacks
        void collisionInternal(std::shared_ptr<Entity> entity);
        void physicsStartInternal();
        static void physicsAddInternal(std::shared_ptr<Entity> entity);

        bool canPickUp();

        virtual CompoundNode * toCompound();
        static std::shared_ptr<Entity> fromCompound(CompoundNode * rootNode, std::shared_ptr<RenderElement> rElem);
        static std::shared_ptr<Entity> getFromWorld(btCollisionObject * obj);
        static void clearWorld();

        static std::shared_ptr<Entity> createEntityFromType(level::LEVEL_PLACEMENT & placementData, std::shared_ptr<RenderElement> rElem);
        static void registerEntityType(std::string typeName, EntityCreator creator);

        static std::shared_ptr<Entity> create(Structure * strc, Vector3 pos, Quaternion rot, double mass, std::shared_ptr<RenderElement> rElem, bool pickUp, CompoundNode * data);

        std::shared_ptr<RenderElement> getRenderElement();

        PhysicsObject * getPhysicsObject();


        static void processCollision(btCollisionObject * objA, btCollisionObject * objB);

    protected:

        Entity();

        std::shared_ptr<RenderElement> rElem;
        PhysicsObject * physObj;
        AudioSource * audioSource;
        Vector3 startPos;

    private:

        Sound * collisionSound;
        bool pickUp;

        static std::unordered_map<btCollisionObject *, std::shared_ptr<Entity>> entsByCollisionObject;
        static std::unordered_map<std::string, EntityCreator> creators;



};

#endif // ENTITY_H
