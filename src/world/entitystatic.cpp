#include "entitystatic.h"

#include "util/math/vector3.h"
#include "util/math/conversions.h"

using namespace Math;

EntityStatic::EntityStatic(std::shared_ptr<StaticRenderElement> rElem, Vector3 position) {

    glm::vec3 rPos = asRender(position);

    this->srElem = rElem;
    this->instanceId = rElem->addInstance(rPos.x, rPos.y, rPos.z);
    this->rElem = rElem;

    this->physObj = new PhysicsObject(0, position);

}

EntityStatic::EntityStatic(std::shared_ptr<StaticRenderElement> rElem, int instanceId) {

    this->srElem = rElem;
    this->instanceId = instanceId;
    this->rElem = rElem;

    glm::vec3 rPos = rElem->getInstancePosition(instanceId);

    this->physObj = new PhysicsObject(0, fromRender(rPos));

}

EntityStatic::~EntityStatic()
{
    //dtor
}

void EntityStatic::setScale(double scale) {

    this->srElem->updateInstance(this->instanceId, this->srElem->getInstancePosition(instanceId), this->srElem->getInstanceRotation(instanceId), scale);

}

void EntityStatic::setRotation(double x, double y, double z, double angle) {

    Math::Quaternion quat = Math::Quaternion::fromAxisAngle(Vector3(x,y,z), angle);

    this->srElem->updateInstance(this->instanceId, this->srElem->getInstancePosition(instanceId), quat, this->srElem->getInstanceScale(instanceId));
    this->physObj->setRotation(quat);

}

void EntityStatic::onUpdate(double dTime) {

}

CompoundNode * EntityStatic::toCompound() {

    CompoundNode * rootNode = new CompoundNode();

    rootNode->addChildNode((Node<void*>*) new Node<double>("mass", this->physObj->getMass()));
    rootNode->addChildNode((Node<void*>*) new Node<double>("x", this->physObj->position.x));
    rootNode->addChildNode((Node<void*>*) new Node<double>("y", this->physObj->position.y));
    rootNode->addChildNode((Node<void*>*) new Node<double>("z", this->physObj->position.z));

    Math::Quaternion rot = srElem->getInstanceRotation(instanceId);

    rootNode->addChildNode((Node<void*>*) new Node<double>("ra", rot.a));
    rootNode->addChildNode((Node<void*>*) new Node<double>("rb", rot.b));
    rootNode->addChildNode((Node<void*>*) new Node<double>("rc", rot.c));
    rootNode->addChildNode((Node<void*>*) new Node<double>("rd", rot.d));

    rootNode->addChildNode((Node<void*>*) new Node<int>("type", STATIC_ENTITY));
    rootNode->addChildNode((Node<void*>*) new Node<double>("scale", this->srElem->getInstanceScale(instanceId)));

    return rootNode;

}
