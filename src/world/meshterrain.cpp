#include "meshterrain.h"

#include "util/math/constants.h"

using namespace Math;

MeshTerrain::MeshTerrain(Mesh * vertexData, double w, double h) {

    this->mesh = vertexData;
    this->meshScale = w;
    this->mesh->normalize();
    width = w;
    height = h;
    std::cout << "Creating Mesh Terrain" << std::endl;
    this->mesh->buildTree();
    this->mesh->setScale(meshScale);

}

MeshTerrain::~MeshTerrain()
{
    delete mesh;
}

double MeshTerrain::getHeight(double x, double y) {
    double h = this->mesh->getHeight(x/meshScale ,y/meshScale);
    return h;

}


std::vector<double> MeshTerrain::getFace(double x, double z) {

    return this->mesh->getFace(x/meshScale,z/meshScale);

}

double MeshTerrain::getMeshScale() {
    return meshScale;
}
