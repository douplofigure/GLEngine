#ifndef TERRAIN_H
#define TERRAIN_H

#include "entity.h"

#include <vector>


class Terrain : public Entity
{
    public:
        Terrain();
        virtual ~Terrain();

        virtual double getHeight(double x, double y){return 0;};
        Math::Vector3 getNormal(double x, double y);

        virtual std::vector<double> getFace(double x, double y){
            return std::vector<double>(0);
        };

    protected:

        Math::Mesh * mesh;


    private:
};

#endif // TERRAIN_H
