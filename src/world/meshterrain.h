#ifndef MESHTERRAIN_H
#define MESHTERRAIN_H

#include "terrain.h"
#include "util/math/mesh.h"

class MeshTerrain : public Terrain
{
    public:
        /** Default constructor */
        MeshTerrain(Math::Mesh * model, double width, double height);
        /** Default destructor */
        virtual ~MeshTerrain();

        virtual double getHeight(double x, double y) override;
        std::vector<double> getFace(double x, double y) override;

        double getMeshScale();

    protected:

    private:

        double width;
        double height;

        double meshScale;

};

#endif // MESHTERRAIN_H
