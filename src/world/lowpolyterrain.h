#ifndef LOWPOLYTERRAIN_H
#define LOWPOLYTERRAIN_H

#include "terrain.h"
#include "heightmap.h"
#include "util/binarytree.h"

class LowPolyTerrain : public Terrain
{
    public:
        /** Default constructor */
        LowPolyTerrain(double x, double y, double width, double length, int resolution);
        /** Default destructor */
        virtual ~LowPolyTerrain();

        virtual double getHeight(double x, double y);
        virtual std::vector<double> getFace(double x, double y);

    protected:

        double dx;
        double dy;

        double minX;
        double minY;

        double width;
        double length;

    private:

        BinaryTree<Vector3, int *> * faceTree;

        std::vector<Vector3> generateTriangle(double x, double y, int rotation, double w, double l);
        double getActualHeight(double x, double y);

        HeightMap * heightMap;

};

#endif // LOWPOLYTERRAIN_H
