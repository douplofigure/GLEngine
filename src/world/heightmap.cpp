#include "heightmap.h"

#include <iostream>

#include <math.h>

HeightMap::HeightMap(std::vector<double> heights, int width, int length) {

    this->heights = heights;
    this->width = width;
    this->length = length;


    this->xGrads = std::vector<double>(width * length);
    this->yGrads = std::vector<double>(width * length);

    for (int i = 0; i < width * length; ++i){
        xGrads[i] = 0;
        yGrads[i] = 0;
    }

    for (int i = 1; i < width-1; ++i) {

        for (int j = 1; j < length-1; ++j) {

            double dhdx = getPixel(i+1, j) - getPixel(i-1, j);
            double dhdy = getPixel(i, j+1) - getPixel(i, j-1);

            setXGrad(i, j, dhdx/2.0);
            setYGrad(i, j, dhdy/2.0);


        }

    }


}

HeightMap::~HeightMap()
{
    //dtor
}

double HeightMap::getHeightValue(double x, double y) {

    int ix = round((1-x) * width);
    int iy = round(y * length);

    double wx = (1-x) * width;
    double wy = y * length;

    //std::cout << x << " " << y << std::endl;

    double h1 = getPixel(ix, iy);
    //std::cout << ((double)ix-wx) * ((double)ix-wx) + ((double)iy-wy)*((double)iy-wy) << std::endl;
    double dhdx = getXGrad(ix, iy);
    double dhdy = getYGrad(ix, iy);
    /*double h4 = heights[(ix + iy * width) % heights.size()];
    double d4 = 1.0 - sqrt(((double)ix-wx) * ((double)ix-wx) + ((double)iy-wy)*((double)iy-wy));*/

    //std::cout << "distances: " << d1 << " " << d2 << " " << d3 << std::endl;

    return h1 + dhdx * (wx-(double)ix) + dhdy * (wy-(double)iy);

}

double HeightMap::getPixel(int x, int y) {

    return this->heights[(y%length)*width+(x%width)];

}

double HeightMap::getXGrad(int x, int y) {

    return this->xGrads[(y%length)*width+(x%width)];

}

double HeightMap::getYGrad(int x, int y) {

    return this->yGrads[(y%length)*width+(x%width)];

}

void HeightMap::setPixel(int x, int y, double h) {

    this->heights[(y%length)*width+(x%width)] = h;

}

void HeightMap::setXGrad(int x, int y, double h) {

    this->xGrads[(y%length)*width+(x%width)] = h;

}

void HeightMap::setYGrad(int x, int y, double h) {

    this->yGrads[(y%length)*width+(x%width)] = h;

}

HeightMap * HeightMap::create(std::vector<float> imgData, int width, int height, int chanelCount) {

    std::vector<double> heights(width * height);

    std::cout << "Creating height map " << width << "x" << height << std::endl;

    for (int i = 0; i < width * height; ++i) {

        heights[i] = (imgData[i * chanelCount] + imgData[i*chanelCount] + imgData[i*chanelCount]);

    }

    return new HeightMap(heights, width, height);

}
