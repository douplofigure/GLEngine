#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include <vector>

class HeightMap
{
    public:
        /** Default constructor */
        HeightMap(std::vector<double> heights, int width, int length);
        /** Default destructor */
        virtual ~HeightMap();

        double getHeightValue(double x, double y);

        static HeightMap * create(std::vector<float> imageData, int width, int length, int chanelCount);

    protected:

        double getPixel(int x, int y);
        double getXGrad(int x, int y);
        double getYGrad(int x, int y);
        void setPixel(int x, int y, double h);
        void setXGrad(int x, int y, double h);
        void setYGrad(int x, int y, double h);

        std::vector<double> heights;
        std::vector<double> xGrads;
        std::vector<double> yGrads;
        int width;
        int length;

    private:
};

#endif // HEIGHTMAP_H
