#ifndef ENTITYSTATIC_H
#define ENTITYSTATIC_H

#include "entity.h"

#include "render/staticrenderelement.h"

#include "util/math/vector3.h"

class EntityStatic : public Entity
{
    public:
        EntityStatic(std::shared_ptr<StaticRenderElement> rElem, Vector3 position);
        EntityStatic(std::shared_ptr<StaticRenderElement> rElem, int instanceId);
        virtual ~EntityStatic();

        virtual void onUpdate(double dTime) override;
        virtual void setScale(double scale) override;
        virtual void setRotation(double x, double y, double z, double angle) override;

        CompoundNode * toCompound();

    protected:

        int instanceId;
        std::shared_ptr<StaticRenderElement> srElem;

    private:
};

#endif // ENTITYSTATIC_H
