#include "world.h"

#include <algorithm>

#include <configloader.h>

#include <btBulletDynamicsCommon.h>

#include "flatterrain.h"
#include "lowpolyterrain.h"

/// gravitational pull of the current planet
#define GRAVITATIONAL_PULL 9.81

struct entity_add_comp_t {
    std::shared_ptr<Entity> entity;
    bool hasPhysics;
};

World::World(Terrain * terrain, PhysicsConfig * physicsConfig) {

    this->physics = new PhysicsContext(physicsConfig->getGravity(), terrain, physicsConfig->getMaxThreads());
    this->physicsConfig = physicsConfig;
    this->terrain = terrain;
    entities = std::vector<std::shared_ptr<Entity>>();

}

World::~World()
{
    this->physics->stop();
    delete physics;
}

void World::setTerrain(Terrain * t) {
    this->physics->setTerrain(t);
    this->terrain = t;
}

Terrain * World::getTerrain() {
    return terrain;
}

void World::addEntity(std::shared_ptr<Entity> ent, bool hasPhysics) {

    ENTITY_ADD_CONFIG conf;
    conf.entity = ent;
    conf.hasPhysics = hasPhysics;

    ent->onWorldAdded(this);

    this->newEntities.push_back(conf);

}

void World::addPlayer(std::shared_ptr<Entity> player, bool hasPhysics) {

    addEntity(player, hasPhysics);
    this->player = player;

}

std::shared_ptr<Entity> World::getPlayer() {
    return player;
}

void World::clearEntities() {

    this->physics->stop();
    entities = std::vector<std::shared_ptr<Entity>>();
    this->physics = new PhysicsContext(-9.81, new FlatTerrain(0.0));
    Entity::clearWorld();

}

void World::update(double dTime) {

    for (std::list<ENTITY_ADD_CONFIG>::iterator it = this->newEntities.begin(); it != this->newEntities.end(); ++it) {
        this->entities.push_back((*it).entity);
        if ((*it).hasPhysics){
            this->physics->addObject((*it).entity->getPhysicsObject());
            Entity::physicsAddInternal((*it).entity);
        }
    }

    this->newEntities = std::list<ENTITY_ADD_CONFIG>();

    this->physics->simulateStep(dTime);
    for (int i = 0; i < entities.size(); ++i) {
        entities[i]->onUpdate(dTime);

    }

}

void World::startPhysics() {
    this->physics->start();

    for (int i = 0; i < entities.size(); ++i) {
        entities[i]->physicsStartInternal();
    }

}

void World::removeEntity(std::shared_ptr<Entity> ent) {
    std::remove(entities.begin(), entities.end(), ent);
}

void World::saveToFile(std::string fname) {

    CompoundNode * rootNode = new CompoundNode();
    Node<int> * versionNode = new Node<int>("version", 0);
    rootNode->addChildNode((Node<void*>*) versionNode);

    ArrayNode<CompoundNode *> * entityList = new ArrayNode<CompoundNode *>("entities");
    ArrayNode<int> * entityRElemList = new ArrayNode<int>("renderBindings");
    ArrayNode<CompoundNode *> * renderElementList = new ArrayNode<CompoundNode *>("renderElements");

    int rElemCount = 0;
    std::vector<std::shared_ptr<RenderElement>> renderElements = std::vector<std::shared_ptr<RenderElement>>(entities.size());

    bool contains = false;
    for (int i = 0; i < entities.size(); ++i) {
        contains = false;
        for (int j = 0; j < rElemCount; ++j) {

            //std::cout << "Ent : " << entities[i]->getRenderElement() << " : " << renderElements[i] << std::endl;
            if (entities[i]->getRenderElement() == renderElements[j]) {
                contains = true;
                //std::cout << "Found RenderElement" << std::endl;
                entityRElemList->addValue(j);
                break;
            }

        }
        if (!contains) {
            entityRElemList->addValue(rElemCount);
            renderElementList->addValue(entities[i]->getRenderElement()->asCompound());
            renderElements[rElemCount] = entities[i]->getRenderElement();
            rElemCount++;
        }

    }

    for (int i = 0; i < entities.size(); ++i) {

        entityList->addValue(entities[i]->toCompound());

    }

    rootNode->addChildNode((Node<void*>*)entityList);
    rootNode->addChildNode((Node<void*>*)entityRElemList);
    rootNode->addChildNode((Node<void*>*)renderElementList);

    rootNode->addChildNode((Node<void*>*) new Node<const char *>("physicsConfig", (const char*)this->physicsConfig->getFileName().c_str()));

    //writeTreeToBinaryFile(rootNode, fname);


}

void World::addRenderElement(std::string name, std::shared_ptr<RenderElement> elem) {
    this->elementsByName[name] = elem;
}

std::shared_ptr<RenderElement> World::getRenderElement(std::string name) {
    return this->elementsByName[name];
}

World * World::loadFromFile(std::string fname, Viewport * viewport) {

    World * world = new World(new LowPolyTerrain(0, 0, 1024, 1024, 64), new PhysicsConfig("physics.conf"));

    CompoundNode * rootNode = ConfigLoader::loadFileTree(fname);

    std::vector<CompoundNode *> renderElementList = rootNode->getArray<CompoundNode*>(std::string("renderElements"))->getValue();
    std::vector<std::shared_ptr<RenderElement>> renderElements = std::vector<std::shared_ptr<RenderElement>>(renderElementList.size());

    for (int i = 0; i < renderElementList.size(); ++i) {

        renderElements[i] = std::shared_ptr<RenderElement>(RenderElement::fromCompound(renderElementList[i]));
        viewport->addElement(0, renderElements[i]);
        world->addRenderElement(renderElements[i]->getModelName(), renderElements[i]);

    }

    std::cout << "RenderElements OK : " << renderElements.size() << std::endl;

    std::vector<CompoundNode *> entityList = rootNode->getArray<CompoundNode*>("entities")->getValue();
    std::vector<int> bindings = rootNode->getArray<int>("renderBindings")->getValue();

    std::cout << "Loaded Bindings" << std::endl;

    for (int i = 0; i < entityList.size(); ++i) {

        std::cout << "Using RenderElement " <<  bindings[i] << std::endl;
        std::cout << renderElements[bindings[i]] << std::endl;
        world->addEntity(Entity::fromCompound(entityList[i], renderElements[bindings[i]]), false);
        std::cout << "Added Entity " << (i+1) << " of " << entityList.size() << std::endl;

    }

    std::cout << "Done Loading world" << std::endl;

    return world;

}

btDiscreteDynamicsWorld * World::getRigidBodyWorld() {

    return this->physics->getWorld();

}

void World::stopPhysics() {
    this->physics->stop();
}
