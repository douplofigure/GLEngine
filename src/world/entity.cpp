#include "entity.h"

#include "util/math/conversions.h"
#include "util/math/constants.h"
#include "util/terraingenerator.h"

#include "gameengine.h"

#include "entitystatic.h"

using namespace Math;

std::unordered_map<btCollisionObject *, std::shared_ptr<Entity>> Entity::entsByCollisionObject;
std::unordered_map<std::string, EntityCreator> Entity::creators;

Entity::Entity(Structure * strc, Vector3 pos, Vector3 rot, double mass, std::shared_ptr<RenderElement> elem) : Entity() {

    this->rElem = elem;

    this->physObj = new PhysicsObject(mass, pos);
    this->physObj->setRotation(rot.y, rot.x, rot.z);
    this->physObj->setCollisionShape(strc->getCollisionShape());

    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->collisionSound = nullptr;
    this->startPos = pos;

}

Entity::Entity(Structure * strc, Vector3 pos, Vector3 rot, double mass, std::shared_ptr<RenderElement> elem, bool pickup) : Entity() {

    this->rElem = elem;

    this->physObj = new PhysicsObject(mass, pos);
    this->physObj->setRotation(rot.y, rot.x, rot.z);
    this->physObj->setCollisionShape(strc->getCollisionShape());

    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->collisionSound = nullptr;//GameEngine::resources->get<Sound>("bounce");
    this->startPos = pos;

    this->pickUp = pickup;

}

Entity::Entity(Structure * strc, Vector3 pos, Vector3 rot, double mass, std::shared_ptr<RenderElement> elem, Sound * sound) : Entity() {

    this->rElem = elem;

    this->physObj = new PhysicsObject(mass, pos);
    this->physObj->setRotation(rot.y, rot.x, rot.z);
    this->physObj->setCollisionShape(strc->getCollisionShape());

    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->audioSource->setLooping(false);
    //this->audioSource->play(sound);
    this->collisionSound = sound;
    this->startPos = pos;

}

Entity::Entity(Structure * strc, Vector3 pos, Math::Quaternion rot, double mass, std::shared_ptr<RenderElement> elem, bool pickup) : Entity() {

    this->rElem = elem;

    this->physObj = new PhysicsObject(mass, pos);
    this->physObj->setRotation(rot);
    this->physObj->setCollisionShape(strc->getCollisionShape());

    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->collisionSound = nullptr;//GameEngine::resources->get<Sound>("bounce");
    this->startPos = pos;

    this->pickUp = pickup;

}

Entity::Entity(btCollisionShape * shape, Vector3 pos, Quaternion rot, double mass, std::shared_ptr<RenderElement> elem, bool pickup) : Entity() {

    this->rElem = elem;

    this->physObj = new PhysicsObject(mass, pos);
    this->physObj->setRotation(rot);
    this->physObj->setCollisionShape(shape);

    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->collisionSound = nullptr;//GameEngine::resources->get<Sound>("bounce");
    this->startPos = pos;

    this->pickUp = pickup;

}

Entity::Entity(std::shared_ptr<RenderElement> elem) {
    this->rElem = elem;

    //this->physObj->position = fromRender(elem->getPos());

    //double volume = elem->getModel()->getMeshData()->computeVolume();

    this->physObj = new PhysicsObject(1.0, Vector3(elem->getPos().x, elem->getPos().y, elem->getPos().z), 1.0);
    glm::vec3 pos = elem->getPos();
    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->collisionSound = nullptr;

    this->startPos = Vector3(pos.x, pos.y, pos.z);

    std::cout << "new Entity at " << physObj->position << std::endl;

}

Entity::Entity(std::shared_ptr<RenderElement> elem, Vector3 pos) {
    this->rElem = elem;

    //this->physObj->position = fromRender(elem->getPos());

    double volume = elem->getModel()->getMeshData()->computeVolume();

    this->physObj = new PhysicsObject(1000.0 * volume, pos, volume);
    glm::vec3 rPos = asRender(pos);
    this->rElem->setPosition(rPos.x, rPos.y, rPos.z);
    this->audioSource = new AudioSource(rPos.x, rPos.y, rPos.z);
    this->collisionSound = nullptr;

    this->startPos = pos;

    std::cout << "new Entity at " << physObj->position << std::endl;

}

Entity::Entity(std::shared_ptr<RenderElement> elem, double density) {

    this->rElem = elem;

    std::cout << "Computing volume" << std::endl;
    double volume = elem->getModel()->getMeshData()->computeVolume();
    std::cout << "Volume : " << volume << std::endl;

    this->physObj = new PhysicsObject(density * volume, fromRender(elem->getPos()), volume);
    glm::vec3 pos = elem->getPos();
    this->audioSource = new AudioSource(pos.x, pos.y, pos.z);
    this->collisionSound = nullptr;

    this->startPos = Vector3(pos.x, pos.y, pos.z);

}

Entity::Entity() {

    collisionSound = nullptr;
    pickUp = false;

}

Entity::~Entity()
{
    std::cout << "deleting Entity" << std::endl;
}

void Entity::onUpdate(double dTime) {

    Vector3 rPos = this->physObj->position;
    this->rElem->setPosition(rPos.x, rPos.y, rPos.z);
    this->audioSource->setPosition(rPos.x, rPos.y, rPos.z);
    this->rElem->setRotation(physObj->getRotation());

}

void Entity::onWorldAdded(World * world) {

}

void Entity::onPhysicsAdded() {

    btRigidBody * body = this->physObj->getRigidBody();

    //body->setCollisionFlags( body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
    //body->setActivationState(DISABLE_DEACTIVATION);
    body->setFriction(1.0);

    btTransform trans;
    trans.setOrigin(btVector3(startPos.x, startPos.y, startPos.z));

}

void Entity::onPhysicsStart() {

    this->physObj->getRigidBody()->setLinearVelocity(btVector3(0,0,0));
    btTransform trans;
    trans.setOrigin(btVector3(startPos.x, startPos.y, startPos.z));


    //this->physObj->getRigidBody()->getWorldTransform().setIdentity();
    this->physObj->getRigidBody()->getWorldTransform().setOrigin(btVector3(startPos.x, startPos.y, startPos.z));

}

void Entity::collisionInternal(std::shared_ptr<Entity> entity) {

    this->onCollision(entity);

}

void Entity::physicsStartInternal() {

    this->onPhysicsStart();

}

void Entity::physicsAddInternal(std::shared_ptr<Entity> entity) {

    entity->onPhysicsAdded();
    entsByCollisionObject[entity->physObj->getRigidBody()] = entity;

}

void Entity::onCollision(std::shared_ptr<Entity> entity) {

    /*if (!this->audioSource || !this->collisionSound) return;

    this->audioSource->setLooping(false);
    if (!this->audioSource->isPlaying())
        this->audioSource->play(collisionSound);*/

}

void Entity::setScale(double scale) {

    this->rElem->setScale(scale);
    this->physObj->setScale(scale);

}

CompoundNode * Entity::toCompound() {

    CompoundNode * rootNode = new CompoundNode();

    rootNode->addChildNode((Node<void*>*) new Node<double>("mass", this->physObj->getMass()));
    rootNode->addChildNode((Node<void*>*) new Node<double>("x", this->physObj->position.x));
    rootNode->addChildNode((Node<void*>*) new Node<double>("y", this->physObj->position.y));
    rootNode->addChildNode((Node<void*>*) new Node<double>("z", this->physObj->position.z));

    Math::Quaternion rot = rElem->getRotation();

    rootNode->addChildNode((Node<void*>*) new Node<double>("ra", rot.a));
    rootNode->addChildNode((Node<void*>*) new Node<double>("rb", rot.b));
    rootNode->addChildNode((Node<void*>*) new Node<double>("rc", rot.c));
    rootNode->addChildNode((Node<void*>*) new Node<double>("rd", rot.d));

    rootNode->addChildNode((Node<void*>*) new Node<int>("type", DEFAULT_ENTITY));
    rootNode->addChildNode((Node<void*>*) new Node<double>("scale", this->rElem->getScale()));

    return rootNode;

}

std::shared_ptr<Entity> Entity::fromCompound(CompoundNode * rootNode, std::shared_ptr<RenderElement> rElem) {

    double x = rootNode->get<double>("x");
    double y = rootNode->get<double>("y");
    double z = rootNode->get<double>("z");

    if (z == 0.0) {

        glm::vec3 rPos = asRender(Vector3(x,y,z));
        z = TerrainGenerator::getHeight(rPos.x,rPos.z);

    }

    EntityType type = (EntityType) rootNode->get<int>("type");

    std::shared_ptr<Entity> ent;

    switch (type) {

        case STATIC_ENTITY:

            ent = std::shared_ptr<Entity>(new EntityStatic(std::static_pointer_cast<StaticRenderElement>(rElem), Vector3(x,y,z)));
            ent->setScale(rootNode->get<double>("scale"));
            ent->setRotation(rootNode->get<double>("rx"), rootNode->get<double>("ry"), rootNode->get<double>("rz"), 0.0);
            return ent;

            break;

        default:
            ent = std::shared_ptr<Entity>(new Entity(rElem, Vector3(x,y,z)));
            ent->setScale(rootNode->get<double>("scale"));
            ent->setRotation(rootNode->get<double>("rx"), rootNode->get<double>("ry"), rootNode->get<double>("rz"), rootNode->get<double>("angle"));
            return ent;

    }

    return std::shared_ptr<Entity>(new Entity(rElem, Vector3(x,y,z)));

}

void Entity::setRotation(double x, double y, double z, double angle) {

    this->rElem->setRotation(Quaternion::fromAxisAngle(Vector3(x,y,z), angle));

    this->physObj->setAxis(fromRender(glm::vec3(x,y,z)));
    this->physObj->setAngle(radians(angle));

}

std::shared_ptr<RenderElement> Entity::getRenderElement() {
    return this->rElem;
}

PhysicsObject * Entity::getPhysicsObject() {
    return this->physObj;
}

void Entity::processCollision(btCollisionObject * objA, btCollisionObject * objB) {

    std::shared_ptr<Entity> entA = entsByCollisionObject[objA];
    std::shared_ptr<Entity> entB = entsByCollisionObject[objB];

    if (!entA || !entB) return;

    entA->collisionInternal(entB);
    entB->collisionInternal(entA);

}

std::shared_ptr<Entity> Entity::getFromWorld(btCollisionObject * obj) {
    return entsByCollisionObject[obj];
}

void Entity::clearWorld() {
    Entity::entsByCollisionObject = std::unordered_map<btCollisionObject*, std::shared_ptr<Entity>>();
}

bool Entity::canPickUp() {
    return pickUp;
}

std::shared_ptr<Entity> Entity::create(Structure * strc, Vector3 pos, Quaternion rot, double mass, std::shared_ptr<RenderElement> rElem, bool pickUp, CompoundNode * data) {
    return std::shared_ptr<Entity>(new Entity(strc, pos, rot, mass, rElem, pickUp));
}

std::shared_ptr<Entity> Entity::createEntityFromType(level::LEVEL_PLACEMENT & placement, std::shared_ptr<RenderElement> rElem) {

    std::string type = placement.entityType;

    std::cout << "Creating entity of type " << type << std::endl;

    Vector3 pos = placement.pos;
    Quaternion rot = Quaternion::fromAxisAngle(placement.rot, placement.angle);
    double mass = placement.mass;
    bool pickUp = placement.pickUp;

    Structure * strc = GameEngine::resources->get<Structure>(placement.name);


    if (creators.find(type) != creators.end()) {
        std::shared_ptr<Entity> ent = creators[type](strc, pos, rot, mass, rElem, pickUp, placement.entityData);
        ent->setScale(placement.scale);
        return ent;
    }

    std::shared_ptr<Entity> ent = create(strc, pos, rot, mass, rElem, pickUp, placement.entityData);
    ent->setScale(placement.scale);

    return ent;

}

void Entity::registerEntityType(std::string type, EntityCreator c) {
    creators[type] = c;
}
