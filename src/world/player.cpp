#include "player.h"

#include <GLFW/glfw3.h>

#include "gameengine.h"
#include "util/math/constants.h"
#include "util/math/conversions.h"
#include "render/viewportdeffered.h"
#include "resources/textureloader.h"
#include "audio/audiolistener.h"

#define SPEED 0.7
#define TARGET_SPEED 3.5

#define JUMP_HEIGHT 1.02

#define PLAYER_HEIGHT 1.80
#define SPRING_HEIGHT 0.45
#define SPRING_STIFFNESS 250
#define SPRING_DAMPING 10.0

using namespace player;

bool takeScreenShot = false;

Player::Player(Vector3 pos, Vector3 rot, double mass, Camera * camera, Viewport * view) {

    this->physObj = new PhysicsObject(mass, pos);

    btCompoundShape * shape = new btCompoundShape();
    btTransform topTrans;
    topTrans.setOrigin(btVector3(0, 0.5, 0));

    btTransform botTrans;
    botTrans.setOrigin(btVector3(0, -0.5, 0));

    btTransform midTrans;
    midTrans.setOrigin(btVector3(0, 0, 0));

    shape->addChildShape(topTrans, new btSphereShape(0.6));
    //shape->addChildShape(botTrans, new btSphereShape(0.6));
    //shape->addChildShape(midTrans, new btSphereShape(0.4));

    double capsuleHeight = PLAYER_HEIGHT;// - SPRING_HEIGHT;

    this->cameraHeightOffset = 0.8 * capsuleHeight/2;

    this->physObj->setCollisionShape(new btCapsuleShape(0.4, capsuleHeight - 2 * 0.4));
    this->physObj->setRotation(0, 0, 0);
    this->physObj->setAngularFactor(0.0);

    this->camera = camera;
    this->view = view;

    for (int i = 0; i < PLAYER_BUTTON_COUNT; ++i) {

        this->buttons[i] = false;

    }

    this->theta = 0.0;
    this->phi = 0.0;

    this->startPos = pos;

    this->controlCamera = false;

    //this->spring = (SPRING *) malloc(sizeof(player::SPRING));

    this->spring.l0 = SPRING_HEIGHT;
    this->spring.k = SPRING_STIFFNESS;

    this->useSpring = true;
    this->carryEntity = nullptr;
    this->carryConstraint = nullptr;
    this->springReset = false;

    this->heightDisplay = std::shared_ptr<FontUIElement>(new FontUIElement(0.008, 1.0, 0.15, 0.15, "Max height: 0.0 m", GameEngine::resources->get<Font>("distance"), WEST));
    this->heightDisplay->setTextColor(1,1,1);
    view->addUIElement(heightDisplay);
    maxHeight = 0.0;

}

Player::~Player()
{
    //dtor
}

void Player::onUpdate(double dt) {

    Vector3 facing = Vector3(0,0,0);
    facing.x = cos(phi) * cos(theta);
    facing.y = sin(theta);
    facing.z = sin(phi) * cos(theta);

    facing = facing / facing.length();

    btTransform trans;
    this->physObj->getRigidBody()->getMotionState()->getWorldTransform(trans);

    Vector3 pos = Vector3(0,0,0);
    pos.x = trans.getOrigin().getX();
    pos.y = trans.getOrigin().getY() + cameraHeightOffset;
    pos.z = trans.getOrigin().getZ();

    this->camera->position = glm::vec3(pos.x, pos.y, pos.z);
    this->camera->facing = glm::vec3(facing.x, facing.y, facing.z);

    AudioListener::setPosition(pos.x, pos.y, pos.z);
    AudioListener::setFacing(facing.x, facing.y, facing.z);

    this->camera->updateViewMatrix();

    if (pos.y-cameraHeightOffset-PLAYER_HEIGHT/2 > maxHeight) {

        maxHeight = pos.y-cameraHeightOffset-PLAYER_HEIGHT/2;
        //this->heightDisplay->setText("Max height: " + std::to_string(maxHeight) + " m");

    }

}

void Player::onKeyboard(int key, int scancode, int action, int mods) {

    switch (key) {

        case GLFW_KEY_W:
            if (action == GLFW_PRESS) {
                this->buttons[B_FWD] = true;
            } else if (action == GLFW_RELEASE) {
                this->buttons[B_FWD] = false;
            }
            break;

        case GLFW_KEY_S:
            if (action == GLFW_PRESS) {
                this->buttons[B_BACK] = true;
            } else if (action == GLFW_RELEASE) {
                this->buttons[B_BACK] = false;
            }
            break;

        case GLFW_KEY_A:
            if (action == GLFW_PRESS) {
                this->buttons[B_LEFT] = true;
            } else if (action == GLFW_RELEASE) {
                this->buttons[B_LEFT] = false;
            }
            break;

        case GLFW_KEY_D:
            if (action == GLFW_PRESS) {
                this->buttons[B_RIGHT] = true;
            } else if (action == GLFW_RELEASE) {
                this->buttons[B_RIGHT] = false;
            }
            break;
        case GLFW_KEY_C:
            if (action == GLFW_PRESS) {

                controlCamera = !controlCamera;
                GameEngine::togleCursor();

            }
            break;

        case GLFW_KEY_SPACE:
            if (action == GLFW_PRESS) {
                buttons[B_JUMP] = true;
                useSpring = false;
                springReset = false;
            }
            break;

        case GLFW_KEY_J:
            if (action == GLFW_PRESS) {
                GameEngine::world->startPhysics();
            }
        case GLFW_KEY_R:
            if (action == GLFW_PRESS) {
                this->onPhysicsStart();
            }
            break;
        case GLFW_KEY_E:
            if (action == GLFW_PRESS) {
                buttons[B_PICKUP] = !buttons[B_PICKUP];
            }
            break;

        case GLFW_KEY_K:
            if (action == GLFW_PRESS) {
                std::cout << camera->facing.x << " " << camera->facing.y << " " << camera->facing.z << std::endl;
            }
            break;

        case GLFW_KEY_F2:
            takeScreenShot = true;
            break;

    }

}

#define MAX_THROW_COOLDOWN 0.25

void saveScreenshot(std::vector<float> data, int width, int height) {


}

void Player::onTick(double dTime) {

    if (controlCamera) {

        Vector3 facing = Vector3(0,0,0);
        facing.x = cos(phi) * cos(theta);
        facing.y = sin(theta);
        facing.z = sin(phi) * cos(theta);

        btTransform trans;
        this->physObj->getRigidBody()->getMotionState()->getWorldTransform(trans);

        Vector3 pos = Vector3(0,0,0);
        pos.x = trans.getOrigin().getX();
        pos.y = trans.getOrigin().getY() + cameraHeightOffset;
        pos.z = trans.getOrigin().getZ();

        Vector3 impulse = Vector3(0,0,0);

        if (buttons[B_FWD]) {
            impulse += Vector3(facing.x, 0, facing.z);
            //this->physObj->getRigidBody()->applyCentralImpulse(btVector3(SPEED*facing.x, facing.y*SPEED, facing.z*SPEED));
            //this->physObj->getRigidBody()->applyCentralForce(btVector3(SPEED*this->physObj->getMass()*facing.x, facing.y*this->physObj->getMass()*SPEED, facing.z*this->physObj->getMass()*SPEED));
        }
        if (buttons[B_BACK]) {
            impulse += -1.0 * Vector3(facing.x, 0, facing.z);
            //this->physObj->getRigidBody()->s0etLinearVelocity(btVector3(-SPEED*facing.x, facing.y*-SPEED, facing.z*-SPEED));
        }
        if (buttons[B_LEFT]) {
            impulse += Vector3(facing.z, 0, -facing.x);
            //this->physObj->getRigidBody()->setLinearVelocity(btVector3(SPEED*facing.z, facing.y*SPEED, -facing.x*SPEED));
        }
        if (buttons[B_RIGHT]) {
            impulse += Vector3(-facing.z, 0, facing.x);
            //this->physObj->getRigidBody()->setLinearVelocity(btVector3(-SPEED*facing.z, facing.y*SPEED, facing.x*SPEED));
        }

        if (buttons[B_JUMP]) {
            std::cout << "jumping" << std::endl;
            btVector3 btFrom = trans.getOrigin() + btVector3(0, 0, 0);
            btVector3 btTo = trans.getOrigin() - btVector3(0, 0.3 + cameraHeightOffset, 0);

            std::cout << "Raycast from " << Vector3(btFrom.getX(), btFrom.getY(), btFrom.getZ()) << " to " << Vector3(btTo.getX(), btTo.getY(), btTo.getZ()) << std::endl;

            btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

            GameEngine::world->getRigidBodyWorld()->rayTest(btFrom, btTo, res);

            //#ifndef _WIN64
            if (res.hasHit()) {
            //#endif
                double l = (btFrom-res.m_hitPointWorld).length();
                std::cout << "hit at " << Vector3(res.m_hitPointWorld.getX(), res.m_hitPointWorld.getY(), res.m_hitPointWorld.getZ()) << std::endl;
                this->physObj->getRigidBody()->activate(true);
                std::cout << "l = " << l << std::endl;
                //#ifndef _WIN64
                if (l < 0.3 + cameraHeightOffset)
                //#endif // _WIN64
                    this->physObj->getRigidBody()->applyCentralImpulse(btVector3(0, this->physObj->getMass() * sqrt(2 * 9.81 * JUMP_HEIGHT), 0));
            //#ifndef _WIN64
            }
            //#endif // _WIN64
            buttons[B_JUMP] = false;
        }

        if (!IS_ZERO(impulse.length())) {
            this->physObj->getRigidBody()->activate(true);
            btVector3 lvel = this->physObj->getRigidBody()->getLinearVelocity();
            Vector3 vel = Vector3(lvel.getX(), lvel.getY(), lvel.getZ());

            double fac = ((TARGET_SPEED - vel.length()) >= 0 ? (TARGET_SPEED - vel.length()) : 0 );

            impulse = impulse * SPEED * this->physObj->getMass() * fac / impulse.length();
            this->physObj->getRigidBody()->applyCentralImpulse(btVector3(impulse.x, impulse.y, impulse.z));

            useSpring = false;

        }

        if (buttons[B_AXE_THROW] && this->throwCoolDown <= 0.0) {
            std::shared_ptr<RenderElement> elem(new RenderElement(GameEngine::resources->get<Structure>("crate"), 0 ,0 ,0));
            std::shared_ptr<Entity> ent(new Entity(GameEngine::resources->get<Structure>("crate"), pos + 0.41 * facing, Vector3(0,0,0), 500.0, elem, true));

            ent->getPhysicsObject()->velocity = facing * 4;

            GameEngine::world->addEntity(ent, true);
            this->view->addElement(0, elem);

            this->throwCoolDown = MAX_THROW_COOLDOWN;
        }

        this->throwCoolDown -= dTime;

        /*btVector3 btFrom = trans.getOrigin() + btVector3(0, -cameraHeightOffset, 0);
        btVector3 btTo = trans.getOrigin() + btVector3(0, -50000.0, 0);

        btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

        GameEngine::world->getRigidBodyWorld()->rayTest(btFrom, btTo, res);

        if (res.hasHit()) {
            double l = (btFrom-res.m_hitPointWorld).length();
            double vy = this->physObj->getRigidBody()->getLinearVelocity().getY();

            printf("Collision at: <%.2f, %.2f, %.2f>\n", res.m_hitPointWorld.getX(), res.m_hitPointWorld.getY(), res.m_hitPointWorld.getZ());
            if (useSpring) {
                double F = -spring.k * (l - spring.l0) - SPRING_DAMPING * vy;

                double lambda = sqrt(spring.k / this->physObj->getMass());
                double v = -lambda * SPRING_HEIGHT * exp(-lambda * (springContactTime));
                springContactTime += dTime;

                std::cout << "FORCE: " << F << " length " << l << std::endl;
                this->physObj->getRigidBody()->applyCentralForce(btVector3(0, F, 0));
            } else if (springReset && l <= spring.l0) {
                useSpring = true;
                springReset = false;
                springContactTime = 0;
            } else if (l > 1.2 * spring.l0) {
                springReset = true;
            }
        }*/

        if (carryEntity) {

            btMatrix3x3 mat;
            mat.setEulerZYX(0, -phi, 0);

            btTransform trans;
            trans.setOrigin(btVector3(pos.x+facing.x*1.5, pos.y+facing.y*1.5, pos.z+facing.z*1.5));
            trans.setBasis(mat);

            this->carryEntity->getPhysicsObject()->getRigidBody()->setWorldTransform(trans);
            this->carryEntity->getPhysicsObject()->getRigidBody()->setLinearVelocity(btVector3(0,0,0));
            this->carryEntity->getPhysicsObject()->getRigidBody()->setAngularVelocity(btVector3(0,0,0));


        }

        if (buttons[B_PICKUP]) {


            Level * lvl;

            GameEngine::loadWithScreen([&] (ResourceContainer * resources) -> void {
                lvl = Level::loadFromFile("resources/levels/test2.lvl");
                lvl->loadRequirements(resources);
            });

            GameEngine::useLevel(lvl);

            ///TODO : save player in world to be able to add him back here
            GameEngine::world->addEntity(GameEngine::world->getPlayer(), true);
            GameEngine::world->startPhysics();
            GameEngine::world->update(0.0);

            btTransform trans;
            trans.setOrigin(btVector3(0,1,0));
            trans.setRotation(btQuaternion(0, 0, 0, 1));

            this->physObj->getRigidBody()->setWorldTransform(trans);
            this->physObj->getRigidBody()->setLinearVelocity(btVector3(0,0,0));
            this->physObj->getRigidBody()->setAngularVelocity(btVector3(0,0,0));

            buttons[B_PICKUP] = false;


            /*if (carryEntity) {

                this->carryEntity->getPhysicsObject()->getRigidBody()->setCollisionFlags(0);
                ((btCollisionObject*)this->carryEntity->getPhysicsObject()->getRigidBody())->setIgnoreCollisionCheck(this->physObj->getRigidBody(), false);
                this->carryEntity->getPhysicsObject()->getRigidBody()->applyCentralImpulse(btVector3(0, -0.01, 0));
                this->carryEntity->getPhysicsObject()->getRigidBody()->setLinearVelocity(btVector3(0, -0.1, 0));

                this->carryEntity->getPhysicsObject()->getRigidBody()->activate(true);

                carryEntity = nullptr;

            } else {

                btVector3 btFrom(pos.x, pos.y, pos.z);
                btVector3 btTo(pos.x + 3*facing.x, pos.y + 3*facing.y, pos.z+3*facing.z);

                btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

                GameEngine::world->getRigidBodyWorld()->rayTest(btFrom, btTo, res);

                if (res.hasHit()) {

                    std::cout << "hit at " << Vector3(res.m_hitPointWorld.getX(), res.m_hitPointWorld.getY(), res.m_hitPointWorld.getZ()) << std::endl;

                    Entity * ent = Entity::getFromWorld((btCollisionObject*)res.m_collisionObject);

                    if (ent->canPickUp()) {
                        this->carryEntity = ent;

                        btMatrix3x3 mat;
                        mat.setEulerZYX(phi, theta, 0);

                        btTransform trans;
                        trans.setOrigin(btVector3(pos.x+facing.x*1.5, pos.y+facing.y*1.5, pos.z+facing.z*1.5));
                        trans.setBasis(mat);

                        this->carryEntity->getPhysicsObject()->getRigidBody()->setWorldTransform(trans);

                        this->carryEntity->getPhysicsObject()->getRigidBody()->setLinearVelocity(btVector3(0,0,0));
                        this->carryEntity->getPhysicsObject()->getRigidBody()->setAngularVelocity(btVector3(0,0,0));
                        this->carryEntity->getPhysicsObject()->getRigidBody()->setIgnoreCollisionCheck(this->physObj->getRigidBody(), true);

                    }
                }

            }

            buttons[B_PICKUP] = false;*/
        }

    }


    this->heightDisplay->setText("Max height: " + std::to_string(maxHeight) + " m\nfps: " + std::to_string(1 / (GameEngine::getTime() - frameTime)));
    frameTime = GameEngine::getTime();

    if (takeScreenShot) {

        takeScreenShot = false;
        int width, height;

        std::vector<float> data = ((ViewportDeffered*)GameEngine::getActiveWindow()->currentViewport)->getContent(&width, &height);
        saveImagePPM(data.data(), width, height, "Screenshot.ppm");
    }

}

#define MAX_ANGLE_FACTOR 0.999
#define MOUSE_SENSITIVITY 0.0025

void Player::onMouseMotion(double dx, double dy) {

    //this->physObj->addAngle(-0.005 * dx);
    if (controlCamera) {
        this->phi += MOUSE_SENSITIVITY * dx;
        this->theta -= MOUSE_SENSITIVITY * dy;
        if (this->theta > M_PI_2 * MAX_ANGLE_FACTOR) {
            theta = M_PI_2 * MAX_ANGLE_FACTOR;
        }
        else if (theta < -M_PI_2 * MAX_ANGLE_FACTOR) {
            theta = -M_PI_2 * MAX_ANGLE_FACTOR;
        }
    }

}

void Player::onMouseButton(int button, int action, int mods, double x, double y) {

    switch (button) {

        case GLFW_MOUSE_BUTTON_1 :
            if (action == GLFW_PRESS) {
                this->buttons[B_AXE_THROW] = true;
                this->throwCoolDown = 0.0;
            } else if (action == GLFW_RELEASE) {
                this->buttons[B_AXE_THROW] = false;
                this->throwCoolDown = MAX_THROW_COOLDOWN;
            }
            break;

    }

}
