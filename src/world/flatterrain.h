#ifndef FLATTERRAIN_H
#define FLATTERRAIN_H

#include "terrain.h"

class FlatTerrain : public Terrain {

    public:
        FlatTerrain(double height);
        virtual ~FlatTerrain();

        double getHeight(double x, double y) override;
        std::vector<double> getFace(double x, double y) override;

    private:
        double height;

};

#endif
