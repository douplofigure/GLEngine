#ifndef PLAYER_H
#define PLAYER_H

#include "entity.h"
#include "input/inputmanager.h"
#include "render/camera.h"
#include "render/viewport.h"
#include "ui/fontuielement.h"

namespace player {

    #define PLAYER_BUTTON_COUNT 7

    typedef enum buttons_e {

        B_FWD,
        B_BACK,
        B_LEFT,
        B_RIGHT,
        B_JUMP,
        B_AXE_THROW,
        B_PICKUP

    } BUTTONS;

    typedef struct spring_t{

        double l0;
        double k;

    } SPRING;

};

class Player : public Entity, public InputManager
{
    public:
        Player(Vector3 pos, Vector3 rot, double mass, Camera * camera, Viewport * view);
        virtual ~Player();

        /// Entity Callbacks
        virtual void onUpdate(double dTime) override;

        /// Input Callbacks
        virtual void onKeyboard(int key, int scancode, int action, int mods);
        virtual void onTick(double dtime);
        virtual void onMouseMotion(double dx, double dy);
        virtual void onMouseButton(int button, int action, int mods, double x, double y);

    protected:

        Camera * camera;
        Viewport * view;

        bool buttons[PLAYER_BUTTON_COUNT];

    private:

        bool controlCamera;
        bool useSpring;
        bool springReset;

        double theta;
        double phi;

        double cameraHeightOffset;

        double throwCoolDown;

        player::SPRING spring;
        double springContactTime;

        btConeTwistConstraint * carryConstraint;

        Entity * carryEntity;

        std::shared_ptr<FontUIElement> heightDisplay;
        double maxHeight;

        double frameTime;


};

#endif // PLAYER_H
