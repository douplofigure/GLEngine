#ifndef WORLD_H
#define WORLD_H

#include <vector>

#include "physics/physicscontext.h"
#include "physics/physicsconfig.h"
#include "entity.h"
#include "terrain.h"

#include "render/viewport.h"

typedef struct entity_add_comp_t ENTITY_ADD_CONFIG;

class World
{
    public:
        World(Terrain * terrain, PhysicsConfig * physicsConfig);
        virtual ~World();

        void addEntity(std::shared_ptr<Entity> ent, bool hasPhysics);
        void addPlayer(std::shared_ptr<Entity> ent, bool hasPhysics);
        void removeEntity(std::shared_ptr<Entity> ent);
        void startPhysics();
        void stopPhysics();
        void update(double dTime);

        std::shared_ptr<Entity> getPlayer();

        void clearEntities();

        void saveToFile(std::string fname);
        void setTerrain(Terrain * terrain);
        Terrain * getTerrain();

        std::shared_ptr<RenderElement> getRenderElement(std::string name);
        void addRenderElement(std::string name, std::shared_ptr<RenderElement> elem);

        btDiscreteDynamicsWorld * getRigidBodyWorld();

        static World * loadFromFile(std::string fname, Viewport * viewport);

    protected:

    private:



        PhysicsContext * physics;
        PhysicsConfig * physicsConfig;
        Terrain * terrain;
        std::shared_ptr<Entity> player;
        std::vector<std::shared_ptr<Entity>> entities;
        std::list<ENTITY_ADD_CONFIG> newEntities;
        std::map<std::string, std::shared_ptr<RenderElement>> elementsByName;

};

#endif // WORLD_H
