#include "lowpolyterrain.h"

#include "util/simplexnoise.h"
#include "gameengine.h"
#include "util/terraingenerator.h"

int compareFaceKeys(Vector3 key, BinTreeNode<Vector3, int*> * node) {

    double l1 = 0.0;
    double l2 = 0.0;

    if (node->getChildCount() & 1)
        l1 = (key - node->getLeft()->getKey()).length();
    if (node->getChildCount() & 2)
        l2 = (key - node->getRight()->getKey()).length();

    return l1 < l2 ? -1 : 1;

}

LowPolyTerrain::LowPolyTerrain(double x, double y, double width, double length, int resolution) {

    this->dx = width / resolution;
    this->dy = length / resolution;

    minX = x;
    minY = y;

    this->width = width;
    this->length = length;

    //this->heightMap = GameEngine::resources->get<HeightMap>("heightmap");

    this->faceTree = new BinaryTree<Vector3, int *>(&compareFaceKeys, NULL, Vector3(0,0,0));

    /*this->faceTree->getTopNode()->setLeftChild(new BinTreeNode<Vector3, int*>(NULL, Vector3(width/2-width/4, 0, length/2-length/4)));
    this->faceTree->getTopNode()->setRightChild(new BinTreeNode<Vector3, int*>(NULL, Vector3(width/2+width/4, 0, length/2+length/4)));

    this->faceTree->getTopNode()->overrideChildCount(3);

    for (int i = 2; i < resolution; ++i) {

        for (int j = 0; j < i; ++j) {

            for (int k = 0; k < i; ++k) {

                this->faceTree->insertElement(Vector3(width/i + (j*width /(2*i)), 0, length/i + (k*length /(2*i))), NULL);

            }

        }

    }

    //this->faceTree->print();

    std::vector<Vector3> faceData;

    for (int i = 0; i < resolution; ++i) {

        for (int j = 0; j < resolution; ++j) {

            faceData = generateTriangle(x+i, y+j, 0, width/resolution, length/resolution);

            Vector3 center = (faceData[0] + faceData[1] + faceData[2]) / 3;

            this->faceTree->insertElement(center, NULL);
            //this->faceTree->print();

        }

    }*/

    //this->faceTree->print();

}

LowPolyTerrain::~LowPolyTerrain()
{
    //dtor
}

double LowPolyTerrain::getHeight(double x, double y) {

    double x1 = x;
    double x2 = x;
    double x3 = x;


    double y1 = y;
    double y2 = y;
    double y3 = y;

    return getActualHeight(x, y);


}

double LowPolyTerrain::getActualHeight(double x, double y) {

    TerrainGenerator::getHeight(x,y);
    //return 200 * this->heightMap->getHeightValue((x - this->minX)/width, (x - this->minY)/length) - 20;// + SimplexNoise1234::noise(x*0.1, y*0.1) + 20;

}

std::vector<double> LowPolyTerrain::getFace(double x, double y) {
    return std::vector<double>();
}

std::vector<Vector3> LowPolyTerrain::generateTriangle(double x, double y, int rotation, double w, double l) {

    std::vector<Vector3> verts(4);

    Vector3 pos1;
    Vector3 pos2;
    Vector3 pos3;

    double tmpX, tmpY;

    if (!rotation) {

        pos1 = Vector3(x, getActualHeight(x, y), y);
        pos3 = Vector3(x+w, getActualHeight(x+w, y), y);
        pos2 = Vector3(x, getActualHeight(x, y + l), y + l);

    }
    else {

        pos1 = Vector3(x+w, getActualHeight(x+w, y), y);
        pos3 = Vector3(x+w, getActualHeight(x+w, y + l), y + l);
        pos2 = Vector3(x, getActualHeight(x, y + l), y + l);

    }

    Vector3 v1 = pos3-pos1;
    Vector3 v2 = pos2-pos1;

    Vector3 normal = Vector3::cross(v1, v2);
    normal = normal * 1 / normal.length();

    normal.x *= -1;

    //std::cout << "Normal Vector (" << normal.x << " " << normal.y << " " << normal.z << ")\n";
    /*VERTEX tmp;
    tmp.position = pos1;
    tmp.normal = normal;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos1.y > 1 ) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos1.y > 0.9 * terrainGenHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    //tmp.tangent = (VECTOR3F) {SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001),SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001),SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001)};
    tmp.uv = (VECTOR2F) {0,0};
    verts.push_back(tmp);
    tmp.position = pos2;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos2.y > 1) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos2.y > 0.9 * terrainGenHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    verts.push_back(tmp);

    tmp.position = pos3;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos3.y > 1) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos3.y > 0.9 * terrainGenHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    verts.push_back(tmp);*/

    verts[0] = pos1;
    verts[1] = pos2;
    verts[2] = pos3;
    verts[3] = normal;

    std::cerr << "pos1 " << pos1 << " pos2 " << pos2 << " pos3 " << pos3 << std::endl;

    return verts;


}
