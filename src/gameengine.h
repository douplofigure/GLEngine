#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <list>
#include <functional>
#include <memory>
#ifndef _WIN32
    #include <thread>
#else
    #include <mingw.thread.h>
#endif

#include "render/window.h"
#include "input/inputmanager.h"
#include "resources/resourcecontainer.h"
#include "world/world.h"
#include "ui/uielement.h"
#include "ui/fontuielement.h"

typedef enum rendermode_e {

    DIRECT,
    DEFFERED,
    PBR

} RENDERING_MODE;

class GameEngine {

    public:

        static void setLoadingFunc(std::function<void(ResourceContainer *)> f);
        static void setPreloadFunc(std::function<void()> f);

        static void init(bool vSync, RENDERING_MODE renderMode, bool createWindow = true, bool loadPlugins=false);
        static void addViewport(std::string name, Viewport * viewport);
        static void setViewport(std::string name);
        static Viewport * getActiveViewport();
        static void mainloop();

        static void addInputManager(std::shared_ptr<InputManager> manager);

        static void stop();
        static void terminate();

        static void setResourceLocation(std::string directory);

        static void useLevel(Level * level);

        static void loadWithScreen(std::function<void(ResourceContainer *)> f);

        static void onMouseMotion(double dx, double dy);
        static void onMouseButton(int button, int action, int mods, double x, double y);
        static void onKeyboardUpdate(int key, int scancode, int action, int mods);

        static void showCursor();
        static void hideCursor();
        static void togleCursor();

        static bool getFullScreen();

        static int getGLVersion();

        static void displayError(std::string msg);

        static double getTime();


        static RENDERING_MODE getRenderMode();
        static std::vector<int> getRenderSize();

        static int getCPUCount();

        static Window * getActiveWindow();

        static bool isRunning();

        static void setLoadingMessage(std::string msg);

        static ResourceContainer * resources;
        static World * world;

    private:

        static Window * window;
        static std::list<std::shared_ptr<InputManager>> inputManagers;
        static bool shouldStop;

        static bool cursorVisible;

        static void onTick();
        static void loadInit(std::function<void(ResourceContainer *)> loadingFunc);
        static double oldTime;

        static std::thread * pollThread;
        static bool loading;

        static std::string loadingMessage;

        static RENDERING_MODE renderMode;

        static int openglVersion;
        static std::vector<int> renderSize;

        static std::function<void(ResourceContainer *)> loadingFunc;
        static std::function<void()> preLoadFunc;

        static std::shared_ptr<UIElement> loadingElem;
        static std::shared_ptr<FontUIElement> message;

};

#endif // GAMEENGINE_H
