#include "daeloader.h"

#include <configloader.h>

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "util/math/mesh.h"
#include "gameengine.h"
#include "physics/physicsutil.h"

using namespace Math;

const double ZUPMATRIX[16] = {

    1, 0, 0, 0,
    0, 0, 1, 0,
    0, -1, 0, 0,
    0, 0, 0, 1

};

DAELoader::DAELoader()
{
    //ctor
}

DAELoader::~DAELoader()
{
    //dtor
}

typedef struct dae_offset_def_t {

    int vOffset;
    int nOffset;
    int tOffset;
    int colorOffset;

} DAE_OFFSET_DEF;

typedef struct dae_elem_info_t {

    Mesh * mesh;
    std::string matName;

} DAE_ELEM_INFO;

int readDoublesFromString(const char * str, int valueCount, double * dataLoc) {

    char * tmp = (char *) str;

    for (int i = 0; i < valueCount; ++i) {

        double d;
        sscanf(tmp, "%lf", &d);
        dataLoc[i] = d;
        while(*(tmp++) != ' ');

    }

    return tmp - str;

}

int readIntsFromString(const char * str, int valueCount, int * dataLoc) {

    char * tmp = (char *) str;

    for (int i = 0; i < valueCount; ++i) {

        int d;
        sscanf(tmp, "%d", &d);
        dataLoc[i] = d;
        while(*(tmp++) != ' ');

    }

    return tmp - str;

}

void setOffsetValue(DAE_OFFSET_DEF * offsets, std::string semantic, int value) {

    std::cout << "setting offset for " << semantic << " to " << value << std::endl;

    if (!semantic.compare("VERTEX")) {

        offsets->vOffset = value;

    } else if (!semantic.compare("NORMAL")) {

        offsets->nOffset = value;

    } else if (!semantic.compare("TEXCOORD")) {

        offsets->tOffset = value;

    }

}

std::vector<Vector3> getVertexPositions(CompoundNode * mesh, CompoundNode * source) {

    std::cout << "getting child map from " << source << std::endl;
    auto cMap = source->getChildMap();
    auto it = cMap.begin();
    while(it->first.substr(0,5).compare("input") && it != cMap.end()) it++;

    if (it == cMap.end()) return std::vector<Vector3>(0);

    std::cout << "getting input definition" << std::endl;
    CompoundNode * iDef = (CompoundNode *) it->second;


    std::string dataLocName = std::string(iDef->get<const char *>("source"));
    CompoundNode * dataCompound = mesh->getCompound(dataLocName);
    dataLocName.append("_array");
    CompoundNode * arrayData = dataCompound->getCompound(dataLocName);

    int valueCount = atoi(arrayData->get<const char *>("count"));
    double * data = (double *) malloc(sizeof(double) * valueCount);

    readDoublesFromString(arrayData->get<const char *>("__text"), valueCount, data);

    std::vector<Vector3> positions(valueCount/3);

    for (int i = 0; i < valueCount/3; ++i) {

        positions[i] = Vector3(data[i*3], data[i*3+1], data[i*3+2]);

    }

    return positions;

}

std::vector<Vector3> getNormalVectors(CompoundNode * mesh, CompoundNode * source) {

    std::string dataLocName = std::string(source->get<const char *>("id"));
    dataLocName.append("_array");
    CompoundNode * arrayData = source->getCompound(dataLocName);

    int valueCount = atoi(arrayData->get<const char *>("count"));
    double * data = (double *) malloc(sizeof(double) * valueCount);

    readDoublesFromString(arrayData->get<const char *>("__text"), valueCount, data);

    std::vector<Vector3> positions(valueCount/3);

    for (int i = 0; i < valueCount/3; ++i) {

        positions[i] = Vector3(data[i*3], data[i*3+1], data[i*3+2]);

    }

    return positions;

}

std::vector<Vector3> getUVCoords(CompoundNode * mesh, CompoundNode * source) {

    if (!source) return std::vector<Vector3>(0);

    std::string dataLocName = std::string(source->get<const char *>("id"));
    dataLocName.append("_array");
    CompoundNode * arrayData = source->getCompound(dataLocName);

    int valueCount = atoi(arrayData->get<const char *>("count"));
    double * data = (double *) malloc(sizeof(double) * valueCount);

    readDoublesFromString(arrayData->get<const char *>("__text"), valueCount, data);

    std::vector<Vector3> positions(valueCount/2);

    for (int i = 0; i < valueCount/2; ++i) {

        positions[i] = Vector3(data[i*2], data[i*2+1], 0);

    }

    return positions;

}

DAE_ELEM_INFO buildMesh(CompoundNode * root, std::string meshId) {

    CompoundNode * library = root->getCompound("library_geometries");
    CompoundNode * mesh = library->getCompound(meshId);

    CompoundNode * data = mesh->getCompound("mesh");

    CompoundNode * triangles = data->getCompound("triangles");

    DAE_OFFSET_DEF offsets = {-1, -1, -1, -1};

    int inputCount = 0;
    std::vector<CompoundNode *> sources(10);

    auto cMap = triangles->getChildMap();
    for (auto it = cMap.begin(); it != cMap.end(); ++it) {

        if (it->first.substr(0, 5).compare("input"))
            continue;

        CompoundNode * child = (CompoundNode *) it->second;
        std::string source(child->get<const char *>("source"));
        std::string semantic(child->get<const char *>("semantic"));
        int offset = atoi(child->get<const char *>("offset"));

        setOffsetValue(&offsets, semantic, offset);
        inputCount++;
        std::cout << "getting compound " << source << " for slot " << offset << std::endl;
        sources[offset] = data->getCompound(source);
        std::cout << "getting compound " << sources[offset] << std::endl;

    }

    std::cout << "Getting vertex positions from source " << offsets.vOffset << std::endl;

    Vector3 pos = Vector3(0,0,0);
    Vector3 norm = Vector3(0,0,0);
    Vector3 uv = Vector3(0,0,0);

    std::vector<Vector3> positions = getVertexPositions(data, sources[offsets.vOffset]);
    std::vector<Vector3> normals = getNormalVectors(data, sources[offsets.nOffset]);
    std::vector<Vector3> uvs = getUVCoords(data, sources[offsets.tOffset]);

    int faceCount = atoi(triangles->get<const char *>("count"));
    int indicesPerVertex = 0;
    if (offsets.vOffset > -1) indicesPerVertex++;
    if (offsets.vOffset > -1) indicesPerVertex++;
    if (offsets.tOffset > -1) indicesPerVertex++;

    std::vector<int> rawIndices(faceCount * indicesPerVertex * 3);
    readIntsFromString(triangles->getCompound("p")->get<const char *>("__text"), faceCount * indicesPerVertex * 3, rawIndices.data());

    Mesh * result = new Mesh();

    for (int i = 0; i < faceCount * 3; ++i) {

        pos = Vector3(0,0,0);
        norm = Vector3(0,0,0);
        uv = Vector3(0,0,0);

        for (int j = 0; j < indicesPerVertex; ++j) {

            if (j == offsets.vOffset) {
                pos = positions[rawIndices[i*indicesPerVertex+j]];
            } else if (j == offsets.nOffset) {
                norm = normals[rawIndices[i*indicesPerVertex+j]];
            } else if (j == offsets.tOffset) {
                uv = uvs[rawIndices[i*indicesPerVertex+j]];
            }

        }

        result->appendVertex(pos, norm, uv);

    }

    result->computeTangents(true);

    DAE_ELEM_INFO resInfo;
    resInfo.mesh = result;

    if (triangles->getChildNode("material")) {
        std::string matId(triangles->get<const char *>("material"));
        CompoundNode * matNode = root->getCompound("library_materials")->getCompound(matId);
        resInfo.matName = std::string(matNode->get<const char *>("name"));
    } else {
        resInfo.matName = "planks";
    }

    return resInfo;

}

Matrix4 matrixFromString(const char * str) {

    char * tmp = (char *) str;

    std::vector<double> vals(16);

    for (int i = 0; i < 16; ++i) {

        double d;
        sscanf(tmp, "%lf", &d);
        vals[i] = d;
        while(*(tmp++) != ' ');

    }

    return Matrix4(vals, false);

}

Structure * buildScene(CompoundNode * root, std::string strcName) {

    CompoundNode * scene = root->getCompound("scene");

    auto it = scene->getChildMap().begin();

    CompoundNode * instance = (CompoundNode*) it->second;
    std::string sceneId(instance->get<const char *>("url"));

    std::cout << "loading scene " << sceneId << std::endl;

    CompoundNode * visualScenes = root->getCompound("library_visual_scenes");

    CompoundNode * currentScene = visualScenes->getCompound(sceneId);

    auto mp = currentScene->getChildMap();

    Structure * strc = new Structure(strcName);

    std::string worldUP(root->getCompound("asset")->getCompound("up_axis")->get<const char *>("__text"));
    Matrix4 worldOrient;
    if (!worldUP.compare("Z_UP")) {
        worldOrient = Matrix4((double *)ZUPMATRIX);
    } else {
        worldOrient = Matrix4();
    }

    for (auto i = mp.begin(); i != mp.end(); ++i) {
        if (!i->second->getName().compare("name") || !i->second->getName().compare("id")) {
            continue;
        }
        CompoundNode * child = (CompoundNode *) i->second;
        CompoundNode * geoInstance = child->getCompound("instance_geometry");

        std::string meshId(geoInstance->get<const char *>("url"));
        //std::cout << "building Mesh " << meshId << std::endl;
        Mesh * mesh;
        DAE_ELEM_INFO info = buildMesh(root, meshId);
        mesh = info.mesh;

        CompoundNode * transNode = child->getCompound("matrix");
        const char * rawData = transNode->get<const char *>("__text");
        Matrix4 trans = matrixFromString(rawData);

        int vertexCount;
        std::cout << "Getting model Data " << std::endl;
        std::vector<float> modeldata = mesh->getModelData(&vertexCount);

        std::cout << "Building injector" << std::endl;

        ModelInjector * model = new ModelInjector(modeldata, vertexCount, mesh->getIndexData(), mesh, GL_TRIANGLES);

        MaterialInjector * mat = (MaterialInjector *) GameEngine::resources->load<Material>(info.matName);

        strc->addElement(model, mat, worldOrient * trans);


        //std::cout << trans << std::endl;

    }

    strc->buildData();
    //strc->saveToBinFile("resources/models/collada/");

    return strc;

}

StructureInjector * DAELoader::create(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".model");

    CompoundNode * docData = ConfigLoader::loadFileTree(fname);
    CompoundNode * root = docData->getCompound("COLLADA");

    Structure * strc = buildScene(root, name);

    strc->setCollisionShape(PhysicsUtil::createShapeFromMesh(strc->getMesh()));

    StructureInjector * injector = new StructureInjector(strc);

    injector->setName(name);
    injector->setType("Structure");

    return injector;

}

std::vector<std::string> DAELoader::getFilenames(std::string dir, std::string name){
    std::vector<std::string> files(1);
    files[0] = dir.append(name).append(".model");
    return files;
}

bool DAELoader::canCreate(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".model");

    return fileExists(fname);

}

DAELoader * DAELoader::buildLoader() {
    return new DAELoader();
}
