#ifndef BINSTRUCTURELOADER_H
#define BINSTRUCTURELOADER_H

#include "structure.h"
#include "structureloader.h"

class BinStructureLoader : public ResourceLoader<Structure>
{
    public:
        BinStructureLoader();
        virtual ~BinStructureLoader();

        static BinStructureLoader * buildLoader();

        bool canCreate(std::string dir, std::string name);
        StructureInjector * create(std::string dir, std::string name);
        std::vector<std::string> getFilenames(std::string dir, std::string name);


    protected:



    private:


};

#endif // BINSTRUCTURELOADER_H
