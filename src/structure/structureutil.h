#ifndef STRUCTUREUTIL_H_INCLUDED
#define STRUCTUREUTIL_H_INCLUDED

#include <string>
#include <vector>

#include "render/shader.h"

std::string readFile(std::string fname);

std::string buildShaderSource(std::vector<ShaderInjector*> shaderNames);

#endif // STRUCTUREUTIL_H_INCLUDED
