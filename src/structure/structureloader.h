#ifndef STRUCTURELOADER_H
#define STRUCTURELOADER_H

#include "structure.h"

class StructureInjector : public ResourceInjector<Structure> {

    public:
        StructureInjector(Structure * strc);

        virtual ~StructureInjector(){};


        Structure * upload();

        void deleteTempData();

    private:

        Structure * strc;

};

class StructureLoader : public ResourceLoader<Structure>
{
    public:
        StructureLoader();
        virtual ~StructureLoader();

        static StructureLoader * buildLoader();

        bool canCreate(std::string dir, std::string name);
        StructureInjector * create(std::string dir, std::string name);
        std::vector<std::string> getFilenames(std::string dir, std::string name);


    protected:



    private:


};

#endif // STRUCTURELOADER_H
