#include "binstructureloader.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <configloader.h>

#include "gameengine.h"

#include "util/fileutils.h"
#include "util/math/vector3.h"
#include "util/math/quaternion.h"
#include "util/math/conversions.h"
#include "physics/physicsutil.h"
#include "structureutil.h"

using namespace Math;

const double ROT_MATRIX[16] = {

    1, 0, 0, 0,
    0, 0, 1, 0,
    0, -1, 0, 0,
    0, 0, 0, 1

};

BinStructureLoader::BinStructureLoader()
{
    //ctor
}

BinStructureLoader::~BinStructureLoader()
{
    //dtor
}

bool BinStructureLoader::canCreate(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".binstruct");

    return fileExists(fname);

}

typedef struct struct_vert_t {
    float x;
    float y;
    float z;
    float nx;
    float ny;
    float nz;
    float s;
    float t;
    unsigned int matIndex;
} BinStructVert;

StructureInjector * BinStructureLoader::create(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".binstruct");

    FILE * file = fopen(fname.c_str(), "rb");

    int32_t magic;
    int32_t version;

    fread(&magic, sizeof(int32_t), 1, file);
    fread(&version, sizeof(int32_t), 1, file);

    int32_t matCount;
    fread(&matCount, sizeof(int32_t), 1, file);

    std::vector<std::string> materialNames(matCount);

    for (int i = 0; i < matCount; ++i) {

        int32_t index;
        int16_t nameLength;

        fread(&index, sizeof(int32_t), 1, file);
        fread(&nameLength, sizeof(int16_t), 1, file);

        std::vector<uint8_t> chars(nameLength);
        fread(chars.data(), sizeof(uint8_t), nameLength, file);

        std::vector<char> sData(nameLength+1);
        for (int j = 0; j < nameLength; ++j)
            sData[j] = chars[j];
        sData[nameLength] = 0;

        materialNames[index] = std::string(sData.data());

    }

    int32_t vertexCount;
    fread(&vertexCount, sizeof(int32_t), 1, file);
    std::vector<BinStructVert> rawVertData(vertexCount);

    for (int i = 0; i < vertexCount; ++i) {
        fread(rawVertData.data()+i, sizeof(BinStructVert), 1, file);
    }

    int32_t indexCount;
    fread(&indexCount, sizeof(int32_t), 1, file);
    std::vector<int> indexData(indexCount);

    fread(indexData.data(), sizeof(int), indexCount, file);

    fclose(file);

    std::vector<float> rawModelVerts(vertexCount * 3);
    std::vector<float> rawModelNormals(vertexCount * 3);
    std::vector<float> rawModelUvs(vertexCount * 2);

    for (int i = 0; i < vertexCount; ++i) {

        rawModelVerts[i*3+0] = rawVertData[i].x;
        rawModelVerts[i*3+1] = rawVertData[i].y;
        rawModelVerts[i*3+2] = rawVertData[i].z;

        rawModelNormals[i*3+0] = rawVertData[i].nx;
        rawModelNormals[i*3+1] = rawVertData[i].ny;
        rawModelNormals[i*3+2] = rawVertData[i].nz;

        rawModelUvs[i*2+0] = rawVertData[i].s;
        rawModelUvs[i*2+1] = rawVertData[i].t;

    }

    Math::Matrix4 trans((double *)ROT_MATRIX);

    Math::Mesh * mesh = new Math::Mesh(rawModelVerts, rawModelNormals, rawModelUvs, vertexCount, indexData);
    mesh = Math::Mesh::withTransform(mesh, trans);
    mesh->computeTangents();
    //mesh->saveToFile("bin_test.ply");
    int tmp;
    std::vector<float> meshData = mesh->getModelData(&vertexCount);

    std::vector<float> modelData(vertexCount * 12);

    for (int i = 0; i < vertexCount; ++i) {

        modelData[i*12+0] = meshData[i*11+0];
        modelData[i*12+1] = meshData[i*11+1];
        modelData[i*12+2] = meshData[i*11+2];
        modelData[i*12+3] = meshData[i*11+3];
        modelData[i*12+4] = meshData[i*11+4];
        modelData[i*12+5] = meshData[i*11+5];
        modelData[i*12+6] = meshData[i*11+6];
        modelData[i*12+7] = meshData[i*11+7];
        modelData[i*12+8] = meshData[i*11+8];
        modelData[i*12+9] = meshData[i*11+9];
        modelData[i*12+10] = meshData[i*11+10];

        ((int*) (modelData.data()))[i*12+11] = rawVertData[i].matIndex + 1;

    }

    ModelInjector * model = new ModelInjector(modelData, vertexCount, indexData, mesh, GL_TRIANGLES, true);

    std::vector<ShaderInjector*> shaders;
    std::vector<MaterialInjector *> materials;
    for (int i = 0; i < materialNames.size(); ++i) {

        MaterialInjector * matInjector = (MaterialInjector *) GameEngine::resources->load<Material>(materialNames[i]);

        materials.push_back(matInjector);
        shaders.push_back(matInjector->getShader());

    }

    MaterialInjector * mat;

    {
        std::string source = buildShaderSource(shaders);

        std::string vertexSource = readFile("resources/shaders/vertex_structure.glsl");
        std::string svertexSource = readFile("resources/shaders/vertex_structure_static.glsl");

        ShaderInjector * shaderInjector = new ShaderInjector(vertexSource, source);
        ShaderInjector * sshaderInjector = new ShaderInjector(svertexSource, source);

        shaderInjector->setUniformName("transformationMatrix", "transform");
        shaderInjector->setUniformName("projectionMatrix", "projection");
        shaderInjector->setUniformName("viewMatrix", "view");
        shaderInjector->setUniformName("clipping", "useClipping");

        sshaderInjector->setUniformName("projectionMatrix", "projection");
        sshaderInjector->setUniformName("viewMatrix", "view");

        shaderInjector->setAttribute(0, "position");
        shaderInjector->setAttribute(1, "normal");
        shaderInjector->setAttribute(2, "uv");
        shaderInjector->setAttribute(3, "tangent");
        shaderInjector->setAttribute(4, "matIndex");

        sshaderInjector->setAttribute(0, "position");
        sshaderInjector->setAttribute(1, "normal");
        sshaderInjector->setAttribute(2, "uv");
        sshaderInjector->setAttribute(3, "tangent");
        sshaderInjector->setAttribute(4, "matIndex");

        sshaderInjector->setAttribute(5, "transform_0");
        sshaderInjector->setAttribute(6, "transform_1");
        sshaderInjector->setAttribute(7, "transform_2");
        sshaderInjector->setAttribute(8, "transform_3");

        int textureSlot = 0;
        int staticSlot = 0;

        for (int i = 0; i < materialNames.size(); ++i) {

            std::string suffix = std::to_string(i+1);

            std::map<std::string, int> shaderBindings = materials[i]->getShader()->getTextureBindings();

            for (auto it = shaderBindings.begin(); it != shaderBindings.end(); ++it) {

                shaderInjector->setTextureSlot(it->first + suffix, textureSlot);
                sshaderInjector->setTextureSlot(it->first + suffix, textureSlot);

                textureSlot++;

            }

        }
        mat = new MaterialInjector(shaderInjector, sshaderInjector);

        textureSlot = 0;

        for (int i = 0; i < materialNames.size(); ++i) {

            int textureCount = materials[i]->getTextureCount();

            std::map<std::string, int> shaderBindings = materials[i]->getShader()->getTextureBindings();

            for (auto jt = shaderBindings.begin(); jt != shaderBindings.end(); ++jt) {

                mat->addTexture(textureSlot++, materials[i]->getTexture(jt->second));

            }

            /*resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(0));
            resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(1));
            resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(2));*/

        }
    }

    Structure * strc = new Structure(name, model, mat);
    strc->setCollisionShape(PhysicsUtil::createShapeFromMesh(mesh));

    StructureInjector * injector = new StructureInjector(strc);
    injector->setName(name);
    injector->setType("Structure");

    return injector;

}

std::vector<std::string> BinStructureLoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".binstruct");

    std::vector<std::string> files(1);
    files[0] = fname;
    return files;

}

BinStructureLoader * BinStructureLoader::buildLoader() {
    return new BinStructureLoader();
}
