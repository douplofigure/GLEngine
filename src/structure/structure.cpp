#include "structure.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#define GLM_ENABLE_EXPERIMENTAL
#include <GL/gl.h>
#include <glm/gtx/transform.hpp>

#include "structureutil.h"
#include "gameengine.h"

using namespace Math;
using namespace glstructure;

std::string positionLine = "gPosition = vec4(pos.xyz, gl_FragCoord.z);";

Structure::Structure(std::string name) {

    this->resultModel = nullptr;
    this->resultShader = nullptr;

    materialCount = 0;
    this->lights = std::vector<STRUCTURE_LIGHT>();

    this->name = name;

}

Structure::Structure(std::string name, ModelInjector * model, MaterialInjector * material) : Structure(name) {

    resultModel = model;
    resultShader = material;

    materialCount = 1;

}

Structure::~Structure()
{
    //dtor
}

glm::mat4 getTransformationMatrix(float s, Vector3 pos, Vector3 rot) {

    glm::vec3 position(pos.x, pos.y, pos.z);
    glm::vec3 rotation(rot.x, rot.y, rot.z);

    glm::mat4 smat = glm::scale(glm::vec3(s, s, s));

    glm::mat4 tmat = glm::translate(position);

    glm::mat4 rmat = glm::rotate(rotation.y, glm::vec3(0, 1, 0));
    rmat = glm::rotate(rmat, rotation.x, glm::vec3(1, 0, 0));
    rmat = glm::rotate(rmat, rotation.z, glm::vec3(0, 0, 1));

    return tmat * rmat * smat;
}

void Structure::addElement(ModelInjector * model, MaterialInjector * material, Vector3 pos, Vector3 rot, double scale) {

    Matrix4 mat = Matrix4::fromGLM(getTransformationMatrix(scale, pos, rot));

    this->elements.push_back((glstructure::STRUCTURE_ELEM) {model, material, mat});

    if ( !this->materialIndex[material->getName()] ) {
        this->materialIndex[material->getName()] = ++materialCount;
        this->materials.push_back(material);
    }

}

void Structure::addElement(ModelInjector * model, MaterialInjector * material, Matrix4 trans) {

    this->elements.push_back((glstructure::STRUCTURE_ELEM) {model, material, trans});

    if ( !this->materialIndex[material->getName()] ) {
        this->materialIndex[material->getName()] = ++materialCount;
        this->materials.push_back(material);
    }

}

std::vector<STRUCTURE_LIGHT> Structure::getLights() {
    return lights;
}

void Structure::addLight(Vector3 pos, Vector3 color, int type, double falloff) {

    STRUCTURE_LIGHT light;

    light.pos = pos;
    light.color = color;

    light.type = type;
    light.falloff = falloff;

    this->lights.push_back(light);

}

void Structure::setCollisionShape(btCollisionShape * shape) {

    this->collisionShape = shape;

}

btCollisionShape * Structure::getCollisionShape() {
    return this->collisionShape;
}

Mesh * Structure::getMesh() {

    return this->resultModel->getMeshData();

}

void Structure::buildData() {


    Mesh * mesh = Mesh::withTransform(elements[0].model->getMeshData(), elements[0].trans);
    std::vector<int> materialBuffer;
    for (int i = 0; i < mesh->getVertexCount(); ++i) {
        materialBuffer.push_back(materialIndex[elements[0].shader->getName()]);
    }

    for (int i = 1; i < elements.size(); ++i) {

        Matrix4 trans = elements[i].trans;//Matrix4::fromGLM(getTransformationMatrix(elements[i].scale, elements[i].position, elements[i].rotation));

        elements[i].model->getMeshData()->computeTangents();

        Mesh * tmpMesh = Mesh::withTransform(elements[i].model->getMeshData(), trans);

        mesh->mergeWith(tmpMesh);

        delete tmpMesh;
        //delete elements[i].model->getMeshData();

        for (int j = 0; j < elements[i].model->getMeshData()->getVertexCount(); ++j) {
            materialBuffer.push_back(materialIndex[elements[i].shader->getName()]);
        }

    }

    int meshVertexCount = 0;
    int vertexSize = 0;

    std::vector<float> meshData = mesh->getModelData(&meshVertexCount);
    vertexSize = meshData.size()/meshVertexCount+1;

    float * tmpData = (float*) malloc(sizeof(float) * meshVertexCount * vertexSize);

    int mDataOffset = 0;
    int nDataOffset = 0;
    for (int i = 0; i < meshVertexCount; ++i) {

        for (int j = 0; j < vertexSize-1; ++j) {
            if (nDataOffset >= meshData.size() + meshVertexCount) throw std::runtime_error("Outside of mesh data " + std::to_string(nDataOffset));
            tmpData[nDataOffset++] = meshData[mDataOffset++];
        }

        if (nDataOffset >= meshData.size() + meshVertexCount) throw std::runtime_error("Outside of mesh data " + std::to_string(nDataOffset));
        ((int*) (tmpData))[nDataOffset++] = materialBuffer[i];

    }

    std::vector<float> modelData = std::vector<float>(nDataOffset);
    for (int i = 0; i < nDataOffset; ++i) {
        modelData[i] = (tmpData[i]);
    }

    free(tmpData);

    resultModel = new ModelInjector(modelData, meshVertexCount, mesh->getIndexData(), mesh, GL_TRIANGLES, true);

    std::vector<ShaderInjector*> shaders;
    for (int i = 0; i < materials.size(); ++i) {

        shaders.push_back(materials[i]->getShader());

    }

    std::string source = buildShaderSource(shaders);

    std::string fragmentSource = source;

    std::cout << fragmentSource;

    std::string vertexSource = readFile("resources/shaders/vertex_structure.glsl");
    std::string svertexSource = readFile("resources/shaders/vertex_structure_static.glsl");

    ShaderInjector * shaderInjector = new ShaderInjector(vertexSource, fragmentSource);
    ShaderInjector * sshaderInjector = new ShaderInjector(svertexSource, fragmentSource);

    shaderInjector->setUniformName("transformationMatrix", "transform");
    shaderInjector->setUniformName("projectionMatrix", "projection");
    shaderInjector->setUniformName("viewMatrix", "view");
    shaderInjector->setUniformName("clipping", "useClipping");

    sshaderInjector->setUniformName("projectionMatrix", "projection");
    sshaderInjector->setUniformName("viewMatrix", "view");

    shaderInjector->setAttribute(0, "position");
    shaderInjector->setAttribute(1, "normal");
    shaderInjector->setAttribute(2, "uv");
    shaderInjector->setAttribute(3, "tangent");
    shaderInjector->setAttribute(4, "matIndex");

    sshaderInjector->setAttribute(0, "position");
    sshaderInjector->setAttribute(1, "normal");
    sshaderInjector->setAttribute(2, "uv");
    sshaderInjector->setAttribute(3, "tangent");
    sshaderInjector->setAttribute(4, "matIndex");

    sshaderInjector->setAttribute(5, "transform_0");
    sshaderInjector->setAttribute(6, "transform_1");
    sshaderInjector->setAttribute(7, "transform_2");
    sshaderInjector->setAttribute(8, "transform_3");

    int textureSlot = 0;
    int staticSlot = 0;

    for (std::map<std::string, int>::iterator it = materialIndex.begin(); it != materialIndex.end(); ++it) {

        std::string suffix = std::to_string(it->second);

        std::map<std::string, int> shaderBindings = materials[it->second-1]->getShader()->getTextureBindings();

        for (auto it = shaderBindings.begin(); it != shaderBindings.end(); ++it) {

            shaderInjector->setTextureSlot(it->first + suffix, textureSlot);
            sshaderInjector->setTextureSlot(it->first + suffix, textureSlot);

            textureSlot++;

        }

    }

    this->resultShader = new MaterialInjector(shaderInjector, sshaderInjector);

    textureSlot = 0;

    for (std::map<std::string, int>::iterator it = materialIndex.begin(); it != materialIndex.end(); ++it) {

        int textureCount = materials[it->second-1]->getTextureCount();

        std::map<std::string, int> shaderBindings = materials[it->second-1]->getShader()->getTextureBindings();

        for (auto jt = shaderBindings.begin(); jt != shaderBindings.end(); ++jt) {

            resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(jt->second));

        }

        /*resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(0));
        resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(1));
        resultShader->addTexture(textureSlot++, materials[it->second-1]->getTexture(2));*/

    }

    //std::cout << materialBuffer[128] << std::endl;

}

ModelInjector * Structure::getModelInjector() {
    return resultModel;
}

Model * Structure::getModel() {
    return resultModel->uploadInternal();
}

Material * Structure::getMaterial() {
    return resultShader->uploadInternal();
}

void Structure::upload() {

    resultModel->uploadInternal();
    resultShader->uploadInternal();

}

std::string Structure::getName() {
    return name;
}

void Structure::saveToBinFile(std::string dir) {

    std::string fname = dir;
    fname.append(this->name).append(".binstr");

    FILE * file = fopen(fname.c_str(), "wb");

    int version = 1;

    fwrite(&version, sizeof(int), 1, file);

    int vertexCount = 0;
    std::vector<float> meshData = resultModel->getMeshData()->getModelData(&vertexCount);
    fwrite(&vertexCount, sizeof(int), 1, file);
    int valCount = meshData.size();
    fwrite(&valCount, sizeof(int), 1, file);
    fwrite(meshData.data(), sizeof(float), vertexCount, file);

    std::vector<int> meshIndices = resultModel->getMeshData()->getIndexData();
    vertexCount = meshIndices.size();
    fwrite(&vertexCount, sizeof(int), 1, file);
    fwrite(meshIndices.data(), sizeof(float), vertexCount, file);

    std::string fragsource = resultShader->getShader()->getFragmentSource();
    std::string vertsource = resultShader->getShader()->getVertexSource();

    int tmp = 0;
    tmp = fragsource.length();
    fwrite(&tmp, sizeof(int), 1, file);
    fwrite(fragsource.c_str(), sizeof(char), tmp, file);


    tmp = vertsource.length();
    fwrite(&tmp, sizeof(int), 1, file);
    fwrite(vertsource.c_str(), sizeof(char), tmp, file);

    int texCount = resultShader->getTextureCount();

    fwrite(&texCount, sizeof(int), 1, file);

    for (int i = 0; i < texCount; ++i) {

        std::string texData = resultShader->getTexture(i)->getName();
        int texSize = texData.length();
        fwrite(&texSize, sizeof(int), 1, file);
        fwrite(texData.c_str(), sizeof(char), texSize, file);

    }

    fclose(file);


}

void Structure::deleteData() {

    std::cout << "Deleting Structure data" << std::endl;
    delete resultModel->getMeshData();
    delete resultModel;
    delete resultShader;

}
