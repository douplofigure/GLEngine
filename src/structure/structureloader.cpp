#include "structureloader.h"

#include <configloader.h>

#include "gameengine.h"

#include "util/fileutils.h"
#include "util/math/vector3.h"
#include "util/math/quaternion.h"
#include "util/math/conversions.h"
#include "physics/physicsutil.h"

using namespace Math;

StructureLoader::StructureLoader()
{
    //ctor
}

StructureLoader::~StructureLoader()
{
    //dtor
}

bool StructureLoader::canCreate(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".struct");

    return fileExists(fname);

}

btCollisionShape * getCollisionShape(CompoundNode * data, Structure * strc) {

    std::string type = std::string(data->get<const char *>("type"));

    if (!type.compare("box")) {

        std::vector<double> sizeData = data->getArray<double>("size")->getValue();

        return new btBoxShape(btVector3(sizeData[0], sizeData[1], sizeData[2]));

    }

    else if (!type.compare("sphere")) {

        double radius = data->get<double>("radius");

        return new btSphereShape(radius);

    }

    else if (!type.compare("capsule")) {

        double radius = data->get<double>("radius");
        double height = data->get<double>("height");

        return new btCapsuleShape(radius, height);

    }

    else if (!type.compare("static_mesh")) {


        return PhysicsUtil::createShapeFromMesh(strc->getMesh());

    }

    throw std::runtime_error("Unknown shape type " + type);

    return new btSphereShape(1.0);

}

btCollisionShape * buildShapeFromCompound(CompoundNode * data, bool compoundShape, Structure * strc) {

    if (compoundShape) {

        std::vector<CompoundNode *> elements = data->getArray<CompoundNode*>("shape")->getValue();

        btCompoundShape * shape = new btCompoundShape();

        for (int i = 0; i < elements.size(); ++i) {

            std::vector<double> rawRot = elements[i]->getArray<double>("rotation")->getValue();
            std::vector<double> rawPos = elements[i]->getArray<double>("position")->getValue();

            btMatrix3x3 mat;
            mat.setEulerZYX(rawRot[0], rawRot[1], rawRot[2]);

            btTransform trans;
            trans.setBasis(mat);
            trans.setOrigin(btVector3(rawPos[0], rawPos[1], rawPos[2]));

            shape->addChildShape(trans, getCollisionShape(elements[i], strc));

        }

        return shape;

    }

    return getCollisionShape(data->getCompound("shape"), strc);


}

StructureInjector * StructureLoader::create(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".struct");

    std::cout << "Loading Structure " << name << std::endl;

    CompoundNode * rootNode = ConfigLoader::loadFileTree(fname);

    std::vector<CompoundNode*> elements = rootNode->getArray<CompoundNode*>("elements")->getValue();

    Structure * strc = new Structure(name);

    for (int i = 0; i < elements.size(); ++i) {

        std::string modelName = std::string(elements[i]->get<const char *>("model"));
        std::string materialName = std::string(elements[i]->get<const char *>("material"));

        ModelInjector * model = (ModelInjector*) GameEngine::resources->load<Model>(modelName);
        MaterialInjector * material = (MaterialInjector*) GameEngine::resources->load<Material>(materialName);

        std::vector<double> rawPos = elements[i]->getArray<double>("position")->getValue();
        Vector3 pos = Vector3(rawPos[0], rawPos[1], rawPos[2]);

        double scale = elements[i]->get<double>("scale");

        std::vector<double> rawRot = elements[i]->getArray<double>("rotation")->getValue();
        Vector3 rot = Vector3(radians(rawRot[0]), radians(rawRot[1]), radians(rawRot[2]));

        strc->addElement(model, material, pos, rot, scale);
    }
    if (rootNode->getArray<CompoundNode*>("lights")) {
        std::vector<CompoundNode*> lights = rootNode->getArray<CompoundNode*>("lights")->getValue();
        for (int i = 0; i < lights.size(); ++i) {

            std::vector<double> rawPos = lights[i]->getArray<double>("position")->getValue();
            std::vector<double> rawCol = lights[i]->getArray<double>("color")->getValue();

            double falloff = lights[i]->get<double>("falloff");
            int type = lights[i]->get<int>("type");

            Vector3 pos = Vector3(rawPos[0], rawPos[1], rawPos[2]);
            Vector3 rot = Vector3(rawCol[0], rawCol[1], rawCol[2]);

            strc->addLight(pos, rot, type, falloff);

        }
    }
    strc->buildData();

    if (rootNode->getCompound("collision")) {

        CompoundNode * colData = rootNode->getCompound("collision");

        bool isCompound = colData->get<bool>("compound");

        strc->setCollisionShape(buildShapeFromCompound(colData, isCompound, strc));

    }

    delete rootNode;

    return new StructureInjector(strc);


}

std::vector<std::string> StructureLoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;
    fname.append(name).append(".struct");

    std::vector<std::string> files(1);
    files[0] = fname;
    return files;

}

StructureLoader * StructureLoader::buildLoader() {
    return new StructureLoader();
}

StructureInjector::StructureInjector(Structure * strc) {

    this->strc = strc;

}

void StructureInjector::deleteTempData() {



}

Structure * StructureInjector::upload() {

    strc->upload();
    return strc;

}
