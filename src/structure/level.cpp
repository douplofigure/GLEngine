#include "level.h"

#include <configloader.h>

#include "gameengine.h"
#include "world/entity.h"
#include "util/math/conversions.h"
#include "util/math/constants.h"
#include "render/cubemap.h"
#include "render/viewportdeffered.h"
#include "render/staticrenderelement.h"

using namespace Math;
using namespace level;

Level::Level() {
    //ctor
}

Level::~Level()
{
    //dtor
}

std::vector<std::string> Level::getStructuresToLoad() {

    std::vector<std::string> names;

    for (int i = 0; i < this->placements.size(); ++i) {
        bool add = true;

        for (int j = 0; j < names.size(); ++j) {

            if (!names[j].compare(placements[i].name)) {
                add = false;
                std::cout << names[j] << " == " << placements[i].name << std::endl;
                exit(1);
                break;
            }

        }
        if (add) names.push_back(placements[i].name);

    }

    return names;

}

void Level::loadRequirements(ResourceContainer * resources) {

    std::vector<std::string> names;

    for (int i = 0; i < this->placements.size(); ++i) {

        bool add = true;

        for (int j = 0; j < names.size(); ++j) {

            if (!names[j].compare(placements[i].name)) {
                add = false;
                break;
            }

        }
        if (add) {
            GameEngine::setLoadingMessage("Loading " + placements[i].name);
            resources->load<Structure>(placements[i].name);
            names.push_back(placements[i].name);

        }

    }

    for (int i = 0; i < this->ppEffects.size(); ++i) {

        GameEngine::setLoadingMessage("Loading " + ppEffects[i].shaderName);
        resources->load<Shader>(ppEffects[i].shaderName);
        for (int j = 0; j < ppEffects[i].textureNames.size(); ++j) {

            switch (ppEffects[i].textureTypes[j]) {

                case level::TEXTURE_2D:
                    resources->load<Texture>(ppEffects[i].textureNames[j]);
                    break;

                case level::TEXTURE_3D:
                    resources->load<CubeMap>(ppEffects[i].textureNames[j]);
                    break;

                case level::TEXTURE_LUT:
                    resources->load<LUT>(ppEffects[i].textureNames[j]);
                    break;


            }

        }

    }

    GameEngine::setLoadingMessage("Loading " + this->skyBoxName);
    resources->load<CubeMap>(this->skyBoxName);
    resources->load<LUT>(this->lutName);

}

void Level::addPlacement(std::string name, std::string entityType, Vector3 pos, Vector3 rot, double angle, double scale, int layer, double mass, bool pickUp, CompoundNode * entityData) {

    LEVEL_PLACEMENT placement;
    placement.name = name;
    placement.entityType = entityType;
    placement.pos = pos;
    placement.rot = rot.normalize();
    placement.angle = radians(angle);
    placement.scale = scale;
    placement.mass = mass;
    placement.layer = layer;
    placement.pickUp = pickUp;

    placement.entityData = entityData;

    if (mass == 0.0){
        this->countsPerStruct[name]++;
    }

    this->placements.push_back(placement);

}

void Level::addPPEffect(LEVEL_PP_EFFECT effect) {

    this->ppEffects.push_back(effect);

}

void Level::applyGeneralSettings(Viewport * view) {

    if (ViewportDeffered * dv = dynamic_cast<ViewportDeffered*>(view)) {

        std::cout << "Setting skybox" <<  std::endl;
        dv->setSkybox(GameEngine::resources->get<CubeMap>(this->skyBoxName));
        std::cout << "Skybox ok" << std::endl;
        auto renderSize = GameEngine::getRenderSize();

        for (int i = 0; i < this->ppEffects.size(); ++i) {

            PostProcessingEffect * effect = new PostProcessingEffect(GameEngine::resources->get<Shader>(ppEffects[i].shaderName), renderSize[0], renderSize[1]);

            for (int j = 0; j < ppEffects[i].textureNames.size(); ++j) {

                switch (ppEffects[i].textureTypes[j]) {

                    case level::TEXTURE_2D:
                        effect->addTexture(GameEngine::resources->get<Texture>(ppEffects[i].textureNames[j]));
                        break;

                    case level::TEXTURE_3D:
                        effect->addTexture(GameEngine::resources->get<CubeMap>(ppEffects[i].textureNames[j]));
                        break;

                    case level::TEXTURE_LUT:
                        effect->addTexture(GameEngine::resources->get<LUT>(ppEffects[i].textureNames[j]));
                        break;


                }

            }

            dv->addPostProcessingEffect(effect);

        }

    }

}

void Level::addToViewport(Viewport * view) {

    for (int i = 0; i < placements.size(); ++i) {

        std::shared_ptr<RenderElement> rElem(new RenderElement(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos.x, placements[i].pos.y, placements[i].pos.z));
        rElem->setRotationAxis(0, 1, 0);
        //rElem->setRotation(placements[i].rot.x, placements[i].rot.y, placements[i].rot.z);
        rElem->setRotation(Math::Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle));
        rElem->setScale(placements[i].scale);

        view->addElement(placements[i].layer, rElem);

        view->addStructureLights(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos, Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle), placements[i].scale);

    }

    this->applyGeneralSettings(view);

    std::cout << "Skybox: " << this->skyBoxName << std::endl;
    ((ViewportDeffered *)view)->setSkybox(GameEngine::resources->get<CubeMap>(this->skyBoxName));
    exit(1);

}

void Level::applyToWorld(World * world, Viewport * view) {

    std::map<std::string, StaticRenderElement *> elemsByName;

    for (int i = 0; i < sceneLights.size(); ++i) {
        int l = view->addLight(sceneLights[i].pos, sceneLights[i].color, (LIGHT_TYPE) sceneLights[i].type, sceneLights[i].falloff);
        if (i == this->sunLightIndex)
            view->setLightAsSun(l);
    }

    for (int i = 0; i < placements.size(); ++i) {

        /*if (IS_ZERO(placements[i].mass) && this->countsPerStruct[placements[i].name] > 1) {

            StaticRenderElement * sElem = elemsByName[placements[i].name];

            if (!sElem) {

                std::cout << "Creating space for " << (countsPerStruct[placements[i].name]) << " instances" << std::endl;
                sElem = new StaticRenderElement(GameEngine::resources->get<Structure>(placements[i].name), countsPerStruct[placements[i].name]);
                elemsByName[placements[i].name] = sElem;
                sElem->setRotationAxis(0, 1, 0);

                view->addElement(placements[i].layer, sElem);


            }

            std::cout << "Adding instance for element " << placements[i].name << std::endl;

            int inst = sElem->addInstance(placements[i].pos.x, placements[i].pos.y, placements[i].pos.z);
            sElem->updateInstance(inst, glm::vec3(placements[i].pos.x, placements[i].pos.y, placements[i].pos.z),
                                        Math::Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle),
                                        placements[i].scale);

            std::cout << inst << std::endl;

            view->addStructureLights(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos, placements[i].rot, placements[i].scale);

            Entity * ent = new Entity(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos, Math::Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle), placements[i].mass, sElem, placements[i].pickUp);
            world->addEntity(ent, true);

            continue;

        }*/

        std::shared_ptr<RenderElement> rElem(new RenderElement(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos.x, placements[i].pos.y, placements[i].pos.z));
        rElem->setRotationAxis(0, 1, 0);
        rElem->setRotation(Math::Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle));
        rElem->setScale(placements[i].scale);

        view->addElement(placements[i].layer, rElem);

        view->addStructureLights(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos, Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle), placements[i].scale);

        //Entity * ent = new Entity(GameEngine::resources->get<Structure>(placements[i].name), placements[i].pos, Math::Quaternion::fromAxisAngle(placements[i].rot, placements[i].angle), placements[i].mass, rElem, placements[i].pickUp);
        std::shared_ptr<Entity> ent = Entity::createEntityFromType(placements[i], rElem);
        world->addEntity(ent, true);

    }

    this->applyGeneralSettings(view);

}

level::TEXTURE_TYPE textureTypeFromString(std::string type) {

     if (!type.compare("lut")) {
        return level::TEXTURE_LUT;
    } else if (!type.compare("2d")) {
        return level::TEXTURE_2D;
    } else if (!type.compare("3d")) {
        return level::TEXTURE_LUT;
    }

    return level::TEXTURE_LUT;

}

level::LEVEL_PP_EFFECT buildEffectFromCompound(CompoundNode * node) {

    level::LEVEL_PP_EFFECT effect;
    effect.shaderName = std::string(node->get<const char *>("shader"));

    std::vector<CompoundNode *> textures = node->getArray<CompoundNode *>("textures")->getValue();
    for (int i = 0; i < textures.size(); ++i) {

        std::string name(textures[i]->get<const char *>("name"));
        std::string typestr(textures[i]->get<const char *>("type"));

        std::cout << "Adding texture " << name << " of type " << typestr << std::endl;

        level::TEXTURE_TYPE type = textureTypeFromString(typestr);

        effect.textureNames.push_back(name);
        effect.textureTypes.push_back(type);

    }

    std::cout << "Done adding Textures to effect" << std::endl;

    return effect;

}

void Level::addLight(Vector3 pos, Vector3 color, int type, double falloff) {

    glstructure::STRUCTURE_LIGHT light;

    light.pos = pos;
    light.color = color;

    light.type = type;
    light.falloff = falloff;

    this->sceneLights.push_back(light);

}

void Level::setSunLightIndex(int index) {
    this->sunLightIndex = index;
}

Level * Level::loadFromFile(std::string fname) {

    std::cout << "Loading from file" << std::endl;
    CompoundNode * rootNode = ConfigLoader::loadFileTree(fname);
    std::cout << "Loding done, got node at " << rootNode << std::endl;

    std::vector<CompoundNode *> pData = rootNode->getArray<CompoundNode *>("structures")->getValue();

    Level * lvl = new Level();

    for (int i = 0; i < pData.size(); ++i) {

        std::string name(pData[i]->get<const char *>("name"));
        std::vector<double> rPos = pData[i]->getArray<double>("position")->getValue();
        Vector3 pos = Vector3(rPos[0], rPos[1], rPos[2]);

        std::vector<double> rRot = pData[i]->getArray<double>("rotation")->getValue();
        Vector3 rot = Vector3(radians(rRot[0]), radians(rRot[1]), radians(rRot[2]));

        double angle = 0.0;

        if(pData[i]->getChildNode("angle")){
            angle = pData[i]->get<double>("angle");
        }

        std::string entType = "Entity";
        if (pData[i]->getChildNode("entityType")) {
            entType = std::string(pData[i]->get<const char *>("entityType"));
        }

        CompoundNode * entityData = nullptr;
        if (pData[i]->getChildNode("entityData")) {
            entityData = pData[i]->getCompound("entityData");
        }

        double scale = pData[i]->get<double>("scale");
        double mass = pData[i]->get<double>("mass");
        int layer = pData[i]->get<int>("layer");

        bool pickup = pData[i]->get<bool>("pickUp");

        lvl->addPlacement(name, entType, pos, rot, angle, scale, layer, mass, pickup, entityData);

    }

    CompoundNode * skyData = rootNode->getCompound("scene");

    if (rootNode->getArray<CompoundNode*>("lights")) {

        std::vector<CompoundNode *> lights = rootNode->getArray<CompoundNode*>("lights")->getValue();
        for (int i = 0; i < lights.size(); ++i) {

            std::vector<double> rawPos = lights[i]->getArray<double>("position")->getValue();
            std::vector<double> rawCol = lights[i]->getArray<double>("color")->getValue();

            double falloff = lights[i]->get<double>("falloff");
            int type = lights[i]->get<int>("type");

            Vector3 pos = Vector3(rawPos[0], rawPos[1], rawPos[2]);
            Vector3 rot = Vector3(rawCol[0], rawCol[1], rawCol[2]);

            lvl->addLight(pos, rot, type, falloff);

        }

        lvl->setSunLightIndex(skyData->get<int>("sunLight"));

    }

    std::cout << "Adding Effects" << std::endl;

    std::vector<CompoundNode *> effectData = skyData->getArray<CompoundNode*>("effects")->getValue();
    for (int i = 0; i < effectData.size(); ++i) {

        lvl->addPPEffect(buildEffectFromCompound(effectData[i]));

    }

    lvl->skyBoxName = std::string(skyData->get<const char *>("skybox"));
    lvl->lutName = "neutral";

    return lvl;

}
