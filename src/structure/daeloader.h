#ifndef DAELOADER_H
#define DAELOADER_H

#include "resources/resourceloader.h"
#include "structure.h"
#include "structureloader.h"


class DAELoader : public ResourceLoader<Structure>
{
    public:
        DAELoader();
        virtual ~DAELoader();

        virtual StructureInjector * create(std::string dir, std::string name);
        virtual bool canCreate(std::string dir, std::string name);
        virtual std::vector<std::string> getFilenames(std::string dir, std::string name);

        static DAELoader * buildLoader();

    protected:

    private:
};

#endif // DAELOADER_H
