#include "structureutil.h"

#include <iostream>
#include <stdlib.h>

#include "gameengine.h"
#include "render/shader.h"

std::string shaderHead = "in vec2 uvPos;\n"
    "in vec4 pos;\n"
    "in vec3 faceNormal;\n"
    "in vec3 lightDirection;\n"
    "in vec3 cameraDirection;\n"
    "in float visibility;\n"
    "in float cameraDistance;\n"
    "in vec4 shadowCoord;\n"
    "flat in int materialIndex;\n"

    "in vec3 passTangent;\n"

    "in mat3 tangentToWorld;\n";

std::string readFile(std::string fname) {

    FILE * file = fopen(fname.c_str(), "r");

    if (!file) {
        exit(1);
    }

    int fileSize;
    fseek(file, 0, SEEK_END);
	fileSize = ftell(file);
	fseek(file, 0, SEEK_SET);

	std::string data = "";
	char buffer[128];

	while (fgets(buffer, 128, file)) {
        data.append(std::string(buffer));
        buffer[0] = 0;
	}
	data.append(std::string(buffer));
	fclose(file);
	return data;
}

std::string getMainContents(std::string shaderSource) {

    int pos = shaderSource.find("void main");

    if (pos == std::string::npos)
        throw std::runtime_error("Could not find void main");

    const char * rawData = shaderSource.c_str();

    int index = pos;
    int level = 0;

    int startIndex;

    while (rawData[index] != '{') {
        if (index >= shaderSource.length()) {
            throw std::runtime_error("Unable to find start of main in shader");

        }
        std::cout << "rawData[" << index << "] = " << rawData[index++] << std::endl;
    }
    startIndex = ++index;
    level = 0;
    while (rawData[index] != '}' || level) {
        //std::cout << "rawData[" << index << "] = " << rawData[index++] << std::endl;

        if (index >= shaderSource.length())
            throw std::runtime_error("Could not find End of main function in shader");

        if (rawData[index] == '{') level++;
        else if (rawData[index] == '}') level--;
        index++;
    }

    std::cout << "index " << index << " length " << shaderSource.length() << std::endl;

    std::string result = shaderSource.substr(startIndex, index - startIndex);

    std::cout << "Result " << result << std::endl;

    return result;

}

std::string addVarSuffixes(std::string shaderSource, std::vector<std::string> varNames, std::string suffix) {

    for (int i = 0; i < varNames.size(); ++i) {

        int currentPos = shaderSource.find(varNames[i]);

        while (currentPos != std::string::npos) {

            std::cout << "Replacing " << varNames[i] << " at " << currentPos << std::endl;

            shaderSource.insert(currentPos + varNames[i].length(), suffix);

            currentPos = shaderSource.find(varNames[i], currentPos+1);

        }

    }

    return shaderSource;

}

std::string buildShaderSource(std::vector<ShaderInjector*> shaderNames) {

    std::string fragmentSource;

    /*if (GameEngine::getGLVersion() >= 440) {
        fragmentSource = "#version 440 core\n"
    "layout (location = 0) out vec4 gPosition;\n"
    "layout (location = 1) out vec4 gNormal;\n"
    "layout (location = 2) out vec4 gAlbedoSpec;\n";
    } else {*/

        fragmentSource = "#version 330 core\n"
        "out vec4 data[3];\n";

    //}

     fragmentSource.append(shaderHead);

    std::vector<std::string> switchSources(shaderNames.size());

    for (int i = 0; i < shaderNames.size(); ++i) {
        ShaderInjector * injector = shaderNames[i];

        std::map<std::string, int> shaderBindings = injector->getTextureBindings();

        std::vector<std::string> textureNames;

        for (auto it = shaderBindings.begin(); it != shaderBindings.end(); ++it) {

            textureNames.push_back(it->first);
            std::cout << "Adding " << it->first << " for replacement" << std::endl;

            fragmentSource.append("uniform sampler2D " + it->first + std::to_string(i+1) + ";\n");

        }

        textureNames.push_back("roughness");

        try {
            std::string src = injector->getFragmentSource();
            std::string mainContents = getMainContents(src);
            mainContents = addVarSuffixes(mainContents, textureNames, std::to_string(i+1));

            switchSources[i] = mainContents;

        } catch (std::runtime_error e) {

            std::cerr << "Problem loading " << shaderNames[i]->getName() << std::endl;
            std::cerr << injector->getFragmentSource() << std::endl;
            throw std::runtime_error("Unable to load " + injector->getName());

        }

    }

    fragmentSource.append("void main() {\n\t");

    fragmentSource.append("\tswitch (materialIndex) { \n");

    for (int i = 0; i < switchSources.size(); ++i) {

        fragmentSource.append("\tcase " + std::to_string(i+1) + ":\n");
        fragmentSource.append(switchSources[i]);
        fragmentSource.append("\tbreak;");

    }

    fragmentSource.append("\t}\n}");

    return fragmentSource;

}
