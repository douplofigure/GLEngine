#ifndef LEVEL_H
#define LEVEL_H

#include <vector>
#include <map>

#include "structure.h"

#include "resources/resourcecontainer.h"
#include "render/viewport.h"

#include <configloader.h>

namespace level {

typedef struct level_placement_t {

    std::string name;
    std::string entityType;
    Math::Vector3 pos;
    Math::Vector3 rot;
    double angle;
    double scale;
    double mass;
    int layer;
    bool pickUp;

    CompoundNode * entityData;


} LEVEL_PLACEMENT;

typedef enum level_effect_texture_type_e {

    TEXTURE_2D,
    TEXTURE_3D,
    TEXTURE_LUT

} TEXTURE_TYPE;

typedef struct level_pp_effect_t{

    std::string shaderName;
    std::vector<std::string> textureNames;
    std::vector<TEXTURE_TYPE> textureTypes;

} LEVEL_PP_EFFECT;

};

class World;

class Level
{
    public:
        Level();
        virtual ~Level();

        std::vector<std::string> getStructuresToLoad();

        void loadRequirements(ResourceContainer * resources);

        void addToViewport(Viewport * view);

        void applyToWorld(World * world, Viewport * view);

        static Level * loadFromFile(std::string fname);

    protected:

    private:

        void addPlacement(std::string name, std::string entityType, Math::Vector3 pos, Math::Vector3 rot, double angle, double scale, int layer, double mass, bool pickup, CompoundNode * entityData);
        void addPPEffect(level::LEVEL_PP_EFFECT effect);
        void applyGeneralSettings(Viewport * view);

        void addLight(Math::Vector3 position, Math::Vector3 color, int type, double falloff);
        void setSunLightIndex(int i);

        std::vector<level::LEVEL_PLACEMENT> placements;
        std::string skyBoxName;
        std::string lutName;
        std::vector<level::LEVEL_PP_EFFECT> ppEffects;
        int sunLightIndex;
        std::vector<glstructure::STRUCTURE_LIGHT> sceneLights;

        std::map<std::string, int> countsPerStruct;


};

#endif // LEVEL_H
