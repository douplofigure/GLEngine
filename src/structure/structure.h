#ifndef STRUCTURE_H
#define STRUCTURE_H

#include <map>

#include <btBulletDynamicsCommon.h>

#include "render/model.h"
#include "render/shader.h"
#include "render/material.h"
#include "util/math/vector3.h"

namespace glstructure {

typedef struct struct_element_t {

    ModelInjector * model;
    MaterialInjector * shader;

    /*Math::Vector3 position;
    Math::Vector3 rotation;
    double scale;*/
    Math::Matrix4 trans;

} STRUCTURE_ELEM;

typedef struct {

    Math::Vector3 pos;
    Math::Vector3 color;

    int type;
    double falloff;

} STRUCTURE_LIGHT;

}

class Structure : Resource
{
    public:
        Structure(std::string name);
        Structure(std::string name, ModelInjector * model, MaterialInjector * material);
        virtual ~Structure();

        void addElement(ModelInjector * model, MaterialInjector * shader, Math::Vector3 pos, Math::Vector3 rot, double scale);
        void addElement(ModelInjector * model, MaterialInjector * material, Math::Matrix4 trans);

        void addLight(Math::Vector3 position, Math::Vector3 color, int type, double falloff);
        std::vector<glstructure::STRUCTURE_LIGHT> getLights();

        void setCollisionShape(btCollisionShape * shape);
        btCollisionShape * getCollisionShape();

        void buildData();
        void deleteData();

        void upload();

        void saveToBinFile(std::string dir);

        std::string getName();

        ModelInjector * getModelInjector();
        Model * getModel();
        Material * getMaterial();

        Math::Mesh * getMesh();

    protected:

    private:

        ModelInjector * resultModel;
        MaterialInjector * resultShader;

        std::vector<glstructure::STRUCTURE_ELEM> elements;
        std::map<std::string, int> materialIndex;
        std::vector<MaterialInjector*> materials;

        std::vector<glstructure::STRUCTURE_LIGHT> lights;

        std::string name;

        int materialCount;

        btCollisionShape * collisionShape;

};


#endif // STRUCTURE_H
