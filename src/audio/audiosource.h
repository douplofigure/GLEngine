#ifndef _AUDIOSOURCE_H
#define _AUDIOSOURCE_H

#include "sound.h"

class AudioSource {
	
	public:
		AudioSource(float x, float y, float z);
		virtual ~AudioSource();
		
		void play(Sound * sound);
		
		void setPosition(float x, float y, float z);
		
		void pause();
        void stop();
        void setLooping(bool looping);
        bool isPlaying();
		
	protected:
		
		int sourceId;
		
		float x;
		float y;
		float z;
		
		bool playing;
	
};

void initSoundEngine();

#endif
