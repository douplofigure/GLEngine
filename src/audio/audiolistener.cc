#include "audiolistener.h"

#include <AL/al.h>

float AudioListener::facing[6] = {0,0,-1,0,1,0};
float AudioListener::position[3] = {0,0,0};

void AudioListener::init(float x, float y, float z) {
	
	alListenerfv(AL_POSITION, position);
    alListenerfv(AL_ORIENTATION, facing);
    
    alListenerf(AL_GAIN, 1);
	
}

void AudioListener::setPosition(float x, float y, float z) {
	
	position[0] = x;
	position[1] = y;
	position[2] = z;
	alListenerfv(AL_POSITION, position);
	
}

void AudioListener::setFacing(float x, float y, float z) {
	
	facing[0] = x;
	facing[1] = y;
	facing[2] = z;
	
	alListenerfv(AL_ORIENTATION, facing);
	
}
