#ifndef _AUDIO_LISTENER_H
#define _AUDIO_LISTENER_H

class AudioListener {
	
	public:
		static void init(float x, float y, float z);
		static void setPosition(float x, float y, float z);
		
		static void setFacing(float x, float y, float z);
		
	private:
	
		static float facing[6];
		static float position[3];
	
};

#endif
