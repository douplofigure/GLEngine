#include "audiosource.h"

#include <AL/al.h>
#include <AL/alut.h>
#include <stdlib.h>

AudioSource::AudioSource(float x, float y, float z) {

	alGenSources(1, (ALuint*)&this->sourceId);
	alSource3f(sourceId, AL_POSITION, x, y, z);

    this->x = x;
    this->y = y;
    this->z = z;

}

AudioSource::~AudioSource() {

}

void AudioSource::setPosition(float x, float y, float z) {

	alSource3f(sourceId, AL_POSITION, x, y, z);

    this->x = x;
    this->y = y;
    this->z = z;

}

void AudioSource::setLooping(bool looping) {

    alSourcei(sourceId, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);

}

void AudioSource::play(Sound * sound) {

	alSourcei(sourceId, AL_BUFFER, sound->getBuffer());

    alSourcePlay(this->sourceId);
    playing = true;

}

void AudioSource::pause() {

    alSourcePause(this->sourceId);
    playing = false;

}

void AudioSource::stop() {

    alSourceStop(sourceId);
    playing = false;

}

bool AudioSource::isPlaying() {

    int state;
    alGetSourcei(sourceId, AL_SOURCE_STATE, &state);

    return state == AL_PLAYING && playing;

}


void initSoundEngine() {

    #ifndef _WIN32
	alutInit(0, NULL);
	#endif
    //alDistanceModel(AL_LINEAR_DISTANCE);

}
