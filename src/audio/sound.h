#ifndef _SOUND_H
#define _SOUND_H

#include "resources/resourceregistry.h"

class Sound : Resource {

	public:
		Sound(int bufferId);
		virtual ~Sound();

		int getBuffer();
		void deleteData();

		static Sound * createSound(unsigned int format, void * data, int size, int freq);

	protected:
		int bufferId;

};

class SoundLoader : public ResourceLoader<Sound> {

	public:
		ResourceInjector<Sound> * create(std::string dir, std::string name);
		bool canCreate(std::string dir, std::string name);
		std::vector<std::string> getFilenames(std::string dir, std::string name);

		static SoundLoader * buildLoader();

};

#endif
