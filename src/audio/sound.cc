#include "sound.h"

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include <AL/al.h>
#include <AL/alut.h>

Sound::Sound(int bufferId) {

	this->bufferId = bufferId;

}

Sound::~Sound(){

	alDeleteBuffers(1, (ALuint*) &bufferId);

}

int Sound::getBuffer() {

	return bufferId;

}

Sound * Sound::createSound(unsigned int format, void * data, int size, int freq) {

	unsigned int bufferId;
	int error;
	alGenBuffers(1, &bufferId);

	if ((error = alGetError()) != AL_NO_ERROR)
    {
      fprintf(stderr, "alGenBuffers buffer 0 : %d\n", error);
      // Delete buffers
      alDeleteBuffers(1, (ALuint*)&bufferId);
    }

    alBufferData(bufferId,format,data,size,freq);

    if ((error = alGetError()) != AL_NO_ERROR)
    {
      printf("alBufferData buffer 0 : %d\n", error);
      // Delete buffers
      alDeleteBuffers(1, (ALuint*)&bufferId);
      exit(1);
    }

	return new Sound(bufferId);

}

class SoundInjector : public ResourceInjector<Sound> {

    public:
        SoundInjector(Sound * sound) {
            this->sound = sound;
        }
        virtual ~SoundInjector(){}

        Sound * upload(){
            return this->sound;
        }

        void deleteTempData() {

        }


    private:
        Sound * sound;

};

ResourceInjector<Sound> * SoundLoader::create(std::string dir, std::string name) {

	std::string fname = dir;
	fname.append(name).append(".wav");

	char loop;
	int freq;
	void * data;
	int format;
	int size;

	alutLoadWAVFile((ALbyte*)fname.c_str(), &format, &data, &size, &freq, &loop);

    Sound * sound = Sound::createSound(format, data, size, freq);
    alutUnloadWAV(format,data,size,freq);

    return new SoundInjector(sound);

}

bool SoundLoader::canCreate(std::string dir, std::string name) {

	std::string fname = dir;
	fname.append(name).append(".wav");

	return fileExists(fname);

}

SoundLoader * SoundLoader::buildLoader() {
    return new SoundLoader();
}
std::vector<std::string> SoundLoader::getFilenames(std::string dir, std::string name) {

    std::string fname = dir;
	fname.append(name).append(".wav");

    std::vector<std::string> files;
    files.push_back(fname);
    return files;

}

void Sound::deleteData() {



}
