#ifndef AMATURE_H
#define AMATURE_H

#include "util/math/quaternion.h"
#include "util/math/vector3.h"
#include "util/math/matrix4.h"

namespace Anim {

class Bone {

    public:
        Bone(Math::Quaternion rotation, Math::Vector3 position, double length);
        Bone(Math::Vector3 position, Math::Vector3 vec);

        void addChild(Bone * b);
        std::vector<Bone *> getChildren();
        void setTransform(Math::Quaternion rotation, Math::Vector3 position);
        Math::Matrix4 getMatrixTransform();

        Math::Quaternion getRotation();
        Math::Vector3 getOffset();
        Math::Vector3 getPosition();

        void addRotation(Math::Quaternion rot);
        void addOffset(Math::Vector3 offset);
        void setOffset(Math::Vector3 offset);

        void reset();

    private:

        std::vector<Bone *> children;
        Math::Quaternion rotation;
        Math::Quaternion initRotation;
        Math::Vector3 position;
        Math::Vector3 offset;
        Math::Vector3 initVector;
        Math::Vector3 dirVec;
        double length;

};

class Armature
{
    public:
        Armature();
        virtual ~Armature();

        int getBoneCount();
        void setBoneTransform(int boneId, Math::Quaternion rotation, Math::Vector3 position);

        void addBone(Bone * bone);
        Math::Matrix4 getBoneTransform(int id);
        Math::Quaternion getBoneRotation(int id);
        Math::Vector3 getBoneOffset(int id);
        Math::Vector3 getBonePosition(int id);

        void reset();

    protected:

    private:

        std::vector<Bone *> bones;

};

}

#endif // AMATURE_H
