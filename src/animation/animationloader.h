#ifndef ANIMATIONLOADER_H
#define ANIMATIONLOADER_H

#include "resources/resourceloader.h"
#include "animation/animationrig.h"

class AnimationLoader : public ResourceLoader<AnimationRig>
{
    public:
        /** Default constructor */
        AnimationLoader();
        /** Default destructor */
        virtual ~AnimationLoader();

        AnimationInjector * create(std::string dir, std::string name);
        bool canCreate(std::string dir, std::string name);
        std::vector<std::string> getFilenames(std::string dir, std::string name);

        static AnimationLoader * buildLoader();

    protected:

    private:
};

#endif // ANIMATIONLOADER_H
