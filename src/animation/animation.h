#ifndef ANIMATION_H
#define ANIMATION_H

#include <vector>

#include "keyframe.h"
#include "armature.h"
#include "util/math/quaternion.h"
#include "util/math/vector3.h"
#include "util/math/spline.h"

namespace Anim {

class Animation
{
    public:
        Animation(std::vector<KeyFrame*> keyframes);
        Animation();
        virtual ~Animation();

        Armature * applyTo(Armature * armature, double time);

        void deleteData();

        double getDuration();


    protected:

    private:

        std::vector<double> frameTimes;
        double duration;
        std::vector<Math::Spline<double>> XPositions;
        std::vector<Math::Spline<double>> YPositions;
        std::vector<Math::Spline<double>> ZPositions;

        std::vector<Math::Spline<double>> rotationsA;
        std::vector<Math::Spline<double>> rotationsB;
        std::vector<Math::Spline<double>> rotationsC;
        std::vector<Math::Spline<double>> rotationsD;
};

}

#endif // ANIMATION_H
