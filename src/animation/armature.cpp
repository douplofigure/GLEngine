#include "armature.h"

#include <cmath>

using namespace Anim;
using namespace Math;

const double ZUPMATRIX[16] = {

    1, 0, 0, 0,
    0, 0, 1, 0,
    0, -1, 0, 0,
    0, 0, 0, 1

};

const double SCALEMATRIX[16] = {

    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1

};

const Matrix4 zMatrix((double*)ZUPMATRIX);
const Matrix4 scaleMatrix((double*)SCALEMATRIX);

Armature::Armature()
{
    //ctor
}

Armature::~Armature()
{
    //dtor
}


int Armature::getBoneCount(){

    return this->bones.size();

}

void Armature::setBoneTransform(int bId, Quaternion q, Vector3 v) {

    this->bones[bId]->setTransform(q, v);

}

void Armature::addBone(Bone * bone) {

    this->bones.push_back(bone);

    for (int i = 0; i < bone->getChildren().size(); ++i) {
        this->addBone(bone->getChildren()[i]);
    }

}

Matrix4 Armature::getBoneTransform(int i) {
    return bones[i]->getMatrixTransform();
}

void Armature::reset() {
    for (int i = 0; i < bones.size(); ++i)
        bones[i]->reset();
}

Quaternion Armature::getBoneRotation(int i) {
    return this->bones[i]->getRotation();
}

Vector3 Armature::getBoneOffset(int i) {
    return this->bones[i]->getOffset();
}

Vector3 Armature::getBonePosition(int i) {
    return this->bones[i]->getPosition();
}

Bone::Bone(Math::Quaternion q, Vector3 pos, double length) {

    this->rotation = q;
    this->initRotation = q;
    this->offset = pos;
    this->length = length;

}

Bone::Bone(Vector3 position, Vector3 vec) {

    this->offset = position;
    Vector3 axis = Vector3::cross(Vector3(1,0,0), vec).normalize();
    double angle = atan((vec * Vector3::cross(axis, Vector3(1,0,0)))/(vec * Vector3(1,0,0)));

    double s = sin(angle/2);
    this->rotation.a = 1;//cos(angle/2);
    this->rotation.b = 0;//axis.x * s;
    this->rotation.c = 0;//axis.y * s;
    this->rotation.d = 0;//axis.z * s;

    this->initRotation.a = 1;//cos(angle/2);
    initRotation.b = 0;//axis.x * s;
    initRotation.c = 0;//axis.y * s;
    initRotation.d = 0;//axis.z * s;
    this->initVector = vec;
    this->dirVec = vec;
    this->position = position;

    this->length = vec.length();


}

void Bone::setTransform(Quaternion rot, Vector3 pos) {

    addRotation(rot);
    addOffset(pos);

    for (int i = 0; i < this->children.size(); ++i) {
        //this->children[i]->addRotation(rot);
        this->children[i]->setOffset(position + dirVec);
    }

    //std::cout << "dirVec: " << dirVec - initVector << std::endl;

}

void Bone::addRotation(Quaternion rot) {

    this->rotation = rotation * rot;
    this->dirVec = rotation.toRotationMatrix() * initVector;

    for (int i = 0; i < children.size(); ++i)
        children[i]->addRotation(rot);

}

void Bone::addOffset(Vector3 offset) {

    this->position += offset;

    for (int i = 0; i < children.size(); ++i)
        children[i]->addOffset(offset);

}

void Bone::setOffset(Vector3 offset) {

    this->position = offset;

}

void Bone::addChild(Bone * b) {

    this->children.push_back(b);

}

Matrix4 Bone::getMatrixTransform() {

    Matrix4 mat;
    mat.fromQuaternion(this->rotation, this->position);
    return (Matrix4) zMatrix * mat;

}

std::vector<Bone *> Bone::getChildren() {
    return this->children;
}

void Bone::reset() {
    this->position = Vector3(0,0,0);
    this->rotation = initRotation;
}

Quaternion Bone::getRotation() {
    return this->rotation;
}

Vector3 Bone::getOffset() {

    return rotation.toRotationMatrix() * offset - position;

}

Vector3 Bone::getPosition() {

    return position;

}
