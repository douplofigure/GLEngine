#include "animationrig.h"

using namespace Anim;

AnimationRig::AnimationRig(std::map<std::string, Animation *> anims, Armature * arm, Model * mod)
{
    this->animations = anims;
    this->armature = arm;
    this->model = mod;
}

AnimationRig::~AnimationRig()
{
    //dtor
}

Model * AnimationRig::getModel(){
    return model;
}

Armature * AnimationRig::getArmature(){
    return armature;
}

std::map<std::string, Animation *> AnimationRig::getAnimations() {
    return animations;
}

void AnimationRig::deleteData() {

    //this->model->deleteData();

}

AnimationInjector::AnimationInjector(ModelInjector * model, Armature * arm, std::map<std::string, Animation *> animations) {
    this->model = model;
    this->arm = arm;
    this->animations = animations;
}

AnimationInjector::~AnimationInjector() {

}

AnimationRig * AnimationInjector::upload() {

    std::cout << "uploading animation rig" << std::endl;
    Model * m = this->model->uploadInternal();

    return new AnimationRig(animations, arm, m);

}

void AnimationInjector::deleteTempData() {

}
