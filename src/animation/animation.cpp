#include "animation.h"

using namespace Anim;
using namespace Math;

Animation::Animation(std::vector<KeyFrame*> keyframes) {

    this->frameTimes = std::vector<double>(keyframes.size());
    for (int i = 0; i < keyframes.size(); ++i)
        frameTimes[i] = keyframes[i]->getTimeStamp();

    this->duration = frameTimes[frameTimes.size()-1];

    std::cout << "Reorganizing frame data" << std::endl;

    for (int i = 0; i < keyframes[0]->getPositionData().size(); ++i) {

        std::vector<double> boneXPositions(keyframes.size());
        std::vector<double> boneYPositions(keyframes.size());
        std::vector<double> boneZPositions(keyframes.size());

        std::vector<double> boneARotations(keyframes.size());
        std::vector<double> boneBRotations(keyframes.size());
        std::vector<double> boneCRotations(keyframes.size());
        std::vector<double> boneDRotations(keyframes.size());

        for (int j = 0; j < keyframes.size(); ++j) {

            boneXPositions[j] = keyframes[j]->getPositionData()[i].x;
            boneYPositions[j] = keyframes[j]->getPositionData()[i].y;
            boneZPositions[j] = keyframes[j]->getPositionData()[i].z;

            boneARotations[j] = keyframes[j]->getRotationData()[i].a;
            boneBRotations[j] = keyframes[j]->getRotationData()[i].b;
            boneCRotations[j] = keyframes[j]->getRotationData()[i].c;
            boneDRotations[j] = keyframes[j]->getRotationData()[i].d;


        }

        this->XPositions.push_back(Spline<double>(frameTimes, boneXPositions));
        this->YPositions.push_back(Spline<double>(frameTimes, boneYPositions));
        this->ZPositions.push_back(Spline<double>(frameTimes, boneZPositions));
        //this->positions.push_back(Spline<Vector3>(frameTimes, bonePositions));

        this->rotationsA.push_back(Spline<double>(frameTimes, boneARotations));
        this->rotationsB.push_back(Spline<double>(frameTimes, boneBRotations));
        this->rotationsC.push_back(Spline<double>(frameTimes, boneCRotations));
        this->rotationsD.push_back(Spline<double>(frameTimes, boneDRotations));

    }

}

Animation::~Animation()
{
    //dtor
}

void Animation::deleteData() {

}

Armature * Animation::applyTo(Armature * arm, double time) {

    //std::cout << "Appling animation to "<< arm->getBoneCount() << " bones" << std::endl;
    for (int i = 0; i < arm->getBoneCount(); ++i) {

        //std::cout << "x: " << XPositions[i](time) << std::endl;
        Vector3 pos(XPositions[i](time), YPositions[i](time), ZPositions[i](time));
        Quaternion rot(rotationsA[i](time), rotationsB[i](time), rotationsC[i](time), rotationsD[i](time));
        //Vector3 pos(0,0,0);
        //std::cout << "pos: " << pos << std::endl;
        ///rotations[i](time)
        arm->setBoneTransform(i, rot, pos);
        //std::cout << "Set for " << i << " / " << arm->getBoneCount() << " bones" << std::endl;
    }

    return arm;

}

double Animation::getDuration() {
    return duration;
}
