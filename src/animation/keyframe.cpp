#include "keyframe.h"

using namespace Math;
//using namespace Anim;

KeyFrame::KeyFrame(double time, std::vector<Quaternion> rot, std::vector<Vector3> pos) {

    this->frameTime = time;
    this->positions = pos;
    this->rotations = rot;

}

KeyFrame::~KeyFrame()
{
    //dtor
}

double KeyFrame::getTimeStamp() {
    return frameTime;
}

std::vector<Quaternion> KeyFrame::getRotationData() {
    return rotations;
}

std::vector<Vector3> KeyFrame::getPositionData() {
    return positions;
}
