#ifndef ANIMATIONRIG_H
#define ANIMATIONRIG_H

#include <map>
#include <string>

#include "resources/resourceloader.h"
#include "armature.h"
#include "animation.h"
#include "render/model.h"

class AnimationRig : public Resource
{
    public:
        /** Default constructor */
        AnimationRig(std::map<std::string, Anim::Animation *> animations, Anim::Armature * arm, Model * model);
        /** Default destructor */
        virtual ~AnimationRig();

        Model * getModel();
        Anim::Armature * getArmature();
        std::map<std::string, Anim::Animation *> getAnimations();


        void deleteData();


    protected:

    private:

        std::map<std::string, Anim::Animation *> animations;
        Anim::Armature * armature;
        Model * model;

};


class AnimationInjector : public ResourceInjector<AnimationRig> {

    public:

        AnimationInjector(ModelInjector * model, Anim::Armature * arm, std::map<std::string, Anim::Animation *> animations);
        virtual ~AnimationInjector();

        AnimationRig * upload();
        void deleteTempData();

    private:

        ModelInjector * model;
        Anim::Armature * arm;
        std::map<std::string, Anim::Animation *> animations;

};

#endif // ANIMATIONRIG_H
