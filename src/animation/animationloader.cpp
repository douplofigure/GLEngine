#include "animationloader.h"

#include <configloader.h>
#include "gameengine.h"
#include "util/math/vector3.h"

#ifdef _WIN32

#include <GL/glew.h>

#endif

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

using namespace Anim;
using namespace Math;

typedef union floatint_u {
    float f;
    int i;
} fint;

AnimationLoader::AnimationLoader()
{
    //ctor
}

AnimationLoader::~AnimationLoader()
{
    //dtor
}

Bone * constructBone(CompoundNode * node, Vector3 parentPos) {

    std::vector<double> positionData = node->getArray<double>("position")->getValue();
    std::vector<double> vectorData = node->getArray<double>("vector")->getValue();
    std::vector<double> rotationData = node->getArray<double>("rotation")->getValue();

    Vector3 position = Vector3(positionData[0], positionData[1], positionData[2]) + parentPos;
    Vector3 vec = Vector3(vectorData[0], vectorData[1], vectorData[2]);
    Quaternion rotation = Quaternion(rotationData[0], rotationData[1], rotationData[2], rotationData[3]);

    Bone * b = new Bone(position, vec);

    if (!node->getChildNode("children"))
        return b;

    std::vector<CompoundNode *> children = node->getArray<CompoundNode *>("children")->getValue();
    for (int i = 0; i < children.size(); ++i) {
        b->addChild(constructBone(children[i], position+vec));
    }

    return b;

}

Bone * constructBone(CompoundNode * node) {

    std::vector<double> positionData = node->getArray<double>("position")->getValue();
    std::vector<double> vectorData = node->getArray<double>("vector")->getValue();
    std::vector<double> rotationData = node->getArray<double>("rotation")->getValue();

    Vector3 position = Vector3(positionData[0], positionData[1], positionData[2]);
    Vector3 vec = Vector3(vectorData[0], vectorData[1], vectorData[2]);
    Quaternion rotation = Quaternion(rotationData[0], rotationData[1], rotationData[2], rotationData[3]);

    Bone * b = new Bone(position, vec);

    if (!node->getChildNode("children"))
        return b;

    std::vector<CompoundNode *> children = node->getArray<CompoundNode *>("children")->getValue();
    for (int i = 0; i < children.size(); ++i) {
        b->addChild(constructBone(children[i], position+vec));
    }

    return b;

}

KeyFrame * constructKeyframe(CompoundNode * node, std::map<std::string, int> idByName) {

    double time = node->get<double>("time");
    std::vector<CompoundNode *> transformData = node->getArray<CompoundNode *>("transforms")->getValue();

    std::vector<Quaternion> rotations(transformData.size());
    std::vector<Vector3> positions(transformData.size());

    for (int i = 0; i < transformData.size(); ++i) {

        std::vector<double> rot = transformData[i]->getArray<double>("rotation")->getValue();
        std::vector<double> pos = transformData[i]->getArray<double>("position")->getValue();

        std::string name(transformData[i]->get<const char *>("name"));

        int boneId = idByName[name];

        rotations[boneId] = Quaternion(rot[0], rot[1], rot[2], rot[3]);
        positions[boneId] = Vector3(pos[0], pos[1], pos[2]);

    }

    std::cout << "Done with frame at " << time << std::endl;

    return new KeyFrame(time, rotations, positions);

}

void bQSort(std::vector<float> values, std::vector<int> ids, int l, int r) {

    if (r <= l) return;
    int i = l;
    int j = r;

    float piv = values[(l+r)/2];

    do {

        while (values[i] > piv) i++;
        while (values[j] < piv) j--;

        if (i <= j) {

            float tmpf = values[i];
            values[i] = values[j];
            values[j] = tmpf;

            int tmpi = ids[i];
            ids[i] = ids[j];
            ids[j] = tmpi;

            i++;
            j++;

        }

    } while ( i <= j);

    bQSort(values, ids, l, j);
    bQSort(values, ids, i, r);

}

void orderBoneWeights(std::vector<float> values, std::vector<int> boneIds) {

    if (values.size() < 5)
        return;

    bQSort(values, boneIds, 0, values.size());

    float v = values[0] + values[1] + values[2] + values[3];

    values[0] /= v;
    values[1] /= v;
    values[2] /= v;
    values[3] /= v;

}

AnimationInjector * AnimationLoader::create(std::string dir, std::string name) {

    std::string filename = dir;
    filename.append(name).append(".anim");

    CompoundNode * root = ConfigLoader::loadFileTree(filename);
    CompoundNode * meshData = root->getCompound("mesh");

    std::string modelFile(meshData->get<const char *>("filename"));
    std::string directory = dir;
    ModelInjector * modelData = (ModelInjector*) GameEngine::resources->load<Model>(dir, modelFile);

    int vertexCount;

    Math::Mesh * mesh = modelData->getMeshData();//Mesh::withTransform(modelData->getMeshData(), zMatrix);
    mesh->computeTangents();
    std::vector<float> computedData = mesh->getModelData(&vertexCount);
    std::vector<int> indexData = mesh->getIndexData();

    std::vector<CompoundNode *> boneOrder = meshData->getArray<CompoundNode *>("bone_order")->getValue();
    std::vector<std::string> bonesById(boneOrder.size());
    std::map<std::string, int> idByName;

    for (int i = 0; i < boneOrder.size(); ++i) {

        std::string name(boneOrder[i]->get<const char *>("name"));

        bonesById[i] = name;
        idByName[name] = i;

    }

    std::vector<float> weights(vertexCount*4);
    std::vector<int> boneIds(vertexCount*4);

    std::vector<CompoundNode *> weightData = meshData->getArray<CompoundNode *>("weights")->getValue();
    std::cout << "optained weight data" << std::endl;

    std::vector<int> bones;
    std::vector<float> values;

    ///TODO: Add sorting and weight balancing for Bones in case there are more than 4 Bones for 1 Vertex.

    for (int i = 0; i < weightData.size(); ++i) {

        CompoundNode * weightInfo = weightData[i];
        std::vector<float> rawPosition = weightInfo->getArray<float>("position")->getValue();
        Vector3 position = Vector3(rawPosition[0], rawPosition[1], rawPosition[2]);
        if (weightInfo->getChildNode("empty")) {

            for (int j = 0; j < vertexCount; ++j) {

                Vector3 vPos = Vector3(computedData[j*11], computedData[j*11+1], computedData[j*11+2]);
                if ((vPos - position).length() < 1e-4) {
                    weights[j*4] = 0;
                    weights[j*4+1] = 0;
                    weights[j*4+2] = 0;
                    weights[j*4+3] = 0;

                    boneIds[j*4] = -1;
                    boneIds[j*4+1] = -1;
                    boneIds[j*4+2] = -1;
                    boneIds[j*4+3] = -1;

                }

            }
            continue;
        }

        bones = weightInfo->getArray<int>("bones")->getValue();
        values = weightInfo->getArray<float>("values")->getValue();

        orderBoneWeights(values, bones);

        for (int j = 0; j < vertexCount; ++j) {
            Vector3 vPos = Vector3(computedData[j*11], computedData[j*11+1], computedData[j*11+2]);
            if ((vPos - position).length() < 1e-4) {

                int k;
                for (k = 0; k < bones.size(); ++k) {

                    weights[j*4+k] = values[k];
                    boneIds[j*4+k] = bones[k];

                }
                for (int l = k; l < 4; ++l) {

                    weights[j*4+k] = 0;
                    boneIds[j*4+k] = -1;

                }


            }

        }

    }

    std::cout << "Gotten Mesh data" << std::endl;

    //mesh = Mesh::withTransform(modelData->getMeshData(), zMatrix);
    mesh->computeTangents();
    computedData = mesh->getModelData(&vertexCount);

    std::vector<float> finalData(computedData.size() + vertexCount * 8);

    int index = 0;

    fint tmp;

    for (int i = 0; i < vertexCount; ++i) {

        finalData[index++] = computedData[i*11];
        finalData[index++] = computedData[i*11+1];
        finalData[index++] = computedData[i*11+2];

        finalData[index++] = computedData[i*11+3];
        finalData[index++] = computedData[i*11+4];
        finalData[index++] = computedData[i*11+5];

        finalData[index++] = computedData[i*11+6];
        finalData[index++] = computedData[i*11+7];

        finalData[index++] = computedData[i*11+8];
        finalData[index++] = computedData[i*11+9];
        finalData[index++] = computedData[i*11+10];

        tmp.i = boneIds[i*4] + 1;
        finalData[index++] = tmp.f;
        tmp.i = boneIds[i*4+1] + 1;
        finalData[index++] = tmp.f;
        tmp.i = boneIds[i*4+2] + 1;
        finalData[index++] = tmp.f;
        tmp.i = boneIds[i*4+3] + 1;
        finalData[index++] = tmp.f;

        tmp.i = boneIds[i*4];
        std::cout << "Set bone to " << boneIds[i*4] << " or " << tmp.f << std::endl;

        finalData[index++] = weights[i*4];
        finalData[index++] = weights[i*4+1];
        finalData[index++] = weights[i*4+2];
        finalData[index++] = weights[i*4+3];

    }

    std::cout << "finalData " << finalData.size() << " / " << vertexCount << " = " << (finalData.size() / vertexCount) << std::endl;

    ModelInjector * modelInjector = new ModelInjector(finalData, vertexCount, indexData, mesh, GL_TRIANGLES);

    Armature * armature = new Armature();
    std::map<std::string, Animation *> animations;

    std::vector<CompoundNode *> boneData = root->getArray<CompoundNode *>("bones")->getValue();

    for (int i = 0; i < boneData.size(); ++i) {
        armature->addBone(constructBone(boneData[i]));
    }

    std::vector<CompoundNode *> animData = root->getArray<CompoundNode *>("animations")->getValue();
    for (int i = 0; i < animData.size(); ++i) {

        CompoundNode * anim = animData[i];
        std::string name(anim->get<const char *>("name"));

        std::vector<CompoundNode *> frames = anim->getArray<CompoundNode *>("keyframes")->getValue();
        std::vector<KeyFrame*> keyframes(frames.size());

        for (int j = 0; j < frames.size(); ++j) {

            keyframes[j] = constructKeyframe(frames[j], idByName);

        }

        std::cout << "Creating animation" << std::endl;
        animations[name] = new Animation(keyframes);

    }

    std::cout << "Done loading animation" << std::endl;
    return new AnimationInjector(modelInjector, armature, animations);

}

bool AnimationLoader::canCreate(std::string dir, std::string name) {
    std::string filename = dir;
    filename.append(name).append(".anim");

    return fileExists(filename);
}

std::vector<std::string> AnimationLoader::getFilenames(std::string dir, std::string name) {
    std::string filename = dir;
    filename.append(name).append(".anim");

    std::vector<std::string> files(1);
    files[0] = filename;

    return files;

}

AnimationLoader * AnimationLoader::buildLoader() {
    return new AnimationLoader();
}
