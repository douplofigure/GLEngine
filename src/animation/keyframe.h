#ifndef KEYFRAME_H
#define KEYFRAME_H

#include <vector>
#include "util/math/vector3.h"
#include "util/math/quaternion.h"

class KeyFrame
{
    public:
        KeyFrame(double time, std::vector<Math::Quaternion> rot, std::vector<Math::Vector3> pos);
        virtual ~KeyFrame();

        double getTimeStamp();
        std::vector<Math::Quaternion> getRotationData();
        std::vector<Math::Vector3> getPositionData();

    protected:

    private:

        double frameTime;
        std::vector<Math::Quaternion> rotations;
        std::vector<Math::Vector3> positions;

};

#endif // KEYFRAME_H
