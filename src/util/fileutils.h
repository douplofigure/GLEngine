#ifndef _FILEUTILS_H
#define _FILEUTILS_H

#include <string>
#include <stdio.h>

#include <iostream>

inline bool fileExists(std::string fname) {

    FILE * file = fopen(fname.c_str(), "r");
    bool res = file != NULL;
    if (res) fclose(file);
    return res;

}

#endif // _FILEUTILS_H
