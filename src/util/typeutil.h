#ifndef TYPEUTIL_H_INCLUDED
#define TYPEUTIL_H_INCLUDED

#include <typeinfo>
#include <string>

template <typename T> unsigned long getTypeId() {

    return (unsigned long) typeid(T).name();

}

template <typename T> std::string getTypeName() {

    const char * tmpName = typeid(T).name();
    while (*tmpName >= '0' && *tmpName <= '9') tmpName++;
    return std::string(tmpName);

}


#endif // TYPEUTIL_H_INCLUDED
