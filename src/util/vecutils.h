#ifndef VECUTILS_H
#define VECUTILS_H

typedef struct vector3d_t VECTOR3D;
typedef struct vector3f_t VECTOR3F;

typedef struct vector3i_t {

	int x;
	int y;
	int z;

} VECTOR3I;

typedef struct vector2i_t {

    int x;
    int y;

} VECTOR2I;

struct vector3d_t {

    operator VECTOR3F ();

	double x;
	double y;
	double z;

};

struct vector3f_t {

    operator VECTOR3D () {return (VECTOR3D) {x, y,z};};
	float x;
	float y;
	float z;

};

typedef struct vec2d_t {

    double x;
    double y;

} VECTOR2D;

typedef struct vector2f_t {

    operator VECTOR2D () {return (VECTOR2D) {x,y};};
	float x;
	float y;

} VECTOR2F;

inline VECTOR3D operator*(double d, VECTOR3D vec) {

    return (VECTOR3D) {d*vec.x, d*vec.y, d*vec.z};

}

inline VECTOR3D operator*(VECTOR3D vec, double d) {

    return (VECTOR3D) {d*vec.x, d*vec.y, d*vec.z};

}

inline double operator*(VECTOR3D vec1, VECTOR3D vec2) {

    return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z + vec2.z;

}

inline VECTOR3D operator+(VECTOR3D vec1, VECTOR3D vec2) {

    return (VECTOR3D) {vec1.x + vec2.x, vec1.y + vec2.y , vec1.z + vec2.z};

}

inline VECTOR3D operator-(VECTOR3D vec1, VECTOR3D vec2) {

    return (VECTOR3D) {vec1.x - vec2.x, vec1.y - vec2.y , vec1.z - vec2.z};

}

inline VECTOR2D operator-(VECTOR2D vec1, VECTOR2D vec2) {

    return (VECTOR2D) {vec1.x - vec2.x, vec1.y - vec2.y};

}

typedef struct matrix_t {

	float values[16];

} MATRIX;

typedef enum rot_axis {

	X_AXIS,
	Y_AXIS,
	Z_AXIS

} AXIS;

float radians(float x);

void scaleMatrix(MATRIX * mat, float scale);

void translateMatrix(MATRIX * mat, VECTOR3F vec);

MATRIX generateIdentity();
MATRIX generateTransformationMatrix(VECTOR3F pos, float scale, VECTOR3F rot);
void printMatrix(MATRIX * mat);
MATRIX generateMatrixFromFloat(float * vals);
void setValueInMatrix(MATRIX * mat, int line, int col, float value);
MATRIX generateViewMatrix(VECTOR3F pos, float pitch, float yaw, VECTOR3F facing);
MATRIX generateShadowProjectionMatrix(VECTOR3F, VECTOR3F);
VECTOR3D matrixVectorProduct(MATRIX mat, VECTOR3D vec);

double scalarProduct(VECTOR3D vec1, VECTOR3D vec2);
double vec2dScalar(VECTOR2D vec1, VECTOR2D vec2);

VECTOR3D vec3DCrossProduct(VECTOR3D vec1, VECTOR3D vec2);
VECTOR3D vec3DNormalize(VECTOR3D vec);

double degrees(double d);
double dist(VECTOR3F pos1, VECTOR3F pos2);
float randomFloatInBounds(float min, float max);
float getValueFromMatrix(MATRIX * mat, int line, int col);

#endif // VECUTILS_H
