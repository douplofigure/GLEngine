#ifndef SMARTARRAY_H
#define SMARTARRAY_H

#include <stdlib.h>
#include <assert.h>

template <typename T> class SmartArray
{
    public:

        SmartArray() : SmartArray(20) {
        }

        SmartArray(int initSize) {
            data = (T*) malloc(sizeof(T) * initSize);
            maxElemCount = initSize;
            elemCount = 0;
        }
        virtual ~SmartArray() {
            free(data);
        }

        T& operator[] (int index) {
            if (index > elemCount) throw std::runtime_error("Out of array bounds");
            else return data[index];
        }

        void addElement(T elem) {
            if (elemCount < maxElemCount) {
                //std::cout << "Still space for " << elem << std::endl;
                assert(data);
                this->data[elemCount++] = elem;
            }
            else {
                this->data = (T*) realloc(this->data, sizeof(T) * maxElemCount * 2);
                this->maxElemCount *= 2;
                this->data[elemCount++] = elem;
            }
        }

        void removeElement(int index)  {

            if (index > elemCount) {
                throw std::runtime_error("Out of array bounds");
                return;
            }

            this->data[index] = this->data[--elemCount];

        }

        int size() {
            return elemCount;
        }

    protected:

    private:

        int maxElemCount;
        int elemCount;

        T * data;

};

#endif // SMARTARRAY_H
