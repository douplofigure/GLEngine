#ifndef OCTREE_H
#define OCTREE_H

#include <iostream>

#define CHILD_COUNT 4

namespace data {

template <typename K, typename T> class octree_node {

    public:
        octree_node(K key, int childLayers, K(*subKey)(K, int, int)) : octree_node(key, childLayers, subKey, 0){};
        octree_node(K key, int childLayers, K(*subKey)(K, int, int), int depth) {
            this->center = key;
            this->hasChildren = childLayers > 0;
            if (childLayers) {
                this->children = std::vector<octree_node*>(CHILD_COUNT);
                for (int i = 0; i < CHILD_COUNT; ++i)
                    children[i] = new octree_node(subKey(key, i, depth), childLayers-1, subKey, depth+1);
            }
            else {
                elements = std::vector<T>();
            }
        }

        virtual ~octree_node() {

            for (int i = 0; i < this->children.size(); ++i) {
                delete children[i];
            }

            std::vector<octree_node*>().swap(this->children);
            std::vector<T>().swap(this->elements);

        }

        T get(K key, std::vector<int> (*func)(K, K)) {
            if (!hasChildren) return elements[0];
            return this->children[func(center, key)[0]]->get(key, func);
        }

        std::vector<T> getAll(K key, std::vector<int> (*func)(K,K)) {
            if (!hasChildren) {
                return elements;
            }
            return children[func(center, key)[0]]->getAll(key, func);
        }

        void add(K key, T elem, std::vector<int> (*func)(K,K)) {
            if (hasChildren) {
                std::vector<int> v = func(center, key);
                for (int i = 0; i < v.size(); ++i)
                    this->children[v[i]]->add(key, elem, func);
            }
            else {
                elements.push_back(elem);
            }
        }

    private:
        K center;
        std::vector<T> elements;
        std::vector<octree_node*> children;
        bool hasChildren;

};

template <typename K, typename T> class octree {

    public:
        octree(int maxDepth, K center, std::vector<int>(*func)(K, K), K(*subKey)(K, int, int)) {

            this->maxDepth = maxDepth;
            this->rootNode = new octree_node<K,T>(center, maxDepth, subKey);
            this->getSubTreeIndex = func;

        }
        virtual ~octree() {
            delete rootNode;
        }

        T get(K key) {

        }

        std::vector<T> getAll(K key) {
            return this->rootNode->getAll(key, getSubTreeIndex);
        }

        void add(K key, T elem) {
            this->rootNode->add(key, elem, getSubTreeIndex);
        }

    protected:

    private:

        int maxDepth;
        std::vector<int>(*getSubTreeIndex)(K, K);
        octree_node<K, T> * rootNode;

};

}

#endif // OCTREE_H
