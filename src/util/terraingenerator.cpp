#include "terraingenerator.h"

#include <iostream>
#include <vector>

#include <math.h>

#include "simplexnoise.h"
#include "vecutils.h"

#include "gameengine.h"

#include "world/heightmap.h"

#define HL3 5.773502692e-1
#ifndef M_PI
#define M_PI 3.14159265358979323846  /* pi */
#endif // M_PI

struct vertex_t {

    VECTOR3F position;
    VECTOR3F normal;
    VECTOR2F uv;
    VECTOR3F tangent;

};

double TerrainGenerator::lfHeight = 3000;
double TerrainGenerator::hfHeight = 8;

double TerrainGenerator::lfScale  = 0.00043;
double TerrainGenerator::hfScale = 0.01;

double TerrainGenerator::lfxOffset = 7;
double TerrainGenerator::lfyOffset = 13;
double TerrainGenerator::hfxOffset = 13567;
double TerrainGenerator::hfyOffset = 1312;

double TerrainGenerator::worldYOffset = 200;

HeightMap * heightMap;

double weights[6] = {2.0, 0.5, 0.2, 0.1, 0.005, 0.002};
double freqs[6]  =  {0.00013, 0.00245, 0.005234, 0.01325, 0.021234, 0.41232};
double offsets[12] = {135000.0, 12124300, 7848, 7673, 12, 34, 345, 1231, 12, 34, 12, 56};

float TerrainGenerator::getHeight(double x, double y) {

    /*if (! heightMap) heightMap = GameEngine::resources->get<HeightMap>("heightmap");
    std::cout << "Looking into heightmap" << std::endl;
    return 200 * heightMap->getHeightValue((x + 1024.0) / 2048.0, (y + 1024.0) / 2048.0)- 90;*/

    double d = (sqrt(x*x + y*y) / 800);

    return lfHeight * cos( (d < 1.0 ? d*d : 1.0) * M_PI) + (lfHeight / 4) * SimplexNoise1234::noise(x * lfScale, y * lfScale) + hfHeight * SimplexNoise1234::noise(x * hfScale, y * hfScale) + 50;

    double h = 0;
    double w = 0;
    for (int i = 0; i < 6; ++i) {

        h += weights[i] * SimplexNoise1234::noise((x-offsets[2*i])*freqs[i], (y-offsets[2*i+1])*freqs[i]);
        w += weights[i];

    }

    return lfHeight * h / w + worldYOffset;

    return lfHeight * SimplexNoise1234::noise(x*lfScale + lfxOffset,y*lfScale+lfyOffset) + hfHeight * SimplexNoise1234::noise(x*hfScale+hfxOffset, y*hfScale+hfyOffset) + worldYOffset;// + SimplexNoise1234::noise(x*0.1, y*0.1) + 20;

}

void TerrainGenerator::initValues(double lfh, double hfh, double lfs, double hfs, double lfx, double lfy, double hfx, double hfy, double wy) {

    lfHeight = lfh;
    hfHeight = hfh;

    lfScale = lfs;
    hfScale = hfs;

    lfxOffset = lfx;
    lfyOffset = lfy;

    hfxOffset = hfx;
    hfyOffset = hfy;

    worldYOffset = wy;

}

std::vector<VERTEX> TerrainGenerator::generateTriangle(double x, double y, int rotation, double size) {

    std::vector<VERTEX> verts(3);

    VECTOR3D pos1;
    VECTOR3D pos2;
    VECTOR3D pos3;

    double tmpX, tmpY;

    if (!rotation) {

        pos1 = (VECTOR3D) {x, TerrainGenerator::getHeight(x, y), y};

        tmpX = x + size;
        tmpY = y + size;

        pos3 = (VECTOR3D) {x+size, TerrainGenerator::getHeight(x+size, y), y};

        tmpY = y + size;
        pos2 = (VECTOR3D) {x, TerrainGenerator::getHeight(x, y + size), y + size};

    }
    else {

        tmpX = x;
        tmpY = y;
        pos1 = (VECTOR3D) {x+size, TerrainGenerator::getHeight(x+size, y), y};

        tmpY = y + size;
        tmpX = x;

        pos3 = (VECTOR3D) {x+size, TerrainGenerator::getHeight(x+size, y + size), y + size};

        tmpX = x - size;
        pos2 = (VECTOR3D) {x, TerrainGenerator::getHeight(x, y + size), y + size};

    }

    VECTOR3D v1 = pos3-pos1;
    VECTOR3D v2 = pos2-pos1;

    VECTOR3D normal = vec3DNormalize(vec3DCrossProduct(v1, v2));

    normal.x *= -1;

    //std::cout << "Normal Vector (" << normal.x << " " << normal.y << " " << normal.z << ")\n";
    VERTEX tmp;
    tmp.position = pos1;
    tmp.normal = normal;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos1.y > 1 ) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos1.y > 1.9 * lfHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    //tmp.tangent = (VECTOR3F) {SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001),SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001),SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001)};
    tmp.uv = (VECTOR2F) {(float)pos1.x, (float)pos1.z};
    verts.push_back(tmp);
    tmp.position = pos2;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos2.y > 1) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos2.y > 1.9 * lfHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    tmp.uv = (VECTOR2F) {(float)pos2.x, (float)pos2.z};
    verts.push_back(tmp);

    tmp.position = pos3;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos3.y > 1) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos3.y > 1.9 * lfHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    tmp.uv = (VECTOR2F) {(float)pos3.x, (float)pos3.z};
    verts.push_back(tmp);

    return verts;


}
#ifdef OLD_TRIANGLE_GEN
std::vector<VERTEX> generateTriangleOLD(double x, double  y, int rotation, double size) {

    std::vector<VERTEX> verts;

    VECTOR3D pos1;
    VECTOR3D pos2;
    VECTOR3D pos3;

    double tmpX, tmpY;

    if (!rotation) {

        pos1 = (VECTOR3D) {x, TerrainGenerator::getHeight(x, y), y};

        tmpX = x - HL3 * size;
        tmpY = y + size;

        pos2 = (VECTOR3D) {tmpX, TerrainGenerator::getHeight(tmpX, tmpY), tmpY};

        tmpX = x + HL3 * size;
        pos3 = (VECTOR3D) {tmpX, TerrainGenerator::getHeight(tmpX, tmpY), tmpY};

    }
    else {

        tmpX = x;
        tmpY = y + size;
        pos1 = (VECTOR3D) {x, TerrainGenerator::getHeight(x, tmpY), tmpY};

        tmpY = y;
        tmpX = x + HL3 * size;

        pos2 = (VECTOR3D) {tmpX, TerrainGenerator::getHeight(tmpX, tmpY), tmpY};

        tmpX = x - HL3 * size;
        pos3 = (VECTOR3D) {tmpX, TerrainGenerator::getHeight(tmpX, tmpY), tmpY};

    }

    VECTOR3D v1 = pos3-pos1;
    VECTOR3D v2 = pos2-pos1;

    VECTOR3D normal = vec3DNormalize(vec3DCrossProduct(v1, v2));

        //normal.y *= -1;
    normal.x *= -1;
        //normal.z *= -1;

    //std::cout << "Normal Vector (" << normal.x << " " << normal.y << " " << normal.z << ")\n";
    VERTEX tmp;
    tmp.position = pos1;
    tmp.normal = normal;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos1.y > 1 ) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos1.y > 0.9 * terrainGenHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    //tmp.tangent = (VECTOR3F) {SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001),SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001),SimplexNoise1234::noise(tmp.position.x*0.001, tmp.position.y*0.001, tmp.position.z*0.001)};
    tmp.uv = (VECTOR2F) {0,0};
    verts.push_back(tmp);
    tmp.position = pos2;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos2.y > 1) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos2.y > 0.9 * terrainGenHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    verts.push_back(tmp);

    tmp.position = pos3;
    tmp.tangent = (VECTOR3F) {0xb7/255.0, 0xac/255.0, 0x76/255.0};
    if (pos3.y > 1) tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0x22/255.0};
    if (pos3.y > 0.9 * terrainGenHeight) tmp.tangent = (VECTOR3F) {1,1,1};
    verts.push_back(tmp);

    return verts;

}

#endif

std::vector<VERTEX> generateWaterTriangle(double x, double  y, int rotation, double size) {

    std::vector<VERTEX> verts;

    VECTOR3D pos1;
    VECTOR3D pos2;
    VECTOR3D pos3;

    double tmpX, tmpY;

    if (!rotation) {

        pos1 = (VECTOR3D) {x, 0, y};

        tmpX = x - HL3 * size;
        tmpY = y + size;

        pos2 = (VECTOR3D) {tmpX, 0, tmpY};

        tmpX = x + HL3 * size;
        pos3 = (VECTOR3D) {tmpX, 0, tmpY};

    }
    else {

        tmpX = x;
        tmpY = y + size;
        pos1 = (VECTOR3D) {x, 0, tmpY};

        tmpY = y;
        tmpX = x + HL3 * size;

        pos2 = (VECTOR3D) {tmpX, 0, tmpY};

        tmpX = x - HL3 * size;
        pos3 = (VECTOR3D) {tmpX, 0, tmpY};

    }

    VECTOR3D v1 = pos3-pos1;
    VECTOR3D v2 = pos2-pos1;

    VECTOR3D normal = vec3DNormalize(vec3DCrossProduct(v1, v2));

        //normal.y *= -1;
    normal.x *= -1;
        //normal.z *= -1;

    //std::cout << "Normal Vector (" << normal.x << " " << normal.y << " " << normal.z << ")\n";
    VERTEX tmp;
    tmp.position = pos1;
    tmp.normal = normal;
    tmp.tangent = (VECTOR3F) {0x62/255.0, 0x77/255.0, 0xd2/255.0};
    tmp.uv = (VECTOR2F) {0,0};
    verts.push_back(tmp);
    tmp.position = pos2;
    verts.push_back(tmp);

    tmp.position = pos3;
    verts.push_back(tmp);

    return verts;

}

Model * TerrainGenerator::generateTerrainTile(double x, double y, double size, int resolution) {

    int i, j;
    std::vector<VERTEX> verts;// = generateTriangle(x,y,1, 20.0);

    std::vector<Math::Vector3> positions;

    //int offset = ((int)(x / size)) % 2;
    double triangleSize = size / resolution;

    /// OLD Loop i < resolution * 1 / HL3

    for (i = 0; i < resolution; ++i) {

        for (j = 0; j < resolution; ++j) {

            /// OLD X COORD x + triangleSize * i * HL3 + HL3, y + triangleSize*j

            std::vector<VERTEX> tmpData = generateTriangle(x + triangleSize *i, y + triangleSize * j, 0, triangleSize);
            for (int k = 0; k < tmpData.size(); ++k) {

                verts.push_back(tmpData[k]);
                positions.push_back(Math::Vector3(tmpData[k].position.x, tmpData[k].position.y, tmpData[k].position.z));

            }

            tmpData = generateTriangle(x + triangleSize *i, y + triangleSize * j, 1, triangleSize);
            for (int k = 0; k < tmpData.size(); ++k) {

                verts.push_back(tmpData[k]);
                positions.push_back(Math::Vector3(tmpData[k].position.x, tmpData[k].position.y, tmpData[k].position.z));

            }

        }

    }

    VERTEX * rawVerts = (VERTEX*) malloc(verts.size()*sizeof(VERTEX));
    int * indecies = (int*) malloc(sizeof(i) * verts.size());

    for (i = 0; i < verts.size(); ++i) {

        rawVerts[i] = verts[i];
        indecies[i] = i;

    }

    std::vector<float> data((float*)rawVerts, (float*) rawVerts+verts.size() * sizeof(VERTEX) / sizeof(float));
    std::vector<int> indexData(indecies, indecies + verts.size());

    return createModel(data, verts.size(), indexData, new Math::Mesh(positions, indexData));

}


/*Model * TerrainGenerator::generateTerrainTile(double x, double y, double size, int resolution) {

    int i, j;
    std::vector<VERTEX> verts;// = generateTriangle(x,y,1, 20.0);

    std::vector<Math::Vector3> positions;

    //int offset = ((int)(x / size)) % 2;
    double triangleSize = size / resolution;

    /// OLD Loop i < resolution * 1 / HL3

    for (i = 0; i < resolution; ++i) {

        for (j = 0; j < resolution; ++j) {

            /// OLD X COORD x + triangleSize * i * HL3 + HL3, y + triangleSize*j

            std::vector<VERTEX> tmpData = generateTriangle(x + triangleSize *i, y + triangleSize * j, (i+j)%2, triangleSize);
            for (int k = 0; k < tmpData.size(); ++k) {

                verts.push_back(tmpData[k]);
                positions.push_back(Math::Vector3(tmpData[k].position.x, tmpData[k].position.y, tmpData[k].position.z));

            }

        }

    }

    VERTEX * rawVerts = (VERTEX*) malloc(verts.size()*sizeof(VERTEX));
    int * indecies = (int*) malloc(sizeof(i) * verts.size());

    for (i = 0; i < verts.size(); ++i) {

        rawVerts[i] = verts[i];
        indecies[i] = i;

    }

    std::vector<float> data((float*)rawVerts, (float*) rawVerts+verts.size() * sizeof(VERTEX) / sizeof(float));
    std::vector<int> indexData(indecies, indecies + verts.size());

    return createModel(data, verts.size(), indexData, new Math::Mesh(positions, indexData));

}*/

Model * TerrainGenerator::generateWaterTile(double x, double y, double size, int resolution) {

    int i, j;
    std::vector<VERTEX> verts;// = generateTriangle(x,y,1, 20.0);

    std::vector<Math::Vector3> positions;

    //int offset = ((int)(x / size)) % 2;

    double triangleSize = size / resolution;

    for (i = 0; i < resolution * 1 / HL3; ++i) {

        for (j = 0; j < resolution; ++j) {

            std::vector<VERTEX> tmpData = generateWaterTriangle(x + triangleSize * i * HL3, y + triangleSize*j, (i+j)%2, triangleSize);
            for (int k = 0; k < tmpData.size(); ++k) {

                verts.push_back(tmpData[k]);
                positions.push_back(Math::Vector3(tmpData[k].position.x, tmpData[k].position.y, tmpData[k].position.z));

            }

        }

    }

    VERTEX * rawVerts = (VERTEX*) malloc(verts.size()*sizeof(VERTEX));
    int * indecies = (int*) malloc(sizeof(i) * verts.size());

    for (i = 0; i < verts.size(); ++i) {

        rawVerts[i] = verts[i];
        indecies[i] = i;

    }

    std::vector<float> data((float*)rawVerts, (float*) rawVerts+verts.size() * sizeof(VERTEX) / sizeof(float));
    std::vector<int> indexData(indecies, indecies + verts.size());

    return createModel(data, verts.size(), indexData, new Math::Mesh(positions, indexData));

}
