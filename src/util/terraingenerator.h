#ifndef TERRAINGENERATOR_H
#define TERRAINGENERATOR_H

#include "render/model.h"

typedef struct vertex_t VERTEX;

class TerrainGenerator
{
    public:

        static void initValues(double lfh, double hfh, double lfs, double hfs, double lfx, double lfy, double hfx, double hfy, double wy);

        static float getHeight(double x, double y);
        static Model * generateTerrainTile(double x, double y, double size, int resolution);
        static Model * generateWaterTile(double x, double y, double size, int resolution);

    protected:

    private:

        static std::vector<VERTEX> generateTriangle(double x, double y, int rotation, double size);

        static double lfHeight;
        static double hfHeight;

        static double lfScale;
        static double hfScale;

        static double lfxOffset;
        static double lfyOffset;

        static double hfxOffset;
        static double hfyOffset;

        static double worldYOffset;

};

#endif // TERRAINGENERATOR_H
