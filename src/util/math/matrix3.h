#ifndef MATRIX3_H
#define MATRIX3_H

#include <vector>
#include "vector3.h"

namespace Math {

class Matrix3
{
    public:
        Matrix3();
        Matrix3(std::vector<double> values);
        Matrix3(double * values);
        virtual ~Matrix3();

        Vector3 operator[] (int i);

        Matrix3 operator*(Matrix3 mat);
        Vector3 operator*(Vector3 vec);

        void setValue(int line, int column, double value);
        double getValue(int line, int column);

    protected:

        double values[9];

    private:
};

}

std::ostream& operator<< (std::ostream& stream, Math::Matrix3 matrix);

#endif // MATRIX3_H
