#include "vector3.h"
#include <math.h>
using namespace Math;

Vector3::Vector3() {

    this->x = 0;
    this->y = 0;
    this->z = 0;

}

Vector3::Vector3(double x, double y, double z) {

    this->x = x;
    this->y = y;
    this->z = z;

}

Vector3::~Vector3() {



}

double Vector3::length() {

    return sqrt(x*x + y*y + z*z);

}

double * Vector3::operator[](int i) {
    switch (i) {

        case 0:
            return &x;
        case 1:
            return &y;
        case 2:
            return &z;
        default:
            throw std::runtime_error("No a memeber of Vector3");
            return 0;

    }
}

double Vector3::operator* (Vector3 vec) {
    return this->x * vec.x + this->y * vec.y + this->z * vec.z;
}

Vector3 Vector3::operator*(double val) {
    return Vector3(x * val, y * val, z * val);
}

Vector3 Vector3::operator/(double val) {
    return Vector3(x / val, y / val, z / val);
}

Vector3 Vector3::operator/(Vector3 vec) {
    return Vector3(x / vec.x, y / vec.y, z / vec.z);
}

Vector3 Vector3::operator-(Vector3 vec) {
    return Vector3(x-vec.x, y-vec.y, z-vec.z);
}

Vector3 Vector3::operator+(Vector3 vec) {
    return Vector3(x+vec.x, y+vec.y, z+vec.z);
}



Vector3 Vector3::operator+= (Vector3 vec) {
    x += vec.x;
    y += vec.y;
    z += vec.z;
    return Vector3(x, y, z);
}

Vector3 Vector3::cross(Vector3 vec1, Vector3 vec2) {

return Vector3(vec1.y * vec2.z - vec1.z * vec2.y, -vec1.z * vec2.x + vec1.x * vec2.z, vec1.x * vec2.y - vec1.y * vec2.x);

}

std::ostream& operator<<(std::ostream& stream, Vector3 vec) {

    stream << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";

    return stream;

}

Vector3 Vector3::normalize() {
    double l = this->length();
    this->x /= l;
    this->y /= l;
    this->z /= l;
    return Vector3(x,y,z);
}
