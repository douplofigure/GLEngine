#ifndef HELPERS_H_INCLUDED
#define HELPERS_H_INCLUDED

inline double sign(double x) {
    if (x < -DOUBLE_0) return -1;
    if (x > DOUBLE_0) return 1;
    return 0;
}

#endif // HELPERS_H_INCLUDED
