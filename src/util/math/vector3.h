#ifndef VECTOR3_H
#define VECTOR3_H

#include <iostream>

#include "vecmemoffset.h"

namespace Math {

class Vector3
{
    public:

        double x;
        double y;
        double z;

        Vector3();
        Vector3(double x, double y, double z);
        virtual ~Vector3();


        double length();

        double operator* (Vector3 vec);
        double * operator[] (int i);
        Vector3 operator*(double val);
        Vector3 operator/(double val);
        Vector3 operator/(Vector3 vec);
        Vector3 operator+(Vector3 vec);
        Vector3 operator-(Vector3 vec);
        Vector3 operator+= (Vector3 vec);

        Vector3 normalize();

        static Vector3 cross(Vector3 v1, Vector3 v2);

};

inline Vector3 operator*(double d, Vector3 vec) {
    return vec * d;
}

}
std::ostream& operator<< (std::ostream& stream, Math::Vector3 vec);

#endif // VECTOR3_H
