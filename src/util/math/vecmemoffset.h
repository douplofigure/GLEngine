#ifndef VECMEMOFFSET_H_INCLUDED
#define VECMEMOFFSET_H_INCLUDED


template <typename T> struct vec_elem_offset_t {

    T x;
    T y;
    T z;
    T w;

};


#endif // VECMEMOFFSET_H_INCLUDED
