#ifndef CONVERSIONS_H_INCLUDED
#define CONVERSIONS_H_INCLUDED

#include <glm/vec3.hpp>
#include "vector3.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846  /* pi */
#endif // M_PI

namespace Math {

    inline glm::vec3 asRender(Vector3 vec) {
        return glm::vec3(vec.x, vec.z, -vec.y);
    }

    inline Vector3 fromRender(glm::vec3 vec) {
        return Vector3(vec.x, -vec.z, vec.y);
    }

    inline double degrees(double d) {
        return d;// * 180.0 / M_PI;
    }

    inline double radians(double d) {
        return d / 180.0 * M_PI;
    }

}

#endif // CONVERSIONS_H_INCLUDED
