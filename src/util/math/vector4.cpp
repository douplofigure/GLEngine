#include "vector4.h"
#include <math.h>
using namespace Math;

Vector4::Vector4() {

    this->x = 0;
    this->y = 0;
    this->z = 0;
    this->w = 0;

}

Vector4::Vector4(double x, double y, double z, double w) {

    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;

}

Vector4::Vector4(Vector3 vec, double w) {

    this->x = vec.x;
    this->y = vec.y;
    this->z = vec.z;
    this->w = w;

}

Vector4::~Vector4() {



}

double Vector4::length() {

    return sqrt(x*x + y*y + z*z + w*w);

}

double * Vector4::operator[](int i) {
    switch (i) {

        case 0:
            return &x;
        case 1:
            return &y;
        case 2:
            return &z;
        case 3:
            return &w;
        default:
            throw std::runtime_error("No a memeber of Vector3");
            return 0;

    }
}

double Vector4::operator* (Vector4 vec) {
    return this->x * vec.x + this->y * vec.y + this->z * vec.z + this->w * vec.w;
}

Vector4 Vector4::operator*(double val) {
    return Vector4(x * val, y * val, z * val, w * val);
}

Vector4 Vector4::operator/(double val) {
    return Vector4(x / val, y / val, z / val, w/val);
}

Vector4 Vector4::operator-(Vector4 vec) {
    return Vector4(x-vec.x, y-vec.y, z-vec.z, w-vec.w);
}

Vector4 Vector4::operator+(Vector4 vec) {
    return Vector4(x+vec.x, y+vec.y, z+vec.z, w+vec.w);
}

Vector4 Vector4::operator+= (Vector4 vec) {
    x += vec.x;
    y += vec.y;
    z += vec.z;
    w += vec.w;
    return Vector4(x, y, z, w);
}

