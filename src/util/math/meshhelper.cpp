#include "meshhelper.h"

#include "constants.h"

using namespace Math;

std::vector<Vector3> MeshHelper::computeTangents(std::vector<Vector3> positions, std::vector<Vector3> normals, std::vector<Vector3> uvs, std::vector<int> indices) {

    std::vector<Vector3> tangents(positions.size());
    tangents.reserve(positions.size());
    std::vector<bool> hasValue;
    hasValue.reserve(positions.size());

    std::cout << "Computing " << positions.size() << " tangents" << std::endl;

    for (int i = 0; i < positions.size(); ++i) {
        tangents[i] = Vector3(0,0,0);
        hasValue[i] = 0;
    }

    for (int i = 0; i < indices.size()/3; ++i) {

        int i1 = indices[i*3];
        int i2 = indices[i*3+1];
        int i3 = indices[i*3+2];

        double du1 = uvs[i2].x - uvs[i1].x;
        double dv1 = uvs[i2].y - uvs[i1].y;

        double du2 = uvs[i3].x - uvs[i1].x;
        double dv2 = uvs[i3].y - uvs[i1].y;

        double det = du1 * dv2 - dv1 * du2;

        double detA = dv2;
        double detB = -dv1;

        Vector3 tan = (detA/det) * (positions[i2]-positions[i1]) + (detB/det) * (positions[i3]-positions[i1]);

        tangents[i1] = tan / tan.length();
        tangents[i2] = tan / tan.length();
        tangents[i3] = tan / tan.length();

        //std::cout << "Solution " << detA / det << " " << detB / det << " => " << tan << std::endl;
        //std::cout << "Basis (" << tangents[i1] << ", " << -1.0 * Vector3::cross(tangents[i1], normals[i1]) << ", " << normals[i1] << ")" <<std::endl;

    }

    return tangents;


}
