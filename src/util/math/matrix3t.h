#ifndef MATRIX3T_H
#define MATRIX3T_H

namespace Math {

template <typename T> class Matrix3T
{
    public:
        Matrix3T() {

        }
        virtual ~Matrix3T() {

        }

        Matrix3T(std::vector<T> values) {

            for (int i = 0; i < 9 && i < values.size(); ++i) {

                this->values[i] = values[i];

            }

        }


        Matrix3T(T * values) {

            for (int i = 0; i < 9; ++i) {

                this->values[i] = values[i];

            }

        }

        //Vector3 operator[] (int i);

        Matrix3T<T> operator*(Matrix3T<T> mat) {

            Matrix3T<T> result;

            double tmp;

            for (int i = 0; i < 3; ++i) {

                for (int j = 0; j < 3; ++j) {

                    tmp = 0;

                    for (int k = 0; k < 3; ++k) {
                        tmp += this->getValue(i, k) * mat.getValue(k,j);
                    }

                    result.setValue(i, j, tmp);

                }

            }

            return result;

        }

        void setValue(int line, int column, T value) {
            this->values[line*3 + column] = value;
        }
        double getValue(int line, int column) {
            return this->values[line*3 + column];
        }

    protected:

    private:

        T values[9];

};

}

#endif // MATRIX3T_H
