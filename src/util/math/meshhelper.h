#ifndef MESHHELPER_H
#define MESHHELPER_H

#include <vector>

#include "vector3.h"
namespace Math {

class MeshHelper
{
    public:

        static std::vector<Vector3> computeTangents(std::vector<Vector3> positions, std::vector<Vector3> normals, std::vector<Vector3> uvs, std::vector<int> indices);

};

}

#endif // MESHHELPER_H
