#include "mesh.h"

#include "constants.h"
#include "helpers.h"

#include "util/smartarray.h"

#include <math.h>
#include <assert.h>

#include <list>

#include "meshhelper.h"

using namespace Math;
using namespace data;

int Mesh::activeMeshes = 0;

struct tri_face_t {
    int i1;
    int i2;
    int i3;
};

typedef struct list_face_t {

    int i1;
    int i2;
    int i3;
    list_face_t * next;

} LIST_FACE;

struct mesh_tree_node_t {

    bool hasChildren;
    int childCount;
    int faceCount;
    LIST_FACE * firstFace;
    mesh_tree_node_t ** children;
    double x;
    double y;
    double z;

};

double pow(double x, int n) {

    if (!n) return 1;
    return x * pow(x, n-1);

}

std::vector<int> getIndex(Vector3 center, Vector3 pos) {

    std::vector<int> v = std::vector<int>(1);
    //std::cout << "Index of " << pos << " in " << center << std::endl;
    v[0] = 0;
    if (pos.x > center.x) {
        v[0] += 1;
    }
    if (pos.z > center.z) {
        v[0] += 2;
    }
    return v;

}

Vector3 generateSubIndices(Vector3 center, int i, int depth) {

    switch (i) {
        case 0:
            return center + Vector3(-1.0 / (pow(2, depth)),0.0 ,-1.0 / (pow(2,depth)));
        case 1:
            return center + Vector3(1.0 / (pow(2, depth)),0.0 ,-1.0 / (pow(2,depth)));
        case 2:
            return center + Vector3(-1.0 / (pow(2, depth)),0.0 ,1.0 / (pow(2,depth)));
        case 3:
            return center + Vector3(1.0 / (pow(2, depth)), 0.0 , 1.0 / (pow(2,depth)));

    }

}

Mesh::Mesh() {

    this->fastData = new octree<Vector3, TRI_FACE*>(4, Vector3(0,0,0), getIndex, generateSubIndices);
    maxRadius = 1.0;
    treeHeight = 4;
    this->tree = (MESH_NODE *) malloc(sizeof(MESH_NODE));
    this->tree->hasChildren = false;
    this->tree->firstFace = NULL;
    /// Very important
    this->tree->x = 0;
    this->tree->y = 0;
    this->tree->z = 0;

    this->hasTree = false;
    hasTangents = false;

    activeMeshes++;

}

Mesh::Mesh(std::vector<Vector3> positions, std::vector<int> indices) : Mesh() {

    this->hasTree = false;

    this->indices = std::vector<int>(indices.size());
    for (int i = 0; i < indices.size(); ++i) {

        this->indices[i] = indices[i];

    }

    this->positions = std::vector<Vector3>(positions.size());
    for (int i = 0; i < positions.size(); ++i) {

        this->positions[i] = positions[i];

    }

    this->vertexAmount = positions.size();
    this->indexAmount = indices.size();
    hasTangents = false;
    //normalize();

}

Mesh::Mesh(std::vector<float> rawModelData, int vertexCount, std::vector<int> indices) : Mesh() {

    long comparisonCount = 0;

    this->hasTree = false;

    this->indices = std::vector<int>(indices.size());
    for (int i = 0; i < indices.size(); ++i) {

        this->indices[i] = indices[i];

    }

    std::vector<Vector3> tmpVecs = std::vector<Vector3>(0);
    std::vector<Vector3> tmpNorm = std::vector<Vector3>(0);
    std::vector<Vector3> tmpUv = std::vector<Vector3>(0);
    std::vector<Vector3> tmpTang = std::vector<Vector3>(0);
    bool append;

    for (int i = 0; i < rawModelData.size(); i += rawModelData.size() / vertexCount) {
        Vector3 tmp = Vector3(rawModelData[i], rawModelData[i+1], rawModelData[i+2]);
        Vector3 norm = Vector3(rawModelData[i+3], rawModelData[i+4], rawModelData[i+5]);
        Vector3 uv = Vector3(rawModelData[i+6], rawModelData[i+7], 0);
        Vector3 tangent = Vector3(rawModelData[i+8], rawModelData[i+9], rawModelData[i+10]);
        append = true;

        if (append) {
            tmpVecs.push_back(tmp);
            tmpNorm.push_back(norm);
            tmpUv.push_back(uv);
            tmpTang.push_back(tangent);

        }

    }

    this->positions = std::vector<Vector3>(tmpVecs.size());
    this->normals = std::vector<Vector3>(tmpVecs.size());
    this->uvs = std::vector<Vector3>(tmpVecs.size());
    this->tangents = std::vector<Vector3>(tmpVecs.size());
    for (int i = 0; i < tmpVecs.size(); ++i) {

        this->positions[i] = tmpVecs[i];
        this->normals[i] = tmpNorm[i];
        this->uvs[i] = tmpUv[i];
        this->tangents[i] = tmpTang[i];

    }

    //std::cout << "Done " << comparisonCount << " comparisons" << std::endl;

    this->vertexAmount = tmpVecs.size();
    this->indexAmount = indices.size();

    hasTangents = false;

}

Mesh::Mesh(std::vector<float> positionData, std::vector<float> normalData, std::vector<float> uvData, int vertexCount, std::vector<int> indices) : Mesh() {

    long comparisonCount = 0;

    this->hasTree = false;

    this->indices = std::vector<int>(indices.size());
    for (int i = 0; i < indices.size(); ++i) {

        this->indices[i] = indices[i];

    }

    std::vector<Vector3> tmpVecs = std::vector<Vector3>(0);
    std::vector<Vector3> tmpNorm = std::vector<Vector3>(0);
    std::vector<Vector3> tmpUv = std::vector<Vector3>(0);
    std::vector<Vector3> tmpTang = std::vector<Vector3>(0);
    bool append;

    for (int i = 0; i < positionData.size()/3; ++i) {
        Vector3 tmp = Vector3(positionData[i*3], positionData[i*3+1], positionData[i*3+2]);
        Vector3 norm = Vector3(normalData[i*3], normalData[i*3+1], normalData[i*3+2]);
        Vector3 uv = Vector3(uvData[i*2], uvData[i*2+1], 0);
        Vector3 tangent = Vector3(0, 0, 0);
        append = true;

        if (append) {
            tmpVecs.push_back(tmp);
            tmpNorm.push_back(norm);
            tmpUv.push_back(uv);
            tmpTang.push_back(tangent);

        }

    }

    this->positions = std::vector<Vector3>(tmpVecs.size());
    this->normals = std::vector<Vector3>(tmpVecs.size());
    this->uvs = std::vector<Vector3>(tmpVecs.size());
    this->tangents = std::vector<Vector3>(tmpVecs.size());
    for (int i = 0; i < tmpVecs.size(); ++i) {

        this->positions[i] = tmpVecs[i];
        this->normals[i] = tmpNorm[i];
        this->uvs[i] = tmpUv[i];
        this->tangents[i] = tmpTang[i];

    }

    //std::cout << "Done " << comparisonCount << " comparisons" << std::endl;

    this->vertexAmount = tmpVecs.size();
    this->indexAmount = indices.size();

    hasTangents = false;

}

template <typename T> std::vector<T> copyVector(std::vector<T> vec) {

    std::vector<T> res = std::vector<T>();
    res.reserve(vec.size());
    res.insert(res.end(), vec.begin(), vec.end());
    return res;

}

Mesh::Mesh(Mesh * mesh) {

    this->positions = copyVector<Vector3>(mesh->positions);
    this->normals = copyVector<Vector3>(mesh->normals);
    this->uvs = copyVector<Vector3>(mesh->uvs);
    this->tangents = copyVector<Vector3>(mesh->tangents);

    this->indices = copyVector<int>(mesh->indices);

    this->fastData = nullptr;
    this->allFaces = NULL;
    this->tree = NULL;

    activeMeshes++;

}

int Mesh::getVertexCount() {
    return positions.size();
}

Vector3 getMinCorner(int depth, Vector3 pos) {

    Vector3 m = pos - Vector3(1.0 / (pow(2, depth)), 0, 1.0 / (pow(2, depth)));
    return m;

}


Vector3 getMaxCorner(int depth, Vector3 pos) {

    Vector3 m = pos + Vector3(1.0 / (pow(2, depth)), 0, 1.0 / (pow(2, depth)));
    return m;

}

Vector3 Mesh::getFaceCenter(TRI_FACE f) {

    Vector3 v = (positions[f.i1] + positions[f.i2] + positions[f.i3]) / 3;
    v.y = 0;
    return v;

}

LIST_FACE * appendFace(LIST_FACE * ls, TRI_FACE face) {

    LIST_FACE * nFace = (LIST_FACE *) malloc(sizeof(LIST_FACE));
    nFace->i1 = face.i1;
    nFace->i2 = face.i2;
    nFace->i3 = face.i3;
    nFace->next = ls;

    return nFace;

}

bool pointInTriangle2D(Vector3 pt, Vector3 v1, Vector3 v2, Vector3 v3) {
    bool b1, b2, b3;

    b1 = Vector3(pt.x-v1.x, 0, pt.z - v1.z) * Vector3(v1.z-v2.z, 0, v2.x-v1.x) > 0.0;
    b2 = Vector3(pt.x-v2.x, 0, pt.z - v2.z) * Vector3(v2.z-v3.z, 0, v3.x-v2.x) > 0.0;
    b3 = Vector3(pt.x-v3.x, 0, pt.z - v3.z) * Vector3(v3.z-v1.z, 0, v1.x-v3.x) > 0.0;

    return ((b1 == b2) && (b2 == b3));
}

double signedSum(Vector3 v1, Vector3 v2, Vector3 v3) {
    return (v1.x - v3.x) * (v2.z - v3.z) + (v2.x - v3.z) * (v1.z - v3.z);
}

std::vector<Vector3> getNodeBounds(MESH_NODE * node, int depth) {

    Vector3 maxPos = getMaxCorner(depth, Vector3(node->x, 0, node->z));
    Vector3 minPos = getMinCorner(depth, Vector3(node->x, 0, node->z));

    std::vector<Vector3> quad = std::vector<Vector3>(4);

    quad[0] = minPos;
    quad[1] = Vector3(maxPos.x, 0, minPos.z);
    quad[2] = maxPos;
    quad[3] = Vector3(minPos.x, 0, maxPos.z);

    return quad;

}

Vector3 getIntersectionPoint(Vector3 v1, Vector3 v2, Vector3 u1, Vector3 u2) {

    Vector3 a = Vector3(v1.x, v1.z, 1);
    Vector3 b = Vector3(v2.x, v2.z, 1);
    Vector3 c = Vector3(u1.x, u1.z, 1);
    Vector3 d = Vector3(u2.x, u2.z, 1);

    Vector3 r1 = Vector3::cross(a, b);
    Vector3 r2 = Vector3::cross(c, d);

    Vector3 result = Vector3::cross(r1, r2);

    if (!IS_ZERO(result.z))
        result = result / result.z;

    return result;

}

bool intersect(std::vector<Vector3> tri, std::vector<Vector3> quad) {

    for (int i = 0; i < tri.size(); ++i) tri[i].y = 0;

    for (int i = 0; i < 3; ++i) {
        if (tri[i].x > quad[0].x && tri[i].x < quad[2].x && tri[i].z > quad[0].z && tri[i].z < quad[2].z) {

            return true;

        }
    }

    for (int i = 0; i < 4; ++i) {
        if (pointInTriangle2D(quad[i], tri[0], tri[1], tri[2])) {

            return true;

        }
    }

    for (int i = 0; i < 3; ++i) {

        for (int j = 0; j < 4; ++j) {

            Vector3 p = getIntersectionPoint(tri[i], tri[(i+1)%3], quad[j], quad[(j+1)%4]);
            if (IS_ZERO(p.z)) continue;
            Vector3 tmp = (quad[(j+1)%4] - quad[j]); // Vector3(0,0,0) = quad[j]
            Vector3 v = Vector3(tmp.x, tmp.z, 0);
            p.z = 0;
            tmp = Vector3(quad[j].x, quad[j].z, 0);
            Vector3 u = (p - tmp);
            //Vector3 u = Vector3(tmp.x, tmp.z, 0);

            double t1 = u * v / (v.length() * v.length());

            tmp = (tri[(i+1)%3] - tri[i]);
            v = Vector3(tmp.x, tmp.z, 0);
            tmp = Vector3(tri[i].x, tri[i].z, 0);
            u = p - tmp;
            double t2 = u * v / (v.length() * v.length());

            //std::cout << "t = " << t << " p " << v << " j " << j << std::endl;
            if (t1 >= -0.1 && t1 <= 1.1 && t2 >= -0.1 && t2 <= 1.1) {
                //std::cout << " Intersection " << tri[i] << " " << tri[(i+1)%3] << " " << quad[j] << " " << quad[(j+1)%4] << " " << p <<std::endl;
                return true;
            }

        }

    }

    return false;

}

Mesh * Mesh::withTransform(Mesh * mesh, Matrix4 mat) {
    Mesh * res = new Mesh(mesh);
    res->applyTransform(mat);
    return res;

}

void Mesh::applyTransform(Matrix4 mat) {

    for (int i = 0; i < positions.size(); ++i) {

        Vector4 pos = Vector4(positions[i], 1.0);
        Vector4 norm = Vector4(normals[i], 0.0);
        Vector4 tang = Vector4(tangents[i], 0.0);

        pos = mat * pos;
        norm = mat * norm;
        tang = mat * tang;

        positions[i] = Vector3(pos.x, pos.y, pos.z) / pos.w;
        normals[i] = Vector3(norm.x, norm.y, norm.z);
        tangents[i] = Vector3(tang.x, tang.y, tang.z);

    }

}

std::vector<float> Mesh::getModelData(int * vertexCount) {

    *vertexCount = positions.size();

    std::cout << "mesh has " << positions.size() << " verts" << std::endl;
    std::cout << "mesh has " << normals.size() << " normals" << std::endl;
    std::cout << "mesh has " << uvs.size() << " uvs" << std::endl;
    std::cout << "mesh has " << tangents.size() << " tangents" << std::endl;

    std::vector<float> data(positions.size()*11);

    int index = 0;

    for (int i = 0; i < positions.size(); ++i) {

        data[index++] = positions[i].x;
        data[index++] = positions[i].y;
        data[index++] = positions[i].z;

        data[index++] = normals[i].x;
        data[index++] = normals[i].y;
        data[index++] = normals[i].z;


        data[index++] = uvs[i].x;
        data[index++] = uvs[i].y;

        data[index++] = tangents[i].x;
        data[index++] = tangents[i].y;
        data[index++] = tangents[i].z;

    }

    return data;

}

std::vector<int> Mesh::getIndexData() {

    return indices;

}

template <typename T> std::vector<T> concatVectors(std::vector<T> v1, std::vector<T> v2) {

    std::vector<T> newPos = std::vector<T>();
    newPos.reserve(v1.size() + v2.size());
    newPos.insert(newPos.end(), v1.begin(), v1.end());
    newPos.insert(newPos.end(), v2.begin(), v2.end());

    return newPos;

}

void Mesh::mergeWith(Mesh * mesh) {

    assert(mesh);

    int oldSize = positions.size();
    int oldIndexCount = indices.size();

    std::vector<Vector3> vec = concatVectors<Vector3>(positions, mesh->positions);
    this->positions = vec;
    this->normals = concatVectors<Vector3>(normals, mesh->normals);
    this->uvs = concatVectors<Vector3>(uvs, mesh->uvs);
    this->tangents = concatVectors<Vector3>(tangents, mesh->tangents);

    this->indices = concatVectors<int>(indices, mesh->indices);

    for (int i = oldIndexCount; i < indices.size(); ++i) {

        indices[i] += oldSize;

    }

}

void Mesh::addFace(MESH_NODE * node, TRI_FACE face, int depth, bool checkBounds) {

    assert(node);
    if (depth > this->treeHeight) {
        if (!node->firstFace) {

            LIST_FACE * nFace = (LIST_FACE *) malloc(sizeof(LIST_FACE));
            nFace->i1 = face.i1;
            nFace->i2 = face.i2;
            nFace->i3 = face.i3;
            nFace->next = NULL;
            node->firstFace = nFace;
            node->hasChildren = false;
            node->faceCount = 0;
            return;

        }
        node->faceCount++;
        assert(node->firstFace);
        node->firstFace = appendFace(node->firstFace, face);
        //std::cout << "Node has " << node->faceCount << " faces" << std::endl;
        return;
    }

    Vector3 position = Vector3(node->x, node->y, node->z);

    if (!node->hasChildren) {
        node->children = (MESH_NODE **) malloc(sizeof(MESH_NODE*) * 4);
        node->childCount = 4;
        node->hasChildren = true;
        for (int i = 0; i < 4; ++i) {
            node->children[i] = (MESH_NODE*) malloc(sizeof(MESH_NODE));
            assert(node->children[i]);
            node->firstFace = NULL;
            Vector3 v = generateSubIndices(position, i, depth+1);
            node->children[i]->x = v.x;
            node->children[i]->y = v.y;
            node->children[i]->z = v.z;
            node->children[i]->faceCount = 0;
            node->children[i]->hasChildren = false;
        }
    }
    //int index = getIndex(position, getFaceCenter(face))[0];
    assert(node->children);
//    assert(node->children[index]);
    //addFace(node->children[index], face, depth+1, checkBounds);
    std::vector<Vector3> tri = std::vector<Vector3>(3);
    tri[0] = positions[face.i1];
    tri[1] = positions[face.i2];
    tri[2] = positions[face.i3];

    for (int i = 0; i < 4; ++i) {

        std::vector<Vector3> quad = getNodeBounds(node->children[i], depth+1);
        //std::cout << "quad[0] = " << quad[0] << " quad[2] = " << quad[2] << " pos = " << getFaceCenter(face);
        if (intersect(tri, quad)){
            //std::cout << " OK" << std::endl;
            addFace(node->children[i], face, depth+1, false);
        }
            //std::cout << std::endl;
    }

}

void printSubFaces(FILE * file, MESH_NODE * node) {

    if (node->hasChildren) {

        for (int i = 0; i < 4 ; ++i) {
            printSubFaces(file, node->children[i]);
        }

    }
    else {
        fprintf(file, "%.6g\t%.6g\t%d\n", node->x, node->z, node->faceCount);
    }

}

void Mesh::printFaceCounts(FILE * file) {

    fprintf(file, "X\tY\tFaces\n");

    fprintf(file, "%.6g\t%.6g\t", this->tree->x, this->tree->z);
    fprintf(file, "%d\n", ((int)this->indices.size())/3);

    printSubFaces(file, this->tree);

}

void Mesh::buildTree() {

    if (hasTree) return;

    allFaces = (TRI_FACE *) malloc(sizeof(TRI_FACE) * indexAmount/3);

    for (int i = 0; i < indexAmount/3; ++i) {
        Vector3 v1 = this->positions[this->indices[i*3]];
        Vector3 v2 = this->positions[this->indices[i*3+1]];
        Vector3 v3 = this->positions[this->indices[i*3+2]];
        TRI_FACE * face = (allFaces + i);
        face->i1 = this->indices[i*3];
        face->i2 = this->indices[i*3+1];
        face->i3 = this->indices[i*3+2];
        Vector3 vec = (v1+v2+v3)/3;

        //this->fastData->add(Vector3(vec.x, 0, vec.z), face);
        this->addFace(tree, *face, 0, true);
    }

    hasTree = true;

}

std::vector<TRI_FACE> Mesh::getAllFaces(MESH_NODE * node, Vector3 pos) {

    if (!node->hasChildren) {
        std::vector<TRI_FACE> data = std::vector<TRI_FACE>(node->faceCount);
        LIST_FACE * ls = node->firstFace;
        for (int i = 0; i < node->faceCount; ++i) {
            data[i] = (TRI_FACE){ls->i1, ls->i2, ls->i3};
            ls = ls->next;
        }
        return data;
    }

    Vector3 center = Vector3(node->x, node->y, node->z);

    return getAllFaces(node->children[getIndex(center, pos)[0]], pos);

}

std::vector<double> Mesh::getFace(double x, double y) {

    Vector3 pos = Vector3(x,0,y);
    std::vector<TRI_FACE> faces = getAllFaces(this->tree, pos);

    for (int i = 0; i < faces.size(); ++i) {
        if (pointInTriangle2D(Vector3(x, 0, y), positions[faces[i].i1] / maxRadius, positions[faces[i].i2]  / maxRadius, positions[faces[i].i3] / maxRadius)) {

            std::vector<double> fvec = std::vector<double>(9);
            fvec[0] = positions[faces[i].i1].x;
            fvec[1] = positions[faces[i].i1].y;
            fvec[2] = positions[faces[i].i1].z;

            fvec[3] = positions[faces[i].i2].x;
            fvec[4] = positions[faces[i].i2].y;
            fvec[5] = positions[faces[i].i2].z;

            fvec[6] = positions[faces[i].i3].x;
            fvec[7] = positions[faces[i].i3].y;
            fvec[8] = positions[faces[i].i3].z;

            return fvec;

        }
    }

    return std::vector<double>();

}

std::vector<TRI_FACE> Mesh::getAllFaces(double x, double y) {

    Vector3 pos = Vector3(x, 0, y);

    return getAllFaces(tree, pos);

}

double Mesh::getHeight(double x, double y) {

    std::vector<TRI_FACE> faces = getAllFaces(x, y);// = fastData->getAll(Vector3(x, 0, y));
    for (int i = 0; i < faces.size(); ++i) {
        if (pointInTriangle2D(Vector3(x, 0, y), positions[faces[i].i1] / maxRadius, positions[faces[i].i2]  / maxRadius, positions[faces[i].i3] / maxRadius)) {

            /// Fetching coords
            double xA = positions[faces[i].i1].x;
            double yA = positions[faces[i].i1].z;

            double xB = positions[faces[i].i2].x;
            double yB = positions[faces[i].i2].z;

            double xC = positions[faces[i].i3].x;
            double yC = positions[faces[i].i3].z;

            double xP = maxRadius * x;
            double yP = maxRadius * y;

            /// alpha = 1 / det M
            double alpha = 1.0 / ((xB-xA)*(yC-yA) - (xC-xA)*(yB-yA));

            /// computing M^(-1) AP
            double x = alpha * ( (yC - yA) * (xP - xA) + (xA - xC) * (yP - yA) );
            double y = alpha * ( (yA - yB) * (xP - xA) + (xB - xA) * (yP - yA) );

            /// getting height difference on each axis in Triangle space
            double dhx = (positions[faces[i].i2].y - positions[faces[i].i1].y);
            double dhy = (positions[faces[i].i3].y - positions[faces[i].i1].y);

            /// returning interpolated height.
            return positions[faces[i].i1].y + x * dhx + y * dhy;

        }
    }
    return 0;

}

void Mesh::setScale(double scale) {

    for (int i = 0; i < positions.size(); ++i) {
        positions[i] = positions[i] * scale;
    }
    maxRadius = scale;

}

void Mesh::normalize() {

    maxRadius = 0.0;
    for (int i = 0; i < positions.size(); ++i) {
        Vector3 vec = Vector3(positions[i].x, 0, positions[i].z);
        if (maxRadius < vec.length())
            maxRadius = vec.length();
    }

    for (int i = 0; i < positions.size(); ++i) {
        positions[i] = positions[i] / maxRadius;
    }


}

void Mesh::normalize(double val) {

    maxRadius = val;

    for (int i = 0; i < positions.size(); ++i) {
        positions[i] = positions[i] / maxRadius;
    }


}

int Mesh::appendVertex(Vector3 position, Vector3 normal) {

    int id = this->positions.size();

    this->positions.push_back(position);
    this->normals.push_back(normal);

    this->indices.push_back(id);

    return id;

}

int Mesh::appendVertex(Vector3 position, Vector3 normal, Vector3 uv) {

    int id = this->positions.size();

    this->positions.push_back(position);
    this->normals.push_back(normal);
    this->uvs.push_back(uv);

    this->indices.push_back(id);

    return id;

}


Vector3 Mesh::getPosition(int id) {

    return maxRadius * positions[id];

}

double Mesh::getSignedVolume(int i1, int i2, int i3) {

    Vector3 sNorm = (normals[i1] + normals[i2] + normals[i3]) * 1.0 / 3.0;

    Vector3 v1 = maxRadius * positions[i1];
    Vector3 v2 = maxRadius * positions[i2];
    Vector3 v3 = maxRadius * positions[i3];

    return sign(sNorm * positions[i1]) * (1.0 / 6.0) * abs(-v3.x*v2.y*v1.z + v2.x*v3.y*v1.z + v3.x*v1.y*v2.z - v1.x*v3.y*v2.z - v2.x*v1.y*v3.z + v1.x*v2.y*v3.z);

}

double Mesh::computeVolume() {

    double sum = 0;

    for (int i = 0; i < this->indexAmount/3; ++i) {

        sum += getSignedVolume(i*3, i*3+1, i*3+2);
        //std::cout << "Sum: " << sum << std::endl;

    }
    return sum;

}

void Mesh::saveToFile(std::string fname) {

    FILE * file = fopen(fname.c_str(), "w");

    fprintf(file, "ply\nformat ascii 1.0\n");
    fprintf(file, "element vertex %lu\n", positions.size());

    fprintf(file, "property float x\nproperty float y\nproperty float z\nproperty float nx\nproperty float ny\nproperty float nz\nproperty float s\nproperty float t\nproperty float tx\nproperty float ty\nproperty float tz\n");

    fprintf(file, "element face %d\n", (int)indices.size()/3);
    fprintf(file, "property list uchar uint vertex_indices\n");
    fprintf(file, "end_header\n");

    for (int i = 0; i < positions.size(); ++i) {

        fprintf(file, "%f %f %f %f %f %f %f %f %f %f %f\n", positions[i].x, positions[i].y, positions[i].z, normals[i].x, normals[i].y, normals[i].z, uvs[i].x, uvs[i].y, tangents[i].x, tangents[i].y, tangents[i].z);

    }

    for (int i = 0; i < indices.size()/3; ++i) {

        fprintf(file, "3 %d %d %d\n", indices[i*3], indices[i*3+1], indices[i*3+2]);

    }

    fclose(file);

}

void Mesh::computeTangents() {

    if (hasTangents) return;

    std::cout << "Computing Tangents" << std::endl;
    std::vector<Vector3> vec = MeshHelper::computeTangents(positions, normals, uvs, indices);

    for (int i = 0; i < tangents.size(); ++i)
        this->tangents[i] = vec[i];

    hasTangents = true;

}

void Mesh::computeTangents(bool force) {

    if (!force) {
        this->computeTangents();
        return;
    }
    std::vector<Vector3> vec = MeshHelper::computeTangents(positions, normals, uvs, indices);
    std::cout << "Compute yielded " << vec.size() << " tangents" << std::endl;
    this->tangents = std::vector<Vector3>();
    for (int i = 0; i < vec.size(); ++i) {
        tangents.push_back(vec[i]);
    }


}

std::vector<Vector3> Mesh::getTriangles() {

    std::vector<Vector3> tData(indices.size());

    for (int i = 0; i < this->indices.size(); ++i) {

        tData[i] = positions[indices[i]];

    }

    return tData;

}

Mesh::~Mesh() {

    std::vector<Vector3>().swap(this->positions);
    std::vector<Vector3>().swap(this->normals);
    std::vector<Vector3>().swap(this->tangents);
    std::vector<Vector3>().swap(this->uvs);

    std::vector<int>().swap(this->indices);

    if (fastData)
        delete fastData;

    activeMeshes--;
}
