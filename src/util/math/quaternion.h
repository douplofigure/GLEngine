#ifndef QUATERNION_H
#define QUATERNION_H

#include "vector3.h"
#include "matrix3.h"

namespace Math {

class Quaternion
{
    public:
        Quaternion(double a, double b, double c, double d);
        Quaternion(double a, Vector3 vec);
        Quaternion();
        virtual ~Quaternion();

        double getLinearPart();
        Vector3 getVectorPart();

        Matrix3 toRotationMatrix();

        Quaternion operator+(Quaternion q);
        Quaternion operator+(double d);
        Quaternion operator-(Quaternion q);
        Quaternion operator-(double d);
        Quaternion operator*(Quaternion q);
        Quaternion operator*(double d);
        Quaternion operator/(Quaternion q);
        Quaternion operator/(double d);
        Quaternion operator=(double d);

        double norm();
        Quaternion conjugate();

        static Quaternion fromAxisAngle(Vector3 axis, double angle);

        double a;
        double b;
        double c;
        double d;

    protected:

    private:
};

inline Quaternion operator*(double d, Quaternion q) {
    return Quaternion(d, 0, 0, 0) * q;
}

inline Quaternion operator/(double d, Quaternion q) {
    return Quaternion(d, 0, 0, 0) / q;
}

inline std::ostream& operator<<(std::ostream& stream, Quaternion q) {
    stream << q.a << "+i" << q.b << "+j" << q.c << "+k" << q.d;
    return stream;
}

}

#endif // QUATERNION_H
