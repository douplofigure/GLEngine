#ifndef MATRIX4_H
#define MATRIX4_H

#include <vector>
#include <glm/matrix.hpp>
#include <iostream>

#include "vector4.h"
#include "quaternion.h"

namespace Math {

class Matrix4
{
    public:
        /** Default constructor */
        Matrix4();
        Matrix4(std::vector<double>& values);
        Matrix4(double * values);
        Matrix4(std::vector<double>& values, bool transpose);
        /** Default destructor */
        virtual ~Matrix4();

        Vector4 operator*(Vector4 vec);
        Matrix4 operator*(Matrix4 mat);

        Vector4 operator[](int j);

        Vector4 getColumn(int j);
        Vector4 getLine(int i);

        static Matrix4 fromGLM(glm::mat4 mat);
        glm::mat4 toGLM();

        double getVal(int i, int j);
        void setVal(int i, int j, double val);

        void fromQuaternion(Quaternion q, Vector3 loc);
        void fromQuaternion(Quaternion q, glm::vec3 loc);

    protected:

    private:
        //std::vector<double> values;
        double * values;

};

}

Math::Matrix4 operator*(double d, Math::Matrix4 matrix);
std::ostream& operator<< (std::ostream& stream, Math::Matrix4 matrix);

#endif // MATRIX4_H
