#include "matrix4.h"

#include <glm/gtc/type_ptr.hpp>

#include <iostream>

using namespace Math;

Matrix4::Matrix4() {

    this->values = (double *) malloc(sizeof(double) * 16); //std::vector<double>(16);
    for (int i=0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            setVal(i, j, (i==j) ? 1.0 : 0.0);
        }
    }

}

Matrix4::Matrix4(std::vector<double>& values) {

    this->values = (double*) malloc(sizeof(double) * 16);//std::vector<double>(16);

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 4; ++j) {

            setVal(i, j, values[i*4+j]);

        }

    }

}

Matrix4::Matrix4(double * values) {

    this->values = (double*) malloc(sizeof(double) * 16);//std::vector<double>(16);

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 4; ++j) {

            setVal(i, j, values[i*4+j]);

        }

    }

}

Matrix4::Matrix4(std::vector<double>& values, bool transpose) {

    this->values = (double *) malloc(sizeof(double) * 16);//std::vector<double>(16);

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 4; ++j) {

            setVal(i, j, transpose ? values[j*4+i] : values[i*4+j]);

        }

    }

}

Matrix4::~Matrix4()
{
    //free(values);
}

Vector4 Matrix4::operator*(Vector4 vec) {

    double x = getLine(0) * vec;
    double y = getLine(1) * vec;
    double z = getLine(2) * vec;
    double w = getLine(3) * vec;

    return Vector4(x,y,z,w);

}

Matrix4 Matrix4::operator*(Matrix4 mat) {

    std::vector<double> values(16);
    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 4; ++j) {
            values[i*4+j] = getLine(i) * mat.getColumn(j);
        }

    }
    return Matrix4(values);

}

Vector4 Matrix4::operator[](int j) {
    return getLine(j);
}

Vector4 Matrix4::getColumn(int j) {

    return Vector4(getVal(0, j), getVal(1, j), getVal(2, j), getVal(3, j));

}

Vector4 Matrix4::getLine(int i) {
    return Vector4(getVal(i, 0), getVal(i, 1), getVal(i, 2), getVal(i, 3));
}

Matrix4 Matrix4::fromGLM(glm::mat4 mat) {

    std::vector<double> values = std::vector<double>(16);

    const float * src = glm::value_ptr(mat);
    for (int i = 0; i < 16; ++i) {
        values[i] = src[i];
//        data[i] = 0.0;
    }
    Matrix4 res = Matrix4(values, true);

    return res;

}

glm::mat4 Matrix4::toGLM() {

    float data[16];

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 4; ++j) {

            data[j*4+i] = getVal(i, j);

        }

    }

    return glm::make_mat4(data);
}

double Matrix4::getVal(int i, int j) {

    if (i*4+j > 15) throw std::runtime_error("Acessing value outside of matrix");

    return values[i*4+j];
}

void Matrix4::setVal(int i, int j, double val) {

    if (i*4+j > 15) throw std::runtime_error("Acessing value outside of matrix");

    values[i*4+j] = val;

}

void Matrix4::fromQuaternion(Quaternion q, Vector3 loc) {

    double s = 1 / (q.norm()*q.norm());

    values[0] = 1 - 2 * s * (q.c*q.c + q.d*q.d);
    values[1] = 2 * s * (q.b*q.c - q.d*q.a);
    values[2] = 2 * s * (q.b*q.d + q.c*q.a);

    values[4] = 2 * s * (q.b*q.c + q.d*q.a);
    values[5] = 1 - 2 * s * (q.b*q.b + q.d*q.d);
    values[6] = 2 * s * (q.c*q.d - q.b * q.a);

    values[8] = 2 * s * (q.b*q.d - q.c * q.a);
    values[9] = 2 * s * (q.c*q.d + q.b * q.a);
    values[10] = 1 - 2 * s * (q.b*q.b + q.c*q.c);

    values[3] = loc.x;
    values[7] = loc.y;
    values[11] = loc.z;

    values[12] = 0;
    values[13] = 0;
    values[14] = 0;
    values[15] = 1;

}

void Matrix4::fromQuaternion(Quaternion q, glm::vec3 loc) {

    double s = 1 / (q.norm()*q.norm());

    values[0] = 1 - 2 * s * (q.c*q.c + q.d*q.d);
    values[1] = 2 * s * (q.b*q.c - q.d*q.a);
    values[2] = 2 * s * (q.b*q.d + q.c*q.a);

    values[4] = 2 * s * (q.b*q.c + q.d*q.a);
    values[5] = 1 - 2 * s * (q.b*q.b + q.d*q.d);
    values[6] = 2 * s * (q.c*q.d - q.b * q.a);

    values[8] = 2 * s * (q.b*q.d - q.c * q.a);
    values[9] = 2 * s * (q.c*q.d + q.b * q.a);
    values[10] = 1 - 2 * s * (q.b*q.b + q.c*q.c);

    values[3] = loc.x;
    values[7] = loc.y;
    values[11] = loc.z;

    values[12] = 0;
    values[13] = 0;
    values[14] = 0;
    values[15] = 1;

}

std::ostream& operator<<(std::ostream& stream, Matrix4 mat) {

    stream << "|" << mat.getVal(0, 0) << " " << mat.getVal(0, 1) << " " << mat.getVal(0, 2) << " " << mat.getVal(0, 3) << "|" << std::endl;
    stream << "|" << mat.getVal(1, 0) << " " << mat.getVal(1, 1) << " " << mat.getVal(1, 2) << " " << mat.getVal(1, 3) << "|" << std::endl;
    stream << "|" << mat.getVal(2, 0) << " " << mat.getVal(2, 1) << " " << mat.getVal(2, 2) << " " << mat.getVal(2, 3) << "|" << std::endl;
    stream << "|" << mat.getVal(3, 0) << " " << mat.getVal(3, 1) << " " << mat.getVal(3, 2) << " " << mat.getVal(3, 3) << "|" << std::endl;

    return stream;

}

Matrix4 operator*(double d, Matrix4 mat) {

    for (int i = 0; i < 16; ++i) {
        mat.setVal(0, i, d * mat.getVal(0,i));
    }
    return mat;
}
