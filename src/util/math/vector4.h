#ifndef VECTOR4_H
#define VECTOR4_H

#include "vector3.h"

namespace Math {

class Vector4
{
    public:

        double x;
        double y;
        double z;
        double w;

        Vector4();
        Vector4(double x, double y, double z, double w);
        Vector4(Vector3 vec, double w);
        virtual ~Vector4();


        double length();

        double operator* (Vector4 vec);
        double * operator[] (int i);
        Vector4 operator*(double val);
        Vector4 operator/(double val);
        Vector4 operator+(Vector4 vec);
        Vector4 operator-(Vector4 vec);
        Vector4 operator+= (Vector4 vec);

};

inline Vector4 operator*(double d, Vector4 vec) {
    return vec * d;
}

}

#endif // VECTOR4_H
