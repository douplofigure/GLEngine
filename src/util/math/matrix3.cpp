#include "matrix3.h"

using namespace Math;

Matrix3::Matrix3()
{
    //ctor
}

Matrix3::Matrix3(double * values) {

    for (int i = 0; i < 9; ++i) {

        this->values[i] = values[i];

    }

}

Matrix3::Matrix3(std::vector<double> values) {

    for (int i = 0; i < 9 && i < values.size(); ++i) {

        this->values[i] = values[i];

    }

}

Matrix3::~Matrix3()
{
    //dtor
}

Vector3 Matrix3::operator[](int i) {

    return Vector3(getValue(0, i), getValue(1, i), getValue(2, i));

}

void Matrix3::setValue(int line, int column, double value) {

    this->values[line*3 + column] = value;

}

double Matrix3::getValue(int line, int column) {

    return this->values[line*3+column];

}

Matrix3 Matrix3::operator*(Matrix3 mat) {

    Matrix3 result;

    double tmp;

    for (int i = 0; i < 3; ++i) {

        for (int j = 0; j < 3; ++j) {

            tmp = 0;

            for (int k = 0; k < 3; ++k) {
                tmp += this->getValue(i, k) * mat.getValue(k,j);
            }

            result.setValue(i, j, tmp);

        }

    }

    return result;

}

Vector3 Matrix3::operator*(Vector3 vec) {

    return Vector3(getValue(0, 0) * vec.x + getValue(0, 1) * vec.y + getValue(0, 2) * vec.z,
                   getValue(1, 0) * vec.x + getValue(1, 1) * vec.y + getValue(1, 2) * vec.z,
                   getValue(2, 0) * vec.x + getValue(2, 1) * vec.y + getValue(2, 2) * vec.z);

}

std::ostream& operator<<(std::ostream& stream, Matrix3 mat) {

    stream << "|" << mat.getValue(0, 0) << " " << mat.getValue(0, 1) << " " << mat.getValue(0, 2) << "|" << std::endl;
    stream << "|" << mat.getValue(1, 0) << " " << mat.getValue(1, 1) << " " << mat.getValue(1, 2) << "|" << std::endl;
    stream << "|" << mat.getValue(2, 0) << " " << mat.getValue(2, 1) << " " << mat.getValue(2, 2) << "|" << std::endl;

    return stream;

}
