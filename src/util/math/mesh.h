#ifndef MESH_H
#define MESH_H

#include <vector>

#include "vector3.h"

#include "matrix4.h"
#include "util/data/octree.h"

typedef struct tri_face_t TRI_FACE;
typedef struct mesh_tree_node_t MESH_NODE;

namespace Math {

class Mesh
{
    public:
        Mesh(std::vector<Vector3> positions, std::vector<int> indices);
        Mesh(std::vector<float> rawModelData, int vertexCount, std::vector<int> indices);
        Mesh(std::vector<float> positionData, std::vector<float> normalData, std::vector<float> uvData, int vertexCount, std::vector<int> indices);

        /// Copy CTOR
        Mesh(Mesh * mesh);

        Mesh();

        virtual ~Mesh();

        double computeVolume();
        double getHeight(double x, double y);

        int appendVertex(Vector3 pos, Vector3 normal);
        int appendVertex(Vector3 pos, Vector3 normal, Vector3 uv);
        int getVertexCount();
        Vector3 getPosition(int id);
        void setScale(double scale);
        void normalize();
        void normalize(double value);
        void buildTree();

        void printFaceCounts(FILE * file);

        std::vector<double> getFace(double x, double z);

        std::vector<float> getModelData(int * vertexCount);
        std::vector<int> getIndexData();

        std::vector<Vector3> getTriangles();

        void mergeWith(Mesh * mesh);

        void saveToFile(std::string fname);

        void computeTangents();
        void computeTangents(bool force);

        static Mesh * withTransform(Mesh * mesh, Matrix4 trans);

        static int activeMeshes;

    protected:



    private:

        int vertexAmount;
        int indexAmount;

        std::vector<Vector3> positions;
        std::vector<Vector3> normals;
        std::vector<Vector3> uvs;
        std::vector<Vector3> tangents;
        std::vector<int> indices;

        data::octree<Vector3, TRI_FACE*> * fastData;
        MESH_NODE * tree;
        int treeHeight;
        double maxRadius;
        TRI_FACE * allFaces;

        bool hasTree;

        double getSignedVolume(int i1, int i2, int i3);
        void addFace(MESH_NODE * node, TRI_FACE face, int depth, bool checkBounds);
        std::vector<TRI_FACE> getAllFaces(double x, double y);
        std::vector<TRI_FACE> getAllFaces(MESH_NODE * node, Vector3 pos);
        Vector3 getFaceCenter(TRI_FACE f);

        void applyTransform(Matrix4 mat);

        bool hasTangents;

};

}

#endif // MESH_H
