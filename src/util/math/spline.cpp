#include "spline.h"

#include <cmath>

using namespace Math;
/*
Spline::Spline(std::vector<double> xValues, std::vector<double> yValues) {

    int n = xValues.size();
    this->y2 = std::vector<double>(n);
    std::vector<double> u(n);

    for (int i = 1; i < n-1; ++i) {

        double sig = (xValues[i]-xValues[i-1]) / (xValues[i+1]-xValues[i-1]);
        double p = sig * y2[i-1]+2;
        y2[i] = (sig-1) / p;
        u[i] = (6*((yValues[i+1]-yValues[i])/(xValues[i+1]-xValues[i])-(yValues[i]-yValues[i-1])/(xValues[i]-xValues[i-1]))/(xValues[i+1]-xValues[i-1])-sig*u[i-1])/p;

    }

    y2[n-1] = 0;
    for (int i = n-2; i > 0; --i) {
        y2[i] = y2[i] * y2[i+1]+u[i];
    }

    this->xValues = xValues;
    this->yValues = yValues;

}

Spline::~Spline()
{
    //dtor
}

double Spline::operator()(double x) {

    int klo = 0;
    int khi = this->xValues.size();

    while (khi - klo > 1) {
        int k = (khi-klo)/2;
        if (xValues[k] > x)
            khi = k;
        else
            klo = k;
    }

    double h = xValues[khi]-xValues[klo];
    if (h == 0) {
        //throw runtime_error("Identical xValues in spline");
    }

    double a = (xValues[khi]-x)/h;
    double b = (x-xValues[klo])/h;

    return a * yValues[klo] + b * yValues[khi] + ((pow(a, 3)-a)*y2[klo]+(pow(b, 3)-b)*y2[khi])*(pow(h, 2))/6;

}*/
