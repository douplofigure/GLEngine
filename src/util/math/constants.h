#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_PI

#define M_PI 3.14159265358979323846
#define M_PI_2 1.57079632679489661923

#endif

#define DOUBLE_0 1e-6

#define abs(x) (x >= 0 ? x : -x)

#define IS_ZERO(x) (abs(x) < DOUBLE_0)

#endif // CONSTANTS_H_INCLUDED
