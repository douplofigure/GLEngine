#include "vecutils.h"

#ifndef M_PI
#define M_PI    3.14159265358979323846
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>

VECTOR3D vec3DCrossProduct(VECTOR3D vec1, VECTOR3D vec2) {

	return (VECTOR3D) {vec1.y * vec2.z - vec1.z * vec2.y, -vec1.z * vec2.x + vec1.x * vec2.z, vec1.x * vec2.y - vec1.y * vec2.x};

}

VECTOR3D vec3DNormalize(VECTOR3D vec) {

	double len = sqrt(vec.x*vec.x + vec.y * vec.y + vec.z * vec.z);

	return (VECTOR3D) {vec.x / len, vec.y / len, vec.z / len};

}


float radians(float x) {
	return (x/180.0)*M_PI;
}

float getValueFromMatrix(MATRIX * mat, int line, int col) {

	return mat->values[line*4+col];

}

MATRIX matrixProduct(MATRIX * a, MATRIX * b) {

	MATRIX result;
	int i, j, k, l;

	float tmp = 0.0;

	for (i = 0; i < 4; ++i) {

		for (j = 0; j < 4; ++j) {

			tmp = 0.0;
			for (k = 0; k < 4; ++k) {

				tmp += getValueFromMatrix(a, i, k) * getValueFromMatrix(b, k, j);

			}
			setValueInMatrix(&result, i, j, tmp);
		}

	}
	return result;
}

void rotateMatrixDegrees(MATRIX * mat, AXIS axis, float ang) {

	float s = sin(radians(ang));
	float c = cos(radians(ang));

	MATRIX rotMat = generateIdentity();

	switch (axis) {

		case X_AXIS :
			rotMat.values[5] = c;
			rotMat.values[10] = c;
			rotMat.values[6] = -s;
			rotMat.values[9] = s;
			break;

		case Y_AXIS :
			rotMat.values[0] = c;
			rotMat.values[10] = c;
			rotMat.values[2] = s;
			rotMat.values[8] = -s;
			break;

		case Z_AXIS :
			rotMat.values[0] = c;
			rotMat.values[5] = c;
			rotMat.values[1] = -s;
			rotMat.values[4] = s;
			break;
	}
	*mat = matrixProduct(&rotMat, mat);

}

void scaleMatrix(MATRIX * mat, float scale) {

	mat->values[0] *= scale;
	mat->values[5] *= scale;
	mat->values[10] *= scale;

}

void translateMatrix(MATRIX * mat, VECTOR3F vec) {

	mat->values[3] += vec.x;
	mat->values[7] += vec.y;
	mat->values[11] += vec.z;

}

void setValueInMatrix(MATRIX * mat, int line, int col, float value) {

	mat->values[line*4+col] = value;

}

MATRIX generateIdentity() {

	MATRIX mat;
	int i;
	for (i = 0; i < 16; ++i) {

		mat.values[i] = 0;

	}
	mat.values[0] = 1;
	mat.values[5] = 1;
	mat.values[10] = 1;
	mat.values[15] = 1;
	return mat;

}

void printMatrix(MATRIX * mat) {

	printf("| %f %f %f %f |\n", mat->values[0], mat->values[1], mat->values[2], mat->values[3]);
	printf("| %f %f %f %f |\n", mat->values[4], mat->values[5], mat->values[6], mat->values[7]);
	printf("| %f %f %f %f |\n", mat->values[8], mat->values[9], mat->values[10], mat->values[11]);
	printf("| %f %f %f %f |\n", mat->values[12], mat->values[13], mat->values[14], mat->values[15]);

}

MATRIX generateMatrixFromFloat(float * vals) {

	int i;
	MATRIX mat;
	for (i = 0; i < 16; ++i) {
		mat.values[i] = vals[i];
	}
	return mat;
}

MATRIX generateTransformationMatrix(VECTOR3F pos, float scale, VECTOR3F rot) {

	MATRIX m = generateIdentity();

	scaleMatrix(&m, scale);

	rotateMatrixDegrees(&m, X_AXIS, rot.x);
	rotateMatrixDegrees(&m, Y_AXIS, rot.y);
	rotateMatrixDegrees(&m, Z_AXIS, rot.z);

	translateMatrix(&m, pos);

	return m;
}


MATRIX generateShadowProjectionMatrix(VECTOR3F lightDir, VECTOR3F camPos) {

	MATRIX m = generateIdentity();

	float r = 2;
	float t = 2;
	float n = 1;
	float f = 200;

	m.values[0]  = 1 / r;
	m.values[5]  = 1 / t;
	m.values[10] = -2 / (f-n);
	m.values[11] = -(f+n) / (f-n);

	translateMatrix(&m, camPos);

	return m;

}

MATRIX generateViewMatrix(VECTOR3F pos, float pitch, float yaw, VECTOR3F facing) {

	MATRIX m = generateIdentity();
	MATRIX t = generateIdentity();

	rotateMatrixDegrees(&m, X_AXIS, pitch * facing.z);
	rotateMatrixDegrees(&m, Z_AXIS, pitch * (-facing.x));
	rotateMatrixDegrees(&m, Y_AXIS, yaw);

	translateMatrix(&t, (VECTOR3F) {-pos.x, -pos.y, -pos.z});

	return matrixProduct(&m,&t);

}

VECTOR3D matrixVectorProduct(MATRIX mat, VECTOR3D vec) {

	VECTOR3D out = (VECTOR3D) {0, 0,0};

	out.x = mat.values[0] * vec.x + mat.values[1] * vec.y + mat.values[2] * vec.z;
	out.y = mat.values[4] * vec.x + mat.values[5] * vec.y + mat.values[6] * vec.z;
	out.z = mat.values[8] * vec.x + mat.values[9] * vec.y + mat.values[10] * vec.z;

	return out;

}

double scalarProduct(VECTOR3D vec1, VECTOR3D vec2) {

	return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;

}

VECTOR3D::operator VECTOR3F() {return (VECTOR3F) {(float)x, (float)y, (float)z};};


double degrees(double d) {

    return (d / M_PI) * 180;

}

double dist(VECTOR3F pos1, VECTOR3F pos2) {

	return std::sqrt((pos2.x-pos1.x)*(pos2.x-pos1.x) + (pos2.y-pos1.y)*(pos2.y-pos1.y) + (pos2.z-pos1.z)*(pos2.z-pos1.z));

}

float randomFloatInBounds(float min, float max) {

	return (((float)rand() / RAND_MAX) * (max - min) + min);

}

double vec2dScalar(VECTOR2D vec1, VECTOR2D vec2) {

    return vec1.x*vec2.x + vec1.y * vec2.y;

}
