#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>

template <typename K, typename T> class BinTreeNode {

    public:
        BinTreeNode(T value, K key) {

            childCount = 0;
            this->key = key;
            this->value = value;

        }

        virtual ~BinTreeNode() {
            delete leftChild;
            delete rightChild;
        }

        K getKey(){
            return key;
        }

        T getValue() {
            return value;
        }

        void setLeftChild(BinTreeNode * child) {
            this->childCount |= 1;
            this->leftChild = child;
        }

        void setRightChild(BinTreeNode * child) {
            this->childCount |= 2;
            this->rightChild = child;
        }

        BinTreeNode<K, T> * getLeft() {
            return leftChild;
        }

        BinTreeNode<K, T> * getRight() {
            return rightChild;
        }

        int getChildCount() {
            return childCount;
        }

        void overrideChildCount(int cc) {
            this->childCount = cc;
        }

        void print(int depth) {

            //std::cerr << "childCount " << childCount << std::endl;
            std::cout << "Key: " << this->key << std::endl;
            if (!childCount) {
                /*for (int i = 0; i < depth; ++i) {
                    std::cerr << "  ";
                }*/
                return;

            }
            std::cout << "<ul>" << std::endl;

            if (childCount & 1){

                /*for (int i = 0; i < depth; ++i) {
                    std::cerr << "  ";
                }*/
                std::cout << "<li>Left: " << std::endl;
                this->leftChild->print(depth+1);
                std::cout << "</li>" << std::endl;
            }

            if (childCount & 2){

                /*for (int i = 0; i < depth; ++i) {
                    std::cerr << "  ";
                }*/
                std::cout << "<li>RIGHT: " << std::endl;
                this->rightChild->print(depth+1);
                std::cout << "</li>" << std::endl;
            }

            std::cout << "</ul>" << std::endl;

        }

    private:
        T value;
        K key;

        int childCount;

        BinTreeNode<K, T> * leftChild;
        BinTreeNode<K, T> * rightChild;

};

template <typename K, typename T> class BinaryTree
{
    public:
        /** Default constructor */
        BinaryTree(int(*compare)(K,BinTreeNode<K,T>*), T topVal, K topKey) {

            this->compare = compare;
            this->topNode = new BinTreeNode<K,T>(topVal, topKey);

        }
        /** Default destructor */
        virtual ~BinaryTree(){

            delete topNode;

        }

        T findElement(K key) {
            return searchNode(topNode, key);
        }

        void insertElement(K key, T elem) {

            this->insertNode(new BinTreeNode<K,T>(elem, key), topNode);

        }

        BinTreeNode<K, T> * getTopNode() {
            return this->topNode;
        }

        void print() {

            std::cout << "Binary Tree" << std::endl;

            this->topNode->print(0);

        }

    protected:

        BinTreeNode<K,T> * topNode;

        T searchNode(BinTreeNode<K,T> * node, K key) {

            if (!node->getChildCount()) return node->getValue();

            int result = compare(key, node->getLeftChild()->getKey(), node->getRightChild()->getKey());

            if (!result) return node->getValue();
            else if (result > 0)
                return searchNode(node->getRightChild(), key);
            else
                return searchNode(node->getLeftChild(), key);
        }

        void insertNode(BinTreeNode<K, T> * newNode, BinTreeNode<K, T> * node) {
            int result = compare(newNode->getKey(), node);

            if (result <= 0) {
                if(node->getChildCount() & 1)
                    insertNode(newNode, node->getLeft());
                else
                    node->setLeftChild(newNode);
            }
            else {
                if(node->getChildCount() & 2)
                    insertNode(newNode, node->getRight());
                else
                    node->setRightChild(newNode);
            }

        }

    private:

        int(* compare)(K,BinTreeNode<K,T>*);



};

#endif // BINARYTREE_H
