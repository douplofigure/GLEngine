import glengine
import render
import resources
import structure
import world
import inputs
import gltest

class MyTextureLoader(resources.ResourceLoader):

    def __init__(self):
        pass

    def canCreate(self, directory, name):
        return True

    def create(self, directory, name):

        fname = directory + name + ".ppm"

        return render.TextureInjector([], 0, 0, 0, False)

    def getFilenames(self, directory, name):
        return directory + name + ".ppm"


#vp = glengine.getActiveViewport()

gltest.testFunc([i for i in range(12)])

tElem = render.RenderElement(glengine.getStructure("projectile"), 12, 0, 12)
#vp.addElement(0, tElem)
tElem.setScale(12.0)
