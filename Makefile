CFILES := $(shell find src/ -type f -regex ".*\(\.c\|\.cpp\|\.cc\)" -not -path "src/main.cpp" | tr "\n" " ")
HFILES := $(shell find src/ -type f -regex ".*\(\.h\|\.hpp\)" -not -path "src/main.cpp" | tr "\n" " ")

LIB_NAME :=GLEngine
MAKEFILES :=
TEST_NAME :=GLEngine
TEST_FILE := src/main.cpp

#extra files to be included in build archive
EXTRA_BUILD_FILES:=mathtest.py

LINUX_LIBS := -lGL -lglfw -lply -lconfigloader -lpthread -ldl -lopenal -lalut -lBulletDynamics -lBulletCollision -lLinearMath -ltga -lstack -lm -lutil
WIN32_LIBS := -lopengl32 -lglew32 -lglfw3 -lm -lconfigloader -lply_win32 -luser32 -lgdi32 -lpthread -lOpenAL32 -lalut -lBulletDynamics -lBulletCollision -lLinearMath -ltga -lstack
WIN64_LIBS := -lopengl32 -lglew32 -lglfw3 -lm -lconfigloader -lply_win64 -luser32 -lgdi32 -lpthread -lOpenAL32 -lalut -lBulletDynamics -lBulletCollision -lLinearMath -ltga -lstack

LINUX_CC = gcc
LINUX_CXX = g++
LINUX_AR = ar

WIN32_CC = i686-w64-mingw32-gcc
WIN32_CXX = i686-w64-mingw32-g++
WIN32_AR = i686-w64-mingw32-ar

WIN64_CC = x86_64-w64-mingw32-gcc
WIN64_CXX = x86_64-w64-mingw32-g++
WIN64_AR = x86_64-w64-mingw32-ar

CFLAGS =
CXXFLAGS = -std=c++11 -fpermissive -g
AR_MODE = -rcs

LINUX_DIR :=linux
WIN32_DIR :=win32
WIN64_DIR :=win64

WORKSPACE_DIR:=../generated

#INTERNAL : DON NOT CHANGE

obj_target = obj/${2}/$(addsuffix .o,$(basename ${1}))	#$(patsubst %.c,%.o,$(notdir ${1}))
test_target = test/${2}/${1}		#$(patsubst %.c,%.o,$(notdir ${1}))
lib_target = lib/${2}/lib${1}.a
hdr_target = include2/${2}/${1}

objlinux.c :=
objlinux.cpp :=
objlinux.cc :=
liblinux.a :=
objwin32.o :=
objwin64.o :=
libwin32.a :=
libwin64.a :=
hdr.h :=
define obj
  $(call obj_target,${1},${2}) : ${1} | obj/${2}/
  obj${2}$(suffix ${1}) += $(call obj_target,${1},${2})
endef

define test
  $(call test_target,${TEST_NAME},${2}) : ${1} lib/${2}/lib${LIB_NAME}.a |test/${2}/ headers
  test${2}$(suffix ${1}) += $(call test_target,${TEST_NAME},${2})
endef

define lib
  $(call lib_target,${1},${2}) : $(foreach src,${CFILES},$(call obj_target,${src},${2})) | lib/${2}/
  lib${2}.a += $(call lib_target,${1},${2})
endef

define hdr
  $(call hdr_target,${1},${2}) : ${1} | include2/${2}/
  hdr.h += $(call hdr_target,${1},${2})
endef

define SOURCES
  $(foreach src,${1},$(eval $(call obj,${src},${2})))
endef

define HEADERS
  $(foreach src,${1},$(eval $(call hdr,${src},${2})))
endef

all : linux win32 win64 Makefile

fixlibs:
	cd srcLibs && make
	cp -a srcLibs/lib/. lib/

workspace_create: linux win32 win64 build.mak headers
	@echo Setting up workspace in ${WORKSPACE_DIR}
	@mkdir -p ${WORKSPACE_DIR}
	@mkdir -p ${WORKSPACE_DIR}/include
	@echo Copying headers
	@cp -a ./include/. ${WORKSPACE_DIR}/include
	@cp -a ./include2/. ${WORKSPACE_DIR}/include
	@echo Coping libs
	@mkdir -p ${WORKSPACE_DIR}/lib
	@cp -a ./lib/. ${WORKSPACE_DIR}/lib
	@echo Creating source directory
	@mkdir -p ${WORKSPACE_DIR}/src
	@echo copying main.cpp
	@cp $(TEST_FILE) ${WORKSPACE_DIR}/src
	@echo Setting up new makefile
	@cp build.mak ${WORKSPACE_DIR}
	@cp physics.conf ${WORKSPACE_DIR}
	@cp viewport.conf ${WORKSPACE_DIR}
	@mv ${WORKSPACE_DIR}/build.mak ${WORKSPACE_DIR}/Makefile
	@echo Workspace setup done
	@echo ${WORKSPACE_DIR} >> .wkspaces.txt

define ws_update
  ${1} : linux win32 win64 build.mak headers
  wsupdate += ${1}
endef

WK_LIST:=$(shell cat .wkspaces.txt | tr "\n" " ")

define UPDATES
  $(foreach src,${1},$(call ws_update,${src}))
endef

$(eval $(call UPDATES,${WK_LIST}))

workspace_update: ${WK_LIST}

${wsupdate}: % :
	@mkdir -p $@/include
	@echo Copying headers
	@cp -a ./include/. $@/include
	@cp -a ./include2/. $@/include
	@echo Coping libs
	@mkdir -p $@/lib
	@cp -a ./lib/. $@/lib
	@echo Creating source directory
	@mkdir -p $@/src
	@cp build.mak $@
	@mv $@/build.mak $@/Makefile

builds: linux_build win32_build win64_build

clear:
	rm -r obj

linux_build: linux
	@echo building linux release
	mkdir -p builds/${LINUX_DIR}
	cp ./test/${LINUX_DIR}/${TEST_NAME} builds/${LINUX_DIR}
	python3 packResources.py builds/${LINUX_DIR}
	cp ${EXTRA_BUILD_FILES} builds/${LINUX_DIR}
	cp -r env/ builds/${LINUX_DIR}
	cd builds/${LINUX_DIR} ; find ./ -path '*/.*' -prune -o -type f -print | zip ${TEST_NAME}_${LINUX_DIR}.zip -@
	mv builds/${LINUX_DIR}/${TEST_NAME}_${LINUX_DIR}.zip builds/${TEST_NAME}_${LINUX_DIR}.zip

win32_build: win32
	@echo building win32 release
	mkdir -p builds/${WIN32_DIR}
	cp ./test/${WIN32_DIR}/${TEST_NAME} builds/${WIN32_DIR}
	cp ./dlls/${WIN32_DIR}/*.dll builds/${WIN32_DIR}
	cp ${EXTRA_BUILD_FILES} builds/${WIN32_DIR}
	cp -r env/ builds/${WIN32_DIR}
	mv builds/${WIN32_DIR}/${TEST_NAME} builds/${WIN32_DIR}/${TEST_NAME}.exe
	python3 packResources.py builds/${WIN32_DIR}
	cd builds/${WIN32_DIR} ; find ./ -path '*/.*' -prune -o -type f -print | zip ${TEST_NAME}_${WIN32_DIR}.zip -@
	mv builds/${WIN32_DIR}/${TEST_NAME}_${WIN32_DIR}.zip builds/${TEST_NAME}_${WIN32_DIR}.zip

win64_build: win64
	@echo building win64 release
	mkdir -p builds/${WIN64_DIR}
	cp ./test/${WIN64_DIR}/${TEST_NAME} builds/${WIN64_DIR}
	cp ./dlls/${WIN64_DIR}/*.dll builds/${WIN64_DIR}
	cp -r env/ builds/${WIN64_DIR}
	cp ${EXTRA_BUILD_FILES} builds/${WIN64_DIR}
	mv builds/${WIN64_DIR}/${TEST_NAME} builds/${WIN64_DIR}/${TEST_NAME}.exe
	python3 packResources.py builds/${WIN64_DIR}
	cd builds/${WIN64_DIR} ; find ./ -path '*/.*' -prune -o -type f -print | zip ${TEST_NAME}_${WIN64_DIR}.zip -@
	mv builds/${WIN64_DIR}/${TEST_NAME}_${WIN64_DIR}.zip builds/${TEST_NAME}_${WIN64_DIR}.zip


linux: $(call lib_target,${LIB_NAME},${LINUX_DIR}) $(call test_target,${TEST_NAME},${LINUX_DIR})

win32: $(call lib_target,${LIB_NAME},${WIN32_DIR}) $(call test_target,${TEST_NAME},${WIN32_DIR})

win64: $(call lib_target,${LIB_NAME},${WIN64_DIR}) $(call test_target,${TEST_NAME},${WIN64_DIR})

include $(MAKEFILES)

linux_headers: $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${LINUX_DIR}))

win32_headers: $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${WIN32_DIR}))

win64_headers: $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${WIN64_DIR}))

headers: linux_headers win32_headers win64_headers

$(eval $(call test,${TEST_FILE},${LINUX_DIR}))
$(eval $(call test,${TEST_FILE},${WIN32_DIR}))
$(eval $(call test,${TEST_FILE},${WIN64_DIR}))

$(eval $(call SOURCES,${CFILES},${LINUX_DIR}))
$(eval $(call HEADERS,${HFILES},${LINUX_DIR}))
$(eval $(call lib,${LIB_NAME},${LINUX_DIR}))

$(eval $(call SOURCES,${CFILES},${WIN32_DIR}))
$(eval $(call HEADERS,${HFILES},${WIN32_DIR}))
$(eval $(call lib,${LIB_NAME},${WIN32_DIR}))

$(eval $(call SOURCES,${CFILES},${WIN64_DIR}))
$(eval $(call HEADERS,${HFILES},${WIN64_DIR}))
$(eval $(call lib,${LIB_NAME},${WIN64_DIR}))

${objlinux.c} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(LINUX_CC) -c -o $@ $< $(CFLAGS) -I./src -I./include/${LINUX_DIR} -I./include/${LINUX_DIR}/python

${objlinux.cpp} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(LINUX_CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${LINUX_DIR} -I./include/${LINUX_DIR}/python

${objlinux.cc} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(LINUX_CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${LINUX_DIR} -I./include/${LINUX_DIR}/python

${liblinux.a} : % :
	@echo Creating library $@
	@mkdir -p $(dir $@)
	@$(LINUX_AR) $(AR_MODE) $@ $^

${hdr.h}: % :
	@echo Copying header $@
	@mkdir -p $(dir $@)
	@mkdir -p $(basename $@) && cp $< $@

${objwin32.c} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(WIN32_CC) -c -o $@ $< $(CFLAGS) -I./src -I./include/${WIN32_DIR} -I./include/${WIN32_DIR}/python

${objwin32.cpp} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(WIN32_CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${WIN32_DIR} -I./include/${WIN32_DIR}/python

${objwin32.cc} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(WIN32_CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${WIN32_DIR} -I./include/${WIN32_DIR}/python

${libwin32.a} : % :
	@echo Creating library $@
	@mkdir -p $(dir $@)
	@$(WIN32_AR) $(AR_MODE) $@ $^

${objwin64.c} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(WIN64_CC) -c -o $@ $< $(CFLAGS) -I./src -I./include/${WIN64_DIR} -I./include/${WIN64_DIR}/python

${objwin64.cpp} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(WIN64_CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${WIN64_DIR} -I./include/${WIN64_DIR}/python

${objwin64.cc} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(WIN64_CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${WIN64_DIR} -I./include/${WIN64_DIR}/python

${libwin64.a} : % :
	@echo Creating library $@
	@mkdir -p $(dir $@)
	@$(WIN64_AR) $(AR_MODE) $@ $^

${testlinux.c} : % :
	@echo Building $@
	@$(LINUX_CC) -o $@ $< $(CFLAGS) -L./lib/${LINUX_DIR} -I./include2/${LINUX_DIR}/src -I./include/${LINUX_DIR} -L./lib/linux -l${LIB_NAME} ${LINUX_LIBS}

${testlinux.cpp} : % :
	@echo Building $@
	@$(LINUX_CXX) -o $@ $< $(CXXFLAGS) -L./lib/${LINUX_DIR} -I./include2/${LINUX_DIR}/src -I./include/${LINUX_DIR} -L./lib/linux -l${LIB_NAME} ${LINUX_LIBS}

${testlinux.cc} : % :
	@echo Building $@
	@$(LINUX_CXX) -o $@ $< $(CXXFLAGS) -L./lib/${LINUX_DIR} -I./include2/${LINUX_DIR}/src -I./include/${LINUX_DIR}  -L./lib/linux -l${LIB_NAME} ${LINUX_LIBS}

${testwin32.c} : % :
	@echo Building $@
	@$(WIN32_CC) -o $@ $< $(CFLAGS) -L./lib/${WIN32_DIR} -I./include2/${WIN32_DIR}/src -I./include/${WIN32_DIR} -L./lib/win32 -l${LIB_NAME} ${WIN32_LIBS} -static-libgcc -static-libstdc++

${testwin32.cpp} : % :
	@echo Building $@
	@$(WIN32_CXX) -o $@ $< $(CXXFLAGS) -L./lib/${WIN32_DIR} -I./include2/${WIN32_DIR}/src -I./include/${WIN32_DIR} -L./lib/win32 -l${LIB_NAME} ${WIN32_LIBS} -static-libgcc -static-libstdc++

${testwin32.cc} : % :
	@echo Building $@
	@$(WIN32_CXX) -o $@ $< $(CXXFLAGS) -L./lib/${WIN32_DIR} -I./include2/${WIN32_DIR}/src -I./include/${WIN32_DIR} -L./lib/win32 -l${LIB_NAME} ${WIN32_LIBS} -static-libgcc -static-libstdc++

${testwin64.c} : % :
	@echo Building $@
	@$(WIN64_CC) -o $@ $< $(CFLAGS) -L./lib/${WIN64_DIR} -I./include2/${WIN64_DIR}/src -I./include/${WIN64_DIR} -L./lib/win64 -l${LIB_NAME} ${WIN64_LIBS} -static-libgcc -static-libstdc++

${testwin64.cpp} : % :
	@echo Building $@
	@$(WIN64_CXX) -o $@ $< $(CXXFLAGS) -L./lib/${WIN64_DIR} -I./include2/${WIN64_DIR}/src -I./include/${WIN64_DIR} -L./lib/win64 -l${LIB_NAME} ${WIN64_LIBS} -static-libgcc -static-libstdc++

${testwin64.cc} : % :
	@echo Building $@
	@$(WIN64_CXX) -o $@ $< $(CXXFLAGS) -L./lib/${WIN64_DIR} -I./include2/${WIN64_DIR}/src -I./include/${WIN64_DIR} -L./lib/win64 -l${LIB_NAME} ${WIN64_LIBS} -static-libgcc -static-libstdc++

include2/win32/: include2/
	@mkdir -p include2/win32

include2/win64/: include2/
	@mkdir -p include2/win64

include2/linux/: include2/
	@mkdir -p include2/linux

include2/:
	@mkdir -p include

lib/linux/: lib/
	@mkdir -p lib/linux

lib/win32/: lib/
	@mkdir -p lib/win32

lib/win64/: lib/
	@mkdir -p lib/win64

lib/:
	@mkdir -p lib

obj/win64/: obj/
	@mkdir -p obj/win64
	@find src/ -type d -exec mkdir -p -- obj/win64/{} \;

obj/win32/: obj/
	@mkdir -p obj/win32
	@find src/ -type d -exec mkdir -p -- obj/win32/{} \;

obj/linux/: obj/
	@mkdir -p obj/linux
	@echo copying folder structure
	@find src/ -type d -exec mkdir -p -- obj/linux/{} \;

obj/:
	@mkdir -p obj

test/linux/:
	@mkdir -p test/linux

test/win32/:
	@mkdir -p test/win32

test/win64/:
	@mkdir -p test/win64

test/:
	@mkdir -p test

.PHONY: all fixlibs $(foreach d,${WK_LIST},${d})
